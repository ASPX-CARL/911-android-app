package ph.com.auspex.mers.data.repositories;

import java.util.List;

import io.reactivex.Observable;
import ph.com.auspex.mers.data.model.request.FamilyLinkInviteRequest;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.data.model.response.Relationship;

public interface FamilyLinkRepository {
    Observable<List<FamilyLinkDetail>> getFamilyLinks(String userId);

    Observable<String> addFamilyLink(FamilyLinkInviteRequest request);

    Observable<List<FamilyLinkDetail>> getFamilyInvites(String userId);

    Observable<String> acceptInvite(String inviterId, String invitorId);

    Observable<String> rejectInvite(String inviterId, String invitorId);

    Observable<List<Relationship>> getRelationshipTypes();
}