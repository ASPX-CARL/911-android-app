package ph.com.auspex.mers.register.optional.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.register.RegisterContract;
import ph.com.auspex.mers.register.RegisterInteractor;

/**
 * Register Presenter Implementation
 */
public class RegisterOptPresenterImpl extends MvpBasePresenter<RegisterContract.RegisterView> implements RegisterContract.RegisterPresenter {

    private final RegisterInteractor model;

    public RegisterOptPresenterImpl(RegisterInteractor model) {
        this.model = model;
    }

    /**
     * Submit all form information to the API
     *
     * @param registerRequest
     */
    @Override
    public void registerAccount(RegisterRequest registerRequest) {
        getView().showLoading();

        model.register(registerRequest, new BaseInteractor.ResponseListener<String>() {
            @Override
            public void onSuccess(String userId) {
                getView().hideLoading();
                getView().onRegisterSuccessful(userId);
            }

            @Override
            public void onFailure(String message) {
                getView().hideLoading();
                getView().showError(message);
            }
        });
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        model.cancelNetworkRequest();
    }
}
