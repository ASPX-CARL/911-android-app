package ph.com.auspex.mers.login;

import dagger.Subcomponent;

@LoginScope
@Subcomponent(modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginController loginController);
}
