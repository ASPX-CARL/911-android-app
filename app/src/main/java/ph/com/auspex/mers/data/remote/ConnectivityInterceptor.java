package ph.com.auspex.mers.data.remote;

import android.content.Context;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import ph.com.auspex.mers.data.remote.exception.NoConnectivityException;
import ph.com.auspex.mers.util.AppUtil;

/**
 * Create network connectivity interceptor
 */
public class ConnectivityInterceptor implements Interceptor {

    private final Context context;

    /**
     * Constructs an interceptor using the apps {@link Context}
     *
     * @param context
     */
    public ConnectivityInterceptor(Context context) {
        this.context = context;
    }

    /**
     * Returns the current requests chain after verifying that the device has an internet connection
     *
     * @param chain current request chain
     * @return the same request chain
     * @throws IOException when the device doesn't have an internet connection
     */
    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!AppUtil.isNetworkAvailable(context)) {
            throw new NoConnectivityException();
        }
        return chain.proceed(chain.request());
    }
}
