package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Provide a POJO for thumbnail items in {@link Feed}
 */
public class ThumbnailItem {

    @SerializedName("@attributes")
    private Attributes attributes;

    public Attributes getAttributes() {
        return attributes;
    }
}
