package ph.com.auspex.mers.familylink.list.adapter;

import android.content.Context;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.adapter.ItemClickListener;
import ph.com.auspex.mers.data.model.request.InviteRequest;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.util.GlideImageLoader;

/**
 * Provide family link invites list {@link RecyclerView.Adapter}
 */
public class FamilyLinkInvitesRecyclerAdapter extends RecyclerView.Adapter<FamilyLinkInvitesRecyclerAdapter.FamilyLinkInvitesViewHolder> {

    private List<FamilyLinkDetail> familyLinkList;
    private ItemClickListener<InviteRequest> listener;

    /**
     * Constructs {@link FamilyLinkInvitesRecyclerAdapter} with an {@link ItemClickListener}
     *
     * @param listener listener that returns an {@link InviteRequest}
     */
    public FamilyLinkInvitesRecyclerAdapter(ItemClickListener<InviteRequest> listener) {
        this.familyLinkList = new ArrayList<>();
        this.listener = listener;
    }

    /**
     * Update the list of this adapter
     *
     * @param familyLinkList updated {@link FamilyLinkDetail} list
     */
    public void updateList(List<FamilyLinkDetail> familyLinkList) {
        this.familyLinkList = familyLinkList;
        notifyDataSetChanged();
    }

    /**
     * Removes {@link FamilyLinkDetail} invitation from the list
     *
     * @param position position of the {@link FamilyLinkDetail}
     */
    public void removeItem(int position) {
        familyLinkList.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * Inflate the layout resource
     *
     * @param parent   parent view
     * @param viewType {@link android.support.v7.widget.RecyclerView.ViewHolder} view type
     * @return
     */
    @Override
    public FamilyLinkInvitesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_invites, parent, false);
        return new FamilyLinkInvitesViewHolder(parent.getContext(), view, listener);
    }

    /**
     * Binds {@link FamilyLinkDetail} to the {@link FamilyLinkInvitesViewHolder}
     *
     * @param holder   view holder
     * @param position position of {@link FamilyLinkDetail} in the list
     */
    @Override
    public void onBindViewHolder(FamilyLinkInvitesViewHolder holder, int position) {
        holder.bind(familyLinkList.get(position));
    }

    /**
     * Returns the total count of family link invites
     *
     * @return total {@link #familyLinkList} count
     */
    @Override
    public int getItemCount() {
        return familyLinkList != null ? familyLinkList.size() : 0;
    }

    /**
     * Provides a {@link  RecyclerView.ViewHolder} for family link invites
     */
    static class FamilyLinkInvitesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_profile_image) ImageView ivProfileImage;
        @BindView(R.id.tv_relative_name) TextView tvRelativeName;
        @BindView(R.id.tv_relationship) TextView tvRelationship;
        @BindView(R.id.divider) View divider;
        @BindView(R.id.button_accept) ImageButton btnAccept;
        @BindView(R.id.button_reject) ImageButton btnReject;

        private FamilyLinkDetail familyLinkDetail;
        private Context context;
        private ItemClickListener<InviteRequest> listener;

        private final AnimatedVectorDrawableCompat avd;

        /**
         * Constructs {@link FamilyLinkInvitesViewHolder} using the following parameters
         *
         * @param context  activity context
         * @param itemView inflated layout resource
         * @param listener item click listener
         */
        public FamilyLinkInvitesViewHolder(Context context, View itemView, ItemClickListener<InviteRequest> listener) {
            super(itemView);

            this.context = context;
            this.listener = listener;
            ButterKnife.bind(this, itemView);

            avd = AnimatedVectorDrawableCompat.create(context, R.drawable.ic_approve_request_animatable);
        }

        /**
         * Bind {@link FamilyLinkDetail} to this view holder
         *
         * @param familyLinkDetail family link detail POJO
         */
        public void bind(FamilyLinkDetail familyLinkDetail) {
            this.familyLinkDetail = familyLinkDetail;

            GlideImageLoader.loadUrl(context, ivProfileImage, familyLinkDetail.getImageUrl(), familyLinkDetail.getGender().getDrawableRes(), familyLinkDetail.getGender().getDrawableRes(), true);

            tvRelativeName.setText(String.format("%s %s", familyLinkDetail.getFirstName(), familyLinkDetail.getLastName()));
            tvRelationship.setText(familyLinkDetail.getRelationship());

            divider.setVisibility(View.VISIBLE);
            btnAccept.setVisibility(View.VISIBLE);
            btnReject.setVisibility(View.VISIBLE);
            avd.setVisible(true, true);
            btnAccept.setImageDrawable(avd);
        }

        /**
         * Handle click listener for accept button
         */
        @OnClick(R.id.button_accept)
        void onAcceptButtonClicked() {
            divider.setVisibility(View.GONE);
            btnReject.setVisibility(View.GONE);

            avd.start();

            btnAccept.setEnabled(false);

            listener.onItemClicked(createRequest(true));
        }

        /**
         * Handle click listener for reject button
         */
        @OnClick(R.id.button_reject)
        void onRejectButtonClicked() {
            listener.onItemClicked(createRequest(false));
        }

        /**
         * Returns a POJO for invite accept/reject request
         *
         * @param isAccepted <code>true</code> if {@link #btnAccept} is clicked; <code>false</code>
         * @return InviteRequest object
         */
        private InviteRequest createRequest(boolean isAccepted) {
            return new InviteRequest.Builder()
                    .userId(familyLinkDetail.getUserId())
                    .isAccepted(isAccepted)
                    .layoutPosition(getLayoutPosition())
                    .inviterName(String.format("%s %s", familyLinkDetail.getFirstName(), familyLinkDetail.getLastName()))
                    .build();
        }
    }
}
