package ph.com.auspex.mers.data.remote.exception;

/**
 * Exception thrown when making an API Request but is not connected to the internet
 */
public class NoConnectivityException extends RuntimeException {

    /**
     * Constructs the exception with a default message
     */
    public NoConnectivityException() {
        super("No Internet Connection");
    }

    /**
     * Constructs the exception with the provided message
     *
     * @param message custom error message
     */
    public NoConnectivityException(String message) {
        super(message);
    }
}
