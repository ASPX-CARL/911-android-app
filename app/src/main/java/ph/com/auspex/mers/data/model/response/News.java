package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ph.com.auspex.mers.data.remote.ApiService;

/**
 * Provide a POJO for {@link ApiService#getNewsList()}
 */
public class News {

    @SerializedName("news_id")
    private String newsId;
    private String url;
    private List<Feed> feeds;

    /**
     * Returns the news channel id
     *
     * @return news channel id
     */
    public String getNewsId() {
        return newsId;
    }

    /**
     * Returns the news channel url
     *
     * @return news channel url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Returns the list of news feeds
     *
     * @return list of news feed
     */
    public List<Feed> getFeeds() {
        return feeds;
    }
}
