package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.auspex.mers.R;

/**
 * I don't remember why I did this class.
 *
 * @see RecyclerLoadingView
 */
public class RecyclerViewLCE extends FrameLayout implements OnRefreshListener {

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    private LinearLayoutManager layoutManager;

    private OnRefreshListener onRefreshListener = () -> {
    };

    public RecyclerViewLCE(@NonNull Context context) {
        super(context);

        throw new UnsupportedOperationException("Inflate from xml.");
    }

    public RecyclerViewLCE(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RecyclerViewLCE, 0, 0);
        LayoutInflater.from(context).inflate(R.layout.layout_recycler_view_lce, this, true);

        ButterKnife.bind(this);

        int orientation = a.getInt(R.styleable.RecyclerViewLCE_android_orientation, LinearLayout.VERTICAL);
        layoutManager = new LinearLayoutManager(context, orientation, false);

        boolean displayDivider = a.getBoolean(R.styleable.RecyclerViewLCE_rv_has_divider, false);

        initSwipeRefreshLayout();
        initRecyclerView();

        if (displayDivider) {
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));
        }

        a.recycle();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
    }

    private void initSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);

        if (swipeRefreshLayout.isRefreshing()) {
            setRefreshing(false);
        }
    }

    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    public void setOnRefreshListener(OnRefreshListener onRefreshListener) {
        this.onRefreshListener = onRefreshListener;
    }

    @Override
    public void onRefresh() {
        onRefreshListener.onRefresh();
    }

    public RecyclerView.Adapter getAdapter() {
        return recyclerView.getAdapter();
    }

    public void scrollToTop() {
        recyclerView.stopScroll();
        layoutManager.scrollToPositionWithOffset(0, 0);
    }

    public void addOnScrollListener(RecyclerView.OnScrollListener onScrollListener) {
        recyclerView.addOnScrollListener(onScrollListener);
    }

    public interface OnRefreshListener {
        void onRefresh();
    }

    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }
}
