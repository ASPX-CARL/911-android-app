package ph.com.auspex.mers.password.forgotpassword;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.password.forgotpassword.impl.ForgotPasswordPresenterImpl;

@Module
public class ForgotPasswordModule {

    /**
     * Provide ForgotPasswordInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return ForgotPasswordInteractor
     */
    @ForgotPasswordScope
    @Provides
    ForgotPasswordInteractor providesForgotPasswordInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide ForgotPasswordPresenter
     *
     * @param model {@link ForgotPasswordInteractor}
     * @return ForgotPasswordPresenter
     */
    @ForgotPasswordScope
    @Provides
    ForgotPasswordContract.ForgotPasswordPresenter providesForgotPasswordPresenter(ForgotPasswordInteractor model) {
        return new ForgotPasswordPresenterImpl(model);
    }
}
