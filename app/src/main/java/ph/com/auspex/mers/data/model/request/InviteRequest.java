package ph.com.auspex.mers.data.model.request;

public class InviteRequest {
    private String userId;
    private boolean isAccepted;
    private int layoutPosition;
    private String inviterName;

    private InviteRequest(Builder builder) {
        this.userId = builder.userId;
        this.isAccepted = builder.isAccepted;
        this.layoutPosition = builder.layoutPosition;
        this.inviterName = builder.inviterName;
    }

    public String getInviterName() {
        return inviterName;
    }

    public String getUserId() {
        return userId;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public int getLayoutPosition() {
        return layoutPosition;
    }

    public static class Builder {
        private String userId;
        private boolean isAccepted;
        private int layoutPosition;
        private String inviterName;

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder isAccepted(boolean isAccepted) {
            this.isAccepted = isAccepted;
            return this;
        }

        public Builder layoutPosition(int layoutPosition) {
            this.layoutPosition = layoutPosition;
            return this;
        }

        public Builder inviterName(String inviterName) {
            this.inviterName = inviterName;
            return this;
        }

        public InviteRequest build() {
            return new InviteRequest(this);
        }
    }
}
