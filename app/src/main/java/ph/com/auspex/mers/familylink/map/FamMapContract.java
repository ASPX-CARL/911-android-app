package ph.com.auspex.mers.familylink.map;

import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;

public interface FamMapContract {

    interface FamMapPresenter extends MvpPresenter<FamMapView> {
        void getFamilyLinkPins();

        void getPendingInvitesCount();

        void getLastKnownLocation();

        void observeGpsSettingsChanges();

        void observeCurrentLocationChanges();

        // void requestFineLocationPermission();
    }

    interface FamMapView extends RemoteView {
        void onCurrentLocationUpdated(LatLng currentLocation);

        void displayLastKnownLocation(LatLng lastKnownLocation);

        void displayFamilyLinkPins(List<FamilyLinkDetail> familyLinkDetails);

        void displayFamilyLinkInvitesCount(int count);

        // void toggleAccountBlocker(boolean shouldShow);

        void toggleGpsBlocker(boolean shouldShow);

        void togglePermissionBlocker(boolean shouldShow);

        void onLowMemory();
    }
}
