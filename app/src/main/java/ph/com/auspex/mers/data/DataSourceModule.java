package ph.com.auspex.mers.data;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.devland.esperandro.Esperandro;
import de.devland.esperandro.serialization.GsonSerializer;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.data.local.LocalDataSource;
import ph.com.auspex.mers.data.local.RealmController;
import ph.com.auspex.mers.data.remote.ApiService;
import ph.com.auspex.mers.data.remote.RemoteDataSource;
import ph.com.auspex.mers.data.sharedpref.AppPreferences;

@Module(includes = NetworkModule.class)
public class DataSourceModule {

    @Provides
    @Singleton
    RealmController providesRealmController() {
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder()
                .name(RealmController.FILE_REALM)
                .deleteRealmIfMigrationNeeded()
                .build());
        return new RealmController();
    }

    @Provides
    @Singleton
    AppPreferences providesAppPreferences(Application application) {
        Esperandro.setSerializer(new GsonSerializer());
        return Esperandro.getPreferences(AppPreferences.class, application);
    }

    @Provides
    @Singleton
    @Named("local")
    DataSourceRepository providesLocalDataSource(RealmController realmController) {
        return new LocalDataSource(realmController);
    }

    @Provides
    @Singleton
    @Named("remote")
    DataSourceRepository providesRemoteDataSource(ApiService apiService, RealmController realmController) {
        return new RemoteDataSource(apiService, realmController);
    }

    @Provides
    @Singleton
    AppDataSource providesAppDataSource(Context context, @Named("remote") DataSourceRepository remoteDataSource, @Named("local") DataSourceRepository localDataSource, AppPreferences appPreferences, UserWrapper userWrapper) {
        return new AppDataSource(context, remoteDataSource, localDataSource, appPreferences, userWrapper);
    }
}