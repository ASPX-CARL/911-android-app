package ph.com.auspex.mers.verification;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.data.model.request.VerificationRequest.VerificationType;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.password.changepassword.ChangePasswordController;
import ph.com.auspex.mers.util.FormValidationUtil;
import ph.com.auspex.mers.verification.VerificationContract.VerificationPresenter;
import ph.com.auspex.mers.verification.VerificationContract.VerificationView;

/**
 * Verification Controller UI
 */
public class VerificationController extends MosbyController<VerificationView, VerificationPresenter> implements VerificationView {

    public static final String TAG = VerificationController.class.getSimpleName();
    public static final String KEY_USER_ID = TAG + ".userId";
    public static final String KEY_VERIFICATION_TYPE = TAG + ".type";
    public static final String KEY_CODE = TAG + ".code";

    @BindView(R.id.input_registration_code) TextInputEditText inputCode;
    @BindView(R.id.textview_header) TextView tvHeader;

    @Inject VerificationPresenter presenter;
    @Inject Router parentRouter;
    @Inject FormValidationUtil validationUtil;

    private String userId;
    private VerificationType verificationType;
    private String code;

    /**
     * Constructor to create a VerificationController instance
     *
     * @param args Object bundle
     */
    public VerificationController(@Nullable Bundle args) {
        super(args);

        setHasOptionsMenu(true);

        if (args != null) {
            userId = args.getString(KEY_USER_ID);
            verificationType = (VerificationType) args.get(KEY_VERIFICATION_TYPE);
            code = args.getString(KEY_CODE);
        }
    }

    /**
     * Inject {@link ActivityComponent} in this controller
     *
     * @param activityComponent {@link ActivityComponent} instance
     */
    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.verificationComponent(new VerificationModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        if (verificationType == VerificationType.REGISTRATION) {
            tvHeader.setText(R.string.label_successfully_registered);
        } else if (verificationType == VerificationType.USER_NOT_VERIFIED) {
            tvHeader.setText(R.string.label_account_not_verified);
        } else {
            tvHeader.setText(R.string.label_forgot_password_verification);
        }
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (!TextUtils.isEmpty(code)) {
            inputCode.setText(code);
            onConfirmClicked();
        }
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_verification, container, false);
    }

    @NonNull
    @Override
    public VerificationPresenter createPresenter() {
        return presenter;
    }

    /**
     * Verify text code from {{@link #inputCode}}
     */
    @OnClick(R.id.button_confirm)
    void onConfirmClicked() {
        if (!validationUtil.hasEmptyField(inputCode)) {
            presenter.verifyCode(inputCode.getText().toString(), userId, verificationType);
        }
    }

    /**
     * Return method when the code verification is successful
     *
     * @param message display message
     */
    @Override
    public void onVerificationSuccessful(String message) {
        new MaterialDialog.Builder(activity)
                .content(message)
                .positiveText(R.string.proceed)
                .onPositive((dialog, which) -> {
                    if (verificationType == VerificationType.PASSWORD) {
                        openUpdatePasswordController();
                    } else {
                        parentRouter.setRoot(RouterTransaction.with(new NavigationController()));
                    }
                    dialog.dismiss();
                })
                .positiveColorRes(R.color.app_blue)
                .show();
    }

    /**
     * Return method when the code verification returns already used
     *
     * @param message display message
     */
    @Override
    public void onCodeAlreadyUsed(String message) {
        new MaterialDialog.Builder(activity)
                .content(message)
                .positiveText(R.string.proceed)
                .onPositive((dialog, which) -> {
                    parentRouter.setRoot(RouterTransaction.with(new NavigationController()));
                    dialog.dismiss();
                })
                .positiveColorRes(R.color.app_blue)
                .show();
    }

    /**
     * Return method when the code verification returns when the user is not found
     *
     * @param message display message
     */
    @Override
    public void onUserNotFound(String message) {
        parentRouter.setRoot(RouterTransaction.with(new NavigationController()));
    }

    /**
     * Method that opens the next controller in the process
     */
    private void openUpdatePasswordController() {
        Bundle args = new Bundle();
        args.putString(ChangePasswordController.KEY_USER_ID, userId);

        parentRouter.pushController(RouterTransaction.with(new ChangePasswordController(args)));
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_register;
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                handleBack();
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    @Override
    public boolean handleBack() {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_cancel_verification)
                .content(R.string.dialog_negative_cancel_verification)
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_no)
                .onPositive((dialog, which) -> parentRouter.setRoot(RouterTransaction.with(new NavigationController())))
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
        return true;
    }
}
