package ph.com.auspex.mers.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Provide a POJO for the received json object for {@link ph.com.auspex.mers.data.model.response.InternalFeed} in {@link ph.com.auspex.mers.pushnotification.OpenHandler}
 */
public class NewsPushNotification implements Parcelable {

    @SerializedName("url")
    private String url;

    protected NewsPushNotification(Parcel in) {
        url = in.readString();
    }

    public static final Creator<NewsPushNotification> CREATOR = new Creator<NewsPushNotification>() {
        @Override
        public NewsPushNotification createFromParcel(Parcel in) {
            return new NewsPushNotification(in);
        }

        @Override
        public NewsPushNotification[] newArray(int size) {
            return new NewsPushNotification[size];
        }
    };

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
    }
}
