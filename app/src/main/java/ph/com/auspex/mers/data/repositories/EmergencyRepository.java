package ph.com.auspex.mers.data.repositories;

import java.util.List;

import io.reactivex.Observable;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.response.EmergencyType;

public interface EmergencyRepository {

    Observable<List<EmergencyType>> getEmergencyTypes();

    Observable<String> reportEmergency(EmergencyRequest request);
}
