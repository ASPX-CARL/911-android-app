package ph.com.auspex.mers.service;

import android.location.Location;

public class LocationUpdateEvent {

    private Location location;

    public LocationUpdateEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
