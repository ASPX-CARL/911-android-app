package ph.com.auspex.mers.splash;

import dagger.Subcomponent;

@SplashScope
@Subcomponent(modules = SplashModule.class)
public interface SplashComponent {
    void inject(SplashController splashController);
}
