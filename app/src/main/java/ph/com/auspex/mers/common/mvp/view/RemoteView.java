package ph.com.auspex.mers.common.mvp.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface RemoteView extends MvpView {

    /**
     * Show UI Loading Dialog
     */
    void showLoading();

    /**
     * Hide UI Loading Dialog
     */
    void hideLoading();

    /**
     * Show error message in UI
     *
     * @param message String to be displayed
     */
    void showError(String message);
}
