package ph.com.auspex.mers.data.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class NearbyPlaceDetail implements Parcelable {

    @SerializedName("formatted_address")
    private String address;
    @SerializedName("formatted_phone_number")
    private String phoneNumber;
    @SerializedName("international_phone_number")
    private String phoneNumberIntl;

    protected NearbyPlaceDetail(Parcel in) {
        address = in.readString();
        phoneNumber = in.readString();
        phoneNumberIntl = in.readString();
    }

    public static final Creator<NearbyPlaceDetail> CREATOR = new Creator<NearbyPlaceDetail>() {
        @Override
        public NearbyPlaceDetail createFromParcel(Parcel in) {
            return new NearbyPlaceDetail(in);
        }

        @Override
        public NearbyPlaceDetail[] newArray(int size) {
            return new NearbyPlaceDetail[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneNumberIntl() {
        return phoneNumberIntl;
    }

    public NearbyPlaceDetail(String address, String phoneNumber, String phoneNumberIntl) {
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.phoneNumberIntl = phoneNumberIntl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(phoneNumber);
        dest.writeString(phoneNumberIntl);
    }
}
