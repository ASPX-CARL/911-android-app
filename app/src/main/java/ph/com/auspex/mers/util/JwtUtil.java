package ph.com.auspex.mers.util;

import android.util.Base64;

import com.google.gson.Gson;

/**
 * JWT Parsing utility class
 */
public class JwtUtil {

    /**
     * Get JWT Body from JWT
     *
     * @param jwtString JWT string
     * @return JWT String body
     */
    private static String getBodyAsJson(String jwtString) {
        String[] jwtBody = jwtString.split("\\.");
        return new String(Base64.decode(jwtBody[1], Base64.DEFAULT));
    }

    /**
     * Generic method that will be returned after parsing the JWT Body from {@link #getBodyAsJson(String)}
     *
     * @param jwtString JWT Body string
     * @param clazz     Generic class of the parsing object
     * @param <T>       Generic object
     * @return Generic object
     */
    public static <T> T parseJwt(String jwtString, Class<T> clazz) {
        return new Gson().fromJson(getBodyAsJson(jwtString), clazz);
    }
}
