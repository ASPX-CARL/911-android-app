package ph.com.auspex.mers.emergency;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.camera.CameraActivity;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.TitleSpinnerView;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.response.EmergencyType;
import ph.com.auspex.mers.emergency.EmergencyContract.EmergencyPresenter;
import ph.com.auspex.mers.emergency.EmergencyContract.EmergencyView;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.GlideImageLoader;
import ph.com.auspex.mers.util.MaterialDialogManager;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Provide a UI Emergency reporting feature of the application
 */
public class EmergencyController extends MosbyController<EmergencyView, EmergencyPresenter> implements EmergencyView, TitleSpinnerView.OnItemSelectedListener {

    public static final String TAG = EmergencyController.class.getSimpleName();
    public static final int REQUEST_IMAGE_CAPTURE = 1;

    @BindView(R.id.spinner_emergency_type) TitleSpinnerView emergencyTypes;
    @BindView(R.id.spinner_emergency_categories) TitleSpinnerView<EmergencyType> emergencyCategories;
    @BindView(R.id.layout_not_synced) LinearLayout layoutNotSynced;
    @BindView(R.id.layout_not_logged_in) LinearLayout layoutNotLoggedIn;
    @BindView(R.id.layout_synced) LinearLayout layoutSynced;
    @BindView(R.id.attachment_image) ImageView ivAttachment;
    @BindView(R.id.input_remarks) EditText inputRemarks;

    @BindString(R.string.label_emergencies) String title;

    @Inject EmergencyPresenter presenter;
    @Inject UserWrapper userWrapper;

    private File imageFile;

    /**
     * Builds and inject the {@link ActivityComponent} to this controller
     *
     * @param activityComponent {@link ActivityComponent} instance
     */
    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.emergencyComponent(new EmergencyModule()).inject(this);
    }

    /**
     * Inflate layout resource
     *
     * @param inflater  Controller's LayoutInflater
     * @param container Controller's ViewGroup container
     * @return
     */
    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_emergency, container, false);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        emergencyTypes.setListener(this);
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);
    }

    /**
     * Returns the title for this controller
     *
     * @return controller title
     */
    @Override
    protected CharSequence getTitle() {
        return title;
    }

    /**
     * Refresh the emergency types when the app failed to synchronize the data at the splash screen
     */
    @OnClick(R.id.button_refresh)
    void onRefreshClicked() {
        presenter.getEmergencyTypes();
    }

    /**
     * Submits the report information and call the number provided
     */
    @OnClick(R.id.button_call)
    void onCallClicked() {
        Nammu.askForPermission(activity, Manifest.permission.CALL_PHONE, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                call();
            }

            @Override
            public void permissionRefused() {
                showError(activity.getString(R.string.error_permission_not_granted_call));
            }
        });
    }

    @NonNull
    @Override
    public EmergencyPresenter createPresenter() {
        return presenter;
    }

    /**
     * Displays the {@link List} of {@link EmergencyType} on the {@link #emergencyCategories} {@link Spinner}
     *
     * @param emergencyTypes emergency types from {@link ph.com.auspex.mers.data.local.LocalDataSource}
     */
    @Override
    public void displayEmergencyTypes(List<EmergencyType> emergencyTypes) {
        emergencyCategories.setAdapter(new EmergencyCategoryAdapter(getActivity(), R.layout.layout_category, emergencyTypes));
        layoutSynced.setVisibility(VISIBLE);
        layoutNotSynced.setVisibility(GONE);

        if (!AppUtil.hasCamera(activity)) {
            ivAttachment.setVisibility(GONE);
        }
    }

    /**
     * Display empty layout when the data is not properly synced
     */
    @Override
    public void displayEmptyLayout() {
        layoutNotSynced.setVisibility(VISIBLE);
        layoutSynced.setVisibility(GONE);
    }

    /**
     * Submits the report information to the presenter and call the number provided
     */
    @Override
    public void call() {
        if (AppUtil.isNetworkAvailable(getActivity()) && userWrapper.isLoggedIn()) {
            EmergencyRequest.Builder requestBuilder = new EmergencyRequest.Builder();

            requestBuilder.reportTypeId(emergencyTypes.getSelectedItemPosition());
            requestBuilder.emergencyTypeId(emergencyCategories.getSelectedItem().getId());

            if (imageFile != null) {
                requestBuilder.imageFile(AppUtil.compressFile(activity, imageFile));
            }

            if (!TextUtils.isEmpty(inputRemarks.getText().toString())) {
                requestBuilder.remarks(inputRemarks.getText().toString());
            }

            presenter.report(requestBuilder.build());
        }

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(String.format("tel:%s", activity.getString(R.string.contact_number))));
        startActivity(intent);
    }

    /**
     * Opens the internal camera {@link CameraActivity}
     */
    @Override
    public void openImagePicker() {
        Intent intent = new Intent(activity, CameraActivity.class);
        intent.putExtra(CameraActivity.KEY_FLASH, emergencyCategories.getSelectedItem().isFlashEnabled());
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    /**
     * Clear the report form after a successful submit of the previous form data
     */
    @Override
    public void onRequestSuccess() {
        ivAttachment.setImageResource(R.drawable.ic_camera_attachment);
        inputRemarks.setText("");
        inputRemarks.clearFocus();
        imageFile = null;
    }

    /**
     * Display and hide views when the user is not logged in
     */
    @Override
    public void setupNotLoggedIn() {
        layoutSynced.setVisibility(GONE);
        layoutNotSynced.setVisibility(GONE);
        layoutNotLoggedIn.setVisibility(VISIBLE);
    }

    /**
     * Handles back button press event
     *
     * @return always <code>true</code>
     */
    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCloseApplicationDialog(activity);
        return true;
    }

    /**
     * On spinner item selected
     *
     * @param spinner  {@link Spinner} view
     * @param position position of selected item in the {@link EmergencyCategoryAdapter}
     */
    @Override
    public void onItemSelected(Spinner spinner, int position) {
    }

    /**
     * Open {@link CameraActivity} if {@link Manifest.permission#CAMERA} is granted
     */
    @OnClick(R.id.attachment_image)
    void onAttachmentClicked() {
        // presenter.checkPermissions();
        Nammu.askForPermission(activity, Manifest.permission.CAMERA, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                openImagePicker();
            }

            @Override
            public void permissionRefused() {
                showError(activity.getString(R.string.error_permission_not_granted_image_emergency));
            }
        });
    }

    /**
     * Should be overridden if this Controller has called startActivityForResult and needs to handle
     * the result.
     *
     * @param requestCode The requestCode passed to startActivityForResult
     * @param resultCode  The resultCode that was returned to the host Activity's onActivityResult method
     * @param data        The data Intent that was returned to the host Activity's onActivityResult method
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (data != null) {
                        imageFile = new File(data.getStringExtra(CameraActivity.KEY_FILE));

                        GlideImageLoader.loadFile(activity, ivAttachment, imageFile, false);
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Show login screen when account is not found
     */
    void showLoginScreen() {
        if (getParentController() != null) {
            ((NavigationController) getParentController()).replaceCurrentControllerWith(NavigationController.NavigationItem.LOGIN_CONTROLLER);
        }
    }

    /**
     * Handles login button click
     */
    @OnClick(R.id.button_login)
    void onButtonLoginClicked() {
        showLoginScreen();
    }
}
