package ph.com.auspex.mers.password.forgotpassword;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ForgotPasswordScope {
}
