package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import ph.com.auspex.mers.R;

/**
 * A special sub-class of {@link android.widget.Button} that handles version compatibility of {@link android.graphics.drawable.VectorDrawable}
 */
public class CompatButton extends AppCompatButton {

    /**
     * Create a view instance from xml
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    public CompatButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Initialize view
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CompatButton, 0, 0);

        Drawable drawableLeft = null;
        Drawable drawableRight = null;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            drawableLeft = a.getDrawable(R.styleable.CompatButton_cb_drawableLeft);
            drawableRight = a.getDrawable(R.styleable.CompatButton_cb_drawableRight);
        } else {
            final int drawableLeftId = a.getResourceId(R.styleable.CompatButton_cb_drawableLeft, -1);
            final int drawableRightId = a.getResourceId(R.styleable.CompatButton_cb_drawableRight, -1);

            if (drawableLeftId != -1) {
                drawableLeft = AppCompatResources.getDrawable(context, drawableLeftId);
            }

            if (drawableRightId != -1) {
                drawableRight = AppCompatResources.getDrawable(context, drawableRightId);
            }
        }

        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, drawableRight, null);
        a.recycle();
    }
}
