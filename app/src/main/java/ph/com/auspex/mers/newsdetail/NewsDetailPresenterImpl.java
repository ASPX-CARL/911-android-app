package ph.com.auspex.mers.newsdetail;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.news.NewsInteractor;
import ph.com.auspex.mers.newsdetail.NewsDetailContract.NewsDetailPresenter;
import ph.com.auspex.mers.newsdetail.NewsDetailContract.NewsDetailView;

/**
 * News Detail Presenter Implementation
 */
public class NewsDetailPresenterImpl extends MvpBasePresenter<NewsDetailView> implements NewsDetailPresenter {

    private final NewsInteractor interactor;

    public NewsDetailPresenterImpl(NewsInteractor interactor) {
        this.interactor = interactor;
    }

    /**
     * Get News Detail using newsId
     *
     * @param id newsId
     */
    @Override
    public void getNews(int id) {
        getView().showLoading();

        interactor.getSpecificNews(id, new BaseInteractor.ResponseListener<InternalFeed>() {
            @Override
            public void onSuccess(InternalFeed internalFeed) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().displayNews(internalFeed.getUrl());
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
