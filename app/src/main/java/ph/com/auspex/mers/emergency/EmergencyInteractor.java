package ph.com.auspex.mers.emergency;

import java.util.List;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.response.EmergencyType;

public interface EmergencyInteractor extends BaseInteractor {

    void getEmergencyTypes(ResponseListener<List<EmergencyType>> listener);

    void reportEmergency(EmergencyRequest request, ResponseListener<String> listener);
}
