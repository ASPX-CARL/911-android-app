package ph.com.auspex.mers.password;

import dagger.Subcomponent;
import ph.com.auspex.mers.password.changepassword.ChangePasswordController;
import ph.com.auspex.mers.password.updatepassword.UpdatePasswordController;

@PasswordScope
@Subcomponent(modules = PasswordModule.class)
public interface PasswordComponent {
    void inject(ChangePasswordController changePasswordController);

    void inject(UpdatePasswordController updatePasswordController);
}
