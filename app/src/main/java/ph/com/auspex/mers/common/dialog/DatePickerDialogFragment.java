package ph.com.auspex.mers.common.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.Calendar;
import java.util.Date;

/**
 * Date picker with disabled dates (13 years before from current date)
 */
public class DatePickerDialogFragment extends DialogFragment {

    public static final String TAG = DatePickerDialogFragment.class.getSimpleName();
    public static final String CURRENT_DATE = "CURRENT_DATE";
    private DatePickerDialog.OnDateSetListener listener;

    // set 13 years ago
    private long lessYears = 410240376000L;

    /**
     * Create a new DatePickerDialogFragment instance
     *
     * @param currentDate Default date set after showing the Date Picker Dialog
     * @return DatePickerDialogFragment instance
     */
    public static DatePickerDialogFragment newInstance(Date currentDate) {
        final DatePickerDialogFragment fragment = new DatePickerDialogFragment();
        final Bundle args = new Bundle();
        long currentDateInMillis = currentDate != null ? currentDate.getTime() : System.currentTimeMillis();
        args.putLong(CURRENT_DATE, currentDateInMillis);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getArguments().getLong(CURRENT_DATE));

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), listener, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMaxDate(System.currentTimeMillis() - lessYears);

        return dialog;
    }

    /**
     * Set DialogPickerDateFragment date set listener
     *
     * @param listener DateSetListener
     * @return
     */
    public DatePickerDialogFragment setOnDateSelectedListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
        return this;
    }
}
