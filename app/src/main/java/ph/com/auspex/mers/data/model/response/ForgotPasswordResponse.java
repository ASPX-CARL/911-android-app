package ph.com.auspex.mers.data.model.response;

/**
 * Provide a POJO for response in {@link ph.com.auspex.mers.data.remote.ApiService#forgotPassword(String)}
 */
public class ForgotPasswordResponse {

    private String message;
    private String userId;

    /**
     * Constructs a ForgotPasswordResponse from API response message and user id
     *
     * @param message API response message
     * @param userId  user id
     */
    public ForgotPasswordResponse(String message, String userId) {
        this.message = message;
        this.userId = userId;
    }

    /**
     * Returns the API response message
     *
     * @return response message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns the user id
     *
     * @return user id
     */
    public String getUserId() {
        return userId;
    }
}
