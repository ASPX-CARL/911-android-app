package ph.com.auspex.mers.directories.impl;

import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.AppEvent;
import ph.com.auspex.mers.data.bus.events.LocationEvent;
import ph.com.auspex.mers.data.bus.events.LowMemoryEvent;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.directories.DirectoryContract;
import ph.com.auspex.mers.directories.DirectoryInteractor;

/**
 * Provides an implementation for {@link ph.com.auspex.mers.directories.DirectoryContract.DirectoryPresenter}.
 */
public class DirectoryPresenterImpl extends MvpBasePresenter<DirectoryContract.DirectoryView> implements DirectoryContract.DirectoryPresenter {

    private final DirectoryInteractor model;
    private final RxEventBus rxEventBus;

    private List<NearbyPlace> selectedNearbyPlaces;

    public DirectoryPresenterImpl(DirectoryInteractor model, RxEventBus rxEventBus) {
        this.model = model;
        this.rxEventBus = rxEventBus;
    }

    @Override
    public void attachView(DirectoryContract.DirectoryView view) {
        super.attachView(view);

        observeOnLowMemoryEvent();
        observeCurrentLocationChanges();
        observeGpsSettingsChanges();
    }

    /**
     * Get the last known location {@link android.location.Location} in {@link ph.com.auspex.mers.data.sharedpref.AppPreferences}
     */
    @Override
    public void getLastKnownLocation() {
        model.getLastKnownLocation(new BaseInteractor.ResponseListener<LatLng>() {
            @Override
            public void onSuccess(LatLng latLng) {
                if (isViewAttached() && latLng != null) {
                    getView().onCurrentLocationUpdated(latLng);
                }
            }

            @Override
            public void onFailure(String message) {
            }
        });
    }

    /**
     * Listens for GPS status change event
     */
    @Override
    public void observeGpsSettingsChanges() {
        rxEventBus.toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof AppEvent.GpsStatusChangedEvent) {
                        AppEvent.GpsStatusChangedEvent event = (AppEvent.GpsStatusChangedEvent) o;

                        if (isViewAttached()) {
                            getView().showGpsSettingsBlocker(!event.isEnabled());
                        }
                    }
                });
    }

    /**
     * Listens for {@link android.location.Location} updates from {@link ph.com.auspex.mers.service.LocationUpdateService}
     */
    @Override
    public void observeCurrentLocationChanges() {
        rxEventBus.toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof LocationEvent.LocationUpdatedEvent) {
                        LocationEvent.LocationUpdatedEvent event = (LocationEvent.LocationUpdatedEvent) o;

                        if (isViewAttached()) {
                            getView().onCurrentLocationUpdated(event.getLastKnownLocation());
                        }
                    }
                });
    }

    /**
     * Request for nearby places using the given type and the users current location
     *
     * @param placeType place type
     */
    @Override
    public void requestNearbyPlaces(NearbyPlaceRequest.PlaceType placeType) {
        getView().showLoading();

        model.getNearbyPlaces(placeType, new BaseInteractor.ResponseListener<List<NearbyPlace>>() {
            @Override
            public void onSuccess(List<NearbyPlace> nearbyPlaces) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().displayNearbyPlaces(nearbyPlaces);

                    selectedNearbyPlaces = nearbyPlaces;
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    // getView().showError(message);
                }
            }
        });
    }

    /**
     * @param destination {@link android.location.Location} location destination
     * @deprecated This feature is currently not being used
     */
    @Deprecated
    @Override
    public void requestForDirections(LatLng destination) {
        getView().showLoading();

        model.getDirections(destination, new BaseInteractor.ResponseListener<MapDirection>() {
            @Override
            public void onSuccess(MapDirection mapDirection) {
                getView().hideLoading();
                getView().displayNearbyPlaces(selectedNearbyPlaces);
                getView().displayDirection(mapDirection.getPolylinePoints());
            }

            @Override
            public void onFailure(String message) {
                getView().hideLoading();
                getView().showError(message);
            }
        });
    }

    private void observeOnLowMemoryEvent() {
        rxEventBus.toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof LowMemoryEvent) {
                        if (isViewAttached()) {
                            getView().onLowMemory();
                        }
                    }
                });
    }
}
