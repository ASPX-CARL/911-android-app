package ph.com.auspex.mers.verification;

import dagger.Subcomponent;

@VerificationScope
@Subcomponent(modules = VerificationModule.class)
public interface VerificationComponent {
    void inject(VerificationController verificationController);
}
