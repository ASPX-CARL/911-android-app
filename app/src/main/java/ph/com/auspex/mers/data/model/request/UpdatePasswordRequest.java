package ph.com.auspex.mers.data.model.request;

import android.text.TextUtils;

import java.util.HashMap;

/**
 * Update Password request
 */
public class UpdatePasswordRequest {

    private String userId;
    private String updatedPassword;
    private String currentPassword;

    private UpdatePasswordRequest(Builder builder) {
        this.userId = builder.userId;
        this.updatedPassword = builder.updatedPassword;
        this.currentPassword = builder.currentPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public static class Builder {
        private String userId;
        private String updatedPassword;
        private String currentPassword;

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder updatedPassword(String updatedPassword) {
            this.updatedPassword = updatedPassword;
            return this;
        }

        public Builder currentPassword(String currentPassword) {
            this.currentPassword = currentPassword;
            return this;
        }

        public UpdatePasswordRequest build() {
            return new UpdatePasswordRequest(this);
        }
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public HashMap<String, String> create() {
        HashMap<String, String> params = new HashMap<>();

        params.put("user_id", userId);
        params.put("new_password", updatedPassword);
        params.put("confirm_password", updatedPassword);

        if (currentPassword != null && !TextUtils.isEmpty(currentPassword)) {
            params.put("current_password", currentPassword);
        }

        return params;
    }
}
