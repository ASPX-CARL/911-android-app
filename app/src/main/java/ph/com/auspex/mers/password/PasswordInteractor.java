package ph.com.auspex.mers.password;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.UpdatePasswordRequest;

public interface PasswordInteractor extends BaseInteractor {

    void changePassword(UpdatePasswordRequest request, ResponseListener<String> listener);
}
