package ph.com.auspex.mers.common.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.conductor.delegate.MvpConductorDelegateCallback;
import com.hannesdorfmann.mosby3.mvp.conductor.delegate.MvpConductorLifecycleListener;

public abstract class MosbyViewStateController<V extends MvpView, P extends MvpPresenter<V>> extends ViewStateController implements MvpView, MvpConductorDelegateCallback<V, P> {

    {
        addLifecycleListener(getMosbyLifecycleListener());
    }

    /**
     * This method is for internal purpose only.
     * <p><b>Do not override this until you have a very good reason</b></p>
     *
     * @return Mosby's lifecycle listener so that
     */
    protected LifecycleListener getMosbyLifecycleListener() {
        return new MvpConductorLifecycleListener<>(this);
    }

    public MosbyViewStateController() {
    }

    public MosbyViewStateController(@Nullable Bundle args) {
        super(args);
    }

    @Nullable
    protected P presenter;

    @Nullable
    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    public void setPresenter(@NonNull P presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public V getMvpView() {
        return (V) this;
    }
}
