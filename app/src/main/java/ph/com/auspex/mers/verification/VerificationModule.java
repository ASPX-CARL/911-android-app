package ph.com.auspex.mers.verification;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.verification.impl.VerificationPresenterImpl;

/**
 * Provide the verification module object for dependency injection
 */
@Module
public class VerificationModule {

    /**
     * Provide VerificationInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return VerificationInteractor
     */
    @VerificationScope
    @Provides
    VerificationInteractor providesVerificationInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide VerificationPresenter
     *
     * @param model {@link VerificationInteractor}
     * @return VerificationPresenter
     */
    @VerificationScope
    @Provides
    VerificationContract.VerificationPresenter providesVerificationPresenter(VerificationInteractor model) {
        return new VerificationPresenterImpl(model);
    }
}
