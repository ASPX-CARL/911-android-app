package ph.com.auspex.mers.common.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluelinelabs.conductor.RestoreViewOnCreateController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.com.auspex.mers.ActionBarInterface;
import ph.com.auspex.mers.HasComponent;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.main.ActivityComponent;

public abstract class ToolbarController extends RestoreViewOnCreateController implements RemoteView {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;
    @BindView(R.id.content) CoordinatorLayout coordinatorLayout;

    @Inject ActionBarInterface actionBarInterface;

    private Unbinder unbinder;

    public ToolbarController() {
    }

    public ToolbarController(@Nullable Bundle args) {
        super(args);
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedViewState) {
        initDependencies();

        View view = inflateView(inflater, container);
        unbinder = ButterKnife.bind(this, view);

        initToolbar();

        return view;
    }

    private void initDependencies() {
        ActivityComponent activityComponent = getComponent(ActivityComponent.class);
        setupComponent(activityComponent);
    }

    protected abstract void setupComponent(@NonNull ActivityComponent activityComponent);

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    private <C> C getComponent(Class<C> component) {
        return component.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    private void initToolbar() {
        if (toolbar == null) {
            return;
        }

        actionBarInterface.setSupportActionBar(toolbar);
        ActionBar actionBar = actionBarInterface.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);

            /*if (getNavigationIcon() != 0) {
                Drawable drawable = ContextCompat.getDrawable(activity, getNavigationIcon());
                DrawableCompat.setTint(drawable, ContextCompat.getColor(activity, R.color.white));
                DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);

                actionBar.setHomeAsUpIndicator(drawable);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }*/

            /*setTitle(getTitle());*/
            setTitle("Test Title");
        }
    }

    private void setTitle(CharSequence title) {
        toolbarTitle.setText(!TextUtils.isEmpty(title) ? title : "");
    }

    @NonNull
    protected abstract View inflateView(LayoutInflater inflater, ViewGroup container);

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }
}