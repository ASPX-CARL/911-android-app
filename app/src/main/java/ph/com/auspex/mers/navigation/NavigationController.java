package ph.com.auspex.mers.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.controller.BaseController;
import ph.com.auspex.mers.common.views.BottomNavigationViewHelper;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.ListEvent;
import ph.com.auspex.mers.directories.DirectoryController;
import ph.com.auspex.mers.emergency.EmergencyController;
import ph.com.auspex.mers.familylink.map.FamMapController;
import ph.com.auspex.mers.login.LoginController;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.news.NewsViewPagerController;
import ph.com.auspex.mers.profile.ProfileController;

import static ph.com.auspex.mers.navigation.NavigationController.NavigationItem.DIRECTORIES_CONTROLLER;
import static ph.com.auspex.mers.navigation.NavigationController.NavigationItem.EMERGENCIES_CONTROLLER;
import static ph.com.auspex.mers.navigation.NavigationController.NavigationItem.FAMILY_LINK_CONTROLLER;
import static ph.com.auspex.mers.navigation.NavigationController.NavigationItem.LOGIN_CONTROLLER;
import static ph.com.auspex.mers.navigation.NavigationController.NavigationItem.NEWS_CONTROLLER;
import static ph.com.auspex.mers.navigation.NavigationController.NavigationItem.PROFILE_CONTROLLER;

/**
 * Main Navigation Controller UI
 */
public class NavigationController extends BaseController implements BottomNavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemReselectedListener {

    public static final String TAG = NavigationController.class.getSimpleName();

    @BindView(R.id.controller_container_child) ViewGroup viewGroup;
    @BindView(R.id.bottom_navigation_view) BottomNavigationView navigationView;

    @BindColor(R.color.app_blue) int colorBlue;
    @BindColor(R.color.app_gray) int colorGray;

    @Inject Activity activity;
    @Inject UserWrapper userWrapper;
    @Inject RxEventBus rxEventBus;

    private Router childRouter;

    /**
     * Navigation Item objects for {@link BottomNavigationView}.
     */
    public enum NavigationItem {
        NEWS_CONTROLLER(0, R.id.nav_news),
        DIRECTORIES_CONTROLLER(1, R.id.nav_directories),
        EMERGENCIES_CONTROLLER(2, R.id.nav_emergencies),
        FAMILY_LINK_CONTROLLER(3, R.id.nav_family_link),
        LOGIN_CONTROLLER(4, R.id.nav_profile),
        PROFILE_CONTROLLER(4, R.id.nav_profile);

        private int position;
        private int navId;

        /**
         * Position of each item in the {@link BottomNavigationView}.
         *
         * @param position index of this item in the BottomNavigationView.
         * @param navId    navigation id
         */
        NavigationItem(int position, int navId) {
            this.position = position;
            this.navId = navId;
        }
    }

    /**
     * Build {@link ActivityComponent} and inject this controller's dependencies.
     *
     * @param activityComponent {@link ActivityComponent} instance.
     */
    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    /**
     * Set default {@link Controller} to be displayed and initialize {@link BottomNavigationView}.
     *
     * @param view {@link Controller#getView()}
     */
    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        childRouter = getChildRouter(viewGroup).setPopsLastView(true);

        if (!childRouter.hasRootController()) {
            childRouter.pushController(RouterTransaction.with(new NewsViewPagerController()));
        }

        BottomNavigationViewHelper.disableShiftMode(navigationView);
        navigationView.setOnNavigationItemSelectedListener(this);
        navigationView.setOnNavigationItemReselectedListener(this);
    }

    /**
     * Set Layout resource for this {@link Controller}
     *
     * @param inflater  Controller's LayoutInflater
     * @param container Controller's ViewGroup container
     * @return inflated view
     */
    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_navigation, container, false);
    }

    /**
     * {@link BottomNavigationView} listener when a navigation item is selected.
     *
     * @param item {@link BottomNavigationView} item.
     * @return <code>true</code> if the navigation item is consumed.
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_news:
                replaceController(NEWS_CONTROLLER);
                break;
            case R.id.nav_emergencies:
                replaceController(EMERGENCIES_CONTROLLER);
                break;
            case R.id.nav_directories:
                replaceController(DIRECTORIES_CONTROLLER);
                break;
            case R.id.nav_family_link:
                replaceController(FAMILY_LINK_CONTROLLER);
                break;
            case R.id.nav_profile:
                replaceController(userWrapper.getUserDetail() != null ? PROFILE_CONTROLLER : LOGIN_CONTROLLER);
                break;
        }
        return true;
    }


    /**
     * {@link BottomNavigationView} listener when navigation item is re-selected.
     *
     * @param item {@link BottomNavigationView} item.
     */
    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_news:
                rxEventBus.send(new ListEvent.ScrollToTopEvent());
                break;
            default:
                break;
        }
    }

    /**
     * Forward the onActivityResult from the {@link ph.com.auspex.mers.main.MainActivity} to the child {@link Controller}
     *
     * @param requestCode Request Code of the result
     * @param resultCode  Result Code of the request
     * @param data        Intent data from the result
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        childRouter.getBackstack().get(childRouter.getBackstackSize() - 1).controller().onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Replace the current child {@link Controller} based on the {@link NavigationItem}
     *
     * @param navigationItem of the corresponding {@link BottomNavigationView} item
     */
    public void replaceCurrentControllerWith(NavigationItem navigationItem) {
        navigationView.setSelectedItemId(navigationItem.navId);
        replaceController(navigationItem);
    }

    /**
     * Replace the current child controller with a new {@link Controller}
     *
     * @param navigationItem based on the {@link BottomNavigationView} selected item id.
     */
    private void replaceController(NavigationItem navigationItem) {
        Controller controller;
        switch (navigationItem) {
            case NEWS_CONTROLLER:
                controller = new NewsViewPagerController();
                break;
            case DIRECTORIES_CONTROLLER:
                controller = new DirectoryController();
                break;
            case EMERGENCIES_CONTROLLER:
                controller = new EmergencyController();
                break;
            case FAMILY_LINK_CONTROLLER:
                controller = new FamMapController();
                break;
            case PROFILE_CONTROLLER:
                controller = new ProfileController();
                break;
            case LOGIN_CONTROLLER:
                controller = new LoginController();
                break;
            default:
                controller = new NewsViewPagerController();
                break;
        }
        childRouter.pushController(RouterTransaction.with(controller));
    }
}