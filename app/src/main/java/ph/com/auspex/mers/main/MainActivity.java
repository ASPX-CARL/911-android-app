package ph.com.auspex.mers.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.com.auspex.mers.AppComponent;
import ph.com.auspex.mers.HasComponent;
import ph.com.auspex.mers.MersApplication;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.LowMemoryEvent;
import ph.com.auspex.mers.util.MaterialDialogManager;
import ph.com.auspex.mers.main.router.RouterModule;
import ph.com.auspex.mers.newsdetail.NewsDetailController;
import ph.com.auspex.mers.service.LocationUpdateService;
import ph.com.auspex.mers.splash.SplashController;
import ph.com.auspex.mers.util.AppUtil;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Provide a single activity approach for the whole application.
 * This class handles all of the {@link com.bluelinelabs.conductor.Controller} for the different features.
 * This class also handles deep link transactions outside the application.
 * This class handles the app's background services.
 */
public class MainActivity extends AppCompatActivity implements HasComponent<ActivityComponent> {

    public static final String NEWS_KEY = ".news";

    @BindView(R.id.controller_container) ViewGroup container;

    private Router router;
    private Intent serviceIntent;
    private Unbinder unbinder;

    @Inject RxEventBus rxEventBus;

    /**
     * Initialize this activity based on the received intent if present.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        serviceIntent = new Intent(this, LocationUpdateService.class);

        initRouter(savedInstanceState);
        getComponent().inject(this);

        if (getIntent().getStringExtra(NEWS_KEY) != null) {
            Bundle args = new Bundle();
            args.putString(SplashController.KEY_NEWS_ID, getIntent().getStringExtra(NEWS_KEY));
            router.setRoot(RouterTransaction.with(new SplashController(args)));
        } else if (getIntent().getData() != null) {
            Bundle args = new Bundle();
            args.putParcelable(SplashController.KEY_URL, getIntent().getData());
            router.setRoot(RouterTransaction.with(new SplashController(args)));
        } else {
            if (!router.hasRootController()) {
                router.setRoot(RouterTransaction.with(new SplashController()));
            }
        }

        checkForPermissions();
    }

    /**
     * This is called for activities that set launchMode to "singleTop" in their package, or if a client used the {@link Intent#FLAG_ACTIVITY_SINGLE_TOP} flag when calling {@link #startActivity(Intent)}
     */
    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getStringExtra(NEWS_KEY) != null) {  // called when app is open
            Bundle args = new Bundle();
            args.putString(NewsDetailController.KEY_URL, intent.getStringExtra(NEWS_KEY));
            router.pushController(RouterTransaction.with(new NewsDetailController(args)));
        } else if (intent.getData() != null) {
            Bundle args = new Bundle();
            args.putString(SplashController.KEY_URL, intent.getData().toString());
            router.setRoot(RouterTransaction.with(new SplashController(args)));
        }
    }

    /**
     * Request for {@link Manifest.permission#ACCESS_FINE_LOCATION} permissions
     */
    private void checkForPermissions() {
        Nammu.askForPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                startService();
                checkForGpsSettings();
            }

            @Override
            public void permissionRefused() {
            }
        });
    }

    /**
     * Start {@link LocationUpdateService}
     */
    public void startService() {
        startService(serviceIntent);
    }

    /**
     * Check for device's GPS settings
     */
    private void checkForGpsSettings() {
        if (!AppUtil.checkGpsEnabled(this)) {
            MaterialDialogManager.showOpenSettingsDialog(this);
        }
    }

    /**
     * Returns the {@link AppComponent}
     *
     * @return Application component dependencies
     */
    private AppComponent getAppComponent() {
        return ((MersApplication) getApplication()).getAppComponent();
    }

    /**
     * Initialize {@link Router}
     *
     * @param savedInstanceState activity
     */
    private void initRouter(Bundle savedInstanceState) {
        router = Conductor.attachRouter(this, container, savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        stopService(serviceIntent);
        super.onDestroy();
    }

    /**
     * Returns the {@link ActivityComponent} for dependency injection
     *
     * @return activity dependency component
     */
    @Override
    public ActivityComponent getComponent() {
        return DaggerActivityComponent.builder()
                .appComponent(getAppComponent())
                .activityModule(new ActivityModule(this))
                .routerModule(new RouterModule(router))
                .build();
    }

    /**
     * Handles back button pressed event
     */
    @Override
    public void onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * Dispatch incoming result to the correct {@link com.bluelinelabs.conductor.Controller}.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        router.getBackstack().get(router.getBackstackSize() - 1).controller().onActivityResult(requestCode, resultCode, data);
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link android.content.pm.PackageManager#PERMISSION_GRANTED}
     *                     or {@link android.content.pm.PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        rxEventBus.send(new LowMemoryEvent());
    }
}