package ph.com.auspex.mers.data.local;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;
import ph.com.auspex.mers.data.DataSourceRepository;
import ph.com.auspex.mers.data.model.request.DirectionsRequest;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.request.FamilyLinkInviteRequest;
import ph.com.auspex.mers.data.model.request.LoginRequest;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.data.model.request.UpdateLocationRequest;
import ph.com.auspex.mers.data.model.request.UpdatePasswordRequest;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.data.model.response.AccessToken;
import ph.com.auspex.mers.data.model.response.Advertisement;
import ph.com.auspex.mers.data.model.response.EmergencyType;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.data.model.response.Feed;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;
import ph.com.auspex.mers.data.model.response.Relationship;
import ph.com.auspex.mers.data.model.response.UserDetail;
import timber.log.Timber;

/**
 * Application's LocalDataSource that uses Realm
 */
public class LocalDataSource implements DataSourceRepository {

    private final RealmController realmController;

    /**
     * Constructor the creates a LocalDataSource instance
     *
     * @param realmController Realm Manager instance
     */
    public LocalDataSource(RealmController realmController) {
        this.realmController = realmController;
    }

    /**
     * Get available family realtionship list from Realm
     *
     * @return {@link List} of {@link Relationship}
     */
    @Override
    public Observable<List<Relationship>> getFamilyRelationships() {
        Realm realm = Realm.getDefaultInstance();
        List<Relationship> result = realmController.getAllFromRealm(realm, Relationship.class);

        Timber.d("List Size: %s", result.size());

        realm.close();

        return Observable.just(result);
    }

    /**
     * Unused method
     */
    @Override
    public Observable<List<Feed>> getNewsFeeds() {
        return null;
    }

    @Override
    public Observable<List<Advertisement>> getAdvertisements() {
        Realm realm = Realm.getDefaultInstance();
        List<Advertisement> result = realmController.getAllFromRealm(realm, Advertisement.class);
        realm.close();
        return Observable.just(result);
    }

    /**
     * Unused method
     */
    @Override
    public Observable<InternalNews> getInternalNews(int currentPage) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<InternalFeed> getSpecificInternalNews(int id) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<AccessToken> login(LoginRequest request) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<String> register(RegisterRequest request) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<String> verifyCode(VerificationRequest request) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<AccessToken> getUserDetails(String accessToken) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<String> logout(String userId) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<AccessToken> updateProfile(UserDetail userDetail) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<ForgotPasswordResponse> forgotPassword(String emailAddress) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<AccessToken> updatePassword(UpdatePasswordRequest request) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<Boolean> updateLocation(UpdateLocationRequest updateLocationRequest) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<List<FamilyLinkDetail>> getFamilyLinks(String userId) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<String> addFamilyLink(FamilyLinkInviteRequest request) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<List<FamilyLinkDetail>> getFamilyInvites(String userId) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<String> acceptInvite(String inviterId, String invitorId) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<String> rejectInvite(String inviterId, String invitorId) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<List<Relationship>> getRelationshipTypes() {
        return getFamilyRelationships();
    }

    /**
     * Unused method
     */
    @Override
    public Observable<List<NearbyPlace>> getNearbyPlacesByType(NearbyPlaceRequest request) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<NearbyPlaceDetail> getNearbyPlaceDetail(String placeId) {
        return null;
    }

    /**
     * Unused method
     */
    @Override
    public Observable<MapDirection> getDirections(DirectionsRequest request) {
        return null;
    }

    /**
     * Get available Emergency Types from Realm
     *
     * @return {@link List} of {@link EmergencyType}
     */
    @Override
    public Observable<List<EmergencyType>> getEmergencyTypes() {
        Realm realm = Realm.getDefaultInstance();
        List<EmergencyType> result = realmController.getAllFromRealm(realm, EmergencyType.class);
        realm.close();
        return Observable.just(result);
    }

    /**
     * Unused method
     */
    @Override
    public Observable<String> reportEmergency(EmergencyRequest request) {
        return null;
    }
}
