package ph.com.auspex.mers.familylink.dialog;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.response.Relationship;

/**
 * Contract for {@link AddFamilyLinkDialogController} and {@link ph.com.auspex.mers.familylink.dialog.impl.AddFamilyPresenterImpl}
 */
public interface AddFamilyContract {

    /**
     * Presenter for add family
     */
    interface AddFamilyPresenter extends MvpPresenter<AddFamilyView> {
        void getRelationships();

        void addFamilyLink(String inviteCode, String relationshipId);
    }

    /**
     * View for add family
     */
    interface AddFamilyView extends RemoteView {
        void prepareRelationshipList(List<Relationship> relationships);

        void onFamilyLinkAdded(String message);
    }
}
