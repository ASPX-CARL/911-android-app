package ph.com.auspex.mers.data;

import ph.com.auspex.mers.data.repositories.DirectoriesRepository;
import ph.com.auspex.mers.data.repositories.EmergencyRepository;
import ph.com.auspex.mers.data.repositories.FamilyLinkRepository;
import ph.com.auspex.mers.data.repositories.LocationRepository;
import ph.com.auspex.mers.data.repositories.NewsRepository;
import ph.com.auspex.mers.data.repositories.SyncRepository;
import ph.com.auspex.mers.data.repositories.UserRepository;

// TODO NOTE: Useless? implement one by one to prevent the datasource class from having unused methods
public interface DataSourceRepository extends
        SyncRepository,
        NewsRepository,
        UserRepository,
        LocationRepository,
        EmergencyRepository,
        FamilyLinkRepository,
        DirectoriesRepository {
}