package ph.com.auspex.mers.news;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.news.internal.InternalNewsContract;
import ph.com.auspex.mers.news.internal.impl.InternalNewsPresenterImpl;
import ph.com.auspex.mers.news.internal.adapter.InternalNewsAdapter;
import ph.com.auspex.mers.news.rss.adapter.NewsRecyclerAdapter;
import ph.com.auspex.mers.news.rss.impl.NewsPresenterImpl;
import ph.com.auspex.mers.news.rss.NewsContract;
import ph.com.auspex.mers.newsdetail.NewsDetailContract;
import ph.com.auspex.mers.newsdetail.NewsDetailPresenterImpl;

@Module
public class NewsModule {

    /**
     * Provide NewsInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return NewsInteractor
     */
    @Provides
    @NewsScope
    NewsInteractor providesNewsInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide NewsPresenter
     *
     * @param model {@link NewsInteractor}
     * @return NewsPresenter
     */
    @Provides
    @NewsScope
    NewsContract.NewsPresenter providesNewsPresenter(NewsInteractor model, RxEventBus rxEventBus) {
        return new NewsPresenterImpl(model, rxEventBus);
    }

    /**
     * Provide Rss News {@link android.support.v7.widget.RecyclerView.Adapter}
     *
     * @return News RecyclerView Adapter instance
     */
    @Provides
    @NewsScope
    NewsRecyclerAdapter providesNewsRecyclerAdapter() {
        return new NewsRecyclerAdapter();
    }

    /**
     * Provide Internal News / Alerts {@link android.support.v7.widget.RecyclerView.Adapter}
     *
     * @return Internal News / Alerts RecyclerView Adapter instance
     */
    @Provides
    @NewsScope
    InternalNewsAdapter providesInternalNewsAdapter() {
        return new InternalNewsAdapter();
    }

    /**
     * Provide NewsDetailPresenter
     *
     * @param interactor {@link NewsInteractor}
     * @return NewsDetailPresenter
     */
    @Provides
    @NewsScope
    InternalNewsContract.InternalNewsPresenter providesIntNewsPresenter(NewsInteractor interactor, RxEventBus rxEventBus) {
        return new InternalNewsPresenterImpl(interactor, rxEventBus);
    }

    /**
     * Provide NewsDetailPresenter
     *
     * @param newsInteractor {@link NewsInteractor}
     * @return NewsDetailPresenter
     */
    @Provides
    @NewsScope
    NewsDetailContract.NewsDetailPresenter providesNewsDetailPresenter(NewsInteractor newsInteractor) {
        return new NewsDetailPresenterImpl(newsInteractor);
    }
}
