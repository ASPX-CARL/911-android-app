package ph.com.auspex.mers.password.changepassword.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.password.changepassword.ChangePasswordContract.ChangePasswordPresenter;
import ph.com.auspex.mers.password.changepassword.ChangePasswordContract.ChangePasswordView;
import ph.com.auspex.mers.password.PasswordInteractor;
import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.UpdatePasswordRequest;

/**
 * Change Password presenter implementation
 */
public class ChangePasswordPresenterImpl extends MvpBasePresenter<ChangePasswordView> implements ChangePasswordPresenter {

    private final PasswordInteractor model;

    public ChangePasswordPresenterImpl(PasswordInteractor model) {
        this.model = model;
    }

    /**
     * Update password API
     *
     * @param userId   UserId that request the update password
     * @param password New password string
     */
    @Override
    public void updatePassword(String userId, String password) {
        getView().showLoading();

        UpdatePasswordRequest request = new UpdatePasswordRequest.Builder()
                .userId(userId)
                .updatedPassword(password)
                .build();

        model.changePassword(request, new BaseInteractor.ResponseListener<String>() {
            @Override
            public void onSuccess(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onPasswordUpdated(message);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
