package ph.com.auspex.mers.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import timber.log.Timber;

public class DateUtil {
    public static String convertGregorianCalendarToString(int year, int monthOfYear, int dayOfMOnth, String outFormat) {
        GregorianCalendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMOnth, 0, 0, 0);

        SimpleDateFormat sdf = new SimpleDateFormat(outFormat, Locale.getDefault());
        sdf.setCalendar(calendar);
        return sdf.format(calendar.getTime());
    }

    public static Date convertStringToDate(String dateString, String inFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(inFormat, Locale.getDefault());

        Date date = null;

        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            Timber.e(e);
        }

        return date;
    }
}
