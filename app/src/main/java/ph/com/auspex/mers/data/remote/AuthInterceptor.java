package ph.com.auspex.mers.data.remote;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ph.com.auspex.mers.BuildConfig;

/**
 * Creates a Retrofit request interceptor
 */
public class AuthInterceptor implements Interceptor {

    /**
     * Appends the API Key on every API requests
     *
     * @param chain current api requests chain
     * @return Updated api request
     * @throws IOException if a system error occurred
     */
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder()
                .addHeader("911-API-KEY", BuildConfig.API_KEY)
                .method(original.method(), original.body());

        Request request = requestBuilder.build();

        return chain.proceed(request);
    }
}
