package ph.com.auspex.mers.profile;

import android.content.res.Resources;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.profile.impl.ProfilePresenterImpl;

@Module
public class ProfileModule {

    /**
     * Provide ProfileInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return ProfileInteractor
     */
    @ProfileScope
    @Provides
    ProfileInteractor providesProfileInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide ProfilePresenter
     *
     * @param model {@link ProfileInteractor}
     * @return ProfilePresenter
     */
    @ProfileScope
    @Provides
    ProfileContract.ProfilePresenter providesProfilePresenter(ProfileInteractor model, Resources resources) {
        return new ProfilePresenterImpl(model, resources);
    }
}