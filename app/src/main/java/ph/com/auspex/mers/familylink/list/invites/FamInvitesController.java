package ph.com.auspex.mers.familylink.list.invites;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.adapter.ItemClickListener;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.RecyclerLoadingView;
import ph.com.auspex.mers.data.model.request.InviteRequest;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.familylink.FamilyLinkModule;
import ph.com.auspex.mers.familylink.list.adapter.FamilyLinkInvitesRecyclerAdapter;
import ph.com.auspex.mers.familylink.list.invites.FamInvitesContract.FamInvitesPresenter;
import ph.com.auspex.mers.familylink.list.invites.FamInvitesContract.FamInvitesView;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.util.AppUtil;

public class FamInvitesController extends MosbyController<FamInvitesView, FamInvitesPresenter> implements RecyclerLoadingView.ListRefreshListener, ItemClickListener<InviteRequest>, FamInvitesView {

    @BindView(R.id.recycler_loading_view) RecyclerLoadingView recyclerLoadingView;

    @BindString(R.string.dialog_reject_family_link_invite) String textDialogReject;

    private FamilyLinkInvitesRecyclerAdapter adapter;

    @Inject UserWrapper userWrapper;
    @Inject FamInvitesPresenter presenter;

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.familyLinkComponent(new FamilyLinkModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        initRecyclerView();
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (userWrapper != null) {
            recyclerLoadingView.setActionButtonText(userWrapper.getUserDetail().getInviteCode());
        }
    }

    private void initRecyclerView() {
        adapter = new FamilyLinkInvitesRecyclerAdapter(this);
        recyclerLoadingView.setAdapter(adapter);
        recyclerLoadingView.setHasFixedSize(true);
        recyclerLoadingView.setActionButtonListener(v -> AppUtil.copyToClipboard(getActivity(), ((Button) v).getText().toString()));
        recyclerLoadingView.setRefreshListener(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_family_link_invites, container, false);
    }

    @Override
    public void onRefresh() {
        presenter.getInvitesList();
    }

    @Override
    protected void initToolbar() {
    }

    @Override
    public void onItemClicked(InviteRequest inviteRequest) {
        if (inviteRequest.isAccepted()) {
            presenter.approveRequest(inviteRequest);
        } else {
            showRejectDialog(inviteRequest);
        }
    }

    @Override
    public void showLoading() {
        recyclerLoadingView.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        recyclerLoadingView.setRefreshing(false);
    }

    private void showRejectDialog(InviteRequest inviteRequest) {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_reject_family_link_invite)
                .content(String.format(textDialogReject, inviteRequest.getInviterName()))
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_cancel)
                .onPositive((dialog, which) -> presenter.rejectRequest(inviteRequest))
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.colorPrimaryDark)
                .negativeColorRes(R.color.app_gray)
                .show();
    }

    @Override
    public void onRequestAccepted() {

    }

    @Override
    public void onRequestRejected(int position) {
        adapter.removeItem(position);
    }

    @Override
    public void displayInvitesList(List<FamilyLinkDetail> familyLinkDetails) {
        if (familyLinkDetails.size() > 0) {
            adapter.updateList(familyLinkDetails);
            recyclerLoadingView.showList();
        } else {
            recyclerLoadingView.showEmptyView();
        }
    }

    @NonNull
    @Override
    public FamInvitesPresenter createPresenter() {
        return presenter;
    }
}
