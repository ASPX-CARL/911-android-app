package ph.com.auspex.mers.pushnotification;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import ph.com.auspex.mers.data.model.NewsPushNotification;
import ph.com.auspex.mers.main.MainActivity;

/**
 * Push notification open handler
 */
public class OpenHandler implements OneSignal.NotificationOpenedHandler {

    private final Context context;

    public OpenHandler(Context context) {
        this.context = context;
    }

    /**
     * If the push notification is opened, parse the JSON data and open the corresponding {@link ph.com.auspex.mers.data.model.response.InternalNews}
     *
     * @param result
     */
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        String jsonString = result.notification.payload.additionalData.toString();

        if (!TextUtils.isEmpty(jsonString)) {
            NewsPushNotification pushNotification = new Gson().fromJson(jsonString, NewsPushNotification.class);

            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(MainActivity.NEWS_KEY, pushNotification.getUrl());
            context.startActivity(intent);
        }
    }
}
