package ph.com.auspex.mers.data.model.deserializer;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ph.com.auspex.mers.data.model.response.MapDirection;

/**
 * @deprecated This feature is not used in the app anymore
 * Google Directions API Json Parser
 */
@Deprecated
public class DirectionsDeserializer implements JsonDeserializer<MapDirection> {

    @Override
    public MapDirection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject result = json.getAsJsonObject();
        JsonArray jsonArrayRoutes = result.get("routes").getAsJsonArray();

        JsonObject jsonObjectRoute = jsonArrayRoutes.get(0).getAsJsonObject();

        JsonObject jsonObjectPolyline = jsonObjectRoute.get("overview_polyline").getAsJsonObject();

        String points = jsonObjectPolyline.get("points").getAsString();

        return new MapDirection(points);
    }
}