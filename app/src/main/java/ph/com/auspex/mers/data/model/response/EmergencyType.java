package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ph.com.auspex.mers.data.remote.ApiService;

/**
 * Provide a POJO for {@link ApiService#getEmergencyTypes()}
 */
public class EmergencyType extends RealmObject {

    @PrimaryKey
    @SerializedName("emergency_id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("flash_enabled")
    private int isFlashEnabled = 1;

    /**
     * Returns the emergency id
     *
     * @return emergency id
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the emergency name
     *
     * @return emergency name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the icon's image url
     *
     * @return image url
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Return if this category should have the flash enabled on the internal camera
     *
     * @return <code>false</code> if the flash of the camera should be disabled; <code>false</code> otherwise
     */
    public boolean isFlashEnabled() {
        return isFlashEnabled == 1;
    }
}
