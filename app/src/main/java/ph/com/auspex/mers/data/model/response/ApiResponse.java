package ph.com.auspex.mers.data.model.response;

/**
 * Wrapper POJO for all Api Requests
 *
 * @param <T> Generic POJO
 */
public class ApiResponse<T> {

    private boolean status;
    private String code;
    private String message;
    private T data;

    /**
     * Return status of the api requests
     *
     * @return <code>true</code> if API requests is successful
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Return the status code of the api response
     *
     * @return status code of the api response
     */
    public String getCode() {
        return code;
    }

    /**
     * Return that status message of the api response
     *
     * @return status message of the api response
     */
    public String getMessage() {
        return message;
    }

    /**
     * Return the wrapped POJO for the API Response
     *
     * @return POJO of the api response
     */
    public T getData() {
        return data;
    }
}
