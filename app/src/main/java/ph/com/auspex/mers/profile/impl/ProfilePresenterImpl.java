package ph.com.auspex.mers.profile.impl;

import android.content.res.Resources;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.UserDetail;
import ph.com.auspex.mers.profile.ProfileContract;
import ph.com.auspex.mers.profile.ProfileInteractor;

/**
 * Profile Presenter Implementation
 */
public class ProfilePresenterImpl extends MvpBasePresenter<ProfileContract.ProfileView> implements ProfileContract.ProfilePresenter {

    private final ProfileInteractor model;
    private final Resources resources;

    public ProfilePresenterImpl(ProfileInteractor model, Resources resources) {
        this.model = model;
        this.resources = resources;
    }

    /**
     * Get logged in account's {@link UserDetail}
     */
    @Override
    public void getUserDetails() {
        getView().showLoading();

        model.getUserDetails(new ProfileInteractor.ResponseListener() {
            @Override
            public void onSuccess(UserDetail userDetail) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().displayUserDetails(userDetail);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }

            @Override
            public void onUserNotFound() {
                logout();
            }
        });
    }

    /**
     * Logout the current account
     */
    @Override
    public void logout() {
        getView().showLoading();

        model.logout(new BaseInteractor.EmptyResponseListener() {
            @Override
            public void onSuccess() {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onLogoutSuccess();
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }

    /**
     * Observe profile object updates
     *
     * @param originalUserDetail Initial {@link UserDetail} object
     * @param newUserDetails     Updated {@link UserDetail} object
     */
    @Override
    public void checkForProfileUpdates(UserDetail originalUserDetail, UserDetail newUserDetails) {
        Observable.just(originalUserDetail.equals(newUserDetails))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isUserDetailsSame -> {
                    if (isUserDetailsSame) {
                        if (isViewAttached()) {
                            getView().profileReverted();
                        }
                    } else {
                        if (isViewAttached()) {
                            getView().profileChanged();
                        }
                    }
                });
    }

    /**
     * Update {@link UserDetail} in the API
     *
     * @param userDetail Updated UserDetail object
     */
    @Override
    public void updateProfile(UserDetail userDetail) {
        getView().showLoading();

        model.updateUserDetails(userDetail, new BaseInteractor.ResponseListener<UserDetail>() {
            @Override
            public void onSuccess(UserDetail userDetail) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().resetData();
                    getView().onUpdateSuccessful();
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
