package ph.com.auspex.mers.profile;

import dagger.Subcomponent;

@ProfileScope
@Subcomponent(modules = ProfileModule.class)
public interface ProfileComponent {
    void inject(ProfileController profileController);
}
