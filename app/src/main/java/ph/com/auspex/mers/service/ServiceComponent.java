package ph.com.auspex.mers.service;

import dagger.Component;
import ph.com.auspex.mers.AppComponent;

@ServiceScope
@Component(
        dependencies = AppComponent.class,
        modules = ServiceModule.class
)
public interface ServiceComponent {
    void inject(LocationUpdateService locationUpdateService);
}
