package ph.com.auspex.mers.directories;

import dagger.Subcomponent;
import ph.com.auspex.mers.directories.detail.DirectoryDetailController;

@DirectoryScope
@Subcomponent(
        modules = DirectoryModule.class
)
public interface DirectoryComponent {
    void inject(DirectoryController directoryController);

    void inject(DirectoryDetailController directoryDetailController);
}
