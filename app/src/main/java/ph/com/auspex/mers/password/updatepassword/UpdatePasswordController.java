package ph.com.auspex.mers.password.updatepassword;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.AdvancedTextInputEditText;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.password.PasswordModule;
import ph.com.auspex.mers.password.updatepassword.UpdatePasswordContract.UpdatePasswordPresenter;
import ph.com.auspex.mers.password.updatepassword.UpdatePasswordContract.UpdatePasswordView;
import ph.com.auspex.mers.util.FormValidationUtil;

/**
 * Update password controller UI
 */
public class UpdatePasswordController extends MosbyController<UpdatePasswordView, UpdatePasswordPresenter> implements UpdatePasswordView {

    @BindView(R.id.input_current_password) AdvancedTextInputEditText inputCurrentPassword;
    @BindView(R.id.input_new_password) AdvancedTextInputEditText inputNewPassword;
    @BindView(R.id.input_confirm_password) AdvancedTextInputEditText inputConfirmPassword;

    @BindViews({R.id.input_current_password, R.id.input_new_password, R.id.input_confirm_password})
    AdvancedTextInputEditText[] inputEditTexts;

    @BindString(R.string.label_change_password) String title;

    @Inject UpdatePasswordPresenter presenter;
    @Inject FormValidationUtil validationUtil;
    @Inject Router parentRouter;

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.passwordComponent(new PasswordModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_update_password, container, false);
    }

    @NonNull
    @Override
    public UpdatePasswordPresenter createPresenter() {
        return presenter;
    }

    /**
     * On Password successfully updated
     *
     * @param message Display message
     */
    @Override
    public void onPasswordUpdated(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

        for (TextInputEditText editText : inputEditTexts) {
            editText.setText("");
            editText.clearFocus();
        }
    }

    /**
     * ON update password clicked, send the form information to the presenter
     */
    @OnClick(R.id.button_submit)
    void onUpdatePasswordClicked() {
        if (!validationUtil.hasEmptyField(inputCurrentPassword, inputNewPassword, inputCurrentPassword)
                && !validationUtil.hasError(inputCurrentPassword, inputNewPassword, inputConfirmPassword)
                && validationUtil.isPasswordSame(inputNewPassword, inputConfirmPassword)
                && !validationUtil.validateCurrentAndUpdatedPassword(inputCurrentPassword, inputNewPassword)) {
            presenter.updatePassword(inputCurrentPassword.getText().toString(), inputNewPassword.getText().toString());
        }
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_register;
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                handleBack();
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    @Override
    public boolean handleBack() {
        if (inputCurrentPassword.getText().toString().length() > 0 || inputNewPassword.getText().toString().length() > 0 || inputConfirmPassword.getText().toString().length() > 0) {
            new MaterialDialog.Builder(activity)
                    .title(R.string.dialog_title_cancel_change_password)
                    .content(R.string.dialog_negative_change_password)
                    .positiveText(R.string.dialog_button_yes)
                    .negativeText(R.string.dialog_button_no)
                    .onPositive((dialog, which) -> openMainNavigationController())
                    .onNegative((dialog, which) -> dialog.dismiss())
                    .positiveColorRes(R.color.app_blue)
                    .negativeColorRes(R.color.app_gray)
                    .show();
            return true;
        } else {
            openMainNavigationController();
            return true;
        }
    }

    /**
     * Open Main Navigation Controller
     */
    private void openMainNavigationController() {
        parentRouter.setRoot(RouterTransaction.with(new NavigationController()));
    }
}
