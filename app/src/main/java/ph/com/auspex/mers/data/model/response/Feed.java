package ph.com.auspex.mers.data.model.response;

import ph.com.auspex.mers.data.remote.ApiService;

/**
 * Provide a POJO for RSS news feed {@link ApiService#getNewsList()}
 */
public class Feed {

    private String title;
    private String description;
    private String link;
    private String thumbnailUrl;

    /**
     * Returns news title
     *
     * @return news title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns news description
     *
     * @return news description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns news url
     *
     * @return new url
     */
    public String getLink() {
        return link;
    }

    /**
     * Returns news thumbnail url
     *
     * @return thumbnail url
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    private Feed(Builder builder) {
        this.title = builder.title;
        this.description = builder.description;
        this.link = builder.link;
        this.thumbnailUrl = builder.thumbnailUrl;
    }

    public static class Builder {
        private String title;
        private String description;
        private String link;
        private String thumbnailUrl;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder link(String link) {
            this.link = link;
            return this;
        }

        public Builder thumbnailUrl(String thumbnailUrl) {
            this.thumbnailUrl = thumbnailUrl;
            return this;
        }

        public Feed build() {
            return new Feed(this);
        }
    }
}
