package ph.com.auspex.mers.password.forgotpassword;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;

public interface ForgotPasswordContract {

    interface ForgotPasswordPresenter extends MvpPresenter<ForgotPasswordView> {
        void forgotPassword(String emailAddress);
    }

    interface ForgotPasswordView extends RemoteView {
        void onSuccessful(ForgotPasswordResponse forgotPasswordResponse);
    }
}
