package ph.com.auspex.mers.news;

import dagger.Subcomponent;
import ph.com.auspex.mers.news.internal.InternalNewsController;
import ph.com.auspex.mers.news.rss.NewsController;
import ph.com.auspex.mers.newsdetail.NewsDetailController;

@NewsScope
@Subcomponent(modules = NewsModule.class)
public interface NewsComponent {
    void inject(NewsController newsController);

    void inject(InternalNewsController intNewsController);

    void inject(NewsDetailController newsDetailController);
}