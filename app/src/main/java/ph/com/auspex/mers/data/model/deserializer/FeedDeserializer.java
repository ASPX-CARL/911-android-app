package ph.com.auspex.mers.data.model.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ph.com.auspex.mers.data.model.response.Feed;
import ph.com.auspex.mers.data.model.response.ThumbnailItem;
import ph.com.auspex.mers.data.model.response.Thumbnails;
import timber.log.Timber;

/**
 * RSS Feed Json Deserializer
 */
public class FeedDeserializer implements JsonDeserializer<Feed> {

    @Override
    public Feed deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Feed.Builder builder = new Feed.Builder();

        JsonObject jsonObjectFeed = json.getAsJsonObject();

        if (jsonObjectFeed.has("title")) {
            builder.title(jsonObjectFeed.get("title").getAsString());
        }

        if (jsonObjectFeed.has("link")) {
            builder.link(jsonObjectFeed.get("link").getAsString());
        }

        if (jsonObjectFeed.has("description") && !jsonObjectFeed.get("description").isJsonObject()) {
            builder.description(jsonObjectFeed.get("description").getAsString());
        }

        try {
            if (jsonObjectFeed.has("thumbnails")) {
                Thumbnails thumbnails = new Gson().fromJson(jsonObjectFeed.get("thumbnails").getAsJsonObject(), Thumbnails.class);
                builder.thumbnailUrl(thumbnails.getThumbnails().get(0).getAttributes().getUrl());
            } else if (jsonObjectFeed.has("thumbnail")) {
                ThumbnailItem thumbnailItem = new Gson().fromJson(jsonObjectFeed.get("thumbnail").getAsJsonObject(), ThumbnailItem.class);
                builder.thumbnailUrl(thumbnailItem.getAttributes().getUrl());
            }
        } catch (Exception e) {
            Timber.e(e);
        }

        return builder.build();
    }
}
