package ph.com.auspex.mers.news.rss.impl;

import android.util.SparseArray;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.ListEvent;
import ph.com.auspex.mers.news.NewsInteractor;
import ph.com.auspex.mers.news.rss.NewsContract;

/**
 * News Presenter Implementation
 */
public class NewsPresenterImpl extends MvpBasePresenter<NewsContract.NewsView> implements NewsContract.NewsPresenter {

    private final NewsInteractor model;
    private final RxEventBus rxEventBus;

    private SparseArray<Object> oldData;

    public NewsPresenterImpl(NewsInteractor model, RxEventBus rxEventBus) {
        this.model = model;
        this.rxEventBus = rxEventBus;
    }

    /**
     * Upon attaching view to the presenter, check if there is an existing {@link #oldData} and display it on the view
     *
     * @param view {@link ph.com.auspex.mers.news.rss.NewsContract.NewsView} reference
     */
    @Override
    public void attachView(NewsContract.NewsView view) {
        super.attachView(view);

        if (oldData == null && isViewAttached()) {
            getRssNews();
        }

        rxEventBus.toObservable().subscribe(o -> {
            if (o instanceof ListEvent.ScrollToTopEvent) {
                if (isViewAttached()) {
                    getView().scrollListToTop();
                }
            }
        });
    }

    /**
     * Get RSS news from API
     */
    @Override
    public void getRssNews() {
        if (isViewAttached()) {
            getView().showLoading();
        }

        model.getRssNews(new BaseInteractor.ResponseListener<SparseArray<Object>>() {
            @Override
            public void onSuccess(SparseArray<Object> data) {
                if (isViewAttached()) {
                    oldData = data;

                    getView().hideLoading();
                    getView().setData(data);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
