package ph.com.auspex.mers.profile;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.response.UserDetail;

public interface ProfileContract {

    interface ProfilePresenter extends MvpPresenter<ProfileView> {
        void getUserDetails();

        void logout();

        // void checkPermissions();

        void checkForProfileUpdates(UserDetail originalUserDetail, UserDetail newUserDetails);

        void updateProfile(UserDetail userDetail);
    }

    interface ProfileView extends RemoteView {
        void displayUserDetails(UserDetail userDetail);

        void onLogoutSuccess();

        void openImagePicker();

        void profileChanged();

        void profileReverted();

        void resetData();

        void onUpdateSuccessful();
    }
}
