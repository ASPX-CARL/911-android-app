package ph.com.auspex.mers.emergency;

import android.content.res.Resources;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.response.EmergencyType;
import ph.com.auspex.mers.emergency.EmergencyContract.EmergencyPresenter;
import ph.com.auspex.mers.emergency.EmergencyContract.EmergencyView;

/**
 * Provide implementation for {@link EmergencyPresenter}
 */
public class EmergencyPresenterImpl extends MvpBasePresenter<EmergencyView> implements EmergencyPresenter {

    private final EmergencyInteractor interactor;
    private final Resources resources;
    private final boolean isLoggedIn;

    private List<EmergencyType> emergencyTypes;

    /**
     * Constructs {@link EmergencyPresenterImpl} using the following parameters
     *
     * @param interactor  {@link EmergencyInteractor}
     * @param resources   {@link Resources}
     * @param userWrapper {@link UserWrapper}
     */
    public EmergencyPresenterImpl(EmergencyInteractor interactor, Resources resources, UserWrapper userWrapper) {
        this.interactor = interactor;
        this.resources = resources;
        this.isLoggedIn = userWrapper.isLoggedIn();
    }

    /**
     * Get emergency types from API upon attach controller or show login button when there is no logged in user
     *
     * @param view {@link EmergencyController}
     */
    @Override
    public void attachView(EmergencyView view) {
        super.attachView(view);

        if (isLoggedIn) {
            if (emergencyTypes == null) {
                getEmergencyTypes();
            }
        } else {
            if (isViewAttached()) {
                getView().setupNotLoggedIn();
            }
        }
    }

    /**
     * Sync emergency types from API
     */
    @Override
    public void getEmergencyTypes() {
        interactor.getEmergencyTypes(new BaseInteractor.ResponseListener<List<EmergencyType>>() {
            @Override
            public void onSuccess(List<EmergencyType> emergencyTypes) {
                if (emergencyTypes.size() != 0) {
                    setEmergencyTypes(emergencyTypes);
                    getView().displayEmergencyTypes(emergencyTypes);
                } else {
                    getView().displayEmptyLayout();
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().showError(message);
                    getView().displayEmptyLayout();
                }
            }
        });
    }

    /**
     * Report emergency using data in form
     *
     * @param request emergency request
     */
    @Override
    public void report(EmergencyRequest request) {
        interactor.reportEmergency(request, new BaseInteractor.ResponseListener<String>() {
            @Override
            public void onSuccess(String s) {
                if (isViewAttached()) {
                    getView().onRequestSuccess();
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().showError(message);
                }
            }
        });
    }

    /**
     * Sets {@link #emergencyTypes} value
     *
     * @param emergencyTypes emergency types
     */
    private void setEmergencyTypes(List<EmergencyType> emergencyTypes) {
        this.emergencyTypes = emergencyTypes;
    }
}
