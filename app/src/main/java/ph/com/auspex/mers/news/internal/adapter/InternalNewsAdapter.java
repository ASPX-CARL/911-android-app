package ph.com.auspex.mers.news.internal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.adapter.ItemViewClickListener;
import ph.com.auspex.mers.data.model.response.Advertisement;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;
import ph.com.auspex.mers.util.GlideImageLoader;

public class InternalNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_NEWS = 0;
    private final int VIEW_TYPE_ADS = 1;
    private final int VIEW_TYPE_LOADING = 2;

    private InternalNews internalNews;

    private ItemViewClickListener<InternalFeed> listener = (internalFeed, view) -> {

    };

    private boolean isLoading = false;

    public InternalNewsAdapter() {
        this.internalNews = new InternalNews();
    }

    public void updateList(InternalNews internalNews) {
        // check if previous internal news object has empty feeds list
        if (this.internalNews.getFeeds() == null) {
            this.internalNews = internalNews;
            notifyDataSetChanged();
        } else {
            notifyItemRemoved(this.internalNews.getMergedFeedList().size() - 1);
            this.internalNews.getMergedFeedList().remove(this.internalNews.getMergedFeedList().size() - 1);

            this.internalNews = internalNews;
            notifyDataSetChanged();

            setLoaded();
        }
    }

    public void setListener(ItemViewClickListener<InternalFeed> listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_NEWS) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_news, parent, false);
            return new InternalNewsVH(view, listener, parent.getContext());
        } else if (viewType == VIEW_TYPE_ADS) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_ads, parent, false);
            return new AdsViewHolder(view, parent.getContext());
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading, parent, false);
            return new LoadingViewHolder(view);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_ADS:
                AdsViewHolder viewHolderAds = (AdsViewHolder) holder;
                viewHolderAds.bind((Advertisement) internalNews.getMergedFeedList().get(position));
                break;
            case VIEW_TYPE_NEWS:
                InternalNewsVH viewHolderNews = (InternalNewsVH) holder;
                viewHolderNews.bind((InternalFeed) internalNews.getMergedFeedList().get(position));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return internalNews.getMergedFeedList() != null ? internalNews.getMergedFeedList().size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return getViewType(internalNews.getMergedFeedList().get(position));
    }

    private int getViewType(Object object) {
        if (object == null) {
            return VIEW_TYPE_LOADING;
        } else if (object instanceof InternalFeed) {
            return VIEW_TYPE_NEWS;
        } else {
            return VIEW_TYPE_ADS;
        }
    }

    public static class AdsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ads_thumbnail) ImageView ivAdsPlaceholder;

        private Context context;

        public AdsViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;

            ButterKnife.bind(this, itemView);
        }

        public void bind(Advertisement advertisement) {
            GlideImageLoader.loadGif(context, ivAdsPlaceholder, advertisement.getImageUrl());
        }
    }

    public static class InternalNewsVH extends RecyclerView.ViewHolder {

        @BindView(R.id.parent) View parentView;
        @BindView(R.id.news_title) TextView tvTitle;
        @BindView(R.id.news_content) TextView tvContent;
        @BindView(R.id.thumbnail) ImageView thumbnail;

        private InternalFeed internalFeed;
        private ItemViewClickListener<InternalFeed> listener;
        private Context context;

        public InternalNewsVH(View itemView, ItemViewClickListener<InternalFeed> listener, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            this.listener = listener;
            this.context = context;
        }

        public void bind(InternalFeed internalFeed) {
            this.internalFeed = internalFeed;

            tvTitle.setText(internalFeed.getTitle());

            if (!TextUtils.isEmpty(internalFeed.getContent())) {
                tvContent.setText(stripHtml(internalFeed.getContent()));
            }

            GlideImageLoader.loadUrl(context, thumbnail, internalFeed.getThumbnailUrl(), R.drawable.news_place_holder, R.drawable.news_place_holder, false);
        }

        @OnClick(R.id.parent)
        void onParentClicked() {
            listener.onItemClicked(internalFeed, parentView);
        }

        private String stripHtml(String html) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                return String.valueOf(Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY));
            } else {
                return String.valueOf(Html.fromHtml(html));
            }
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void setLoading() {
        isLoading = true;

        int lastItemPosition = internalNews.getMergedFeedList().size() - 1;

        internalNews.getMergedFeedList().put(lastItemPosition, null);
        notifyItemInserted(lastItemPosition);
    }

    void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }
}
