package ph.com.auspex.mers.register.optional;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.register.RegisterContract.RegisterPresenter;
import ph.com.auspex.mers.register.RegisterContract.RegisterView;
import ph.com.auspex.mers.register.RegisterModule;
import ph.com.auspex.mers.verification.VerificationController;

/**
 * Optional screen from the registration process
 */
public class MedicalInformationController extends MosbyController<RegisterView, RegisterPresenter> implements RegisterView {

    public static final String TAG = MedicalInformationController.class.getSimpleName();
    public static final String KEY_REGISTRATION_REQUEST = TAG + ".request";

    @BindView(R.id.input_medical_information) EditText inputMedicalInformation;

    @BindString(R.string.label_medical_information) String title;

    @Inject Router parentRouter;
    @Inject RegisterPresenter presenter;

    private RegisterRequest registerRequest;

    public MedicalInformationController(@Nullable Bundle args) {
        super(args);

        if (args != null) {
            registerRequest = args.getParcelable(KEY_REGISTRATION_REQUEST);
        }
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (presenter != null) {
            presenter.attachView(this);
        }

        initToolbar();
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    @NonNull
    @Override
    public RegisterPresenter createPresenter() {
        return presenter;
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.registerComponent(new RegisterModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_medical_information, container, false);
    }

    /**
     * Submit additional medical information to the registration form data
     */
    @OnClick(R.id.button_finish)
    void onFinishClicked() {
        RegisterRequest newRequest = registerRequest.newBuilder()
                .medicalInformation(inputMedicalInformation.getText().toString())
                .build();

        presenter.registerAccount(newRequest);
    }

    /**
     * If Registration is successful open {@link VerificationController}
     *
     * @param userId user id of the registered account
     */
    @Override
    public void onRegisterSuccessful(String userId) {
        Bundle args = new Bundle();
        args.putString(VerificationController.KEY_USER_ID, userId);
        args.putSerializable(VerificationController.KEY_VERIFICATION_TYPE, VerificationRequest.VerificationType.REGISTRATION);

        parentRouter.pushController(RouterTransaction.with(new VerificationController(args)));
    }

    /**
     * Open End User Agreement when checkbox text is clicked
     */
    @Override
    public void onRegisterFailed(String message) {
        showToast(message);
    }
}