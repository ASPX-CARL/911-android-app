package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;

import ph.com.auspex.mers.R;

public class CustomRadioButton extends AppCompatRadioButton {

    /**
     * Create a view instance from xml
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Initialize view
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomRadioButton, 0, 0);

        Drawable drawable = null;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            drawable = a.getDrawable(R.styleable.CustomRadioButton_crb_drawable);
        } else {
            final int drawableId = a.getResourceId(R.styleable.CustomRadioButton_crb_drawable, -1);

            if (drawableId != -1) {
                drawable = AppCompatResources.getDrawable(context, drawableId);
            }
        }

        setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        a.recycle();
    }
}
