package ph.com.auspex.mers;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.ads.AdRequest;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.sharedpref.AppPreferences;
import ph.com.auspex.mers.pushnotification.OpenHandler;
import ph.com.auspex.mers.receiver.GpsStatusReceiver;
import ph.com.auspex.mers.util.FormValidationUtil;

@Module
public class AppModule {

    private final Application application;
    private final Context context;

    /**
     * Constructor that creates AppModule
     *
     * @param application {@link MersApplication} instance
     */
    public AppModule(Application application) {
        this.application = application;
        this.context = application;
    }

    /**
     * Provide Application context
     *
     * @return application context
     */
    @Singleton
    @Provides
    Context providesContext() {
        return context;
    }

    /**
     * Provide Application instance
     *
     * @return application instance
     */
    @Singleton
    @Provides
    Application providesApplication() {
        return application;
    }

    /**
     * Provide Phone number verification library instance
     *
     * @return Phone number verification utility
     */
    @Provides
    @Singleton
    PhoneNumberUtil providesPhoneNumberUtil() {
        return PhoneNumberUtil.getInstance();
    }

    /**
     * Provide InputMethodManager a.k.a keyboard instance
     *
     * @param context application context
     * @return InputMethodManager instance
     */
    @Provides
    @Singleton
    InputMethodManager providesInputMethodManager(Context context) {
        return (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    /**
     * Provide Application resource object
     *
     * @param context application context
     * @return Application resource instance
     */
    @Provides
    @Singleton
    Resources providesApplicationResources(Context context) {
        return context.getResources();
    }

    /**
     * Provide FormValidationUtil object
     *
     * @param resources          {@link Resources} instance
     * @param inputMethodManager {@link InputMethodManager} instance
     * @param phoneNumberUtil    {@link PhoneNumberUtil} instance
     * @return FormValidationUtil instance
     */
    @Provides
    @Singleton
    FormValidationUtil providesFormValidationUtil(Resources resources, InputMethodManager inputMethodManager, PhoneNumberUtil phoneNumberUtil) {
        return new FormValidationUtil(resources, inputMethodManager, phoneNumberUtil);
    }

    /**
     * Provide {@link UserWrapper} object
     *
     * @param appPreferences {@link AppPreferences} {@link android.content.SharedPreferences} helper library
     * @return {@link UserWrapper} Singleton object
     */
    @Provides
    @Singleton
    UserWrapper providesUserWrapper(AppPreferences appPreferences) {
        if (appPreferences != null && appPreferences.userDetails() != null) {
            return new UserWrapper(appPreferences.userDetails());
        }

        return new UserWrapper();
    }

    /**
     * Provide GpsStatusReceiver object used for listening GPS settings change event
     *
     * @param rxEventBus {@link RxEventBus} instance
     * @return GpsStatusReceiver listener
     */
    @Provides
    @Singleton
    GpsStatusReceiver providesGpsStatusReceiver(RxEventBus rxEventBus) {
        return new GpsStatusReceiver(rxEventBus);
    }

    /*
      Provide location update interval for {@link ph.com.auspex.mers.service.LocationUpdateService}

      @param application {@link Application} instance
     * @return update interval value
     */
    /*
    @Singleton
    @Provides
    @Named("updateInterval")
    long providesUpdateLocationInterval(Application application) {
        if (BuildConfig.DEBUG) {
            return application.getResources().getInteger(R.integer.debug_location_update_interval);
        }

        return application.getResources().getInteger(R.integer.location_update_interval);
    }*/

    /**
     * One-time true boolean
     *
     * @return
     */
    @Provides
    @Singleton
    @Named("badge")
    AtomicBoolean providesShouldShowBadge() {
        return new AtomicBoolean(true);
    }

    /**
     * Provide Push Notification {@link OpenHandler}, which handles the event when the user opened the received push notification.
     *
     * @param context Application context
     * @return Open handler instance
     */
    @Provides
    @Singleton
    OpenHandler providesPushNotificationOpenHandler(Context context) {
        return new OpenHandler(context);
    }

    /**
     * Provide Google AdMob Ad Request
     *
     * @return Google AdMob AdRequest
     */
    @Provides
    @Singleton
    AdRequest provideAdRequest() {
        AdRequest.Builder builder = new AdRequest.Builder();

        if (BuildConfig.DEBUG) {
            builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            builder.addTestDevice("F3DC671D50125B2C7EE6F590197FE0CC");
            builder.addTestDevice("8E7FC9EBE7677F316D4440399CB54724");
        }

        return builder.build();
    }
}