package ph.com.auspex.mers.news.rss;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import javax.inject.Inject;

import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.adapter.ItemViewClickListener;
import ph.com.auspex.mers.common.changehandler.CircularRevealChangeHandlerCompat;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.util.MaterialDialogManager;
import ph.com.auspex.mers.common.views.RecyclerLoadingView;
import ph.com.auspex.mers.data.model.response.Feed;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.news.NewsModule;
import ph.com.auspex.mers.news.rss.adapter.NewsRecyclerAdapter;
import ph.com.auspex.mers.newsdetail.NewsDetailController;
import timber.log.Timber;

/**
 * RSS News Controller UI
 */
public class NewsController extends MosbyController<NewsContract.NewsView, NewsContract.NewsPresenter> implements NewsContract.NewsView, RecyclerLoadingView.ListRefreshListener, ItemViewClickListener<Feed> {

    public static final String TAG = NewsController.class.getSimpleName();
    public static final String KEY_NEWS_TYPE = TAG + ".newsType";

    @BindView(R.id.recycler_loading_view) RecyclerLoadingView recyclerView;
    @BindView(R.id.adView) AdView adView;

    @Inject NewsContract.NewsPresenter presenter;
    @Inject NewsRecyclerAdapter adapter;
    @Inject Router parentRouter;
    @Inject AdRequest adRequest;

    /**
     * Show {@link android.support.v4.widget.SwipeRefreshLayout} refreshing animation
     */
    @Override
    public void showLoading() {
        recyclerView.setRefreshing(true);
    }

    /**
     * Dismiss {@link android.support.v4.widget.SwipeRefreshLayout} refreshing animation
     */
    @Override
    public void hideLoading() {
        recyclerView.setRefreshing(false);
    }

    /**
     * Setup and inject ActivityComponent dependencies
     *
     * @param activityComponent {@link ActivityComponent} instance
     */
    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.newsComponent(new NewsModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        adapter.setListener(this);
        initRecyclerView();

        adView.loadAd(adRequest);

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Timber.e("onAdFailedToLoad: %s", errorCode);
            }
        });
    }

    /**
     * Initialize {@link android.support.v7.widget.RecyclerView} initialization
     */
    private void initRecyclerView() {
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setRefreshListener(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_news, container, false);
    }

    @NonNull
    @Override
    public NewsContract.NewsPresenter createPresenter() {
        return presenter;
    }

    /**
     * Set the {@link SparseArray} to {@link NewsRecyclerAdapter}
     *
     * @param data Collection of generic object which contains {@link Feed} and {@link ph.com.auspex.mers.data.model.response.Advertisement}
     */
    @Override
    public void setData(SparseArray<Object> data) {
        if (data.size() > 0) {
            adapter.updateList(data);
            recyclerView.showList();
        }
    }

    /**
     * Scroll the {@link android.support.v7.widget.RecyclerView} list to the first item
     */
    @Override
    public void scrollListToTop() {
        recyclerView.scrollToTop();
    }

    /**
     * {@link android.support.v4.widget.SwipeRefreshLayout} refresh listener
     */
    @Override
    public void onRefresh() {
        presenter.getRssNews();
    }

    /**
     * Initialize toolbar with nothing
     */
    @Override
    protected void initToolbar() {
    }

    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCloseApplicationDialog(activity);
        return true;
    }

    /**
     * On {@link android.support.v7.widget.RecyclerView} item click, open {@link NewsDetailController}
     *
     * @param feed RSS Feed object
     * @param view {@link android.support.v7.widget.RecyclerView} item view
     */
    @Override
    public void onItemClicked(Feed feed, View view) {
        setRetainViewMode(RetainViewMode.RETAIN_DETACH);
        Bundle args = new Bundle();
        args.putString(NewsDetailController.KEY_URL, feed.getLink());
        parentRouter.pushController(RouterTransaction.with(new NewsDetailController(args))
                .pushChangeHandler(new CircularRevealChangeHandlerCompat(view, recyclerView, -2))
                .popChangeHandler(new CircularRevealChangeHandlerCompat(view, recyclerView, -2)));
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (adRequest != null && adView != null) {
            adView.loadAd(adRequest);
        }
    }
}
