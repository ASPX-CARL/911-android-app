package ph.com.auspex.mers.common.mvp.interactor;

public interface BaseInteractor {

    /**
     * Response Listener from an Interactor implementation
     *
     * @param <T> generic object
     */
    interface ResponseListener<T> {
        void onSuccess(T t);

        void onFailure(String message);
    }

    /**
     * Response listener from an Interactor implementation without a return object
     */
    interface EmptyResponseListener {
        void onSuccess();

        void onFailure(String message);
    }

    /**
     * Cancel an ongoing network request
     */
    void cancelNetworkRequest();
}
