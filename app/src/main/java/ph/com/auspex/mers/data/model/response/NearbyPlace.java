package ph.com.auspex.mers.data.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Provide a POJO for {@link ph.com.auspex.mers.data.remote.ApiService#getNearbyPlaces(HashMap)}
 */
public class NearbyPlace implements Parcelable {

    private String name;
    private String placeId;
    private String address;
    private LatLng coordinates;
    private NearbyPlaceDetail placeDetail;

    private NearbyPlace(Builder builder) {
        this.name = builder.name;
        this.placeId = builder.placeId;
        this.address = builder.address;
        this.coordinates = new LatLng(builder.latitude, builder.longitude);
        this.placeDetail = builder.nearbyPlaceDetail;
    }

    protected NearbyPlace(Parcel in) {
        name = in.readString();
        placeId = in.readString();
        address = in.readString();
        coordinates = in.readParcelable(LatLng.class.getClassLoader());
        placeDetail = in.readParcelable(NearbyPlaceDetail.class.getClassLoader());
    }

    public static final Creator<NearbyPlace> CREATOR = new Creator<NearbyPlace>() {
        @Override
        public NearbyPlace createFromParcel(Parcel in) {
            return new NearbyPlace(in);
        }

        @Override
        public NearbyPlace[] newArray(int size) {
            return new NearbyPlace[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getAddress() {
        return address;
    }

    public LatLng getCoordinates() {
        return coordinates;
    }

    public NearbyPlaceDetail getPlaceDetail() {
        return placeDetail;
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(placeId);
        dest.writeString(address);
        dest.writeParcelable(coordinates, flags);
        dest.writeParcelable(placeDetail, flags);
    }

    public static class Builder {
        private String name;
        private String placeId;
        private String address;
        private Double latitude;
        private Double longitude;
        private NearbyPlaceDetail nearbyPlaceDetail;

        public Builder() {
        }

        public Builder(NearbyPlace nearbyPlace) {
            this.name = nearbyPlace.name;
            this.placeId = nearbyPlace.placeId;
            this.address = nearbyPlace.address;
            this.latitude = nearbyPlace.coordinates.latitude;
            this.longitude = nearbyPlace.coordinates.longitude;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder detail(NearbyPlaceDetail nearbyPlaceDetail) {
            this.nearbyPlaceDetail = nearbyPlaceDetail;
            return this;
        }

        public Builder placeId(String placeId) {
            this.placeId = placeId;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder latitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public NearbyPlace build() {
            return new NearbyPlace(this);
        }
    }
}
