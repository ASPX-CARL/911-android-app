package ph.com.auspex.mers.data.model.request;

import java.util.HashMap;

import ph.com.auspex.mers.BuildConfig;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Create Google Directions API Request
 *
 * @deprecated feature removed from App
 */
@Deprecated
public class DirectionsRequest {

    private String destLat;
    private String destLong;
    private String originLat;
    private String originLong;

    private DirectionsRequest(Builder builder) {
        this.destLat = checkNotNull(Double.toString(builder.destLat));
        this.destLong = checkNotNull(Double.toString(builder.destLong));
        this.originLat = checkNotNull(Double.toString(builder.originLat));
        this.originLong = checkNotNull(Double.toString(builder.originLong));
    }

    public static class Builder {
        private Double destLat;
        private Double destLong;
        private Double originLat;
        private Double originLong;

        public Builder destLat(Double destLat) {
            this.destLat = destLat;
            return this;
        }

        public Builder destLong(Double destLong) {
            this.destLong = destLong;
            return this;
        }

        public Builder originLat(Double originLat) {
            this.originLat = originLat;
            return this;
        }

        public Builder originLong(Double originLong) {
            this.originLong = originLong;
            return this;
        }

        public DirectionsRequest build() {
            return new DirectionsRequest(this);
        }
    }

    public HashMap<String, String> create() {
        HashMap<String, String> params = new HashMap<>();

        params.put("origin", String.format("%1$s, %2$s", originLat, originLong));
        params.put("destination", String.format("%1$s, %2$s", destLat, destLong));
        params.put("key", BuildConfig.GOOGLE_MAPS_API_KEY);

        return params;
    }
}
