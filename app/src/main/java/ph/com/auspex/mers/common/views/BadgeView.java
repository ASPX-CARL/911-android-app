package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.auspex.mers.R;

public class BadgeView extends FrameLayout {

    @BindView(R.id.menu_badge_icon) ImageView imageView;
    @BindView(R.id.menu_badge) MaterialBadgeTextView textView;

    @BindColor(R.color.colorPrimary) int colorPrimaryBg;
    @BindColor(R.color.white) int colorWhite;

    public BadgeView(Context context) {
        super(context);

        throw new UnsupportedOperationException("Inflate from xml.");
    }

    /**
     * Create a badge view from xml
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    public BadgeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Initialize view
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BadgeView, 0, 0);

        LayoutInflater.from(context).inflate(R.layout.menu_action_item_badge, this, true);
        ButterKnife.bind(this);

        int bgColor = a.getResourceId(R.styleable.BadgeView_bv_badge_background_color, 0);
        int textColor = a.getResourceId(R.styleable.BadgeView_bv_font_color, 0);

        imageView.setImageResource(R.drawable.ic_notifications);

        textView.setTextColor(textColor != 0 ? getColor(context, textColor) : colorWhite);
        textView.setBackgroundColor(bgColor != 0 ? getColor(context, bgColor) : colorPrimaryBg);

        a.recycle();
    }

    /**
     * Set badge counter
     *
     * @param count badge counter value
     */
    public void setBadgeCount(int count) {
        textView.setBadgeCount(count);
    }

    /**
     * This method is a utility to get the color value from xml
     *
     * @param context  Activity Context
     * @param colorRes Color resource from xml
     * @return Int Color
     */
    private int getColor(Context context, int colorRes) {
        return ContextCompat.getColor(context, colorRes);
    }
}
