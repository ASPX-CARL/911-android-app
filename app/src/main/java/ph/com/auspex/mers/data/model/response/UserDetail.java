package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class UserDetail {

    @SerializedName("user_id")
    private int userId;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("middle_name")
    private String middleName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("email")
    private String emailAddress;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("gender")
    private String gender;
    @SerializedName("address")
    private String address;
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("contact_person")
    private String contactPerson;
    @SerializedName("status")
    private boolean status;
    @SerializedName("is_verified")
    private boolean isVerified;
    @SerializedName("password")
    private String password;
    @SerializedName("contact_person_number")
    private String contactPersonNumber;
    @SerializedName("invite_code")
    private String inviteCode;
    @SerializedName("medical_info")
    private String medicalInfo;
    private File imageFile;

    public UserDetail() {
    }

    // Create Family Link Detail
    public UserDetail(UserDetail userDetail) {
        this.firstName = userDetail.getFirstName();
        this.middleName = userDetail.getMiddleName();
        this.lastName = userDetail.getLastName();
        this.address = userDetail.getAddress();
        this.mobileNumber = userDetail.getMobileNumber();
        this.imageUrl = userDetail.getImageUrl();
    }

    private UserDetail(Builder builder) {
        this.userId = builder.userId;
        this.firstName = builder.firstName;
        this.middleName = builder.middleName;
        this.lastName = builder.lastName;
        this.emailAddress = builder.emailAddress;
        this.mobileNumber = builder.mobileNumber;
        this.birthday = builder.birthday;
        this.gender = builder.gender;
        this.address = builder.address;
        this.contactPerson = builder.contactPerson;
        this.contactPersonNumber = builder.contactPersonNumber;
        this.imageFile = builder.imageFile;
        this.medicalInfo = builder.medicalInformation;
    }

    public int getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public boolean isStatus() {
        return status;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public String getPassword() {
        return password;
    }

    public String getContactPersonNumber() {
        return contactPersonNumber;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public String getMedicalInfo() {
        return medicalInfo;
    }

    public static class Builder {
        private int userId;
        private String firstName;
        private String middleName;
        private String lastName;
        private String emailAddress;
        private String mobileNumber;
        private String birthday;
        private String gender;
        private String address;
        private String imageUrl;
        private String contactPerson;
        private boolean status;
        private boolean isVerified;
        private String password;
        private String contactPersonNumber;
        private File imageFile;
        private String medicalInformation;

        public Builder userId(int userId) {
            this.userId = userId;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        public Builder mobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
            return this;
        }

        public Builder birthday(String birthday) {
            this.birthday = birthday;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder imageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public Builder contactPerson(String contactPerson) {
            this.contactPerson = contactPerson;
            return this;
        }

        public Builder status(boolean status) {
            this.status = status;
            return this;
        }

        public Builder isVerified(boolean isVerified) {
            this.isVerified = isVerified;
            return this;
        }

        public Builder contactPersonNumber(String contactPersonNumber) {
            this.contactPersonNumber = contactPersonNumber;
            return this;
        }

        public Builder medicalInformation(String medicalInformation) {
            this.medicalInformation = medicalInformation;
            return this;
        }

        public Builder imageFile(File imageFile) {
            this.imageFile = imageFile;
            return this;
        }

        public UserDetail build() {
            return new UserDetail(this);
        }
    }

    public HashMap<String, RequestBody> createRequest() {
        HashMap<String, RequestBody> partMap = new HashMap<>();

        MediaType textPlain = MediaType.parse("text/plain");
        MediaType image = MediaType.parse("image/*");

        partMap.put("user_id", RequestBody.create(textPlain, Integer.toString(userId)));
        partMap.put("first_name", RequestBody.create(textPlain, firstName));
        partMap.put("middle_name", RequestBody.create(textPlain, middleName));
        partMap.put("last_name", RequestBody.create(textPlain, lastName));
        partMap.put("email", RequestBody.create(textPlain, emailAddress));
        partMap.put("mobile_number", RequestBody.create(textPlain, mobileNumber));
        partMap.put("birthday", RequestBody.create(textPlain, birthday));
        partMap.put("gender", RequestBody.create(textPlain, gender));
        partMap.put("address", RequestBody.create(textPlain, address));
        partMap.put("contact_person", RequestBody.create(textPlain, contactPerson));
        partMap.put("contact_person_number", RequestBody.create(textPlain, contactPersonNumber));
        partMap.put("medical_info", RequestBody.create(textPlain, medicalInfo));

        if (imageFile != null) {
            partMap.put("image\"; filename=\"" + imageFile.getName() + ".jpg", RequestBody.create(image, imageFile));
        }

        return partMap;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", birthday='" + birthday + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                ", contactPersonNumber='" + contactPersonNumber + '\'' +
                ", imageFile=" + imageFile +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDetail that = (UserDetail) o;

        if (!firstName.equals(that.firstName)) return false;
        if (!middleName.equals(that.middleName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (!mobileNumber.equals(that.mobileNumber)) return false;
        if (!birthday.equals(that.birthday)) return false;
        if (!gender.equals(that.gender)) return false;
        if (!address.equals(that.address)) return false;
        if (!contactPerson.equals(that.contactPerson)) return false;
        if (!contactPersonNumber.equals(that.contactPersonNumber)) return false;
        if (!medicalInfo.equals(that.medicalInfo)) return false;
        return imageFile != null ? imageFile.equals(that.imageFile) : that.imageFile == null;
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + middleName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + mobileNumber.hashCode();
        result = 31 * result + birthday.hashCode();
        result = 31 * result + gender.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + contactPerson.hashCode();
        result = 31 * result + contactPersonNumber.hashCode();
        result = 31 * result + medicalInfo.hashCode();
        result = 31 * result + (imageFile != null ? imageFile.hashCode() : 0);
        return result;
    }
}
