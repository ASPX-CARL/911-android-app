package ph.com.auspex.mers.familylink.list.invites.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.InviteRequest;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.familylink.FamilyLinkInteractor;
import ph.com.auspex.mers.familylink.list.invites.FamInvitesContract.FamInvitesPresenter;
import ph.com.auspex.mers.familylink.list.invites.FamInvitesContract.FamInvitesView;

public class FamInvitesPresenterImpl extends MvpBasePresenter<FamInvitesView> implements FamInvitesPresenter {

    private final FamilyLinkInteractor model;

    public FamInvitesPresenterImpl(FamilyLinkInteractor model) {
        this.model = model;
    }

    @Override
    public void attachView(FamInvitesView view) {
        super.attachView(view);

        getInvitesList();
    }

    @Override
    public void approveRequest(InviteRequest inviteRequest) {
        if (isViewAttached()) {
            getView().showLoading();
        }

        model.acceptInvite(inviteRequest.getUserId(), new BaseInteractor.ResponseListener<String>() {
            @Override
            public void onSuccess(String s) {
                if (isViewAttached()) {
                    getView().hideLoading();
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }

    @Override
    public void rejectRequest(InviteRequest inviteRequest) {
        if (isViewAttached()) {
            getView().showLoading();
        }

        model.rejectInvite(inviteRequest.getUserId(), new BaseInteractor.ResponseListener<String>() {
            @Override
            public void onSuccess(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onRequestRejected(inviteRequest.getLayoutPosition());
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }

    @Override
    public void getInvitesList() {
        if (isViewAttached()) {
            getView().showLoading();
        }

        model.getFamilyLinkInvites(new BaseInteractor.ResponseListener<List<FamilyLinkDetail>>() {
            @Override
            public void onSuccess(List<FamilyLinkDetail> familyLinkDetails) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().displayInvitesList(familyLinkDetails);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                }
            }
        });
    }
}
