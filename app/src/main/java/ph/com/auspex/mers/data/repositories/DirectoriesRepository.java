package ph.com.auspex.mers.data.repositories;

import java.util.List;

import io.reactivex.Observable;
import ph.com.auspex.mers.data.model.request.DirectionsRequest;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;

public interface DirectoriesRepository {

    Observable<List<NearbyPlace>> getNearbyPlacesByType(NearbyPlaceRequest request);

    Observable<NearbyPlaceDetail> getNearbyPlaceDetail(String placeId);

    Observable<MapDirection> getDirections(DirectionsRequest request);
}