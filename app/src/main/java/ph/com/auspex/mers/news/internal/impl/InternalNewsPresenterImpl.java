package ph.com.auspex.mers.news.internal.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.ListEvent;
import ph.com.auspex.mers.data.model.response.InternalNews;
import ph.com.auspex.mers.news.NewsInteractor;
import ph.com.auspex.mers.news.internal.InternalNewsContract;

public class InternalNewsPresenterImpl extends MvpBasePresenter<InternalNewsContract.InternalNewsView> implements InternalNewsContract.InternalNewsPresenter {

    private final NewsInteractor interactor;
    private final RxEventBus rxEventBus;

    private InternalNews internalNews;

    public InternalNewsPresenterImpl(NewsInteractor interactor, RxEventBus rxEventBus) {
        this.interactor = interactor;
        this.rxEventBus = rxEventBus;
    }

    @Override
    public void attachView(InternalNewsContract.InternalNewsView view) {
        super.attachView(view);

        if (internalNews == null && isViewAttached()) {
            getInternalNews(true);
        }

        if (internalNews != null && isViewAttached()) {
            getView().setData(internalNews);
        }

        rxEventBus.toObservable().subscribe(o -> {
            if (o instanceof ListEvent.ScrollToTopEvent) {
                if (isViewAttached()) {
                    getView().scrollListToTop();
                }
            }
        });
    }

    @Override
    public void getInternalNews(boolean refresh) {
        if (isViewAttached()) {
            if (refresh) {
                getView().showLoading();
                internalNews = new InternalNews();
            } else {
                getView().onLoadMore();
            }
        }

        interactor.getIntNews(internalNews, new BaseInteractor.ResponseListener<InternalNews>() {
            @Override
            public void onSuccess(InternalNews internalNews) {
                if (isViewAttached()) {
                    if (internalNews.getCurrentPage() <= internalNews.getPageCount()) {
                        internalNews.setCurrentPage(internalNews.getCurrentPage() + 1);

                        setInternalNews(internalNews);

                        getView().setData(internalNews);
                        getView().hideLoading();
                    }
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                }
            }
        });
    }

    public void setInternalNews(InternalNews internalNews) {
        this.internalNews = internalNews;
    }
}
