package ph.com.auspex.mers.data.remote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import ph.com.auspex.mers.data.model.response.AccessToken;
import ph.com.auspex.mers.data.model.response.Advertisement;
import ph.com.auspex.mers.data.model.response.ApiResponse;
import ph.com.auspex.mers.data.model.response.EmergencyType;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;
import ph.com.auspex.mers.data.model.response.News;
import ph.com.auspex.mers.data.model.response.Relationship;
import ph.com.auspex.mers.data.model.response.UserDetail;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * ApiService methods
 * <p>
 * see <a href="https://docs.google.com/spreadsheets/d/1j2lr9VcRguj1p7LLOHFsmtd7PVZsJ1e7WoeCK-8lFNI/edit#gid=0">Davao Central 911 API Documentation</a>
 */
public interface ApiService {

    /**
     * Get relationship types from API
     *
     * @return {@link List} of {@link Relationship}
     */
    @GET("relationships")
    Observable<ApiResponse<List<Relationship>>> getRelationshipTypes();

    /**
     * Get RSS news list from API
     *
     * @return {@link List} of {@link News}
     */
    @GET("news")
    Observable<ApiResponse<List<News>>> getNewsList();

    /**
     * Get Internal Alerts/News from API
     *
     * @param page current page
     * @return {@link InternalNews} which contains a {@link List} of {@link ph.com.auspex.mers.data.model.response.InternalFeed}
     */
    @GET("internal_news")
    Observable<ApiResponse<InternalNews>> getInternalNews(@Query("page") String page);

    /**
     * Login user account
     *
     * @param request user's login credentials
     * @return {@link AccessToken} which contains {@link UserDetail}
     */
    @POST("users/login")
    @FormUrlEncoded
    Observable<ApiResponse<AccessToken>> login(@FieldMap HashMap<String, String> request);

    /**
     * Register for a user account
     *
     * @param params user's registration information
     * @return user detail
     */
    @POST("users")
    @Multipart
    Observable<ApiResponse<UserDetail>> register(@PartMap Map<String, RequestBody> params);

    /**
     * Verify user registration code
     *
     * @param query user's registration verification information
     * @return {@link ApiResponse} which will be used to get {@link ApiResponse#getMessage()}
     */
    @GET("users/verify_user")
    Observable<ApiResponse> verifyCode(@QueryMap Map<String, String> query);

    /**
     * Get logged in user's account information
     *
     * @param userId current {@link UserDetail#userId} of the logged in user
     * @return {@link AccessToken} which contains {@link UserDetail}
     */
    @GET("users")
    Observable<ApiResponse<AccessToken>> getUserDetails(@Query("u") String userId);

    /**
     * Logout user out of the application
     *
     * @param userId current {@link UserDetail#userId} of the logged in user
     * @return {@link ApiResponse} which will be used to get {@link ApiResponse#getMessage()}
     */
    @DELETE("users/token")
    Observable<ApiResponse> logout(@Query("user_id") String userId);

    /**
     * Update user's profile information
     *
     * @param params user's update profile information
     * @return {@link AccessToken} which contains {@link UserDetail}
     */
    @POST("users/update_user")
    @Multipart
    Observable<ApiResponse<AccessToken>> updateProfile(@PartMap Map<String, RequestBody> params);

    /**
     * Update user location information in the API
     *
     * @param query user's {@link android.location.Location} information
     * @return {@link ApiResponse} which will be used to get {@link ApiResponse#getMessage()}
     */
    @PUT("users/update_user_location")
    Observable<ApiResponse> updateLocation(@QueryMap Map<String, String> query);

    /**
     * Request for forgot password in API
     *
     * @param emailAddress concerned user email address
     * @return API Response
     */
    @POST("users/forgot_password")
    @FormUrlEncoded
    Observable<ForgotPasswordResponse> forgotPassword(@Field("email") String emailAddress);

    /**
     * Update user's password
     *
     * @param params Forgot password parameter
     * @return {@link AccessToken} which contains updated {@link UserDetail}
     */
    @POST("users/change_password")
    @FormUrlEncoded
    Observable<ApiResponse<AccessToken>> changePassword(@FieldMap Map<String, String> params);

    /**
     * Submit an emergency request
     *
     * @param params Emergency Request parameters
     * @return {@link ApiResponse} which will be used to get {@link ApiResponse#getMessage()}
     */
    @POST("calls")
    @Multipart
    Observable<ApiResponse> reportEmergency(@PartMap Map<String, RequestBody> params);

    /**
     * Get list of {@link Advertisement} from API
     *
     * @return List of Advertisement
     */
    @GET("advertisements")
    Observable<ApiResponse<List<Advertisement>>> getAdvertisements();

    /**
     * Get List of available family link for the logged in user account
     *
     * @param userId current {@link UserDetail#userId} of the logged in user
     * @return List of {@link FamilyLinkDetail}
     */
    @GET("famlink")
    Observable<ApiResponse<List<FamilyLinkDetail>>> getFamilyLink(@Query("user_id") String userId);

    /**
     * Send a family link invitation request
     *
     * @param params Family Link invitation request
     * @return {@link ApiResponse} which will be used to get {@link ApiResponse#getMessage()}
     */
    @POST("famlink/invite_user")
    @FormUrlEncoded
    Observable<ApiResponse> addFamilyLink(@FieldMap Map<String, String> params);

    /**
     * Get Pending Family Link Invites from API
     *
     * @param userId current {@link UserDetail#userId} of the logged in user
     * @return List of {@link FamilyLinkDetail} pending invitations
     */
    @GET("famlink/pending_requests")
    Observable<ApiResponse<List<FamilyLinkDetail>>> getFamilyLinkInvites(@Query("user_id") String userId);

    /**
     * Accept Family Link invitation
     *
     * @param inviterId userId of the user who send the invitation
     * @param invitorId userId of the invited user
     * @return {@link ApiResponse} which will be used to get {@link ApiResponse#getMessage()}
     */
    @POST("famlink/accept_user")
    @FormUrlEncoded
    Observable<ApiResponse> acceptInvite(@Field("inviter_id") String inviterId, @Field("invitor_id") String invitorId);

    /**
     * Reject Family Link invitation
     *
     * @param inviterId userId of the user who send the invitation
     * @param invitorId userId of the invited user
     * @return {@link ApiResponse} which will be used to get {@link ApiResponse#getMessage()}
     */
    @POST("famlink/reject_user")
    @FormUrlEncoded
    Observable<ApiResponse> rejectInvite(@Field("inviter_id") String inviterId, @Field("invitor_id") String invitorId);

    /**
     * Get nearby places using Google Nearby Places API
     *
     * @param params current user location and place type
     * @return {@link List} of {@link NearbyPlace} objects
     */
    @GET("directories")
    Observable<ApiResponse<List<NearbyPlace>>> getNearbyPlaces(@QueryMap HashMap<String, String> params);

    /**
     * Get details of nearby place returned from {@link #getNearbyPlaces(HashMap)}
     *
     * @param placeId placeId of {@link NearbyPlace} object
     * @return Nearby place detail
     */
    @GET("directories/get_details")
    Observable<ApiResponse<NearbyPlaceDetail>> getNearbyPlaceDetails(@Query("place_id") String placeId);

    /**
     * Get Emergency Types from API
     *
     * @return {@link List} of {@link EmergencyType}
     */
    @GET("emergencies")
    Observable<ApiResponse<List<EmergencyType>>> getEmergencyTypes();

    @Deprecated
    @GET
    Observable<MapDirection> getDirection(@Url String url, @QueryMap HashMap<String, String> params);

    /**
     * Get specific internal news using {@link InternalFeed#getId()} from Push Notification
     *
     * @param id {@link InternalFeed#getId()}
     * @return Internal News/Alerts Detail
     */
    @GET("internal_news")
    Observable<ApiResponse<InternalNews>> getSpecificInternalNews(@Query("internal_news_id") String id);
}