package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Patterns;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ph.com.auspex.mers.R;

import static android.text.TextUtils.isEmpty;

/**
 * A special sub-class of {@link android.widget.EditText} designed for the app custom input field requirements
 */
public class AdvancedTextInputEditText extends TextInputEditText implements TextWatcher {

    private String hintStr;
    private String regex;
    private String errorStr;
    private int hintTextColor;
    private int drawableLeftResId;
    private int drawableRightResId;
    private int drawableTint;
    private int inputType;
    private boolean isEnabled;
    private boolean isFocusable;
    private Drawable errorIcon;
    private boolean inputError;
    private float drawablePadding;

    private Pattern patternRegex;
    private PhoneNumberUtil phoneNumberUtil;

    private OnInputTextChangedListener inputTextChangedListener = inputStr -> {
        // Prevents NPE
    };

    public AdvancedTextInputEditText(Context context) {
        super(context);

        throw new UnsupportedOperationException("Please inflate from xml.");
    }

    /**
     * A contructor that will create an instance of the view using xml inflation
     *
     * @param context Activity context
     * @param attrs   xml attributes
     */
    public AdvancedTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context, attrs);
        this.phoneNumberUtil = PhoneNumberUtil.getInstance();
    }

    /**
     * Initialize view configuration
     *
     * @param context Activity context
     * @param attrs   xml attributes
     */
    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AdvancedTextInputEditText, 0, 0);

        hintStr = a.getString(R.styleable.AdvancedTextInputEditText_hint);
        hintTextColor = a.getResourceId(R.styleable.AdvancedTextInputEditText_textColorHint, 0);
        drawableLeftResId = a.getResourceId(R.styleable.AdvancedTextInputEditText_drawableLeft, 0);
        drawableRightResId = a.getResourceId(R.styleable.AdvancedTextInputEditText_drawableRight, 0);
        inputType = a.getInt(R.styleable.AdvancedTextInputEditText_android_inputType, InputType.TYPE_CLASS_TEXT);
        isEnabled = a.getBoolean(R.styleable.AdvancedTextInputEditText_isEnabled, true);
        isFocusable = a.getBoolean(R.styleable.AdvancedTextInputEditText_isFocusable, true);
        drawableTint = a.getResourceId(R.styleable.AdvancedTextInputEditText_drawableTint, 0);
        regex = a.getString(R.styleable.AdvancedTextInputEditText_regex);
        drawablePadding = a.getDimension(R.styleable.AdvancedTextInputEditText_drawablePadding, 0);
        errorStr = a.getString(R.styleable.AdvancedTextInputEditText_errorMessage);

        errorIcon = ContextCompat.getDrawable(context, R.drawable.ic_error_outline_black_24px);
        DrawableCompat.setTint(errorIcon, ContextCompat.getColor(context, R.color.colorPrimary));
        DrawableCompat.setTintMode(errorIcon, PorterDuff.Mode.SRC_IN);

        if (!isEmpty(regex)) {
            patternRegex = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        } else {
            patternRegex = null;
        }

        if (!isEmpty(hintStr)) {
            setHint(hintStr);
        }

        if (hintTextColor != 0) {
            setHintTextColor(ContextCompat.getColor(context, hintTextColor));
        }

        Drawable drawableLeft = null;
        Drawable drawableRight = null;

        if (drawableLeftResId != 0) {
            drawableLeft = ContextCompat.getDrawable(context, drawableLeftResId);
        }

        if (drawableRightResId != 0) {
            drawableRight = ContextCompat.getDrawable(context, drawableRightResId);
        }

        if (drawableTint != 0) {
            if (drawableLeft != null) {
                DrawableCompat.setTint(drawableLeft, drawableTint);
                DrawableCompat.setTintMode(drawableLeft, PorterDuff.Mode.SRC_IN);
            }

            if (drawableRight != null) {
                DrawableCompat.setTint(drawableRight, drawableTint);
                DrawableCompat.setTintMode(drawableRight, PorterDuff.Mode.SRC_IN);
            }
        }

        if (drawablePadding != 0) {
            setCompoundDrawablePadding((int) drawablePadding);
        }

        if (inputType != 0) {
            setInputType(inputType);
        }

        if (inputType == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS) {
            patternRegex = Patterns.EMAIL_ADDRESS;
        }

        if (inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
            setTransformationMethod(PasswordTransformationMethod.getInstance());
        }

        setEnabled(isEnabled);
        setFocusable(isFocusable);

        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, drawableRight, null);

        addTextChangedListener(this);

        a.recycle();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        inputError = false;

        if (s.toString().length() >= 1) {
            if (getInputType() == InputType.TYPE_CLASS_NUMBER) {
                isMobileNumberValid(s.toString());
            } else {
                isValid(s.toString());
            }
        }

        inputTextChangedListener.onTextChanged(s.toString());
    }

    /**
     * Check if the input string matches the regex from the xml attribute
     *
     * @param inputStr User string input
     */
    private void isValid(String inputStr) {
        if (patternRegex != null) {
            Matcher matcher = patternRegex.matcher(inputStr);

            if (!matcher.matches()) {
                showError(errorStr);
            }
        }
    }

    /**
     * Check if the mobile number is valid
     *
     * @param mobileNumber User mobile number input
     */
    private void isMobileNumberValid(String mobileNumber) {
        Phonenumber.PhoneNumber phoneNumber;

        try {
            phoneNumber = phoneNumberUtil.parse(mobileNumber, "PH");

            if (!phoneNumberUtil.isValidNumber(phoneNumber)) {
                showError(errorStr);
            }
        } catch (NumberParseException e) {
            showError(errorStr);
        }
    }

    /**
     * Show an error if the input string does not match the regex of the view
     *
     * @param errorStr Error string message
     */
    public void showError(String errorStr) {
        setError(!isEmpty(errorStr) ? errorStr : "Error");
        inputError = true;
    }

    /**
     * Check if this {@link android.widget.EditText} has error
     *
     * @return Error boolean
     */
    public boolean hasError() {
        return inputError;
    }

    /**
     * Set custom regex and replace the regex provided in the xml attribute
     *
     * @param regex Regex string
     */
    public void setRegex(String regex) {
        if (!isEmpty(regex)) {
            patternRegex = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        } else {
            patternRegex = null;
        }
    }

    /**
     * Set custom error string and replace the default or provided error string in the xml attribute
     *
     * @param errorStr Error string
     */
    public void setErrorStr(String errorStr) {
        this.errorStr = errorStr;
    }

    public interface OnInputTextChangedListener {
        void onTextChanged(String inputStr);
    }

    public void setInputTextChangedListener(OnInputTextChangedListener inputTextChangedListener) {
        this.inputTextChangedListener = inputTextChangedListener;
    }
}
