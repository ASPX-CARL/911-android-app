package ph.com.auspex.mers.familylink.list.invites;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.request.InviteRequest;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;

public interface FamInvitesContract {

    interface FamInvitesPresenter extends MvpPresenter<FamInvitesView> {
        void approveRequest(InviteRequest inviteRequest);

        void rejectRequest(InviteRequest inviteRequest);

        void getInvitesList();
    }

    interface FamInvitesView extends RemoteView {
        void onRequestAccepted();

        void onRequestRejected(int position);

        void displayInvitesList(List<FamilyLinkDetail> familyLinkDetails);
    }
}
