package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import ph.com.auspex.mers.R;

/**
 * A custom view that implements a spinner and title for the spinner
 *
 * @param <T> Generic Object
 */
public class TitleSpinnerView<T> extends FrameLayout {

    @BindView(R.id.title) TextView tvTitle;
    @BindView(R.id.spinner) Spinner spinner;

    BaseAdapter adapter;
    TitleSpinnerView.OnItemSelectedListener listener;

    public TitleSpinnerView(Context context) {
        super(context);

        throw new UnsupportedOperationException("Inflate from xml.");
    }

    /**
     * Create this view from xml
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    public TitleSpinnerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context, attrs);
    }

    /**
     * Initialize view
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TitleSpinnerView, 0, 0);

        View view = LayoutInflater.from(context).inflate(R.layout.layout_spinner_title, this);
        ButterKnife.bind(view);

        String title = a.getString(R.styleable.TitleSpinnerView_tsv_title);
        tvTitle.setText(title);

        int arrayId = a.getResourceId(R.styleable.TitleSpinnerView_tsv_array, -1);
        if (arrayId != -1) {
            String[] arrayString = context.getResources().getStringArray(arrayId);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.layout_spinner_with_icon, R.id.title, arrayString);
            adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
            setAdapter(adapter);
        }

        a.recycle();
    }

    /**
     * Set spinner adapter
     *
     * @param adapter Spinner Adapter {@link BaseAdapter}
     */
    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
        spinner.setAdapter(adapter);
    }

    /**
     * Get spinner spinner adapter
     *
     * @return {@link BaseAdapter} object
     */
    public BaseAdapter getAdapter() {
        return adapter;
    }

    /**
     * Set listener that will be called after selecting an item
     *
     * @param listener
     */
    public void setListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }

    /**
     * get the object of the selected item
     *
     * @return Object
     */
    @SuppressWarnings("unchecked")
    public T getSelectedItem() {
        return (T) adapter.getItem(spinner.getSelectedItemPosition());
    }

    /**
     * Get position of the selected item
     *
     * @return index
     */
    public int getSelectedItemPosition() {
        return spinner.getSelectedItemPosition();
    }

    @OnItemSelected(R.id.spinner)
    void onItemSelectedListener(Spinner spinner, int position) {
        if (listener != null) {
            listener.onItemSelected(spinner, position);
        }
    }

    public interface OnItemSelectedListener {
        void onItemSelected(Spinner spinner, int position);
    }

    public void setSelection(int position) {
        spinner.setSelection(position);
    }
}