package ph.com.auspex.mers.familylink.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.adapter.ItemClickListener;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.GlideImageLoader;

public class FamilyLinkRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VH_INVITE_CODE = 0;
    private static final int VH_FAMILY_LINK = 1;

    private List<FamilyLinkDetail> familyLinkList;
    private ItemClickListener<String> listener;
    private String inviteCode;
    private InviteCodeViewHolder.AddButtonClickListener addButtonClickListener;

    public FamilyLinkRecyclerAdapter(ItemClickListener<String> listener, InviteCodeViewHolder.AddButtonClickListener addButtonClickListener) {
        this.familyLinkList = new ArrayList<>();
        this.listener = listener;
        this.addButtonClickListener = addButtonClickListener;
    }

    public void updateList(String inviteCode, List<FamilyLinkDetail> familyLinkList) {
        this.inviteCode = inviteCode;
        this.familyLinkList = familyLinkList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VH_INVITE_CODE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_invite_code, parent, false);
            return new InviteCodeViewHolder(parent.getContext(), view, addButtonClickListener);
        } else if (viewType == VH_FAMILY_LINK) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_family_link, parent, false);
            return new FamilyLinkViewHolder(parent.getContext(), view, listener);
        } else {
            throw new UnsupportedOperationException("ViewHolder is of invalid type");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isPositionInviteCode(position)) {
            InviteCodeViewHolder viewHolder = (InviteCodeViewHolder) holder;
            viewHolder.bind(inviteCode);
        } else {
            FamilyLinkViewHolder viewHolder = (FamilyLinkViewHolder) holder;
            viewHolder.bind(familyLinkList.get(position - 1));
        }
    }

    @Override
    public int getItemCount() {
        return familyLinkList != null ? familyLinkList.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionInviteCode(position)) {
            return VH_INVITE_CODE;
        } else {
            return VH_FAMILY_LINK;
        }
    }

    private boolean isPositionInviteCode(int position) {
        return position == 0;
    }

    static class FamilyLinkViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_profile_image) ImageView ivProfileImage;
        @BindView(R.id.tv_relative_name) TextView tvRelativeName;
        @BindView(R.id.tv_relationship) TextView tvRelationship;
        @BindView(R.id.tv_relative_mobile_number) TextView tvRelativeMobileNumber;
        @BindView(R.id.cb_availability_indicator) CheckBox cbAvailabilityIndicator;

        private FamilyLinkDetail familyLinkDetail;
        private Context context;
        private ItemClickListener<String> listener;

        public FamilyLinkViewHolder(Context context, View itemView, ItemClickListener<String> listener) {
            super(itemView);

            this.context = context;
            this.listener = listener;
            ButterKnife.bind(this, itemView);
        }

        public void bind(FamilyLinkDetail familyLinkDetail) {
            this.familyLinkDetail = familyLinkDetail;

            /*if (!TextUtils.isEmpty(familyLinkDetail.getImageUrl())) {
                GlideImageLoader.loadImageRounded(context, familyLinkDetail.getImageUrl(), ivProfileImage);
            } else {
                GlideImageLoader.loadImageRounded(context, familyLinkDetail.getGender().getDrawableRes(), ivProfileImage);
            }*/
            GlideImageLoader.loadUrl(context, ivProfileImage, familyLinkDetail.getImageUrl(), familyLinkDetail.getGender().getDrawableRes(), familyLinkDetail.getGender().getDrawableRes(), true);

            tvRelativeName.setText(String.format("%s %s", familyLinkDetail.getFirstName(), familyLinkDetail.getLastName()));
            tvRelationship.setText(familyLinkDetail.getRelationship());
            tvRelativeMobileNumber.setText(AppUtil.convertMobileNumberToNationalFormat(familyLinkDetail.getMobileNumber()));

            cbAvailabilityIndicator.setChecked(familyLinkDetail.isOnline());
        }

        @OnClick(R.id.button_call)
        void onCallButtonClicked() {
            listener.onItemClicked(familyLinkDetail.getMobileNumber());
        }
    }
}
