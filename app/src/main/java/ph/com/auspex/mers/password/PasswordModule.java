package ph.com.auspex.mers.password;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.password.changepassword.ChangePasswordContract;
import ph.com.auspex.mers.password.changepassword.impl.ChangePasswordPresenterImpl;
import ph.com.auspex.mers.password.updatepassword.UpdatePasswordContract;
import ph.com.auspex.mers.password.updatepassword.impl.UpdatePasswordPresenterImpl;

@Module
public class PasswordModule {

    /**
     * Provide PasswordInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return PasswordInteractor
     */
    @PasswordScope
    @Provides
    PasswordInteractor providesChangePasswordInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide ChangePasswordPresenter
     *
     * @param model {@link PasswordInteractor}
     * @return ChangePasswordPresenter
     */
    @PasswordScope
    @Provides
    ChangePasswordContract.ChangePasswordPresenter providesChangePasswordPresenter(PasswordInteractor model) {
        return new ChangePasswordPresenterImpl(model);
    }

    /**
     * Provide UpdatePasswordPresenter
     *
     * @param model {@link PasswordInteractor}
     * @return UpdatePasswordPresenter
     */
    @PasswordScope
    @Provides
    UpdatePasswordContract.UpdatePasswordPresenter providesUpdatePasswordPresenter(PasswordInteractor model) {
        return new UpdatePasswordPresenterImpl(model);
    }
}
