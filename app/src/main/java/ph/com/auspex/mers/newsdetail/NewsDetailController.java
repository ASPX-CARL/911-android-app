package ph.com.auspex.mers.newsdetail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.news.NewsModule;
import timber.log.Timber;

import static ph.com.auspex.mers.newsdetail.NewsDetailContract.NewsDetailPresenter;
import static ph.com.auspex.mers.newsdetail.NewsDetailContract.NewsDetailView;

/**
 * News Detail Controller UI
 */
public class NewsDetailController extends MosbyController<NewsDetailView, NewsDetailContract.NewsDetailPresenter> implements SwipeRefreshLayout.OnRefreshListener, NewsDetailView {

    public static final String TAG = NewsDetailController.class.getSimpleName();
    public static final String KEY_URL = TAG + ".url";

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.web_view) WebView webView;

    @Inject NewsDetailPresenter presenter;
    @Inject Router parentRouter;

    private String url;

    public NewsDetailController(@Nullable Bundle args) {
        super(args);

        if (args != null && args.getString(KEY_URL) != null) {
            url = args.getString(KEY_URL);
        }
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.newsComponent(new NewsModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        initSwipeRefreshLayout();
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (url != null) {
            displayNews(url);
        }
    }

    /**
     * Initialize {@link SwipeRefreshLayout}
     */
    private void initSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_news_detail, container, false);
    }

    /**
     * Action to be done when {@link SwipeRefreshLayout.OnRefreshListener#onRefresh()} is called.
     * Reload url
     */
    @Override
    public void onRefresh() {
        if (!TextUtils.isEmpty(url)) {
            webView.loadUrl(url);
        }
    }

    /**
     * Initialize {@link WebView} using the provided url
     *
     * @param url Url to be loaded in the {@link WebView}
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                swipeRefreshLayout.setRefreshing(true);
                return true;
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                Timber.d("onProgressChanged: %s", newProgress);
                if (newProgress != 100 && newProgress > 95) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
        webView.loadUrl(url);
    }

    @Override
    protected boolean showNavigationIcon() {
        return true;
    }

    @Override
    protected void onClickNavigationIcon() {
        handleBack();
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_webview;
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_open_browser:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    /**
     * Invalidate {@link SwipeRefreshLayout} and {@link WebView} on controller {@link Controller#onDestroyView(View)}
     *
     * @param view Controller view
     */
    @Override
    protected void onDestroyView(@NonNull View view) {
        presenter.detachView(false);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
        if (webView != null) {
            webView.onPause();
            webView.destroy();
        }
        super.onDestroyView(view);
    }

    /**
     * Show {@link SwipeRefreshLayout} refreshing animation
     */
    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    /**
     * Dismiss {@link SwipeRefreshLayout} refreshing animation
     */
    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @NonNull
    @Override
    public NewsDetailPresenter createPresenter() {
        return presenter;
    }

    /**
     * Load this url
     *
     * @param url url to be loaded
     */
    @Override
    public void displayNews(String url) {
        this.url = url;
        initWebView(url);
    }

    /**
     * Action to be done when back button is pressed
     *
     * @return Return true if this controller consumed the back button
     */
    @Override
    public boolean handleBack() {
        Controller navigationController = parentRouter.getControllerWithTag(NavigationController.TAG);

        if (navigationController != null) {
            getRouter().popController(this);
        } else {
            getRouter().setRoot(RouterTransaction.with(new NavigationController()));
        }

        return true;
    }
}
