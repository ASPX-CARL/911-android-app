package ph.com.auspex.mers.news;

import android.util.SparseArray;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;

public interface NewsInteractor extends BaseInteractor {

    void getRssNews(ResponseListener<SparseArray<Object>> listener);

    void getIntNews(InternalNews internalNews, ResponseListener<InternalNews> listener);

    void getSpecificNews(int id, ResponseListener<InternalFeed> listener);
}
