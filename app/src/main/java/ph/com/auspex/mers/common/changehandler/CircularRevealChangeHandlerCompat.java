package ph.com.auspex.mers.common.changehandler;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * An {@link com.bluelinelabs.conductor.changehandler.AnimatorChangeHandler} that will perform a circular reveal
 */
public class CircularRevealChangeHandlerCompat extends CircularRevealChangeHandler {

    public CircularRevealChangeHandlerCompat() {
    }


    /**
     * Constructor that will create a circular reveal from the center of the fromView parameter.
     *
     * @param fromView      The view from which the circular reveal should originate
     * @param containerView The view that hosts fromView
     */
    public CircularRevealChangeHandlerCompat(@NonNull View fromView, @NonNull View containerView) {
        super(fromView, containerView);
    }

    /**
     * Constructor that will create a circular reveal from the center of the fromView parameter.
     *
     * @param fromView      The view from which the circular reveal should originate
     * @param containerView The view that hosts fromView
     * @param duration      The duration of the animation
     */
    public CircularRevealChangeHandlerCompat(@NonNull View fromView, @NonNull View containerView, long duration) {
        super(fromView, containerView, duration, true);
    }

    @Override
    @NonNull
    protected Animator getAnimator(@NonNull ViewGroup container, View from, View to, boolean isPush, boolean toAddedToContainer) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return super.getAnimator(container, from, to, isPush, toAddedToContainer);
        } else {
            AnimatorSet animator = new AnimatorSet();
            if (to != null) {
                float start = toAddedToContainer ? 0 : to.getAlpha();
                animator.play(ObjectAnimator.ofFloat(to, View.ALPHA, start, 1));
            }

            if (from != null) {
                animator.play(ObjectAnimator.ofFloat(from, View.ALPHA, 0));
            }

            return animator;
        }
    }
}
