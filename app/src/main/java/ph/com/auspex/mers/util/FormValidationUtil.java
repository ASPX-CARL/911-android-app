package ph.com.auspex.mers.util;

import android.content.res.Resources;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber;

import ph.com.auspex.mers.common.views.AdvancedTextInputEditText;
import ph.com.auspex.mers.common.views.ProfileEditText;
import timber.log.Timber;

/**
 * Form Validation utility
 */
public class FormValidationUtil {

    private final InputMethodManager inputMethodManager;
    private final Resources resources;
    private final PhoneNumberUtil phoneNumberUtil;

    /**
     * Constructor to create an instance of FormValidationUtil
     *
     * @param resources          Application {@link Resources} instance
     * @param inputMethodManager {@link InputMethodManager} instance
     * @param phoneNumberUtil    {@link PhoneNumberUtil} instance
     */
    public FormValidationUtil(Resources resources, InputMethodManager inputMethodManager, PhoneNumberUtil phoneNumberUtil) {
        this.resources = resources;
        this.inputMethodManager = inputMethodManager;
        this.phoneNumberUtil = phoneNumberUtil;
    }

    /**
     * Check if the {@link EditText} fields has no empty fields
     *
     * @param editTexts Form {@link EditText} fields
     * @return return true if a single EditText field is empty, otherwise false
     */
    public boolean hasEmptyField(EditText... editTexts) {
        for (EditText editText : editTexts) {
            CharSequence text = editText.getText();

            if (TextUtils.isEmpty(text)) {
                setError(editText, "This field is required");
                return true;
            }
        }
        return false;
    }

    /**
     * Validate contact person details.
     *
     * @param inputName   Contact person name string
     * @param inputNumber Contact person mobile number string
     * @return return true if Both fields has value or if both fields are empty
     */
    public boolean validateContactPersonDetails(EditText inputName, EditText inputNumber) {
        if (TextUtils.isEmpty(inputName.getText().toString()) && !TextUtils.isEmpty(inputNumber.getText().toString())) {
            inputName.setError("Contact person name cannot be empty if you provide a contact person mobile number");
            return false;
        } else if (!TextUtils.isEmpty(inputName.getText().toString()) && TextUtils.isEmpty(inputNumber.getText().toString())) {
            inputNumber.setError("Contact person mobile number cannot be empty if you provide a contact person name");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Validate contact person details.
     *
     * @param inputContactPerson             Contact person name string
     * @param inputContactPersonMobileNumber Contact person mobile number string
     * @return return true if Both fields has value or if both fields are empty
     */
    public boolean validateContactPersonDetails(ProfileEditText inputContactPerson, ProfileEditText inputContactPersonMobileNumber) {
        if (TextUtils.isEmpty(inputContactPerson.getText()) && !TextUtils.isEmpty(inputContactPersonMobileNumber.getText())) {
            inputContactPerson.setError("Contact person name cannot be empty if you provide a contact person mobile number");
            return false;
        } else if (!TextUtils.isEmpty(inputContactPerson.getText()) && TextUtils.isEmpty(inputContactPersonMobileNumber.getText())) {
            inputContactPersonMobileNumber.setError("Contact person mobile number cannot be empty if you provide a contact person name");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if the {@link EditText} fields has no empty fields
     *
     * @param editTexts Form {@link EditText} fields
     * @return return true if a single EditText field is empty, otherwise false
     */
    public boolean hasEmptyField(ProfileEditText... editTexts) {
        for (ProfileEditText editText : editTexts) {
            CharSequence text = editText.getText();

            if (TextUtils.isEmpty(text)) {
                setError(editText, "This field is required");
                return true;
            }
        }
        return false;
    }

    /**
     * Display an error on the {@link EditText} view.
     *
     * @param editText {@link EditText} field that has error
     * @param error    Error String
     */
    private void setError(EditText editText, CharSequence error) {
        editText.setError(error);
        editText.requestFocus();
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Display an error on the {@link EditText} view.
     *
     * @param editText {@link EditText} field that has error
     * @param error    Error String
     */
    private void setError(ProfileEditText editText, String error) {
        editText.setError(error);
        editText.requestFocus();
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Convert mobile number format
     *
     * @param mobileNumber Mobile Number String
     * @return Formatted mobile number string
     */
    public String convertFormat(String mobileNumber) {
        if (!TextUtils.isEmpty(mobileNumber)) {
            try {
                Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(mobileNumber, "PH");

                String nationalFormat = phoneNumberUtil.format(phoneNumber, PhoneNumberFormat.NATIONAL);

                return nationalFormat.replaceAll("\\s+", "");
            } catch (NumberParseException e) {
                Timber.e(e, e.getMessage());
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * Check if entered password are the same
     *
     * @param password        Password String
     * @param confirmPassword Confirm Password String
     * @return Return true if passwords are the same
     */
    public boolean isPasswordSame(EditText password, EditText confirmPassword) {
        if (password.getText().toString().equals(confirmPassword.getText().toString())) {
            return true;
        }

        setError(confirmPassword, "Passwords do not match.");
        return false;
    }

    /**
     * Check if old password and new password are the same
     *
     * @param currentPassword Current password string
     * @param updatedPassword News password string
     * @return Return true if passwords are the same
     */
    public boolean validateCurrentAndUpdatedPassword(EditText currentPassword, EditText updatedPassword) {
        if (currentPassword.getText().toString().equals(updatedPassword.getText().toString())) {
            setError(updatedPassword, "New Password cannot be same as your current password.");
            return true;
        }

        return false;
    }

    /**
     * Check if any {@link AdvancedTextInputEditText} has an error message
     *
     * @param inputFields {@link AdvancedTextInputEditText} Form fields
     * @return Return true if a single {@link AdvancedTextInputEditText} has an error
     */
    public boolean hasError(AdvancedTextInputEditText... inputFields) {
        for (AdvancedTextInputEditText editText : inputFields) {
            if (editText.hasError()) {
                editText.requestFocus();
            }

            return editText.hasError();
        }

        return false;
    }

    /**
     * Check if any {@link ProfileEditText} has an error message
     *
     * @param inputFields {@link ProfileEditText} Form fields
     * @return Return true if a single {@link ProfileEditText} has an error
     */
    public boolean hasError(ProfileEditText... inputFields) {
        for (ProfileEditText editText : inputFields) {
            if (editText.hasError()) {
                editText.setRequestFocus();
                return editText.hasError();
            }
        }

        return false;
    }
}
