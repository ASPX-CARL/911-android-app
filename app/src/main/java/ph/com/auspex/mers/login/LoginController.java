package ph.com.auspex.mers.login;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.AdvancedTextInputEditText;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordController;
import ph.com.auspex.mers.profile.ProfileController;
import ph.com.auspex.mers.register.required.RequiredInformationController;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.FormValidationUtil;
import ph.com.auspex.mers.util.MaterialDialogManager;

/**
 * Provides a UI for the Login Feature.
 */
public class LoginController extends MosbyController<LoginContract.LoginView, LoginContract.LoginPresenter> implements LoginContract.LoginView {

    public static final String TAG = LoginController.class.getSimpleName();

    @BindView(R.id.input_email_address) AdvancedTextInputEditText inputEmailAddress;
    @BindView(R.id.input_password) AdvancedTextInputEditText inputPassword;
    @BindView(R.id.button_login) Button btnLogin;

    @Inject LoginContract.LoginPresenter presenter;
    @Inject FormValidationUtil validation;
    @Inject Router parentRouter;

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (presenter != null) {
            presenter.attachView(this);
        }
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.loginComponent(new LoginModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_login, container, false);
    }

    @NonNull
    @Override
    public LoginContract.LoginPresenter createPresenter() {
        return presenter;
    }

    /**
     * Disable the login button when there is an ongoing network request to avoid flooding the {@link ph.com.auspex.mers.data.remote.RemoteDataSource} with requests
     */
    @Override
    public void showLoading() {
        btnLogin.setEnabled(false);
        closeSoftKeyboard();
    }

    /**
     * Enable the login button after an API request
     */
    @Override
    public void hideLoading() {
        btnLogin.setEnabled(true);
    }

    /**
     * Close the soft keyboard programmatically
     */
    private void closeSoftKeyboard() {
        AppUtil.closeSoftkeyboard(activity);
    }

    /**
     * Opens {@link ProfileController} after successful login
     */
    @Override
    public void onLoginSuccessful() {
        getRouter().pushController(RouterTransaction.with(new ProfileController()));
    }

    /**
     * Display an error message if the login is failed
     *
     * @param message error message
     */
    @Override
    public void onLoginFailed(String message) {
        showError(message);
    }

    /**
     * Provide a listener if the user click the IME done on the keyboard after typing a password
     *
     * @return always <code>true</code>
     */
    @OnEditorAction(R.id.input_password)
    boolean onPressDone() {
        onLoginButtonClicked();
        return true;
    }

    /**
     * Submit the form to the API
     */
    @OnClick(R.id.button_login)
    void onLoginButtonClicked() {
        if (!validation.hasEmptyField(inputEmailAddress, inputPassword)) {
            presenter.login(inputEmailAddress.getText().toString(), inputPassword.getText().toString());
        }
    }

    /**
     * Opens {@link RequiredInformationController} for the registration process
     */
    @OnClick(R.id.tv_register)
    void onRegisterTextClicked() {
        setRetainViewMode(RetainViewMode.RETAIN_DETACH);
        parentRouter.pushController(RouterTransaction.with(new RequiredInformationController()).tag(RequiredInformationController.TAG));
    }

    /**
     * Opens {@link ForgotPasswordController} for the forgot password process
     */
    @OnClick(R.id.tv_forgot_password)
    void onForgotPasswordTextClicked() {
        setRetainViewMode(RetainViewMode.RETAIN_DETACH);
        parentRouter.pushController(RouterTransaction.with(new ForgotPasswordController()));
    }

    /**
     * Handles back button press event
     *
     * @return always <code>true</code>
     */
    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCloseApplicationDialog(activity);
        return true;
    }
}
