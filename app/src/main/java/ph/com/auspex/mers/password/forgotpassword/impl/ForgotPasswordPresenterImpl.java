package ph.com.auspex.mers.password.forgotpassword.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordContract.ForgotPasswordPresenter;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordContract.ForgotPasswordView;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordInteractor;

/**
 * Forgot Password presenter implementation
 */
public class ForgotPasswordPresenterImpl extends MvpBasePresenter<ForgotPasswordView> implements ForgotPasswordPresenter {

    private final ForgotPasswordInteractor model;

    public ForgotPasswordPresenterImpl(ForgotPasswordInteractor model) {
        this.model = model;
    }

    /**
     * Send data to API
     *
     * @param emailAddress User input string
     */
    @Override
    public void forgotPassword(String emailAddress) {
        getView().showLoading();

        model.forgotPassword(emailAddress, new BaseInteractor.ResponseListener<ForgotPasswordResponse>() {
            @Override
            public void onSuccess(ForgotPasswordResponse forgotPasswordResponse) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onSuccessful(forgotPasswordResponse);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
