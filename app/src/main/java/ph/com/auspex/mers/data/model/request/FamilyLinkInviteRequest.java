package ph.com.auspex.mers.data.model.request;

import java.util.HashMap;
import java.util.Map;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Create Family Link Invite Request
 */
public class FamilyLinkInviteRequest {

    private String inviteCode;
    private String inviterId;
    private String relationshipId;

    private FamilyLinkInviteRequest(Builder builder) {
        this.inviteCode = checkNotNull(builder.inviteCode);
        this.inviterId = checkNotNull(builder.inviterId);
        this.relationshipId = checkNotNull(builder.relationshipId);
    }

    public static class Builder {
        private String inviteCode;
        private String inviterId;
        private String relationshipId;

        public Builder inviteCode(String inviteCode) {
            this.inviteCode = inviteCode;
            return this;
        }

        public Builder inviterId(String inviterId) {
            this.inviterId = inviterId;
            return this;
        }

        public Builder relationshipId(String relationshipId) {
            this.relationshipId = relationshipId;
            return this;
        }

        public FamilyLinkInviteRequest build() {
            return new FamilyLinkInviteRequest(this);
        }
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public Map<String, String> createRequest() {
        Map<String, String> request = new HashMap<>();

        request.put("invite_code", inviteCode);
        request.put("inviter_id", inviterId);
        request.put("relationship_id", relationshipId);

        return request;
    }
}
