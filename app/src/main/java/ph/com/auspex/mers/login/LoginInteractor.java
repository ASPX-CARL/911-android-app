package ph.com.auspex.mers.login;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.AccessToken;

public interface LoginInteractor extends BaseInteractor {

    void login(String email, String password, ResponseListener<AccessToken> listener);
}
