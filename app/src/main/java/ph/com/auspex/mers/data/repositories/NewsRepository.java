package ph.com.auspex.mers.data.repositories;

import java.util.List;

import io.reactivex.Observable;
import ph.com.auspex.mers.data.model.response.Advertisement;
import ph.com.auspex.mers.data.model.response.Feed;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;

public interface NewsRepository {

    Observable<List<Feed>> getNewsFeeds();

    Observable<List<Advertisement>> getAdvertisements();

    Observable<InternalNews> getInternalNews(int currentPage);

    Observable<InternalFeed> getSpecificInternalNews(int id);
}
