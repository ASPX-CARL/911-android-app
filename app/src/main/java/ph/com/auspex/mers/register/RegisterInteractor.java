package ph.com.auspex.mers.register;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.RegisterRequest;

public interface RegisterInteractor extends BaseInteractor {

    void register(RegisterRequest registerRequest, ResponseListener<String> listener);
}
