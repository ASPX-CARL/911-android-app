package ph.com.auspex.mers.data.model.response;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import ph.com.auspex.mers.data.model.request.GenderEnum;

public class FamilyLinkDetail implements Parcelable {

    @SerializedName("user_id")
    private String userId;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("middle_name")
    private String middleName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("address")
    private String address;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("relationship")
    private String relationship;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("online_availability_status")
    private String status;
    @SerializedName("gender")
    private GenderEnum genderEnum;

    private LatLng currentLocation;
    private boolean isOnline;

    protected FamilyLinkDetail(Parcel in) {
        userId = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        lastName = in.readString();
        address = in.readString();
        mobileNumber = in.readString();
        imageUrl = in.readString();
        relationship = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        status = in.readString();
        currentLocation = in.readParcelable(LatLng.class.getClassLoader());
        isOnline = in.readByte() != 0;
    }

    public static final Creator<FamilyLinkDetail> CREATOR = new Creator<FamilyLinkDetail>() {
        @Override
        public FamilyLinkDetail createFromParcel(Parcel in) {
            return new FamilyLinkDetail(in);
        }

        @Override
        public FamilyLinkDetail[] newArray(int size) {
            return new FamilyLinkDetail[size];
        }
    };

    private FamilyLinkDetail(Builder builder) {
        this.firstName = builder.firstName;
        this.middleName = builder.middleName;
        this.lastName = builder.lastName;
        this.relationship = builder.relationship;
        this.imageUrl = builder.imageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getRelationship() {
        return relationship;
    }

    public GenderEnum getGender() {
        return genderEnum;
    }

    public LatLng getCurrentLocation() {
        if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
            return new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        } else {
            return null;
        }
    }

    public boolean isOnline() {
        return status.equals("ONLINE");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(lastName);
        dest.writeString(address);
        dest.writeString(mobileNumber);
        dest.writeString(imageUrl);
        dest.writeString(relationship);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(status);
        dest.writeParcelable(currentLocation, flags);
        dest.writeByte((byte) (isOnline ? 1 : 0));
    }


    public static class Builder {
        private String firstName;
        private String middleName;
        private String lastName;
        private String relationship;
        private String imageUrl;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder relationship(String relationship) {
            this.relationship = relationship;
            return this;
        }

        public Builder imageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public FamilyLinkDetail build() {
            return new FamilyLinkDetail(this);
        }
    }

    @Override
    public String toString() {
        return "FamilyLinkDetail{" +
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
