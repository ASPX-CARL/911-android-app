package ph.com.auspex.mers.register;

import dagger.Subcomponent;
import ph.com.auspex.mers.register.optional.MedicalInformationController;
import ph.com.auspex.mers.register.optional.OptionalInformationController;
import ph.com.auspex.mers.register.required.RequiredInformationController;

@RegisterScope
@Subcomponent(modules = RegisterModule.class)
public interface RegisterComponent {
    void inject(RequiredInformationController requiredInformationController);

    void inject(OptionalInformationController optionalInformationController);

    void inject(MedicalInformationController medicalInformationController);
}
