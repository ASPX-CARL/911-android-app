package ph.com.auspex.mers.common.mvp.interactor.impl;

import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import timber.log.Timber;

/**
 * BaseInteractor Implementation
 */
public abstract class BaseInteractorImpl implements BaseInteractor {

    protected boolean isRequestInProgress = false;

    /**
     * Transforms an observable to subscribeOn a new thread and observed on {@link AndroidSchedulers#mainThread()}
     */
    protected final ObservableTransformer schedulersTransformer = observable -> observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());

    /**
     * Apply schedulersTransformer on an observable using {@link io.reactivex.Observable#compose(ObservableTransformer)}
     *
     * @param <T> Observable generic
     * @return Transformed Observable
     */
    @SuppressWarnings("unchecked")
    protected <T> ObservableTransformer<T, T> applySchedulers() {
        return (ObservableTransformer<T, T>) schedulersTransformer;
    }

    /**
     * Method that manipulates emitted Throwable from an Observable
     *
     * @param listener Presenter Listener
     * @return Throwable
     */
    protected Consumer<Throwable> consumeOnError(ResponseListener listener) {
        return throwable -> {
            isRequestInProgress = false;
            listener.onFailure(throwable.getMessage());
            Timber.e(throwable);
        };
    }
}
