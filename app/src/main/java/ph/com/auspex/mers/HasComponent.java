package ph.com.auspex.mers;

public interface HasComponent<C> {

    C getComponent();
}
