package ph.com.auspex.mers.password.forgotpassword;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.AdvancedTextInputEditText;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordContract.ForgotPasswordPresenter;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordContract.ForgotPasswordView;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.util.FormValidationUtil;
import ph.com.auspex.mers.verification.VerificationController;

/**
 * Forgot password controller UI
 */
public class ForgotPasswordController extends MosbyController<ForgotPasswordView, ForgotPasswordPresenter> implements ForgotPasswordView {

    @BindView(R.id.input_email_address) AdvancedTextInputEditText inputEmailAddress;

    @BindString(R.string.label_forgot_password) String title;

    @Inject ForgotPasswordPresenter presenter;
    @Inject FormValidationUtil validationUtil;
    @Inject Router parentRouter;

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.forgotPasswordComponent(new ForgotPasswordModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_forgot_password, container, false);
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    @Override
    protected boolean showNavigationIcon() {
        return true;
    }

    @Override
    protected void onClickNavigationIcon() {
        getRouter().popController(this);
    }

    @NonNull
    @Override
    public ForgotPasswordPresenter createPresenter() {
        return presenter;
    }

    /**
     * Request for a new password using the email provided {@link #inputEmailAddress}
     */
    @OnClick(R.id.button_submit)
    void onSubmitButtonClicked() {
        if (!validationUtil.hasEmptyField(inputEmailAddress) && !validationUtil.hasError(inputEmailAddress)) {
            presenter.forgotPassword(inputEmailAddress.getText().toString());
        }
    }

    /**
     * If the email address is found on the database
     *
     * @param forgotPasswordResponse {@link ForgotPasswordResponse} that contains the userId and the response message
     */
    @Override
    public void onSuccessful(ForgotPasswordResponse forgotPasswordResponse) {
        Bundle args = new Bundle();
        args.putString(VerificationController.KEY_USER_ID, forgotPasswordResponse.getUserId());
        args.putSerializable(VerificationController.KEY_VERIFICATION_TYPE, VerificationRequest.VerificationType.PASSWORD);

        new MaterialDialog.Builder(activity)
                .content(forgotPasswordResponse.getMessage())
                .positiveText("Proceed")
                .onPositive((dialog, which) -> parentRouter.pushController(RouterTransaction.with(new VerificationController(args))))
                .positiveColorRes(R.color.app_blue)
                .show();
    }
}
