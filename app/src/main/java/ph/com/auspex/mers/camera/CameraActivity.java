package ph.com.auspex.mers.camera;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.parameter.update.UpdateRequest;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.view.CameraView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.views.FlashToggleButton;
import ph.com.auspex.mers.util.AppUtil;
import timber.log.Timber;

import static io.fotoapparat.log.Loggers.fileLogger;
import static io.fotoapparat.log.Loggers.logcat;
import static io.fotoapparat.log.Loggers.loggers;
import static io.fotoapparat.parameter.selector.AspectRatioSelectors.standardRatio;
import static io.fotoapparat.parameter.selector.FlashSelectors.autoFlash;
import static io.fotoapparat.parameter.selector.FlashSelectors.off;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.autoFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.continuousFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.fixed;
import static io.fotoapparat.parameter.selector.LensPositionSelectors.back;
import static io.fotoapparat.parameter.selector.LensPositionSelectors.front;
import static io.fotoapparat.parameter.selector.Selectors.firstAvailable;
import static io.fotoapparat.parameter.selector.SizeSelectors.biggestSize;

/**
 * App Internal Camera
 */
public class CameraActivity extends AppCompatActivity implements FlashToggleButton.StateChangeListener {

    public static final String TAG = CameraActivity.class.getSimpleName();

    public static final String KEY_FLASH = TAG + ".flash";
    public static final String KEY_FILE = TAG + ".file";

    @BindView(R.id.camera_view) CameraView cameraView;
    @BindView(R.id.result) ImageView imageView;
    @BindView(R.id.flash) FlashToggleButton btnFlash;
    @BindView(R.id.shutter) ImageButton shutter;
    @BindView(R.id.layout_preview) RelativeLayout layoutPreview;

    private Unbinder unbinder;

    private Fotoapparat backFotoapparat;
    private File tempFile;
    String mCurrentPhotoPath;
    private boolean hasFlash;

    /**
     * Initialize activity components on activity onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        unbinder = ButterKnife.bind(this);

        backFotoapparat = createFotoapparat();

        hasFlash = AppUtil.hasFlash(this);

        btnFlash.setListener(this);

        if (!hasFlash) {
            btnFlash.setVisibility(View.INVISIBLE);
        }

        if (getIntent() != null) {
            btnFlash.setVisibility(getIntent().getBooleanExtra(KEY_FLASH, true) ? View.VISIBLE : View.INVISIBLE);
        }
    }

    /**
     * Captures an image using {@link Fotoapparat#takePicture()} then save it to a file and display the image using the imageView
     */
    private void takePicture() {
        PhotoResult photoResult = backFotoapparat.takePicture();

        try {
            tempFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tempFile != null) {
            photoResult
                    .saveToFile(tempFile)
                    .whenAvailable(aVoid -> {
                        Glide.with(CameraActivity.this)
                                .load(tempFile)
                                .into(imageView);

                        layoutPreview.setVisibility(View.VISIBLE);
                    });
        } else {
            Toast.makeText(this, "File Not Created!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Returns {@link Fotoapparat} instance using the following configuration
     * <p>
     * See <a href="https://github.com/Fotoapparat/Fotoapparat">Fotoapparat</a>
     *
     * @return {@link Fotoapparat} instance
     */
    private Fotoapparat createFotoapparat() {
        return Fotoapparat
                .with(this)
                .into(cameraView)
                .photoSize(standardRatio(biggestSize()))
                .lensPosition(firstAvailable(back(), front()))
                .focusMode(firstAvailable(continuousFocus(), autoFocus(), fixed()))
                .flash(hasFlash ? autoFlash() : off())
                .frameProcessor(new SampleFrameProcessor())
                .logger(loggers(
                        logcat(),
                        fileLogger(this)
                ))
                .cameraErrorCallback(Timber::e)
                .build();
    }

    /**
     * Start camera on Activity Start
     */
    @Override
    protected void onStart() {
        super.onStart();
        backFotoapparat.start();
    }

    /**
     * Stop or Close camera on Activity Stop
     */
    @Override
    protected void onStop() {
        backFotoapparat.stop();
        super.onStop();
    }

    /**
     * Unbind bound ui views
     */
    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    /**
     * Update Camera flash states
     *
     * @param state Camera flash state
     */
    @Override
    public void onStateChanged(FlashToggleButton.FlashState state) {
        backFotoapparat.updateParameters(
                UpdateRequest.builder()
                        .flash(state.getFlash())
                        .build()
        );
    }

    /**
     * Provide a sample frame processor
     */
    private class SampleFrameProcessor implements FrameProcessor {
        @Override
        public void processFrame(Frame frame) {
            // Perform frame processing, if needed
        }
    }

    /**
     * Capture an image using the camera
     */
    @OnClick(R.id.shutter)
    void onShutterClicked() {
        takePicture();
    }

    /**
     * Focus camera before taking an image using the camera
     */
    @OnClick(R.id.camera_view)
    void onFocus() {
        backFotoapparat.autoFocus();
    }

    /**
     * Retake a photo and delete the created file
     */
    @OnClick(R.id.btn_retake)
    void onRetryClicked() {
        layoutPreview.setVisibility(View.GONE);
        imageView.setImageBitmap(null);

        if (tempFile.delete()) {
            tempFile = null;
            Timber.d("Delete file success!");
        }
    }

    /**
     * Use the captured image and return the file to the previous activity and controller
     */
    @OnClick(R.id.btn_use)
    void onOkClicked() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(KEY_FILE, tempFile.getAbsolutePath());
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    /**
     * Create a file where the image capture will be saved
     *
     * @return {@link File}
     * @throws IOException if file cannot be created
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Close camera activity
     */
    @OnClick(R.id.back)
    void closeCamera() {
        onBackPressed();
    }
}
