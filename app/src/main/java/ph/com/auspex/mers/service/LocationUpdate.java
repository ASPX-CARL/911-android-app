package ph.com.auspex.mers.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import ph.com.auspex.mers.util.AppUtil;
import timber.log.Timber;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class LocationUpdate extends LocationCallback implements OnSuccessListener<Location>, OnFailureListener {

    private Activity activity;
    private EventBus eventBus;
    private LocationRequest locationRequest;
    private LocationServicePresenter presenter;
    private long apiUpdateInterval;

    private Location lastKnownLocation;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LocationUpdate(Activity activity, EventBus eventBus, LocationRequest locationRequest, LocationServicePresenter presenter, long apiUpdateInterval) {
        this.activity = activity;
        this.eventBus = eventBus;
        this.locationRequest = locationRequest;
        this.presenter = presenter;
        this.apiUpdateInterval = apiUpdateInterval;

        getLastKnownLocation();
    }

    @SuppressLint("MissingPermission")
    private void getLastKnownLocation() {
        FusedLocationProviderClient locationClient = getFusedLocationProviderClient(activity);

        locationClient.getLastLocation().addOnSuccessListener(this).addOnFailureListener(this);
    }

    @Override
    public void onLocationResult(LocationResult locationResult) {
        onLocationChanged(locationResult.getLastLocation());
    }

    private void onLocationChanged(Location lastLocation) {
        eventBus.post(new LocationUpdateEvent(lastLocation));
        presenter.updateCurrentLocation(lastLocation);
    }

    @Override
    public void onLocationAvailability(LocationAvailability locationAvailability) {
        super.onLocationAvailability(locationAvailability);
    }

    @SuppressLint("MissingPermission")
    public void startService() {
        getFusedLocationProviderClient(activity).requestLocationUpdates(locationRequest, this, Looper.myLooper());
    }

    public void stopService() {
        getFusedLocationProviderClient(activity).removeLocationUpdates(this);
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Timber.e(e, e.getMessage());
    }

    @Override
    public void onSuccess(Location location) {
        if (location != null) {
            this.lastKnownLocation = location;

            onLocationChanged(location);

            buildObservableInterval();
        }
    }

    private void buildObservableInterval() {
        compositeDisposable.add(Observable.interval(0, apiUpdateInterval, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Long>() {
                    @Override
                    public void onNext(Long aLong) {
                        if (lastKnownLocation != null && AppUtil.checkGpsEnabled(activity)) {
                            presenter.updateCurrentLocation(lastKnownLocation);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }
}
