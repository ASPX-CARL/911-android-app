package ph.com.auspex.mers.data.remote;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;
import ph.com.auspex.mers.BuildConfig;
import ph.com.auspex.mers.data.DataSourceRepository;
import ph.com.auspex.mers.data.local.RealmController;
import ph.com.auspex.mers.data.model.request.DirectionsRequest;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.request.FamilyLinkInviteRequest;
import ph.com.auspex.mers.data.model.request.LoginRequest;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.data.model.request.UpdateLocationRequest;
import ph.com.auspex.mers.data.model.request.UpdatePasswordRequest;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.data.model.response.AccessToken;
import ph.com.auspex.mers.data.model.response.Advertisement;
import ph.com.auspex.mers.data.model.response.ApiResponse;
import ph.com.auspex.mers.data.model.response.EmergencyType;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.data.model.response.Feed;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;
import ph.com.auspex.mers.data.model.response.Relationship;
import ph.com.auspex.mers.data.model.response.UserDetail;
import ph.com.auspex.mers.data.remote.exception.ApiRequestException;

/**
 * Api Response Observables
 */
public class RemoteDataSource extends BaseRemoteDataSource implements DataSourceRepository {

    private final ApiService apiService;
    private final RealmController realmController;

    /**
     * Constructs a RemoteDataSource object using {@link ApiService} for remote requests and {@link RealmController} for local requests
     *
     * @param apiService      interface for remote/api requests
     * @param realmController helper class for managing {@link io.realm.RealmObject}
     */
    public RemoteDataSource(ApiService apiService, RealmController realmController) {
        this.apiService = apiService;
        this.realmController = realmController;
    }

    /**
     * Returns an {@link Observable} from the API and save it to the {@link Realm}
     *
     * @return observable that emits a {@link List} of {@link Relationship}
     */
    @Override
    public Observable<List<Relationship>> getFamilyRelationships() {
        return execute(apiService.getRelationshipTypes())
                .doOnNext(relationships -> {
                    Realm realm = Realm.getDefaultInstance();
                    realmController.addOrUpdateToRealm(realm, relationships);
                    realm.close();
                });
    }

    /**
     * Returns an {@link Observable} from the API
     *
     * @return observable that emits a {@link List} of {@link Feed}
     */
    @Override
    public Observable<List<Feed>> getNewsFeeds() {
        return execute(apiService.getNewsList()).map(news -> {
            List<Feed> feeds = new ArrayList<>();

            for (int i = 0; i < news.size(); i++) {
                feeds.addAll(news.get(i).getFeeds());
            }

            return feeds;
        });
    }

    /**
     * Returns an {@link Observable} from the API
     *
     * @return observable that emits a {@link List} of {@link Advertisement}
     */
    @Override
    public Observable<List<Advertisement>> getAdvertisements() {
        return execute(apiService.getAdvertisements());
    }

    /**
     * Returns an observable from the API
     *
     * @param currentPage current page of the list
     * @return observable that emits a paged {@link InternalNews}
     */
    @Override
    public Observable<InternalNews> getInternalNews(int currentPage) {
        return execute(apiService.getInternalNews(Integer.toString(currentPage)));
    }

    /**
     * Returns an observable from the API
     *
     * @param id id of a specific internal news feed
     * @return observable that emits a {@link InternalFeed}
     */
    @Override
    public Observable<InternalFeed> getSpecificInternalNews(int id) {
        return execute(apiService.getSpecificInternalNews(Integer.toString(id))).map(internalNews -> internalNews.getFeeds().get(0));
    }

    /**
     * Returns an observable from the API
     *
     * @param request login request
     * @return observable that emits an {@link AccessToken}
     */
    @Override
    public Observable<AccessToken> login(LoginRequest request) {
        return execute(apiService.login(request.create()));
    }

    /**
     * Returns an observable from the API
     *
     * @param request register request
     * @return observable that emits a {@link String} message of a successful API requests
     */
    @Override
    public Observable<String> register(RegisterRequest request) {
        return execute(apiService.register(request.create())).map(userDetail -> Integer.toString(userDetail.getUserId()));
    }

    /**
     * Returns an observable from the API
     *
     * @param userId user id of the logged in user
     * @return observable that emits a {@link AccessToken}
     */
    @Override
    public Observable<AccessToken> getUserDetails(String userId) {
        return apiService.getUserDetails(userId)
                .onErrorResumeNext(passApiException())
                .map(apiResponse -> {
                    if (apiResponse.isStatus()) {
                        return apiResponse.getData();
                    }

                    throw new ApiRequestException(apiResponse.getMessage(), apiResponse.getCode());
                });
    }

    /**
     * Returns an observable from the API
     *
     * @param request verification code request
     * @return observable that emits a {@link String} message of a successful API requests
     */
    @Override
    public Observable<String> verifyCode(VerificationRequest request) {
        return apiService.verifyCode(request.create())
                .onErrorResumeNext(passApiException())
                .map(apiResponse -> {
                    if (apiResponse.isStatus()) {
                        return apiResponse.getMessage();
                    }
                    throw new ApiRequestException(apiResponse.getMessage(), apiResponse.getCode());
                });
    }

    /**
     * Returns an observable from the API
     *
     * @param userId user id of the logged in user
     * @return observable that emits a {@link String} message of a successful API requests
     */
    @Override
    public Observable<String> logout(String userId) {
        return executeApiGetMessage(apiService.logout(userId));
    }

    /**
     * Returns an observable from the API
     *
     * @param userDetail of the logged-in account
     * @return observable that emits an {@link AccessToken}
     */
    @Override
    public Observable<AccessToken> updateProfile(UserDetail userDetail) {
        return execute(apiService.updateProfile(userDetail.createRequest()));
    }

    /**
     * Returns an observable from the API
     *
     * @param emailAddress email address of the user who request for the new password
     * @return observable that emits a {@link ForgotPasswordResponse}
     */
    @Override
    public Observable<ForgotPasswordResponse> forgotPassword(String emailAddress) {
        return executeResponse(apiService.forgotPassword(emailAddress));
    }

    /**
     * Returns an observable from the API
     *
     * @param request update password request
     * @return observable that emits an {@link AccessToken}
     */
    @Override
    public Observable<AccessToken> updatePassword(UpdatePasswordRequest request) {
        return execute(apiService.changePassword(request.create()));
    }

    /**
     * Returns an observable from the API
     *
     * @param updateLocationRequest update user's current location in the API request
     * @return observable that emits <code>true</code> if the requests is successful; <code>false</code> otherwise
     */
    @Override
    public Observable<Boolean> updateLocation(UpdateLocationRequest updateLocationRequest) {
        return executeApiResponse(apiService.updateLocation(updateLocationRequest.createQuery()))
                .map(apiResponse -> {
                    if (apiResponse.isStatus()) {
                        return apiResponse.isStatus();
                    }

                    throw new ApiRequestException(apiResponse.getMessage());
                });
    }

    /**
     * Returns a observable from the API
     *
     * @param userId user id of the logged in account
     * @return observable that emits the {@link List} of {@link FamilyLinkDetail}
     */
    @Override
    public Observable<List<FamilyLinkDetail>> getFamilyLinks(String userId) {
        return execute(apiService.getFamilyLink(userId)).map(familyLinkDetails -> {
            if (familyLinkDetails != null && familyLinkDetails.size() > 0) {
                return familyLinkDetails;
            } else {
                return new ArrayList<>();
            }
        });
    }

    /**
     * Returns a observable from the API
     *
     * @param request family link invitation request
     * @return observable that emits a {@link String} message of a successful API requests
     */
    @Override
    public Observable<String> addFamilyLink(FamilyLinkInviteRequest request) {
        return executeApiGetMessage(apiService.addFamilyLink(request.createRequest()));
    }

    /**
     * Returns a observable from the API
     *
     * @param userId user id of the logged in account
     * @return observable that emits the {@link List} of {@link FamilyLinkDetail}
     */
    @Override
    public Observable<List<FamilyLinkDetail>> getFamilyInvites(String userId) {
        return execute(apiService.getFamilyLinkInvites(userId));
    }

    /**
     * Returns a observable from the API
     *
     * @param inviterId user id of the logged in account
     * @param invitorId user id of the user that will receive the family link request
     * @return observable that emits a {@link String} message of a successful API requests
     */
    @Override
    public Observable<String> acceptInvite(String inviterId, String invitorId) {
        return executeApiResponse(apiService.acceptInvite(inviterId, invitorId)).map(ApiResponse::getMessage);
    }

    /**
     * Returns a observable from the API
     *
     * @param inviterId user id of the logged in account
     * @param invitorId user id of the user that will receive the family link request
     * @return observable that emits a {@link String} message of a successful API requests
     */
    @Override
    public Observable<String> rejectInvite(String inviterId, String invitorId) {
        return executeApiResponse(apiService.rejectInvite(inviterId, invitorId)).map(ApiResponse::getMessage);
    }

    /**
     * What?
     *
     * @return null
     */
    @Override
    public Observable<List<Relationship>> getRelationshipTypes() {
        return null;
    }

    /**
     * Returns an observable from the API
     *
     * @param request nearby place request
     * @return observable that emits a {@link List} of {@link NearbyPlace}
     */
    @Override
    public Observable<List<NearbyPlace>> getNearbyPlacesByType(NearbyPlaceRequest request) {
        return execute(apiService.getNearbyPlaces(request.create()));
    }

    /**
     * Returns an observable from the API
     *
     * @param placeId nearby place id
     * @return observable that emits {@link NearbyPlaceDetail}
     */
    @Override
    public Observable<NearbyPlaceDetail> getNearbyPlaceDetail(String placeId) {
        return execute(apiService.getNearbyPlaceDetails(placeId));
    }

    /**
     * @param request goodluck
     * @return sad life
     * @deprecated Feature is not used anymore
     */
    @Override
    public Observable<MapDirection> getDirections(DirectionsRequest request) {
        return executeResponse(apiService.getDirection(BuildConfig.GOOGLE_MAP_API_ENDPOINT, request.create()));
    }

    /**
     * Returns an {@link Observable} from the API and save it to the {@link Realm}
     *
     * @return observable that emits a {@link List} of {@link EmergencyType}
     */
    @Override
    public Observable<List<EmergencyType>> getEmergencyTypes() {
        return execute(apiService.getEmergencyTypes())
                .doOnNext(emergencyTypes -> {
                    Realm realm = Realm.getDefaultInstance();
                    realmController.addOrUpdateToRealm(realm, emergencyTypes);
                    realm.close();
                });
    }

    /**
     * Returns an observable from the API
     *
     * @param request Emergency Report request
     * @return observable that emits a {@link String} message of a successful API requests
     */
    @Override
    public Observable<String> reportEmergency(EmergencyRequest request) {
        return executeApiGetMessage(apiService.reportEmergency(request.create()));
    }
}