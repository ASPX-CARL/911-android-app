package ph.com.auspex.mers.password.updatepassword;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;

public interface UpdatePasswordContract {

    interface UpdatePasswordPresenter extends MvpPresenter<UpdatePasswordView> {
        void updatePassword(String currentPassword, String updatedPassword);
    }

    interface UpdatePasswordView extends RemoteView {
        void onPasswordUpdated(String message);
    }
}
