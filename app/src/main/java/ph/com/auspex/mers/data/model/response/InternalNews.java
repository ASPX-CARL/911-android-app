package ph.com.auspex.mers.data.model.response;

import android.util.SparseArray;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Provide a POJO for {@link ph.com.auspex.mers.news.internal.adapter.InternalNewsAdapter}
 */
public class InternalNews {

    @SerializedName("page_no")
    private int pageCount;
    @SerializedName("current_page")
    private int currentPage;
    @SerializedName("feeds")
    private List<InternalFeed> feeds;
    @Expose(deserialize = false, serialize = false)
    private SparseArray<Object> mergedFeedList;

    /**
     * Constructs a InternalNews with {@link #currentPage} set to 0
     */
    public InternalNews() {
        this.currentPage = 0;
    }

    /**
     * Returns the total page count for the list
     *
     * @return total page count
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * Returns the current page the list is on
     *
     * @return current page
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * Return all the internal news from the API
     *
     * @return internal news list from the API
     */
    public List<InternalFeed> getFeeds() {
        return feeds;
    }

    /**
     * Sets the current page where the list is on
     *
     * @param currentPage current page of the list
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Sets and append the new list downloaded from the API
     *
     * @param feeds new set of internal news from the API
     */
    public void setFeeds(List<InternalFeed> feeds) {
        this.feeds = feeds;
    }

    /**
     * Returns a merged list of {@link InternalFeed} and {@link Advertisement}
     *
     * @return merged list of internal news feed and advertisements
     */
    public SparseArray<Object> getMergedFeedList() {
        return mergedFeedList;
    }

    /**
     * Sets and updates the current list of {@link InternalFeed} and {@link Advertisement}
     *
     * @param mergedFeedList new set of internal news and advertisement from the API
     */
    public void setMergedFeedList(SparseArray<Object> mergedFeedList) {
        this.mergedFeedList = mergedFeedList;
    }
}