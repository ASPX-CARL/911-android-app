package ph.com.auspex.mers.common.adapter;

/**
 * ItemClickListener is an abstract class that accepts any {@link Object}
 * <p>
 * This class is usually used in {@link android.support.v7.widget.RecyclerView.Adapter}
 *
 * @param <T> any object returned from the listener
 */
public interface ItemClickListener<T> {

    /**
     * Listener on {@link android.support.v7.widget.RecyclerView} item click
     *
     * @param t object returned from the listener
     */
    void onItemClicked(T t);
}
