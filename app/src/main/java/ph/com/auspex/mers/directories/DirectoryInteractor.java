package ph.com.auspex.mers.directories;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;

public interface DirectoryInteractor extends BaseInteractor {
    void getNearbyPlaces(NearbyPlaceRequest.PlaceType placeType, ResponseListener<List<NearbyPlace>> listener);

    void getLastKnownLocation(ResponseListener<LatLng> listener);

    void getNearbyPlaceDetail(String placeId, ResponseListener<NearbyPlaceDetail> listener);

    void getDirections(LatLng destination, ResponseListener<MapDirection> listener);
}
