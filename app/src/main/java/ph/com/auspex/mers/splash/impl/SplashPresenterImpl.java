package ph.com.auspex.mers.splash.impl;

import android.os.Handler;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.splash.SplashContract;
import ph.com.auspex.mers.splash.SplashInteractor;

public class SplashPresenterImpl extends MvpBasePresenter<SplashContract.SplashView> implements SplashContract.SplashPresenter {

    private final SplashInteractor model;

    public SplashPresenterImpl(SplashInteractor model) {
        this.model = model;
    }

    /**
     * Sync relationship type upon open with a delay of 2000ms
     *
     * @param view
     */
    @Override
    public void attachView(SplashContract.SplashView view) {
        super.attachView(view);

        new Handler().postDelayed(this::getRelationshipTypes, 2000);
    }

    /**
     * Sync relationship types upon app open
     */
    @Override
    public void getRelationshipTypes() {
        model.sync(new SplashInteractor.ResponseListener() {
            @Override
            public void onSuccess() {
                if (isViewAttached()) {
                    getView().onSyncComplete();
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().onSyncHasError();
                }
            }
        });
    }
}
