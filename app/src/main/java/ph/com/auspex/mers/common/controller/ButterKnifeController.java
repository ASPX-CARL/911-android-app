package ph.com.auspex.mers.common.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.RestoreViewOnCreateController;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class ButterKnifeController extends RestoreViewOnCreateController {

    private Unbinder unbinder;

    public ButterKnifeController() {
    }

    public ButterKnifeController(@Nullable Bundle args) {
        super(args);
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedViewState) {
        View view = inflateView(inflater, container);
        unbinder = ButterKnife.bind(this, view);
        onViewReady(view, savedViewState);
        return view;
    }

    protected void onViewReady(View view, Bundle savedViewState) {

    }

    protected abstract View inflateView(LayoutInflater inflater, ViewGroup container);

    @Override
    protected void onDestroyView(@NonNull View view) {
        super.onDestroyView(view);
        unbinder.unbind();
        unbinder = null;
    }
}
