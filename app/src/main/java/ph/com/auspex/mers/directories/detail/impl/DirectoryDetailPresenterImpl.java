package ph.com.auspex.mers.directories.detail.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;
import ph.com.auspex.mers.directories.DirectoryContract.DirectoryDetailView;
import ph.com.auspex.mers.directories.DirectoryInteractor;

import static ph.com.auspex.mers.directories.DirectoryContract.DirectoryDetailPresenter;

/**
 * Provide an implementation for {@link DirectoryDetailPresenter}
 */
public class DirectoryDetailPresenterImpl extends MvpBasePresenter<DirectoryDetailView> implements DirectoryDetailPresenter {

    private final DirectoryInteractor model;

    public DirectoryDetailPresenterImpl(DirectoryInteractor model) {
        this.model = model;
    }

    /**
     * Get the nearby place details using Google Places API {@link ph.com.auspex.mers.data.remote.ApiService#getNearbyPlaceDetails(String)}
     *
     * @param placeId nearby place id
     */
    @Override
    public void getNearbyPlaceDetail(String placeId) {
        getView().showLoading();

        model.getNearbyPlaceDetail(placeId, new BaseInteractor.ResponseListener<NearbyPlaceDetail>() {
            @Override
            public void onSuccess(NearbyPlaceDetail nearbyPlaceDetail) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().displayDetail(nearbyPlaceDetail);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
