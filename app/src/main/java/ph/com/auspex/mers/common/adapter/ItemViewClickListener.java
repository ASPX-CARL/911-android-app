package ph.com.auspex.mers.common.adapter;

import android.view.View;

/**
 * This class accepts any generic and returns it with the view, usually a {@link android.support.v7.widget.RecyclerView.ViewHolder} parent view or {@link android.view.ViewGroup}
 *
 * @param <T> any object returned from the listener
 */
public interface ItemViewClickListener<T> {

    /**
     * Listener on {@link android.support.v7.widget.RecyclerView} on item click
     *
     * @param t    object returned from the listener
     * @param view parent view {@link android.support.v7.widget.RecyclerView.ViewHolder}
     */
    void onItemClicked(T t, View view);
}
