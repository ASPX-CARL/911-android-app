package ph.com.auspex.mers.data.bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Event Bus Module
 */
@Module
public class BusModule {

    /**
     * Provide an Event Bus made with RxJava {@link io.reactivex.subjects.PublishSubject}
     *
     * @return {@link RxEventBus} singleton object
     */
    @Provides
    @Singleton
    RxEventBus providesEventBus() {
        return new RxEventBus();
    }
}
