package ph.com.auspex.mers.data.model.response;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.PolyUtil;

import java.util.List;

public class MapDirection {

    @SerializedName("points")
    private String polylinePoints;

    public MapDirection(String polylinePoints) {
        this.polylinePoints = polylinePoints;
    }

    public List<LatLng> getPolylinePoints() {
        return PolyUtil.decode(polylinePoints);
    }
}
