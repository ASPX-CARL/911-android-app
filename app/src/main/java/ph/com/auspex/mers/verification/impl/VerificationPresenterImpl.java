package ph.com.auspex.mers.verification.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.verification.VerificationContract;
import ph.com.auspex.mers.verification.VerificationInteractor;

/**
 * Verification Presenter Implementation
 */
public class VerificationPresenterImpl extends MvpBasePresenter<VerificationContract.VerificationView> implements VerificationContract.VerificationPresenter {

    private final VerificationInteractor model;

    public VerificationPresenterImpl(VerificationInteractor model) {
        this.model = model;
    }

    /**
     * Verify registration code
     *
     * @param code             registration code value
     * @param userId           userId
     * @param verificationType verificationType from API
     */
    @Override
    public void verifyCode(String code, String userId, VerificationRequest.VerificationType verificationType) {
        VerificationRequest request = new VerificationRequest.Builder()
                .userId(userId)
                .code(code)
                .type(verificationType)
                .build();

        getView().showLoading();

        model.verifyCode(request, new VerificationInteractor.ResponseListener() {
            @Override
            public void onSuccess(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onVerificationSuccessful(message);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }

            @Override
            public void onCodeAlreadyUsed(String messsage) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onCodeAlreadyUsed(messsage);
                }
            }

            @Override
            public void onUserNotFound(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onUserNotFound(message);
                }
            }
        });
    }

    @Override
    public void detachView(boolean retainInstance) {
        model.cancelNetworkRequest();
        super.detachView(retainInstance);
    }
}
