package ph.com.auspex.mers.familylink.dialog.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.Relationship;
import ph.com.auspex.mers.familylink.FamilyLinkInteractor;
import ph.com.auspex.mers.familylink.dialog.AddFamilyContract;

/**
 * Provides implementation for add family link presenter
 */
public class AddFamilyPresenterImpl extends MvpBasePresenter<AddFamilyContract.AddFamilyView> implements AddFamilyContract.AddFamilyPresenter {

    private final FamilyLinkInteractor model;

    public AddFamilyPresenterImpl(FamilyLinkInteractor model) {
        this.model = model;
    }

    @Override
    public void attachView(AddFamilyContract.AddFamilyView view) {
        super.attachView(view);

        getRelationships();
    }

    /**
     * Sync relationships types from the API
     */
    @Override
    public void getRelationships() {
        model.getRelationshipTypes(new BaseInteractor.ResponseListener<List<Relationship>>() {
            @Override
            public void onSuccess(List<Relationship> relationships) {
                if (isViewAttached()) {
                    getView().prepareRelationshipList(relationships);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().showError(message);
                }
            }
        });
    }

    /**
     * Add family link using the API
     *
     * @param inviteCode     invite code of the user that will be added
     * @param relationshipId relationship type id {@link Relationship#getId()}
     */
    @Override
    public void addFamilyLink(String inviteCode, String relationshipId) {
        getView().showLoading();

        model.addFamilyLink(inviteCode, relationshipId, new BaseInteractor.ResponseListener<String>() {
            @Override
            public void onSuccess(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onFamilyLinkAdded("Invite Sent");
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
