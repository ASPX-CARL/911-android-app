package ph.com.auspex.mers.news.internal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import javax.inject.Inject;

import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.adapter.ItemViewClickListener;
import ph.com.auspex.mers.common.changehandler.CircularRevealChangeHandlerCompat;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.RecyclerViewLCE;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.news.NewsModule;
import ph.com.auspex.mers.news.internal.adapter.InternalNewsAdapter;
import ph.com.auspex.mers.newsdetail.NewsDetailController;
import timber.log.Timber;

public class InternalNewsController extends MosbyController<InternalNewsContract.InternalNewsView, InternalNewsContract.InternalNewsPresenter> implements InternalNewsContract.InternalNewsView,
        ItemViewClickListener<InternalFeed> {

    @BindView(R.id.recycler_view_lce) RecyclerViewLCE recyclerView;
    @BindView(R.id.adView) AdView adView;

    @Inject InternalNewsContract.InternalNewsPresenter presenter;
    @Inject Router parentRouter;
    @Inject InternalNewsAdapter adapter;
    @Inject AdRequest adRequest;

    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    private int currentPage = 0;

    @NonNull
    @Override
    public InternalNewsContract.InternalNewsPresenter createPresenter() {
        return presenter;
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (adRequest != null) {
            adView.loadAd(adRequest);
        }
    }

    @Override
    public void setData(InternalNews internalNews) {
        Timber.d("setData: %s");
        currentPage = internalNews.getCurrentPage();
        adapter.updateList(internalNews);
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.newsComponent(new NewsModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_internal_news, container, false);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        adapter.setListener(this);
        initRecyclerView();

        adView.loadAd(adRequest);

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Timber.e("onAdFailedToLoad: %s", errorCode);
            }
        });
    }

    private void initRecyclerView() {
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(recyclerView.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (currentPage != page) {
                    if (!adapter.isLoading()) {
                        presenter.getInternalNews(false);
                    }
                }
            }
        };

        recyclerView.setOnRefreshListener(() -> {
            endlessRecyclerViewScrollListener.resetState();
            presenter.getInternalNews(true);
        });
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showLoading() {
        recyclerView.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        recyclerView.setRefreshing(false);
    }

    @Override
    public void onLoadMore() {
        adapter.setLoading();
    }

    @Override
    public void scrollListToTop() {
        recyclerView.scrollToTop();
    }

    @Override
    protected void initToolbar() {
    }

    @Override
    public void onItemClicked(InternalFeed internalFeed, View view) {
        setRetainViewMode(RetainViewMode.RETAIN_DETACH);
        Bundle args = new Bundle();
        args.putString(NewsDetailController.KEY_URL, internalFeed.getUrl());
        parentRouter.pushController(RouterTransaction.with(new NewsDetailController(args))
                .pushChangeHandler(new CircularRevealChangeHandlerCompat(view, recyclerView, -2))
                .popChangeHandler(new CircularRevealChangeHandlerCompat(view, recyclerView, -2)));
    }
}
