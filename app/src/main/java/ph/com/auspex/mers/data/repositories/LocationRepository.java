package ph.com.auspex.mers.data.repositories;

import io.reactivex.Observable;
import ph.com.auspex.mers.data.model.request.UpdateLocationRequest;

public interface LocationRepository {

    Observable<Boolean> updateLocation(UpdateLocationRequest updateLocationRequest);
}
