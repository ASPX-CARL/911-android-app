package ph.com.auspex.mers.service.impl;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.service.LocationServicePresenter;
import ph.com.auspex.mers.service.ServiceInteractor;

/**
 * LocationService Presenter Implementation
 */
public class LocationServicePresenterImpl implements LocationServicePresenter {

    private final ServiceInteractor model;

    public LocationServicePresenterImpl(ServiceInteractor model) {
        this.model = model;
    }

    /**
     * Update Current location in the API
     *
     * @param location Current Location
     */
    @Override
    public void updateCurrentLocation(Location location) {
        model.updateLocation(new LatLng(location.getLatitude(), location.getLongitude()), true, new BaseInteractor.ResponseListener<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {

            }

            @Override
            public void onFailure(String message) {

            }
        });
    }
}
