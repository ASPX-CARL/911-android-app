package ph.com.auspex.mers.register.optional;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.AdvancedTextInputEditText;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.register.RegisterContract.RegisterPresenter;
import ph.com.auspex.mers.register.RegisterContract.RegisterView;
import ph.com.auspex.mers.register.RegisterModule;
import ph.com.auspex.mers.util.FormValidationUtil;
import ph.com.auspex.mers.util.MaterialDialogManager;
import ph.com.auspex.mers.verification.VerificationController;
import ph.com.auspex.mers.web.BrowserController;

/**
 * OptionalInformationController is the second part of the application registration process
 */
public class OptionalInformationController extends MosbyController<RegisterView, RegisterPresenter> implements RegisterView {

    public static final String TAG = OptionalInformationController.class.getSimpleName();
    public static final String KEY_REQUEST = TAG + ".request";

    @BindView(R.id.radio_group_gender) RadioGroup radioGroupGender;
    @BindView(R.id.input_home_address) AdvancedTextInputEditText inputHomeAddress;
    @BindView(R.id.input_contact_person) AdvancedTextInputEditText inputContactPerson;
    @BindView(R.id.checkbox_agreement) CheckBox checkBoxAgreement;
    @BindView(R.id.input_contact_person_mobile_number) AdvancedTextInputEditText inputContactPersonMobileNumber;
    @BindView(R.id.checkbox_agreement_text) TextView tvAgreement;

    private RadioButton radioButton;
    private View view;

    @BindString(R.string.label_basic_information) String title;

    @Inject Router parentRouter;
    @Inject RegisterPresenter presenter;
    @Inject FormValidationUtil validationUtil;

    private RegisterRequest registerRequest;

    public OptionalInformationController(@Nullable Bundle args) {
        super(args);

        setHasOptionsMenu(true);

        if (args != null) {
            registerRequest = args.getParcelable(KEY_REQUEST);
        }
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.registerComponent(new RegisterModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        SpannableString content = new SpannableString(activity.getString(R.string.checkbox_terms_and_condition));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvAgreement.setText(content);
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (presenter != null) {
            presenter.attachView(this);
        }

        initToolbar();

        this.view = view;
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_register_optional, container, false);
    }

    /**
     * Go back to the previous controller / first part of the registration process
     */
    @OnClick(R.id.button_previous)
    void onPreviousButtonClicked() {
        parentRouter.popController(this);
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_register;
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                handleBack();
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    /**
     * Show take medical information dialog
     */
    @OnClick(R.id.button_finish)
    void onFinishButtonClicked() {
        new MaterialDialog.Builder(activity)
                .title("Medical Information")
                .content("Do you want to add your medical information now?")
                .positiveText("Yes")
                .negativeText("Skip")
                .onPositive((dialog, which) -> proceedToRegistration(true))
                .onNegative((dialog, which) -> proceedToRegistration(false))
                .show();
    }

    /**
     * Determine if the next screen will be the verification screen or the medical information screen
     *
     * @param goToMedicalInformation if true, display the medical information as the next screen
     */
    public void proceedToRegistration(boolean goToMedicalInformation) {
        int selectedId = radioGroupGender.getCheckedRadioButtonId();

        radioButton = ButterKnife.findById(view, selectedId);

        if (checkBoxAgreement.isChecked()) {
            if (validationUtil.validateContactPersonDetails(inputContactPerson, inputContactPersonMobileNumber)) {
                registerRequest = registerRequest.newBuilder()
                        .gender(radioButton.getText().toString())
                        .address(inputHomeAddress.getText().toString())
                        .contactPerson(inputContactPerson.getText().toString())
                        .contactPersonNumber(validationUtil.convertFormat(inputContactPersonMobileNumber.getText().toString()))
                        .build();

                if (goToMedicalInformation) {
                    Bundle args = new Bundle();
                    args.putParcelable(MedicalInformationController.KEY_REGISTRATION_REQUEST, registerRequest);

                    parentRouter.pushController(RouterTransaction.with(new MedicalInformationController(args)));
                } else {
                    presenter.registerAccount(registerRequest);
                }
            }
        } else {
            Toast.makeText(getActivity(), R.string.error_terms_and_agreements, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCancelAccountCreationDialog(activity, parentRouter);
        return true;
    }

    @NonNull
    @Override
    public RegisterPresenter createPresenter() {
        return presenter;
    }

    /**
     * If Registration is successful open {@link VerificationController}
     *
     * @param userId user id of the registered account
     */
    @Override
    public void onRegisterSuccessful(String userId) {
        Bundle args = new Bundle();
        args.putString(VerificationController.KEY_USER_ID, userId);
        args.putSerializable(VerificationController.KEY_VERIFICATION_TYPE, VerificationRequest.VerificationType.REGISTRATION);

        parentRouter.pushController(RouterTransaction.with(new VerificationController(args)));
    }

    /**
     * If the registration is failed display a {@link Toast}
     *
     * @param message Toast display message
     */
    @Override
    public void onRegisterFailed(String message) {
        showToast(message);
    }

    /**
     * Open End User Agreement when checkbox text is clicked
     */
    @OnClick(R.id.checkbox_agreement_text)
    void onAgreementTextClicked() {
        setRetainViewMode(RetainViewMode.RETAIN_DETACH);
        getRouter().pushController(RouterTransaction.with(new BrowserController()));
    }
}