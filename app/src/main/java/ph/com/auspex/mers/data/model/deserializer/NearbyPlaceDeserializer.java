package ph.com.auspex.mers.data.model.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ph.com.auspex.mers.data.model.response.NearbyPlace;

/**
 * Google Nearby Places API Deserializer
 */
public class NearbyPlaceDeserializer implements JsonDeserializer<NearbyPlace> {

    @Override
    public NearbyPlace deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject nearbyPlace = json.getAsJsonObject();

        String name = nearbyPlace.get("name").getAsString();
        String placeId = nearbyPlace.get("place_id").getAsString();
        String vicinity = nearbyPlace.get("vicinity").getAsString();

        JsonObject location = json.getAsJsonObject().get("location").getAsJsonObject();

        String latitude = location.get("lat").getAsString();
        String longitude = location.get("lng").getAsString();

        return new NearbyPlace.Builder()
                .name(name)
                .placeId(placeId)
                .address(vicinity)
                .latitude(Double.parseDouble(latitude))
                .longitude(Double.parseDouble(longitude))
                .build();
    }
}
