package ph.com.auspex.mers.profile;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.UserDetail;

public interface ProfileInteractor {

    void getUserDetails(ResponseListener listener);

    void updateUserDetails(UserDetail userDetail, BaseInteractor.ResponseListener<UserDetail> listener);

    void logout(BaseInteractor.EmptyResponseListener listener);

    interface ResponseListener extends BaseInteractor.ResponseListener<UserDetail> {
        void onUserNotFound();
    }
}
