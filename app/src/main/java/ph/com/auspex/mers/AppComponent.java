package ph.com.auspex.mers;

import android.app.Application;
import android.content.res.Resources;

import com.google.android.gms.ads.AdRequest;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.data.DataSourceModule;
import ph.com.auspex.mers.data.bus.BusModule;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.sharedpref.AppPreferences;
import ph.com.auspex.mers.receiver.GpsStatusReceiver;
import ph.com.auspex.mers.util.FormValidationUtil;

@Singleton
@Component(
        modules = {
                AppModule.class,
                DataSourceModule.class,
                BusModule.class
        }
)
public interface AppComponent {

    Application application();

    AppPreferences appPreferences();

    RxEventBus rxEventBus();

    Resources resources();

    AdRequest adRequest();

    FormValidationUtil validationUtil();

    AppDataSource appDataSource();

    UserWrapper userWrapper();

    GpsStatusReceiver gpsStatusReceiver();

    @Named("badge")
    AtomicBoolean shouldShowBadge();

    void inject(MersApplication mersApplication);
}
