package ph.com.auspex.mers.news.rss.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.adapter.ItemViewClickListener;
import ph.com.auspex.mers.data.model.response.Advertisement;
import ph.com.auspex.mers.data.model.response.Feed;
import ph.com.auspex.mers.util.GlideImageLoader;

/**
 * Rss News Feed Recycler Adapter
 */
public class NewsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_HOLDER_NEWS = 0;
    private static final int VIEW_HOLDER_ADS = 2;

    private SparseArray<Object> contentList;

    private ItemViewClickListener<Feed> listener;


    /**
     * Empty constructor that creates a new {@link NewsRecyclerAdapter} instance.
     * Initialize an empty {@link #contentList}.
     */
    public NewsRecyclerAdapter() {
        this.contentList = new SparseArray<>();
    }

    /**
     * This sets the {@link ItemViewClickListener} for this adapter.
     *
     * @param listener {@link ItemViewClickListener} with generic object parameter.
     */
    public void setListener(ItemViewClickListener<Feed> listener) {
        this.listener = listener;
    }

    /**
     * Update this adapter's {@link #contentList}.
     *
     * @param contentList New {@link #contentList} object.
     */
    public void updateList(SparseArray<Object> contentList) {
        this.contentList = contentList;
        notifyDataSetChanged();
    }

    /**
     * Determine which {@link RecyclerView.ViewHolder} will be used based on the {@link #contentList} object instance.
     *
     * @param parent   Parent view.
     * @param viewType {@link RecyclerView.Adapter} viewType.
     * @return {@link RecyclerView.ViewHolder} either of the type {@link NewsViewHolder} or {@link AdsViewHolder}.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case VIEW_HOLDER_NEWS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_news, parent, false);
                return new NewsViewHolder(view, listener, parent.getContext());
            case VIEW_HOLDER_ADS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_ads, parent, false);
                return new AdsViewHolder(view, parent.getContext());
            default:
                throw new RuntimeException("Invalid ViewHolder type");
        }
    }

    /**
     * Bind the object to the {@link RecyclerView.ViewHolder}.
     *
     * @param holder   {@link RecyclerView.ViewHolder} instance for the object type.
     * @param position Index of the object in the {@link #contentList}.
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_HOLDER_ADS:
                AdsViewHolder viewHolderAds = (AdsViewHolder) holder;
                viewHolderAds.bind((Advertisement) contentList.get(position));
                break;
            case VIEW_HOLDER_NEWS:
                NewsViewHolder viewHolderNews = (NewsViewHolder) holder;
                viewHolderNews.bind((Feed) contentList.get(position));
                break;
        }
    }

    /**
     * Return viewType based the return type of {@link #contentList#getItemId(int)}.
     *
     * @param position Position of the object in the list.
     * @return ViewType, either {@link #VIEW_HOLDER_NEWS} or {@link #VIEW_HOLDER_ADS}.
     */
    @Override
    public int getItemViewType(int position) {
        return getViewType(contentList.get(position));
    }

    /**
     * Return a View Type based on the {@link #contentList} object on a given index.
     *
     * @param object Object from {@link #contentList}.
     * @return viewType, either {@link #VIEW_HOLDER_NEWS} or {@link #VIEW_HOLDER_ADS}.
     */
    private int getViewType(Object object) {
        if (object instanceof Feed) {
            return VIEW_HOLDER_NEWS;
        } else {
            return VIEW_HOLDER_ADS;
        }
    }

    /**
     * Get {@link #contentList} item count.
     *
     * @return Total {@link #contentList} item count.
     */
    @Override
    public int getItemCount() {
        return contentList.size();
    }

    /**
     * RSS News {@link RecyclerView.ViewHolder}.
     */
    public static class NewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.parent) View parentView;
        @BindView(R.id.news_title) TextView tvTitle;
        @BindView(R.id.news_content) TextView tvContent;
        @BindView(R.id.thumbnail) ImageView thumbnail;

        private Feed feed;
        private ItemViewClickListener<Feed> listener;
        private Context context;

        /**
         * Constructor to create an instance of this View Holder.
         *
         * @param itemView Inflated view.
         * @param listener Click Listener for this View holder.
         * @param context  Context from view parent.
         */
        public NewsViewHolder(View itemView, ItemViewClickListener<Feed> listener, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            this.listener = listener;
            this.context = context;
        }

        /**
         * Bind {@link Feed} object to this view holder.
         *
         * @param feed Feed object from {@link #contentList}.
         */
        public void bind(Feed feed) {
            this.feed = feed;

            tvTitle.setText(feed.getTitle());

            if (!TextUtils.isEmpty(feed.getDescription())) {
                tvContent.setText(stripHtml(feed.getDescription()));
            }

            GlideImageLoader.loadUrl(context, thumbnail, feed.getThumbnailUrl(), R.drawable.news_place_holder, R.drawable.news_place_holder, false);
        }

        /**
         * Click event for this {@link RecyclerView} item.
         */
        @OnClick(R.id.parent)
        void onParentClicked() {
            listener.onItemClicked(feed, parentView);
        }

        private String stripHtml(String html) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                return String.valueOf(Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY));
            } else {
                return String.valueOf(Html.fromHtml(html));
            }
        }
    }

    /**
     * Advertisement {@link RecyclerView.ViewHolder}.
     */
    public static class AdsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ads_thumbnail) ImageView ivAdsPlaceholder;

        private Context context;

        /**
         * Constructor to create an instance of this View Holder.
         *
         * @param itemView Inflated view.
         * @param context  Context from view parent.
         */
        public AdsViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;

            ButterKnife.bind(this, itemView);
        }

        /**
         * Bind {@link Advertisement} object to this view holder.
         *
         * @param advertisement Advertisement object from {@link #contentList}.
         */
        public void bind(Advertisement advertisement) {
            // GlideImageLoader.loadGif(context, ivAdsPlaceholder, advertisement.getImageUrl());
        }
    }
}