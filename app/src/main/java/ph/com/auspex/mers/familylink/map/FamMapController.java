package ph.com.auspex.mers.familylink.map;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bluelinelabs.conductor.RouterTransaction;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.controller.MosbyViewStateController;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.familylink.FamilyLinkModule;
import ph.com.auspex.mers.familylink.detail.FamLinkDetailDialogController;
import ph.com.auspex.mers.familylink.dialog.AddFamilyLinkDialogController;
import ph.com.auspex.mers.familylink.list.ViewPagerController;
import ph.com.auspex.mers.familylink.map.FamMapContract.FamMapView;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.main.MainActivity;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.MaterialDialogManager;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Provide a UI for the Family Link in map feature.
 * This class displays a {@link GoogleMap} with markers for each {@link FamilyLinkDetail} connected to the logged=in user
 */
public class FamMapController extends MosbyViewStateController<FamMapView, FamMapContract.FamMapPresenter> implements FamMapView,
        OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    public static final String TAG = FamMapController.class.getSimpleName();

    @BindView(R.id.mapview) MapView mapView;
    @BindView(R.id.layout_blocker_account) RelativeLayout layoutAccount;
    @BindView(R.id.layout_blocker_gps) RelativeLayout layoutLocationServices;
    @BindView(R.id.layout_blocker_permission) RelativeLayout layoutPermissions;
    @BindView(R.id.iv_test) ImageView ivTest;
    @BindView(R.id.adView) AdView adView;

    @BindColor(R.color.colorPrimaryDark) int colorPrimaryDark;

    @BindString(R.string.label_family_link) String title;

    @Inject UserWrapper userWrapper;
    @Inject FamMapContract.FamMapPresenter presenter;
    @Inject @Named("badge") AtomicBoolean shouldShowBadge;
    @Inject AdRequest adRequest;

    private GoogleMap googleMap;
    private LatLng currentLocation;
    private boolean isFirstRun = true;

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container, Bundle savedViewState) {
        return inflater.inflate(R.layout.controller_family_link_map, container, false);
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.familyLinkComponent(new FamilyLinkModule()).inject(this);
    }

    /**
     * Initialize {@link GoogleMap} and show an advertisement banner
     */
    @Override
    protected void onViewReady(View view, Bundle savedViewState) {
        mapView.onCreate(savedViewState);
        mapView.getMapAsync(this);

        adView.loadAd(adRequest);
    }

    /**
     * Provide an event listener when the badge/notification icon is clicked
     */
    @Override
    protected void onBadgeClicked() {
        shouldShowBadge.compareAndSet(true, false);
        openFamilyLinkPagerController(true, 1);
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (mapView != null) {
            mapView.onResume();
        }
    }

    /**
     * Provide an event listener on {@link Marker} click
     *
     * @param marker google marker on map
     * @return always <code>false</code> to prevent the default action of marker window info to be called
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        FamilyLinkDetail familyLinkDetails = (FamilyLinkDetail) marker.getTag();

        Bundle args = new Bundle();

        if (familyLinkDetails != null) {
            args.putParcelable(FamLinkDetailDialogController.KEY_FAMILY_LINK_DETAIL, familyLinkDetails);
        }

        setOverlayController(new FamLinkDetailDialogController(args));

        marker.hideInfoWindow();
        return false;
    }

    /**
     * Initialize additional view components after the {@link GoogleMap} has been loaded
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        googleMap.setOnMarkerClickListener(this);

        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);

        if (!userWrapper.isLoggedIn()) {
            layoutAccount.setVisibility(VISIBLE);
        } else {
            Nammu.askForPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION, new PermissionCallback() {
                @Override
                public void permissionGranted() {
                    togglePermissionBlocker(false);
                    presenter.observeGpsSettingsChanges();
                }

                @Override
                public void permissionRefused() {
                    togglePermissionBlocker(true);
                }
            });
            badgeView.setVisibility(VISIBLE);
        }
    }

    /**
     * Provide controller title
     *
     * @return controller title
     */
    @Override
    protected CharSequence getTitle() {
        return title;
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        if (userWrapper.isLoggedIn()) {
            return R.menu.menu_family_link;
        } else {
            return 0;
        }
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toggle_list:
                openFamilyLinkPagerController(true, 0);
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    @Override
    protected void onDestroyView(@NonNull View view) {
        if (mapView != null) {
            mapView.onDestroy();
        }
        super.onDestroyView(view);
    }

    /**
     * Event listener for {@link android.location.Location} updates from {@link ph.com.auspex.mers.service.LocationUpdateService}
     *
     * @param currentLocation current location
     */
    @Override
    public void onCurrentLocationUpdated(LatLng currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     * Display last known location for this account
     *
     * @param lastKnownLocation user's last known location
     */
    @Override
    public void displayLastKnownLocation(LatLng lastKnownLocation) {
        this.currentLocation = lastKnownLocation;

        moveCameraToLocation(!isFirstRun);
        isFirstRun = false;
    }

    /**
     * Populate the {@link GoogleMap} with family link {@link Marker}
     *
     * @param familyLinkDetails collection of {@link FamilyLinkDetail}
     */
    @Override
    public void displayFamilyLinkPins(List<FamilyLinkDetail> familyLinkDetails) {
        googleMap.clear();

        for (FamilyLinkDetail familyLinkDetail : familyLinkDetails) {
            if (familyLinkDetail.getCurrentLocation() != null) {
                if (familyLinkDetail.getImageUrl() != null) {
                    // load image
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .override(AppUtil.dpToPx(getActivity(), 40), AppUtil.dpToPx(getActivity(), 40));

                    Glide.with(activity)
                            .asBitmap()
                            .load(familyLinkDetail.getImageUrl())
                            .apply(options)
                            .listener(new RequestListener<Bitmap>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                    googleMap.addMarker(new MarkerOptions()
                                            .position(familyLinkDetail.getCurrentLocation())
                                            .icon(BitmapDescriptorFactory.fromBitmap(AppUtil.createBitmapForMarker(getActivity(), AppUtil.getBitmapFromVectorDrawable(activity, familyLinkDetail.getGender().getDrawableRes()), familyLinkDetail.isOnline())))
                                            .anchor(0.5f, 0.5f))
                                            .setTag(familyLinkDetail);
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    if (resource != null) {
                                        googleMap.addMarker(new MarkerOptions()
                                                .position(familyLinkDetail.getCurrentLocation())
                                                .icon(BitmapDescriptorFactory.fromBitmap(AppUtil.createBitmapForMarker(getActivity(), resource, familyLinkDetail.isOnline())))
                                                .anchor(0.5f, 0.5f))
                                                .setTag(familyLinkDetail);
                                    } else {
                                        googleMap.addMarker(new MarkerOptions()
                                                .position(familyLinkDetail.getCurrentLocation())
                                                .icon(BitmapDescriptorFactory.fromBitmap(AppUtil.createBitmapForMarker(getActivity(), AppUtil.getBitmapFromVectorDrawable(activity, familyLinkDetail.getGender().getDrawableRes()), familyLinkDetail.isOnline())))
                                                .anchor(0.5f, 0.5f))
                                                .setTag(familyLinkDetail);
                                    }
                                }
                            });
                } else {
                    // load placeholder
                    googleMap.addMarker(new MarkerOptions()
                            .position(familyLinkDetail.getCurrentLocation())
                            .icon(BitmapDescriptorFactory.fromBitmap(AppUtil.createBitmapForMarker(getActivity(), AppUtil.getBitmapFromVectorDrawable(activity, familyLinkDetail.getGender().getDrawableRes()), familyLinkDetail.isOnline())))
                            .anchor(0.5f, 0.5f))
                            .setTag(familyLinkDetail);
                }
            }
        }
    }

    /**
     * Display the pending family link invites on the notifications badge
     *
     * @param count pending family link invites count
     */
    @Override
    public void displayFamilyLinkInvitesCount(int count) {
        badgeView.setBadgeCount(shouldShowBadge.get() ? count : 0);
    }

    /**
     * Display the GPS Settings blocker layout when device's gps settings is turned off
     *
     * @param shouldShow <code>false</code> if the device's gps settings is turned off; <code>true</code> otherwise
     */
    @Override
    public void toggleGpsBlocker(boolean shouldShow) {
        layoutLocationServices.setVisibility(shouldShow ? INVISIBLE : VISIBLE);

        if (shouldShow) {
            presenter.observeCurrentLocationChanges();
            presenter.getFamilyLinkPins();
        }
    }

    /**
     * Display the permission blocker layout when {@link Manifest.permission#ACCESS_FINE_LOCATION} is not granted
     *
     * @param shouldShow <code>true</code> if the permission is not granted; <code>true</code> otherwise
     */
    @SuppressWarnings("MissingPermission")
    @Override
    public void togglePermissionBlocker(boolean shouldShow) {
        layoutPermissions.setVisibility(shouldShow ? VISIBLE : INVISIBLE);

        if (!shouldShow) {
            toggleGpsBlocker(AppUtil.checkGpsEnabled(activity));
            ((MainActivity) activity).startService();

            if (googleMap != null) {
                googleMap.setMyLocationEnabled(true);
            }
            presenter.getLastKnownLocation();
        }
    }

    @Override
    public void onLowMemory() {
        mapView.onLowMemory();
    }

    @NonNull
    @Override
    public FamMapContract.FamMapPresenter createPresenter() {
        return presenter;
    }

    /**
     * Manually focus the current location on the {@link GoogleMap}
     */
    @OnClick(R.id.fab_my_location)
    void onShowCurrentLocationButtonClicked() {
        moveCameraToLocation(true);
    }

    /**
     * Manually refresh the {@link GoogleMap} for new {@link FamilyLinkDetail} location updates
     */
    @OnClick(R.id.fab_refresh_map)
    void onRefreshButtonClicked() {
        presenter.getFamilyLinkPins();
    }

    /**
     * Invite a new user to be added as the user's Family link
     */
    @OnClick(R.id.fab_add_family_link)
    void onAddButtonClicked() {
        setOverlayController(new AddFamilyLinkDialogController());
    }

    /**
     * Go to {@link ph.com.auspex.mers.login.LoginController} when there is no account found on the app
     */
    @OnClick(R.id.button_login)
    void onLoginButtonClicked() {
        if (getParentController() != null) {
            ((NavigationController) getParentController()).replaceCurrentControllerWith(NavigationController.NavigationItem.LOGIN_CONTROLLER);
        }
    }

    /**
     * Opens GPS Settings
     */
    @OnClick(R.id.button_settings)
    void onOpenSettingsButtonClicked() {
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(settingsIntent);
    }

    /**
     * Opens {@link ph.com.auspex.mers.familylink.list.family.FamListController}
     */
    @OnClick(R.id.button_open_list)
    void onOpenFamilyListButtonClicked() {
        openFamilyLinkPagerController(false, 0);
    }

    /**
     * Request for {@link Manifest.permission#ACCESS_FINE_LOCATION} permission
     */
    @OnClick(R.id.button_request_permissions)
    void onPermissionsButtonClicked() {
        // presenter.requestFineLocationPermission();
        Nammu.askForPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                togglePermissionBlocker(false);
                presenter.observeGpsSettingsChanges();
            }

            @Override
            public void permissionRefused() {
                togglePermissionBlocker(true);
            }
        });
    }

    /**
     * Provide a listener to center the current location in the {@link GoogleMap}
     *
     * @param shouldAnimate <code>true</code> will animate the camera while moving to the center; <code>false</code> will not animate the camera
     */
    private void moveCameraToLocation(boolean shouldAnimate) {
        if (currentLocation != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(currentLocation)
                    .zoom(17)
                    .build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);

            if (shouldAnimate) {
                googleMap.animateCamera(cameraUpdate);
            } else {
                googleMap.moveCamera(cameraUpdate);
            }
        }
    }

    /**
     * Opens the Family link {@link ViewPagerController}
     *
     * @param shouldShowOptionsMenu <code>true</code> if the menu options will be displayed
     * @param defaultPosition       determines which page will be displayed when {@link ViewPagerController} is attached
     */
    private void openFamilyLinkPagerController(boolean shouldShowOptionsMenu, int defaultPosition) {
        Bundle args = new Bundle();
        args.putBoolean(ViewPagerController.KEY_HAS_OPTIONS_MENU, shouldShowOptionsMenu);
        args.putInt(ViewPagerController.KEY_DEFAULT_PAGE, defaultPosition);
        getRouter().pushController(RouterTransaction.with(new ViewPagerController(args)));
    }

    /**
     * Handles back button press event
     *
     * @return always <code>true</code>
     */
    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCloseApplicationDialog(activity);
        return true;
    }

    @Override
    protected void onDetach(@NonNull View view) {
        super.onDetach(view);
        hideLoading();
        mapView.onPause();
    }
}
