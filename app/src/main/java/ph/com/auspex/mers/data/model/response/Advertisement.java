package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ph.com.auspex.mers.data.remote.ApiService;

/**
 * Provide a POJO for {@link ApiService#getAdvertisements()}
 */
public class Advertisement extends RealmObject {

    @PrimaryKey
    @SerializedName("advertisement_id")
    private int advertisementId;
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("url")
    private String url;

    /**
     * Return advertisement id
     *
     * @return Advertisement
     */
    public int getAdvertisementId() {
        return advertisementId;
    }

    /**
     * Return the image url of the thumbnail
     *
     * @return thumbnail image url
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Advertisement url
     *
     * @return advertisement url
     */
    public String getUrl() {
        return url;
    }
}