package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import io.realm.annotations.PrimaryKey;

/**
 * AccessToken returned from API after a successful {@link ph.com.auspex.mers.data.remote.ApiService#login(HashMap)} or {@link ph.com.auspex.mers.data.remote.ApiService#updateProfile(Map)}
 */
public class AccessToken {

    @PrimaryKey
    @SerializedName("user_id")
    private String userId;
    @SerializedName("token")
    private String token;
    @SerializedName("expiration")
    private long expiration;

    /**
     * Returns the JWT Token
     *
     * @return JWT Token String
     */
    public String getToken() {
        return token;
    }

    /**
     * Returns the JWT Token Expiration
     *
     * @return JWT Token Expiration
     */
    public long getExpiration() {
        return expiration;
    }

    /**
     * Returns the user id
     *
     * @return the user id of the logged in account
     */
    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "AccessToken{" +
                "token='" + token + '\'' +
                ", expiration=" + expiration +
                ", userId='" + userId + '\'' +
                '}';
    }
}
