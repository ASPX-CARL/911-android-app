package ph.com.auspex.mers.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

/**
 * Image Url load helper
 */
public class GlideImageLoader {

    /*
      Load an image from a Drawable Res

       @param context     Application context
     * @param imageView   {@link ImageView} Placeholder
     * @param drawableRes Drawable resource id
     * @param placeholder Drawable resource id for placeholder
     * @param error       Drawable resource id for placeholder error
     * @param isRounded   Set true if you want your drawable to be rounded
     */
    /*public static void loadDrawableRes(Context context, ImageView imageView, @DrawableRes int drawableRes, @DrawableRes int placeholder, @DrawableRes int error, boolean isRounded) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(getDrawable(context, placeholder))
                .error(getDrawable(context, error))
                .diskCacheStrategy(DiskCacheStrategy.NONE);

        if (isRounded) {
            options.circleCrop();
        }

        Glide.with(context)
                .load(getDrawable(context, drawableRes))
                .apply(options)
                .into(imageView);
    }*/

    /**
     * Load an image from a {@link File}
     *
     * @param context   Application context
     * @param imageView {@link ImageView} Placeholder
     * @param file      File to be loaded
     * @param isRounded Set true if you want your drawable to be rounded
     */
    public static void loadFile(Context context, ImageView imageView, File file, boolean isRounded) {
        RequestOptions options = new RequestOptions()
                .centerCrop();

        if (isRounded) {
            options.circleCrop();
        }

        Glide.with(context)
                .load(file)
                .apply(options)
                .into(imageView);
    }

    /*public static void loadFile(Context context, ImageView imageView, Bitmap bitmap, boolean isRounded) {
        RequestOptions options = new RequestOptions()
                .centerCrop();

        if (isRounded) {
            options.circleCrop();
        }

        Glide.with(context)
                .load(bitmap)
                .apply(options)
                .into(imageView);
    }*/

    /**
     * Load an image from a URL
     *
     * @param context   Application context
     * @param imageView {@link ImageView} Placeholder
     * @param imageUrl  Image url to be loaded
     * @param width     Width of the image when loaded
     * @param height    Height of the image when loaded
     */
    public static void loadUrl(Context context, ImageView imageView, String imageUrl, int width, int height) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .override(width, height)
                .diskCacheStrategy(DiskCacheStrategy.NONE);

        Glide.with(context)
                .load(imageUrl)
                .apply(options)
                .into(imageView);
    }

    /**
     * Load an image from a URL
     *
     * @param context     Application context
     * @param imageView   {@link ImageView} Placeholder
     * @param imageUrl    Image url to be loaded
     * @param placeholder Drawable resource id image placeholder
     * @param error       Drawable resource id image error placeholder
     * @param isRounded   Set to true if you want your drawable to be rounded
     */
    public static void loadUrl(Context context, ImageView imageView, String imageUrl, @DrawableRes int placeholder, @DrawableRes int error, boolean isRounded) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE);

        if (placeholder != 0) {
            options.placeholder(getDrawable(context, placeholder));
        }

        if (error != 0) {
            options.error(getDrawable(context, error));
        }

        if (isRounded) {
            options.circleCrop();
        }

        Glide.with(context)
                .load(imageUrl)
                .apply(options)
                .into(imageView);
    }

    /**
     * Load an animated GIF from a URL
     *
     * @param context   Application context
     * @param imageView {@link ImageView} Placeholder
     * @param imageUrl  Gif url to be loaded
     */
    public static void loadGif(Context context, ImageView imageView, String imageUrl) {
        if (MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(imageUrl)).equals("image/gif")) {
            RequestBuilder<GifDrawable> requestBuilder = Glide.with(context)
                    .asGif()
                    .load(imageUrl);

            requestBuilder
                    .into(imageView);
        } else {
            Glide.with(context)
                    .load(imageUrl)
                    .into(imageView);
        }
    }

    /**
     * Reusable method to get drawable from Application {@link android.content.res.Resources}
     *
     * @param context     Application context
     * @param drawableRes Drawable Resource id
     * @return Drawable from resource id
     */
    private static Drawable getDrawable(Context context, @DrawableRes int drawableRes) {
        return ContextCompat.getDrawable(context, drawableRes);
    }
}
