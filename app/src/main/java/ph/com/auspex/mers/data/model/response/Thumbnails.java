package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Provide a POJO for thumbnail object in {@link Feed}
 */
public class Thumbnails {

    @SerializedName("thumbnail")
    private List<ThumbnailItem> thumbnails;

    public List<ThumbnailItem> getThumbnails() {
        return thumbnails;
    }
}