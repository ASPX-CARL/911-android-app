package ph.com.auspex.mers.data.model.response;

public class Attributes {

    private String width;
    private String medium;
    private String url;
    private String height;

    public String getWidth() {
        return width;
    }

    public String getMedium() {
        return medium;
    }

    public String getUrl() {
        return url;
    }

    public String getHeight() {
        return height;
    }
}
