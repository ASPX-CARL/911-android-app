package ph.com.auspex.mers.splash;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;

public interface SplashInteractor extends BaseInteractor {

    void sync(ResponseListener responseListener);

    interface ResponseListener {
        void onSuccess();

        void onFailure(String message);
    }
}