package ph.com.auspex.mers.data.model.request;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

/**
 * Create Registration request
 */
public class RegisterRequest implements Parcelable {

    private String firstName;
    private String middleName;
    private String lastName;
    private String emailAddress;
    private String password;
    private String confirmPassword;
    private String mobileNumber;
    private String birthday;
    private String gender;
    private String address;
    private String contactPerson;
    private String contactPersonNumber;
    private String medicalInformation;
    private File imageFile;

    private RegisterRequest(Builder builder) {
        this.firstName = builder.firstName;
        this.middleName = builder.middleName;
        this.lastName = builder.lastName;
        this.emailAddress = builder.emailAddress;
        this.password = builder.password;
        this.confirmPassword = builder.confirmPassword;
        this.mobileNumber = builder.mobileNumber;
        this.birthday = builder.birthday;
        this.gender = builder.gender;
        this.address = builder.address;
        this.contactPerson = builder.contactPerson;
        this.contactPersonNumber = builder.contactPersonNumber;
        this.imageFile = builder.imageFile;
        this.medicalInformation = builder.medicalInformation;
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.middleName);
        dest.writeString(this.lastName);
        dest.writeString(this.emailAddress);
        dest.writeString(this.password);
        dest.writeString(this.confirmPassword);
        dest.writeString(this.mobileNumber);
        dest.writeString(this.birthday);
        dest.writeString(this.gender);
        dest.writeString(this.address);
        dest.writeString(this.contactPerson);
        dest.writeString(this.contactPersonNumber);
        dest.writeSerializable(this.imageFile);
    }

    public RegisterRequest() {
    }

    protected RegisterRequest(Parcel in) {
        this.firstName = in.readString();
        this.middleName = in.readString();
        this.lastName = in.readString();
        this.emailAddress = in.readString();
        this.password = in.readString();
        this.confirmPassword = in.readString();
        this.mobileNumber = in.readString();
        this.birthday = in.readString();
        this.gender = in.readString();
        this.address = in.readString();
        this.contactPerson = in.readString();
        this.contactPersonNumber = in.readString();
        this.imageFile = (File) in.readSerializable();
    }

    public static final Parcelable.Creator<RegisterRequest> CREATOR = new Parcelable.Creator<RegisterRequest>() {
        @Override
        public RegisterRequest createFromParcel(Parcel source) {
            return new RegisterRequest(source);
        }

        @Override
        public RegisterRequest[] newArray(int size) {
            return new RegisterRequest[size];
        }
    };

    public static class Builder {
        private String firstName;
        private String middleName;
        private String lastName;
        private String emailAddress;
        private String password;
        private String confirmPassword;
        private String mobileNumber;
        private String birthday;
        private String gender;
        private String address;
        private String contactPerson;
        private String contactPersonNumber;
        private File imageFile;
        private String medicalInformation;

        public Builder() {
        }

        public Builder(RegisterRequest registerRequest) {
            this.firstName = registerRequest.firstName;
            this.middleName = registerRequest.middleName;
            this.lastName = registerRequest.lastName;
            this.emailAddress = registerRequest.emailAddress;
            this.password = registerRequest.password;
            this.confirmPassword = registerRequest.confirmPassword;
            this.mobileNumber = registerRequest.mobileNumber;
            this.birthday = registerRequest.birthday;
            this.gender = registerRequest.gender;
            this.address = registerRequest.address;
            this.contactPerson = registerRequest.contactPerson;
            this.contactPersonNumber = registerRequest.contactPersonNumber;
            this.imageFile = registerRequest.imageFile;
            this.medicalInformation = registerRequest.medicalInformation;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder confirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
            return this;
        }

        public Builder mobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
            return this;
        }

        public Builder birthday(String birthday) {
            this.birthday = birthday;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder contactPerson(String contactPerson) {
            this.contactPerson = contactPerson;
            return this;
        }

        public Builder contactPersonNumber(String contactPersonNumber) {
            this.contactPersonNumber = contactPersonNumber;
            return this;
        }

        public Builder medicalInformation(String medicalInformation) {
            this.medicalInformation = medicalInformation;
            return this;
        }

        public Builder imageFile(File imageFile) {
            this.imageFile = imageFile;
            return this;
        }

        public RegisterRequest build() {
            return new RegisterRequest(this);
        }
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public HashMap<String, RequestBody> create() {
        HashMap<String, RequestBody> partMap = new HashMap<>();

        MediaType textPlain = MediaType.parse("text/plain");
        MediaType image = MediaType.parse("image/*");

        partMap.put("first_name", RequestBody.create(textPlain, firstName));
        partMap.put("middle_name", RequestBody.create(textPlain, middleName));
        partMap.put("last_name", RequestBody.create(textPlain, lastName));
        partMap.put("email", RequestBody.create(textPlain, emailAddress));
        partMap.put("password", RequestBody.create(textPlain, password));
        partMap.put("confirm_password", RequestBody.create(textPlain, confirmPassword));
        partMap.put("mobile_number", RequestBody.create(textPlain, mobileNumber));
        partMap.put("birthday", RequestBody.create(textPlain, birthday));
        partMap.put("gender", RequestBody.create(textPlain, gender));
        partMap.put("address", RequestBody.create(textPlain, address));

        partMap.put("contact_person", RequestBody.create(textPlain, !TextUtils.isEmpty(contactPerson) ? contactPerson : ""));
        partMap.put("contact_person_number", RequestBody.create(textPlain, contactPersonNumber));

        partMap.put("medical_info", RequestBody.create(textPlain, !TextUtils.isEmpty(medicalInformation) ? medicalInformation : ""));

        if (imageFile != null) {
            Timber.d("FILENAME: %s", imageFile.getName());
            partMap.put("image\"; filename=\"" + imageFile.getName(), RequestBody.create(image, imageFile));
        }

        return partMap;
    }
}
