package ph.com.auspex.mers.password.changepassword;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.password.PasswordModule;
import ph.com.auspex.mers.password.changepassword.ChangePasswordContract.ChangePasswordPresenter;
import ph.com.auspex.mers.password.changepassword.ChangePasswordContract.ChangePasswordView;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.AdvancedTextInputEditText;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.util.FormValidationUtil;

/**
 * Change password controller (coming from profile)
 * This class is different from {@link ph.com.auspex.mers.password.updatepassword.UpdatePasswordController} which main function is to Update the password when the user forgot his/her password
 */
public class ChangePasswordController extends MosbyController<ChangePasswordView, ChangePasswordPresenter> implements ChangePasswordView {

    public static final String TAG = ChangePasswordController.class.getSimpleName();
    public static final String KEY_USER_ID = TAG + ".userId";

    @BindView(R.id.input_new_password) AdvancedTextInputEditText inputNewPassword;
    @BindView(R.id.input_confirm_password) AdvancedTextInputEditText inputConfirmPassword;

    @BindString(R.string.label_update_password) String title;

    @Inject ChangePasswordPresenter presenter;
    @Inject Router parentRouter;
    @Inject FormValidationUtil validationUtil;

    private String userId;

    public ChangePasswordController(@Nullable Bundle args) {
        super(args);

        if (args != null) {
            userId = args.getString(KEY_USER_ID);
        }
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.passwordComponent(new PasswordModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_change_password, container, false);
    }

    /**
     * Send the password fields to the presenter
     */
    @OnClick(R.id.button_submit)
    void onSubmitClicked() {
        if (!validationUtil.hasEmptyField(inputNewPassword, inputConfirmPassword)
                && !validationUtil.hasError(inputNewPassword, inputConfirmPassword)
                && validationUtil.isPasswordSame(inputNewPassword, inputConfirmPassword)) {
            presenter.updatePassword(userId, inputNewPassword.getText().toString());
        }
    }

    @NonNull
    @Override
    public ChangePasswordPresenter createPresenter() {
        return presenter;
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_register;
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                handleBack();
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    /**
     * If the password is successfully updated
     *
     * @param message Dialog display message
     */
    @Override
    public void onPasswordUpdated(String message) {
        new MaterialDialog.Builder(activity)
                .content(message)
                .positiveText(R.string.proceed)
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    parentRouter.setRoot(RouterTransaction.with(new NavigationController()));
                })
                .positiveColorRes(R.color.app_blue)
                .show();
    }

    @Override
    public boolean handleBack() {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_cancel_update_password)
                .content(R.string.dialog_negative_update_password)
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_no)
                .onPositive((dialog, which) -> parentRouter.popToRoot())
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
        return true;
    }

    /**
     * Show Error Dialog
     *
     * @param message String to be displayed
     */
    @Override
    public void showError(String message) {
        new MaterialDialog.Builder(activity)
                .content(message)
                .negativeText(R.string.error_retry)
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                    inputConfirmPassword.requestFocus();
                })
                .show();
    }
}
