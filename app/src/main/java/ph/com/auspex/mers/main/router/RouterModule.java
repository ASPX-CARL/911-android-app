package ph.com.auspex.mers.main.router;

import com.bluelinelabs.conductor.Router;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.main.ActivityScope;

/**
 * Provides object for dependency graph injection
 */
@Module
public class RouterModule {

    private final Router parentRouter;

    /**
     * Create this module using the {@link Router} from {@link ph.com.auspex.mers.main.MainActivity}
     *
     * @param parentRouter parent router
     */
    public RouterModule(Router parentRouter) {
        this.parentRouter = parentRouter;
    }

    /**
     * Provides the parent router from {@link ph.com.auspex.mers.main.MainActivity}
     *
     * @return parent router
     */
    @ActivityScope
    @Provides
    Router providesParentRouter() {
        return parentRouter;
    }
}
