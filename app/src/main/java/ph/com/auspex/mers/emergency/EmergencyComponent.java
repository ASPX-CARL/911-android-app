package ph.com.auspex.mers.emergency;

import dagger.Subcomponent;

@EmergencyScope
@Subcomponent(
        modules = EmergencyModule.class
)
public interface EmergencyComponent {
    void inject(EmergencyController emergencyController);
}
