package ph.com.auspex.mers.familylink.detail;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.conductor.ControllerChangeHandler;
import com.bluelinelabs.conductor.ControllerChangeType;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.BaseController;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.familylink.FamilyLinkModule;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.util.TopSheetAnimationUtil;
import ph.com.auspex.mers.util.GlideImageLoader;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

/**
 * Provide a dialog for the family link detail
 */
public class FamLinkDetailDialogController extends BaseController {

    public static final String TAG = FamLinkDetailDialogController.class.getSimpleName();
    public static final String KEY_FAMILY_LINK_DETAIL = TAG + ".familyLinkDetail";

    @BindView(R.id.tv_family_link_name) TextView tvFamilyLinkName;
    @BindView(R.id.tv_family_link_relationship) TextView tvRelationship;
    @BindView(R.id.tv_family_link_mobile_number) TextView tvFamilyLinkMobileNumber;
    @BindView(R.id.iv_profile_image) ImageView ivProfileImage;
    @BindView(R.id.dialog_container) RelativeLayout dialogContainer;

    @Inject Resources resources;

    private FamilyLinkDetail familyLinkDetail;
    private boolean isExiting = false;

    public FamLinkDetailDialogController(Bundle args) {
        super(args);

        if (args != null && args.containsKey(KEY_FAMILY_LINK_DETAIL)) {
            familyLinkDetail = args.getParcelable(KEY_FAMILY_LINK_DETAIL);
        }
    }

    /**
     * Initialize views
     *
     * @param view ui view
     */
    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        tvFamilyLinkName.setText(String.format("%s %s %s", familyLinkDetail.getFirstName(), familyLinkDetail.getMiddleName(), familyLinkDetail.getLastName()));
        tvRelationship.setText(familyLinkDetail.getRelationship());
        tvFamilyLinkMobileNumber.setText(familyLinkDetail.getMobileNumber());

        GlideImageLoader.loadUrl(getActivity(), ivProfileImage, familyLinkDetail.getImageUrl(), R.drawable.placeholder_male, R.drawable.placeholder_male, true);
    }

    /**
     * Build and inject activity component to this class
     *
     * @param activityComponent {@link ActivityComponent} instance
     */
    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.familyLinkComponent(new FamilyLinkModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_family_link_detail, container, false);
    }

    /**
     * Prevent {@link android.support.v7.widget.Toolbar} initialization
     */
    @Override
    protected void initToolbar() {
    }

    /**
     * Dismiss this dialog controller on overlay click
     */
    @OnClick(R.id.parent_overlay)
    void dismiss() {
        isExiting = true;
        TopSheetAnimationUtil.topSheetExitAnimation(dialogContainer, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                closeDialogController();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    /**
     * Use the device's call feature to call the mobile number provided
     */
    @OnClick(R.id.fab_call)
    void onCallButtonClicked() {
        requestCallPhonePermissions();
    }

    /**
     * Request for {@link Manifest.permission#CALL_PHONE} permission
     */
    private void requestCallPhonePermissions() {
        Nammu.askForPermission(activity, Manifest.permission.CALL_PHONE, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                callNumber();
            }

            @Override
            public void permissionRefused() {
                showError(activity.getString(R.string.error_permission_not_granted_call));
            }
        });

    }

    /**
     * Call the provided mobile number
     */
    private void callNumber() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(String.format("tel:%s", familyLinkDetail.getMobileNumber())));
        startActivity(intent);
    }

    /**
     * Listen for controller animation finish
     *
     * @param changeHandler
     * @param changeType
     */
    @Override
    protected void onChangeEnded(@NonNull ControllerChangeHandler changeHandler, @NonNull ControllerChangeType changeType) {
        super.onChangeEnded(changeHandler, changeType);

        if (!isExiting) {
            ObjectAnimator animation = ObjectAnimator.ofFloat(dialogContainer, View.TRANSLATION_Y, -dialogContainer.getHeight(), 0);
            animation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    dialogContainer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    animation.cancel();
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animation.setDuration(250);
            animation.start();
        }
    }

    /**
     * Handle back button press on device
     *
     * @return always <code>true</code>
     */
    @Override
    public boolean handleBack() {
        closeDialogController();
        return true;
    }

    /**
     * Dismiss this dialog controller
     */
    @OnClick(R.id.parent_overlay)
    void closeDialogController() {
        getRouter().popController(this);
    }
}
