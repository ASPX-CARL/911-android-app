package ph.com.auspex.mers.main;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.ActionBarInterface;
import ph.com.auspex.mers.common.dialog.ProgressLoadingDialog;

/**
 * Provides dependency needed for the injection graph
 */
@Module
public class ActivityModule {

    private final Activity activity;

    /**
     * Constructs this module using  {@link MainActivity}
     *
     * @param activity {@link MainActivity}
     */
    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    /**
     * Returns the {@link MainActivity} instance
     *
     * @return {@link MainActivity} but {@link Activity}
     */
    @ActivityScope
    @Provides
    Activity providesActivity() {
        return activity;
    }

    /**
     * Provides the progress loading dialog
     *
     * @param activity {@link MainActivity}
     * @return progress loading dialog
     */
    @ActivityScope
    @Provides
    ProgressLoadingDialog providesProgressLoadingDialog(Activity activity) {
        return new ProgressLoadingDialog(activity);
    }

    /**
     * Provides an interface to access the {@link android.support.v7.app.ActionBar} on a {@link com.bluelinelabs.conductor.Controller}
     *
     * @param activity {@link MainActivity}
     * @return {@link android.support.v7.app.ActionBar} interface
     */
    @Provides
    @ActivityScope
    ActionBarInterface providesActionBarInterface(Activity activity) {
        return (ActionBarInterface) activity;
    }
}
