package ph.com.auspex.mers;

import ph.com.auspex.mers.data.model.response.UserDetail;

/**
 * Provides a Singleton implementation for {@link UserDetail}
 */
public class UserWrapper {

    private UserDetail userDetail;

    /**
     * Constructor to create new UserWrapper instance
     */
    public UserWrapper() {
    }

    /**
     * Create new UserDetail instance
     *
     * @param userDetail from AppModule
     */
    public UserWrapper(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    /**
     * Get userDetail instance
     *
     * @return UserDetail instance
     */
    public UserDetail getUserDetail() {
        return userDetail;
    }

    /**
     * Set or update userDetail instance
     *
     * @param userDetail new or updated userDetail object
     */
    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    /**
     * Check if the user is logged in
     *
     * @return is user logged in
     */
    public boolean isLoggedIn() {
        return userDetail != null;
    }
}
