package ph.com.auspex.mers.emergency;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.response.EmergencyType;

public interface EmergencyContract {

    interface EmergencyPresenter extends MvpPresenter<EmergencyView> {
        void getEmergencyTypes();

        void report(EmergencyRequest request);
    }

    interface EmergencyView extends RemoteView {
        void displayEmergencyTypes(List<EmergencyType> emergencyTypes);

        void displayEmptyLayout();

        void call();

        void openImagePicker();

        void onRequestSuccess();

        void setupNotLoggedIn();
    }
}
