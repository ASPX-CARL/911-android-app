package ph.com.auspex.mers.splash;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.splash.impl.SplashPresenterImpl;

@Module
public class SplashModule {

    /**
     * Provide SplashInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return SplashInteractor
     */
    @SplashScope
    @Provides
    SplashInteractor providesSplashInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide SplashPresenter
     *
     * @param model {@link SplashInteractor}
     * @return SplashPresenter
     */
    @SplashScope
    @Provides
    SplashContract.SplashPresenter providesSplashPresenter(SplashInteractor model) {
        return new SplashPresenterImpl(model);
    }
}
