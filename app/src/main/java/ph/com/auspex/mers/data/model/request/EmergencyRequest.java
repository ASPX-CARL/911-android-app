package ph.com.auspex.mers.data.model.request;

import android.text.TextUtils;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Create Emergency Request
 */
public class EmergencyRequest {
    private String userId;
    private int emergencyTypeId;
    private int reportTypeId;
    private String longitude;
    private String latitude;
    private File imageFile;
    private String remarks;

    private EmergencyRequest(Builder builder) {
        this.userId = builder.userId;
        this.emergencyTypeId = checkNotNull(builder.emergencyTypeId);
        this.reportTypeId = checkNotNull(builder.reportTypeId);
        this.longitude = builder.longitude;
        this.latitude = builder.latitude;
        this.imageFile = builder.imageFile;
        this.remarks = builder.remarks;
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public HashMap<String, RequestBody> create() {
        HashMap<String, RequestBody> partMap = new HashMap<>();

        MediaType textPlain = MediaType.parse("text/plain");
        MediaType image = MediaType.parse("image/*");

        partMap.put("emergency_type_id", RequestBody.create(textPlain, Integer.toString(emergencyTypeId)));
        partMap.put("report_type", RequestBody.create(textPlain, Integer.toString(reportTypeId)));

        if (!TextUtils.isEmpty(userId)) {
            partMap.put("caller_id", RequestBody.create(textPlain, userId));
        }

        if (!TextUtils.isEmpty(longitude)) {
            partMap.put("longitude", RequestBody.create(textPlain, longitude));
        }

        if (!TextUtils.isEmpty(latitude)) {
            partMap.put("latitude", RequestBody.create(textPlain, latitude));
        }

        if (!TextUtils.isEmpty(remarks)) {
            partMap.put("remarks", RequestBody.create(textPlain, remarks));
        }

        if (imageFile != null) {
            partMap.put("image\"; filename=\"" + imageFile.getName() + ".jpg", RequestBody.create(image, imageFile));
        }

        return partMap;
    }

    public static class Builder {
        private String userId;
        private int emergencyTypeId;
        private int reportTypeId;
        private String longitude;
        private String latitude;
        private File imageFile;
        private String remarks;

        public Builder() {
        }

        public Builder(EmergencyRequest request) {
            this.userId = request.userId;
            this.emergencyTypeId = request.emergencyTypeId;
            this.reportTypeId = request.reportTypeId;
            this.longitude = request.longitude;
            this.latitude = request.latitude;
            this.imageFile = request.imageFile;
            this.remarks = request.remarks;
        }

        public Builder userId(String userId) {
            this.userId = !TextUtils.isEmpty(userId) ? userId : "";
            return this;
        }

        public Builder reportTypeId(int reportTypeId) {
            this.reportTypeId = reportTypeId;
            return this;
        }

        public Builder emergencyTypeId(int emergencyTypeId) {
            this.emergencyTypeId = emergencyTypeId;
            return this;
        }

        public Builder longitude(String longitude) {
            this.longitude = !TextUtils.isEmpty(longitude) ? longitude : "";
            return this;
        }

        public Builder latitude(String latitude) {
            this.latitude = !TextUtils.isEmpty(latitude) ? latitude : "";
            return this;
        }

        public Builder remarks(String remarks) {
            this.remarks = remarks;
            return this;
        }

        public Builder imageFile(File imageFile) {
            this.imageFile = imageFile;
            return this;
        }

        public EmergencyRequest build() {
            return new EmergencyRequest(this);
        }
    }

    @Override
    public String toString() {
        return "EmergencyRequest{" +
                "userId='" + userId + '\'' +
                ", emergencyTypeId=" + emergencyTypeId +
                ", reportTypeId=" + reportTypeId +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", imageFile=" + imageFile +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
