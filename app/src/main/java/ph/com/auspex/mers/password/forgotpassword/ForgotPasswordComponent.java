package ph.com.auspex.mers.password.forgotpassword;

import dagger.Subcomponent;

@ForgotPasswordScope
@Subcomponent(modules = ForgotPasswordModule.class)
public interface ForgotPasswordComponent {
    void inject(ForgotPasswordController forgotPasswordController);
}
