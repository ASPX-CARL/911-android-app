package ph.com.auspex.mers.util;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

/**
 * Animation util to replicate {@link android.support.design.widget.BottomSheetBehavior} default animation
 */
public class TopSheetAnimationUtil {

    /**
     * Top sheet entrance animation
     *
     * @param view View to be animated
     */
    public static void topSheetEntranceAnimation(View view) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, -view.getHeight(), 0);
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                animation.cancel();
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animation.setDuration(250);
        animation.start();
    }

    /**
     * Top sheet exit animation
     *
     * @param view View to be animated
     * @param listener Exit animation listener
     */
    public static void topSheetExitAnimation(View view, Animator.AnimatorListener listener) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, -view.getHeight());
        animation.addListener(listener);
        animation.setDuration(250);
        animation.start();
    }
}
