package ph.com.auspex.mers.login;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;

public interface LoginContract {

    interface LoginPresenter extends MvpPresenter<LoginView> {
        void login(String email, String password);
    }

    interface LoginView extends RemoteView {

        void onLoginSuccessful();

        void onLoginFailed(String message);
    }
}
