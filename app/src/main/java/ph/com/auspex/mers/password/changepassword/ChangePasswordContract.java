package ph.com.auspex.mers.password.changepassword;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;

public interface ChangePasswordContract {

    interface ChangePasswordPresenter extends MvpPresenter<ChangePasswordView> {
        void updatePassword(String userId, String password);
    }

    interface ChangePasswordView extends RemoteView {
        void onPasswordUpdated(String message);
    }
}
