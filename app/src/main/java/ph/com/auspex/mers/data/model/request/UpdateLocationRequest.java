package ph.com.auspex.mers.data.model.request;

import java.util.HashMap;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Create update location api request
 */
public class UpdateLocationRequest {

    private String userId;
    private String latitude;
    private String longitude;

    private UpdateLocationRequest(Builder builder) {
        this.userId = checkNotNull(builder.userId);
        this.latitude = checkNotNull(builder.latitude);
        this.longitude = checkNotNull(builder.longitude);
    }

    public String getUserId() {
        return userId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public static class Builder {
        private String userId;
        private String latitude;
        private String longitude;

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder latitude(String latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(String longitude) {
            this.longitude = longitude;
            return this;
        }

        public UpdateLocationRequest build() {
            return new UpdateLocationRequest(this);
        }
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public HashMap<String, String> createQuery() {
        HashMap<String, String> query = new HashMap<>();

        query.put("user_id", userId);
        query.put("lat", latitude);
        query.put("long", longitude);

        return query;
    }
}
