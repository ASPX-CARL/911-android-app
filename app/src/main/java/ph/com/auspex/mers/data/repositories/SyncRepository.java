package ph.com.auspex.mers.data.repositories;

import java.util.List;

import io.reactivex.Observable;
import ph.com.auspex.mers.data.model.response.Relationship;

public interface SyncRepository {

    Observable<List<Relationship>> getFamilyRelationships();
}
