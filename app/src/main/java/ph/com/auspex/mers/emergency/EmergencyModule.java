package ph.com.auspex.mers.emergency;

import android.content.res.Resources;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.data.AppDataSource;

/**
 * Provide the emergency module object for dependency injection
 */
@Module
public class EmergencyModule {

    /**
     * Provide EmergencyInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return EmergencyInteractor
     */
    @EmergencyScope
    @Provides
    EmergencyInteractor providesInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide EmergencyPresenter
     *
     * @param interactor  {@link EmergencyInteractor}
     * @param resources   {@link Resources}
     * @param userWrapper {@link UserWrapper}
     * @return EmergencyPresenter
     */
    @EmergencyScope
    @Provides
    EmergencyContract.EmergencyPresenter providesPresenter(EmergencyInteractor interactor, Resources resources, UserWrapper userWrapper) {
        return new EmergencyPresenterImpl(interactor, resources, userWrapper);
    }
}
