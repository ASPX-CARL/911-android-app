package ph.com.auspex.mers.news.rss;

import android.util.SparseArray;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;

public interface NewsContract {

    interface NewsPresenter extends MvpPresenter<NewsView> {
        void getRssNews();
    }

    interface NewsView extends RemoteView {
        void setData(SparseArray<Object> data);

        void scrollListToTop();
    }
}
