package ph.com.auspex.mers.data.local;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;
import timber.log.Timber;

/**
 * Realm helper class
 * <p>
 * see <a href="https://blog.realm.io/realm-for-android/">Realm for Android</a>
 */
public class RealmController {
    public static final String FILE_REALM = "mers.realm";

    /**
     * Get all object from realm using the given class
     *
     * @param realm Realm instance
     * @param clazz Class of the object to be retrieved from {@link Realm}
     * @param <T>   Generic type
     * @return List of Generic type
     */
    public <T extends RealmModel> List<T> getAllFromRealm(Realm realm, Class<T> clazz) {
        return realm.copyFromRealm(realm.where(clazz).findAll());
    }

    /**
     * Add or Update objects found in realm using the given {@link List}
     *
     * @param realm {@link Realm} instance
     * @param list  List to be added or updated in Realm
     */
    public void addOrUpdateToRealm(Realm realm, final List<? extends RealmObject> list) {
        realm.executeTransaction(new AddTransaction<>(list));
    }

    /**
     * Encapsulates a Realm transaction.
     * <p>
     * Using this class will automatically handle {@link Realm#beginTransaction()} and {@link  Realm#commitTransaction()}
     * If any exception is thrown during the transaction {@link  Realm#cancelTransaction()} will be called instead of
     * {@link  Realm#commitTransaction()}.
     */
    public class AddTransaction<T extends RealmObject> implements Realm.Transaction {

        private List<T> list;

        public AddTransaction(List<T> list) {
            this.list = list;
        }

        @Override
        public void execute(Realm realm) {
            if (list != null) {
                realm.copyToRealmOrUpdate(list);

                if (!list.isEmpty()) {
                    Timber.d("%s items successfully added to %s", list.size(), list.get(0).getClass().getSimpleName());
                } else {
                    Timber.d("No items were added to realm");
                }
            }
        }
    }
}
