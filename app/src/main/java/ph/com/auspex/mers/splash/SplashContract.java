package ph.com.auspex.mers.splash;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import ph.com.auspex.mers.common.mvp.view.RemoteView;

public interface SplashContract {

    interface SplashPresenter extends MvpPresenter<SplashView> {
        void getRelationshipTypes();
    }

    interface SplashView extends RemoteView, MvpView {
        void onSyncComplete();

        void onSyncHasError();
    }
}
