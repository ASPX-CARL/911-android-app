package ph.com.auspex.mers.splash;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import javax.inject.Inject;

import ph.com.auspex.mers.R;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.newsdetail.NewsDetailController;
import ph.com.auspex.mers.verification.VerificationController;

import static ph.com.auspex.mers.data.model.request.VerificationRequest.VerificationType.PASSWORD;
import static ph.com.auspex.mers.data.model.request.VerificationRequest.VerificationType.REGISTRATION;

/**
 * Splash Controller UI
 */
public class SplashController extends MosbyController<SplashContract.SplashView, SplashContract.SplashPresenter> implements SplashContract.SplashView {

    public static final String TAG = SplashController.class.getSimpleName();
    public static final String KEY_URL = TAG + ".url";
    public static final String KEY_NEWS_ID = TAG + ".newsId";

    @Inject SplashContract.SplashPresenter presenter;
    @Inject Router parentRouter;
    @Inject UserWrapper userWrapper;

    private Uri uri;
    private String newsUrl;

    public SplashController() {
    }

    public SplashController(@Nullable Bundle args) {
        super(args);

        if (args != null && args.getString(KEY_URL) != null) {
            uri = Uri.parse(args.getString(KEY_URL));
        } else if (args != null && args.getString(KEY_NEWS_ID) != null) {
            newsUrl = args.getString(KEY_NEWS_ID);
        }
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_splash, container, false);
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.splashComponent(new SplashModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
    }

    @NonNull
    @Override
    public SplashContract.SplashPresenter createPresenter() {
        return presenter;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    protected void onDestroyView(@NonNull View view) {
        presenter.detachView(false);
        super.onDestroyView(view);
    }

    /**
     * On sync complete then handle result
     */
    @Override
    public void onSyncComplete() {
        handleResult();
    }

    /**
     * If encountered an error during sync, proceed with handling result
     */
    @Override
    public void onSyncHasError() {
        handleResult();
    }

    /**
     * Determine which screen should be opened after Splash screen
     */
    private void handleResult() {
        if (uri != null && !userWrapper.isLoggedIn()) {
            String userId = uri.getQueryParameter("u");
            String code = uri.getQueryParameter("verification_code");
            String type = uri.getQueryParameter("verification_type");

            Bundle args = new Bundle();
            args.putString(VerificationController.KEY_USER_ID, userId);
            args.putSerializable(VerificationController.KEY_VERIFICATION_TYPE, type.equals(REGISTRATION.getValue()) ? REGISTRATION : PASSWORD);
            args.putString(VerificationController.KEY_CODE, code);

            parentRouter.pushController(RouterTransaction.with(new VerificationController(args)));
        } else if (newsUrl != null) {
            Bundle args = new Bundle();
            args.putString(NewsDetailController.KEY_URL, newsUrl);
            parentRouter.pushController(RouterTransaction.with(new NewsDetailController(args)));
        } else {
            parentRouter.setRoot(RouterTransaction.with(new NavigationController()).tag(NavigationController.TAG));
        }
    }
}