package ph.com.auspex.mers.news;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.support.RouterPagerAdapter;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.BaseController;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.news.internal.InternalNewsController;
import ph.com.auspex.mers.news.rss.NewsController;
import ph.com.auspex.mers.util.MaterialDialogManager;

/**
 * News ViewPager Controller
 */
public class NewsViewPagerController extends BaseController implements ViewPager.OnPageChangeListener {

    public static final String TAG = NewsViewPagerController.class.getSimpleName();

    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @BindView(R.id.view_pager) ViewPager viewPager;

    @BindString(R.string.label_news) String title;
    @BindArray(R.array.news_pager_titles) String[] pagerTitles;

    private NewsPagerAdapter adapter;

    /**
     * Constructor that creates a new NewsViewPagerController instance and {@link NewsPagerAdapter} instance
     */
    public NewsViewPagerController() {
        adapter = new NewsPagerAdapter(this);
    }

    /**
     * Setup Activity Component
     *
     * @param activityComponent {@link ActivityComponent} instance
     */
    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {

    }

    /**
     * Set layout resource for this controller
     *
     * @param inflater  Controller's LayoutInflater
     * @param container Controller's ViewGroup container
     * @return
     */
    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_view_pager, container, false);
    }

    /**
     * Initialize {@link #viewPager} and {@link #tabLayout}
     *
     * @param view {@link Controller#getView()}
     */
    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * View Pager Adapter for {@link NewsViewPagerController} class
     */
    class NewsPagerAdapter extends RouterPagerAdapter {

        /**
         * Constructor that creates a new NewsPagerAdapter instance
         *
         * @param host Parent Controller that hosts the {@link ViewPager}
         */
        public NewsPagerAdapter(@NonNull Controller host) {
            super(host);
        }

        /**
         * Set which {@link Controller} will be used for the {@link ViewPager}
         *
         * @param router   Hosting {@link Router}
         * @param position {@link ViewPager} page index
         */
        @Override
        public void configureRouter(@NonNull Router router, int position) {
            switch (position) {
                case 0:
                    router.setRoot(RouterTransaction.with(new NewsController()));
                    break;
                case 1:
                    router.setRoot(RouterTransaction.with(new InternalNewsController()));
                    break;
            }
        }

        /**
         * Total {@link ViewPager} page count
         *
         * @return Page count
         */
        @Override
        public int getCount() {
            return pagerTitles.length;
        }

        /**
         * Display the title for the corresponding viewPager page
         *
         * @param position viewPager index
         * @return Title for the controller in the viewPager
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return pagerTitles[position];
        }
    }

    /**
     * This controller's title not the viewPager's controller title
     *
     * @return Controller Title
     */
    @Override
    protected CharSequence getTitle() {
        return title;
    }

    /**
     * On back button pressed action
     *
     * @return Return true if this controller has consumed the back button
     */
    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCloseApplicationDialog(activity);
        return true;
    }
}
