package ph.com.auspex.mers.familylink.dialog.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.data.model.response.Relationship;
import ph.com.auspex.mers.util.AppUtil;

/**
 * Provide a relationship adapter for {@link android.widget.Spinner}
 */
public class RelationshipAdapter extends ArrayAdapter<Relationship> {

    private List<Relationship> relationships;

    public RelationshipAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Relationship> objects) {
        super(context, resource, objects);
        this.relationships = objects;
    }

    /**
     * Return the expanded view for the spinner
     *
     * @param position    object position
     * @param convertView inflated view
     * @param parent      parent view
     * @return View for {@link android.widget.Spinner} items
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final RelationshipViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_spinner, parent, false);
            viewHolder = new RelationshipViewHolder(convertView);
            convertView.setTag(viewHolder);
            viewHolder.bind(relationships.get(position).getName());
        } else {
            viewHolder = (RelationshipViewHolder) convertView.getTag();
            viewHolder.bind(relationships.get(position).getName());
        }

        return convertView;
    }

    /**
     * Return the dropdown view for the spinner
     *
     * @param position    object position
     * @param convertView inflated view
     * @param parent      parent view
     * @return View for {@link android.widget.Spinner} dropdown view
     */
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final RelationshipViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_spinner_dropdown, parent, false);
            viewHolder = new RelationshipViewHolder(convertView);
            convertView.setTag(viewHolder);
            viewHolder.bind(relationships.get(position).getName());
        } else {
            viewHolder = (RelationshipViewHolder) convertView.getTag();
            viewHolder.bind(relationships.get(position).getName());
        }

        return convertView;
    }

    /**
     * Returns object on the given position
     *
     * @param position position of the object in the list
     * @return {@link Relationship}
     */
    @NonNull
    @Override
    public Relationship getItem(int position) {
        return relationships.get(position);
    }

    /**
     * Relationship {@link android.support.v7.widget.RecyclerView.ViewHolder}
     */
    static class RelationshipViewHolder {

        @BindView(R.id.text1) TextView tvRelationshipName;

        public RelationshipViewHolder(View rootView) {
            ButterKnife.bind(this, rootView);
        }

        public void bind(String name) {
            tvRelationshipName.setText(AppUtil.capitalizeFirstLetter(name));
        }
    }
}
