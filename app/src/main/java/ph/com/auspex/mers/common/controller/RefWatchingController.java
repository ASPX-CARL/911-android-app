package ph.com.auspex.mers.common.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bluelinelabs.conductor.ControllerChangeHandler;
import com.bluelinelabs.conductor.ControllerChangeType;

import ph.com.auspex.mers.MersApplication;

public abstract class RefWatchingController extends ButterKnifeController {

    private boolean hasExited;

    public RefWatchingController() {
    }

    public RefWatchingController(@Nullable Bundle args) {
        super(args);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (hasExited) {
            MersApplication.refWatcher.watch(this);
        }
    }

    @Override
    protected void onChangeEnded(@NonNull ControllerChangeHandler changeHandler, @NonNull ControllerChangeType changeType) {
        super.onChangeEnded(changeHandler, changeType);

        hasExited = !changeType.isEnter;

        if (isDestroyed()) {
            MersApplication.refWatcher.watch(this);
        }
    }
}
