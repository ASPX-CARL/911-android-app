package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Provide a POJO for Internal News response from {@link ph.com.auspex.mers.data.remote.ApiService#getInternalNews(String)}
 */
public class InternalFeed {

    @SerializedName("internal_news_id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("content")
    private String content;
    @SerializedName("thumbnail_image")
    private String thumbnailUrl;
    @SerializedName("url")
    private String url;

    /**
     * Returns the internal news id
     *
     * @return internal news id
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the internal news' title
     *
     * @return news title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the internal news' content
     *
     * @return news content
     */
    public String getContent() {
        return content;
    }

    /**
     * Return the internal news' thumbnail url
     *
     * @return thumbnail url
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * Return the internal news' url
     *
     * @return news url
     */
    public String getUrl() {
        return url;
    }
}
