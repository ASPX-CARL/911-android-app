package ph.com.auspex.mers.data.model.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ph.com.auspex.mers.data.model.request.GenderEnum;

public class GenderDeserializer implements JsonDeserializer<GenderEnum> {
    @Override
    public GenderEnum deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String gender = json.getAsString();

        if (gender.equals("Male")) {
            return GenderEnum.MALE;
        } else {
            return GenderEnum.FEMALE;
        }
    }
}
