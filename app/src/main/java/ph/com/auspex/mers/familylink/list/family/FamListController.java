package ph.com.auspex.mers.familylink.list.family;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.adapter.ItemClickListener;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.views.RecyclerLoadingView;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.familylink.FamilyLinkModule;
import ph.com.auspex.mers.familylink.dialog.AddFamilyLinkDialogController;
import ph.com.auspex.mers.familylink.list.adapter.FamilyLinkRecyclerAdapter;
import ph.com.auspex.mers.familylink.list.adapter.InviteCodeViewHolder;
import ph.com.auspex.mers.main.ActivityComponent;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

public class FamListController extends MosbyController<FamListContract.FamListView, FamListContract.FamListPresenter> implements
        RecyclerLoadingView.ListRefreshListener, FamListContract.FamListView, ItemClickListener<String>, InviteCodeViewHolder.AddButtonClickListener {

    @BindView(R.id.recycler_loading_view) RecyclerLoadingView recyclerLoadingView;

    @BindString(R.string.error_permission_not_granted_call) String textPermissionDenied;

    @Inject FamListContract.FamListPresenter presenter;
    @Inject UserWrapper userWrapper;

    private FamilyLinkRecyclerAdapter adapter;

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.familyLinkComponent(new FamilyLinkModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        initRecyclerView();
    }

    private void initRecyclerView() {
        adapter = new FamilyLinkRecyclerAdapter(this, this);
        recyclerLoadingView.setAdapter(adapter);

        recyclerLoadingView.setHasFixedSize(true);
        recyclerLoadingView.setActionButtonListener(v -> onAddButtonClicked());
        recyclerLoadingView.setRefreshListener(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_family_link_list, container, false);
    }

    @Override
    public void onRefresh() {
        presenter.getFamilyLinkList();
    }

    @Override
    protected void initToolbar() {
    }

    @NonNull
    @Override
    public FamListContract.FamListPresenter createPresenter() {
        return presenter;
    }

    @Override
    public void displayFamilyList(List<FamilyLinkDetail> familyLinkList) {
        if (familyLinkList.size() > 0) {
            adapter.updateList(userWrapper.getUserDetail().getInviteCode(), familyLinkList);
            recyclerLoadingView.showList();
        } else {
            recyclerLoadingView.showEmptyView();
        }
    }

    @Override
    public void onItemClicked(String mobileNumber) {
       /* rxPermissions.request(Manifest.permission.CALL_PHONE)
                .subscribe(granted -> {
                    if (granted) {
                        callNumber(mobileNumber);
                    } else {
                        showToast(textPermissionDenied);
                    }
                });*/

        Nammu.askForPermission(activity, Manifest.permission.CALL_PHONE, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                callNumber(mobileNumber);
            }

            @Override
            public void permissionRefused() {
                showError(activity.getString(R.string.error_permission_not_granted_call));
            }
        });
    }

    private void callNumber(String mobileNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(String.format("tel:%s", mobileNumber)));
        startActivity(intent);
    }

    @Override
    public void onAddButtonClicked() {
        setOverlayController(new AddFamilyLinkDialogController());
    }

    @Override
    public void showLoading() {
        recyclerLoadingView.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        recyclerLoadingView.setRefreshing(false);
    }
}
