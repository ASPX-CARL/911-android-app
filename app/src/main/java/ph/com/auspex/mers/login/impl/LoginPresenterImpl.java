package ph.com.auspex.mers.login.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.AccessToken;
import ph.com.auspex.mers.login.LoginContract;
import ph.com.auspex.mers.login.LoginInteractor;

/**
 * Provides an implementation for {@link ph.com.auspex.mers.login.LoginContract.LoginPresenter}
 */
public class LoginPresenterImpl extends MvpBasePresenter<LoginContract.LoginView> implements LoginContract.LoginPresenter {

    private final LoginInteractor model;

    public LoginPresenterImpl(LoginInteractor model) {
        this.model = model;
    }

    /**
     * Send the email and password string to {@link ph.com.auspex.mers.data.AppDataSource} using {@link LoginInteractor}
     *
     * @param email    user input email address
     * @param password user input password
     */
    @Override
    public void login(String email, String password) {
        getView().showLoading();

        model.login(email, password, new BaseInteractor.ResponseListener<AccessToken>() {
            @Override
            public void onSuccess(AccessToken accessToken) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onLoginSuccessful();
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onLoginFailed(message);
                }
            }
        });
    }
}
