package ph.com.auspex.mers.familylink.list.family;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;

public interface FamListContract {

    interface FamListPresenter extends MvpPresenter<FamListView> {
        void getFamilyLinkList();
    }

    interface FamListView extends RemoteView {
        void displayFamilyList(List<FamilyLinkDetail> familyLinkList);
    }
}
