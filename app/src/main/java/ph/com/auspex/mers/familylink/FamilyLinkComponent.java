package ph.com.auspex.mers.familylink;

import dagger.Subcomponent;
import ph.com.auspex.mers.familylink.detail.FamLinkDetailDialogController;
import ph.com.auspex.mers.familylink.dialog.AddFamilyLinkDialogController;
import ph.com.auspex.mers.familylink.list.family.FamListController;
import ph.com.auspex.mers.familylink.list.invites.FamInvitesController;
import ph.com.auspex.mers.familylink.map.FamMapController;

@FamilyLinkScope
@Subcomponent(
        modules = FamilyLinkModule.class
)
public interface FamilyLinkComponent {
    void inject(FamMapController familyLinkMapController);

    void inject(AddFamilyLinkDialogController addFamilyLinkDialogController);

    void inject(FamInvitesController famInvitesController);

    void inject(FamListController famListController);

    void inject(FamLinkDetailDialogController famLinkDetailDialogController);
}
