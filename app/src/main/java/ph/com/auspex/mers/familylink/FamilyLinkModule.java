package ph.com.auspex.mers.familylink;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.familylink.dialog.AddFamilyContract;
import ph.com.auspex.mers.familylink.dialog.impl.AddFamilyPresenterImpl;
import ph.com.auspex.mers.familylink.list.family.FamListContract;
import ph.com.auspex.mers.familylink.list.family.impl.FamListPresenterImpl;
import ph.com.auspex.mers.familylink.list.invites.FamInvitesContract;
import ph.com.auspex.mers.familylink.list.invites.impl.FamInvitesPresenterImpl;
import ph.com.auspex.mers.familylink.map.FamMapContract.FamMapPresenter;
import ph.com.auspex.mers.familylink.map.impl.FamMapPresenterImpl;

/**
 * Provide the family link module objects for dependency injection
 */
@Module
public class FamilyLinkModule {

    /**
     * Provide FamilyLinkInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return FamilyLinkInteractor
     */
    @FamilyLinkScope
    @Provides
    FamilyLinkInteractor providesFamilyLinkInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide FamMapPresenter
     *
     * @param model {@link FamilyLinkInteractor}
     * @return FamMapPresenter
     */
    @FamilyLinkScope
    @Provides
    FamMapPresenter providesFamMapPresenter(FamilyLinkInteractor model, RxEventBus rxEventBus) {
        return new FamMapPresenterImpl(model, rxEventBus);
    }

    /**
     * Provide AddFamilyPresenter
     *
     * @param model {@link FamilyLinkInteractor}
     * @return AddFamilyPresenter
     */
    @FamilyLinkScope
    @Provides
    AddFamilyContract.AddFamilyPresenter providesAddFamilyPresenter(FamilyLinkInteractor model) {
        return new AddFamilyPresenterImpl(model);
    }

    /**
     * Provide FamInvitesPresenter
     *
     * @param model {@link FamilyLinkInteractor}
     * @return FamInvitesPresenter
     */
    @FamilyLinkScope
    @Provides
    FamInvitesContract.FamInvitesPresenter providesFamilyInvitesPresenter(FamilyLinkInteractor model) {
        return new FamInvitesPresenterImpl(model);
    }

    /**
     * Provide FamListPresenter
     *
     * @param model {@link FamilyLinkInteractor}
     * @return FamListPresenter
     */
    @FamilyLinkScope
    @Provides
    FamListContract.FamListPresenter providesFamilyListPresenter(FamilyLinkInteractor model) {
        return new FamListPresenterImpl(model);
    }
}
