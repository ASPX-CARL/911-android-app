package ph.com.auspex.mers.data;

import android.content.Context;
import android.util.SparseArray;

import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.common.mvp.interactor.impl.BaseInteractorImpl;
import ph.com.auspex.mers.data.model.request.DirectionsRequest;
import ph.com.auspex.mers.data.model.request.EmergencyRequest;
import ph.com.auspex.mers.data.model.request.FamilyLinkInviteRequest;
import ph.com.auspex.mers.data.model.request.LoginRequest;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.data.model.request.UpdateLocationRequest;
import ph.com.auspex.mers.data.model.request.UpdatePasswordRequest;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.data.model.response.AccessToken;
import ph.com.auspex.mers.data.model.response.EmergencyType;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.data.model.response.InternalFeed;
import ph.com.auspex.mers.data.model.response.InternalNews;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;
import ph.com.auspex.mers.data.model.response.Relationship;
import ph.com.auspex.mers.data.model.response.UserDetail;
import ph.com.auspex.mers.data.remote.exception.ApiRequestException;
import ph.com.auspex.mers.data.sharedpref.AppPreferences;
import ph.com.auspex.mers.directories.DirectoryInteractor;
import ph.com.auspex.mers.emergency.EmergencyInteractor;
import ph.com.auspex.mers.familylink.FamilyLinkInteractor;
import ph.com.auspex.mers.login.LoginInteractor;
import ph.com.auspex.mers.news.NewsInteractor;
import ph.com.auspex.mers.password.PasswordInteractor;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordInteractor;
import ph.com.auspex.mers.profile.ProfileInteractor;
import ph.com.auspex.mers.register.RegisterInteractor;
import ph.com.auspex.mers.service.ServiceInteractor;
import ph.com.auspex.mers.splash.SplashInteractor;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.JwtUtil;
import ph.com.auspex.mers.verification.VerificationInteractor;

/**
 * This class merges {@link ph.com.auspex.mers.data.local.LocalDataSource} and {@link ph.com.auspex.mers.data.remote.RemoteDataSource}
 * which acts as the main data source for the interactors.
 */
public class AppDataSource extends BaseInteractorImpl implements SplashInteractor,
        NewsInteractor, LoginInteractor, RegisterInteractor, VerificationInteractor,
        ProfileInteractor, ServiceInteractor, ForgotPasswordInteractor, PasswordInteractor,
        FamilyLinkInteractor, DirectoryInteractor, EmergencyInteractor {

    private final DataSourceRepository remoteDataSource;
    private final DataSourceRepository localDataSource;
    private final AppPreferences appPreferences;
    private final UserWrapper userWrapper;
    private final Context context;

    public AppDataSource(Context context, DataSourceRepository remoteDataSource, DataSourceRepository localDataSource, AppPreferences appPreferences, UserWrapper userWrapper) {
        this.context = context;
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
        this.appPreferences = appPreferences;
        this.userWrapper = userWrapper;
    }

    /**
     * Syncs family link relationship types, emergency types and advertisements from API to {@link ph.com.auspex.mers.data.local.LocalDataSource}
     *
     * @param responseListener Listener whether the request is success or failed
     */
    @Override
    public void sync(SplashInteractor.ResponseListener responseListener) {
        Observable
                .concat(
                        remoteDataSource.getFamilyRelationships(),
                        remoteDataSource.getEmergencyTypes(),
                        remoteDataSource.getAdvertisements()
                )
                .compose(applySchedulers())
                .subscribe(
                        realmObjects -> responseListener.onSuccess(),
                        throwable -> responseListener.onFailure(throwable.getMessage())
                );
    }

    /**
     * Fetch the RSS News from the API
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getRssNews(BaseInteractor.ResponseListener<SparseArray<Object>> listener) {
        isRequestInProgress = true;

        remoteDataSource.getAdvertisements()
                .onErrorReturn(throwable -> Collections.emptyList())
                .zipWith(remoteDataSource.getNewsFeeds(), (advertisements, feeds) -> {
                    int tempId = 0;

                    SparseArray<Object> data = new SparseArray<>();

                    for (int i = 0; i < feeds.size(); i++) {
                        if (advertisements.size() > 0) {
                            if (i != 0 && i % 3 == 0) {
                                Random r = new Random();
                                data.put(tempId, advertisements.get(r.nextInt(advertisements.size())));
                                tempId += 1;
                            }
                        }

                        data.put(tempId, feeds.get(i));
                        tempId += 1;
                    }

                    return data;
                })
                .compose(applySchedulers())
                .subscribe(objectSparseArray -> {
                    isRequestInProgress = false;
                    listener.onSuccess(objectSparseArray);
                }, consumeOnError(listener));
    }

    /**
     * Fetch the internal news / alerts from the API
     *
     * @param oldInternalNews Previously downloaded list of internal news
     * @param listener        Listener whether the request is success or failed
     */
    @Override
    public void getIntNews(InternalNews oldInternalNews, BaseInteractor.ResponseListener<InternalNews> listener) {
        isRequestInProgress = true;

        remoteDataSource.getAdvertisements()
                .onErrorReturn(throwable -> Collections.emptyList())
                .zipWith(remoteDataSource.getInternalNews(oldInternalNews.getCurrentPage()), (advertisements, internalNews) -> {
                    if (oldInternalNews.getFeeds() == null) {
                        int tempId = 0;

                        SparseArray<Object> data = new SparseArray<>();

                        for (int i = 0; i < internalNews.getFeeds().size(); i++) {

                            if (advertisements.size() != 0 && advertisements.size() > 0) {
                                if (i != 0 && i % 3 == 0) {
                                    Random r = new Random();
                                    data.put(tempId, advertisements.get(r.nextInt(advertisements.size())));
                                    tempId += 1;
                                }
                            }

                            data.put(tempId, internalNews.getFeeds().get(i));
                            tempId += 1;
                        }

                        internalNews.setMergedFeedList(data);

                        return internalNews;
                    } else {
                        List<InternalFeed> updatedFeeds = oldInternalNews.getFeeds();
                        updatedFeeds.addAll(internalNews.getFeeds());

                        oldInternalNews.setFeeds(updatedFeeds);

                        int tempId = 0;

                        SparseArray<Object> data = new SparseArray<>();

                        for (int i = 0; i < oldInternalNews.getFeeds().size(); i++) {

                            if (advertisements.size() != 0 && advertisements.size() > 0) {
                                if (i != 0 && i % 3 == 0) {
                                    Random r = new Random();
                                    data.put(tempId, advertisements.get(r.nextInt(advertisements.size())));
                                    tempId += 1;
                                }
                            }

                            data.put(tempId, oldInternalNews.getFeeds().get(i));
                            tempId += 1;
                        }

                        oldInternalNews.setMergedFeedList(data);

                        return oldInternalNews;
                    }
                })
                .compose(applySchedulers())
                .subscribe(internalNews -> {
                    isRequestInProgress = false;
                    listener.onSuccess(internalNews);
                }, consumeOnError(listener));
    }

    /**
     * Get specific news from the API using the news id
     *
     * @param id       News Id
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getSpecificNews(int id, BaseInteractor.ResponseListener<InternalFeed> listener) {
        isRequestInProgress = true;

        remoteDataSource.getSpecificInternalNews(id)
                .compose(applySchedulers())
                .subscribe(internalFeed -> {
                    isRequestInProgress = false;
                    listener.onSuccess(internalFeed);
                }, consumeOnError(listener));
    }

    /**
     * Login the user from the application
     *
     * @param email    Email address of the user
     * @param password Password of the user
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void login(String email, String password, BaseInteractor.ResponseListener<AccessToken> listener) {
        isRequestInProgress = true;

        LoginRequest request = new LoginRequest.Builder()
                .email(email)
                .password(password)
                .build();


        remoteDataSource.login(request)
                .doOnNext(accessToken -> {
                    appPreferences.jwtToken(accessToken.getToken());
                    appPreferences.expiration(accessToken.getExpiration());
                    appPreferences.userDetails(JwtUtil.parseJwt(accessToken.getToken(), UserDetail.class));
                    userWrapper.setUserDetail(appPreferences.userDetails());
                })
                .compose(applySchedulers())
                .subscribe(accessToken -> {
                    isRequestInProgress = false;
                    listener.onSuccess(accessToken);
                }, consumeOnError(listener));
    }

    /**
     * Registers the user to use the application
     *
     * @param registerRequest Registration form request
     * @param listener        Listener whether the request is success or failed
     */
    @Override
    public void register(RegisterRequest registerRequest, BaseInteractor.ResponseListener<String> listener) {
        remoteDataSource.register(registerRequest)
                .compose(applySchedulers())
                .subscribe(userId -> {
                    isRequestInProgress = false;
                    listener.onSuccess(userId);
                }, consumeOnError(listener));
    }

    /**
     * Get or update the {@link UserDetail} from the API
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getUserDetails(ProfileInteractor.ResponseListener listener) {
        isRequestInProgress = true;

        remoteDataSource.getUserDetails(Integer.toString(appPreferences.userDetails().getUserId()))
                .doOnNext(accessToken -> {
                    appPreferences.jwtToken(accessToken.getToken());
                    appPreferences.expiration(accessToken.getExpiration());
                    appPreferences.userDetails(JwtUtil.parseJwt(accessToken.getToken(), UserDetail.class));
                    userWrapper.setUserDetail(appPreferences.userDetails());
                })
                .compose(applySchedulers())
                .subscribe(accessToken -> {
                    isRequestInProgress = false;
                    listener.onSuccess(appPreferences.userDetails());
                }, throwable -> {
                    if (throwable instanceof ApiRequestException) {
                        ApiRequestException exception = (ApiRequestException) throwable;

                        if (exception.getRequestCode() != null && exception.getRequestCode().equals(ApiRequestException.REQUEST_CODE.USER_NOT_FOUND)) {
                            listener.onUserNotFound();
                        } else {
                            listener.onFailure(throwable.getMessage());
                        }
                    } else {
                        listener.onFailure(throwable.getMessage());
                    }
                });
    }

    /**
     * Update the logged-in user's {@link UserDetail} on the API
     *
     * @param userDetail Updated {@link UserDetail}
     * @param listener   Listener whether the request is success or failed
     */
    @Override
    public void updateUserDetails(UserDetail userDetail, BaseInteractor.ResponseListener<UserDetail> listener) {
        isRequestInProgress = true;

        remoteDataSource.updateProfile(userDetail)
                .doOnNext(accessToken -> {
                    appPreferences.jwtToken(accessToken.getToken());
                    appPreferences.expiration(accessToken.getExpiration());
                    appPreferences.userDetails(JwtUtil.parseJwt(accessToken.getToken(), UserDetail.class));
                    userWrapper.setUserDetail(appPreferences.userDetails());
                })
                .map(accessToken -> appPreferences.userDetails())
                .compose(applySchedulers())
                .subscribe(userDetail1 -> {
                    isRequestInProgress = false;
                    listener.onSuccess(userDetail1);
                }, consumeOnError(listener));
    }

    /**
     * Logout the user from the application
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void logout(EmptyResponseListener listener) {
        isRequestInProgress = true;

        remoteDataSource.logout(Integer.toString(appPreferences.userDetails().getUserId()))
                .doOnNext(s -> {
                    appPreferences.clear();
                    userWrapper.setUserDetail(null);
                })
                .doOnError(throwable -> {
                    appPreferences.currentLocation();
                    userWrapper.setUserDetail(null);
                    listener.onSuccess();
                })
                .compose(applySchedulers())
                .subscribe(s -> {
                    isRequestInProgress = false;
                    listener.onSuccess();
                }, throwable -> {
                    isRequestInProgress = false;
                    listener.onFailure(throwable.getMessage());
                });
    }

    @Override
    public void cancelNetworkRequest() {
    }

    /**
     * Update the user's current location in the API
     *
     * @param currentLocation User's current {@link android.location.Location}
     * @param shouldUpdateApi If this condition is true, this method should update the user's current location in the API
     * @param listener        Listener whether the request is success or failed
     */
    @Override
    public void updateLocation(LatLng currentLocation, boolean shouldUpdateApi, BaseInteractor.ResponseListener<Boolean> listener) {
        appPreferences.currentLocation(currentLocation);

        if (shouldUpdateApi && appPreferences.userDetails() != null) {
            UpdateLocationRequest request = new UpdateLocationRequest.Builder()
                    .userId(Integer.toString(appPreferences.userDetails().getUserId()))
                    .latitude(Double.toString(appPreferences.currentLocation().latitude))
                    .longitude(Double.toString(appPreferences.currentLocation().longitude))
                    .build();

            remoteDataSource.updateLocation(request)
                    .compose(applySchedulers())
                    .subscribe(listener::onSuccess, consumeOnError(listener));
        }
    }

    /**
     * Request for a new password in the API
     *
     * @param emailAddress The user's email address
     * @param listener     Listener whether the request is success or failed
     */
    @Override
    public void forgotPassword(String emailAddress, BaseInteractor.ResponseListener<ForgotPasswordResponse> listener) {
        isRequestInProgress = true;

        remoteDataSource.forgotPassword(emailAddress)
                .compose(applySchedulers())
                .subscribe(forgotPasswordResponse -> {
                    isRequestInProgress = false;
                    listener.onSuccess(forgotPasswordResponse);
                }, consumeOnError(listener));
    }

    /**
     * Update the user's current password on the API
     *
     * @param request  Current password and new password request
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void changePassword(UpdatePasswordRequest request, BaseInteractor.ResponseListener<String> listener) {
        // Determine if update password (profile) or change password (forgot password)
        if (request.getUserId() == null && userWrapper.isLoggedIn()) {
            request.setUserId(Integer.toString(userWrapper.getUserDetail().getUserId()));
        }

        isRequestInProgress = true;

        remoteDataSource.updatePassword(request)
                .doOnNext(accessToken -> {
                    appPreferences.jwtToken(accessToken.getToken());
                    appPreferences.expiration(accessToken.getExpiration());
                    appPreferences.userDetails(JwtUtil.parseJwt(accessToken.getToken(), UserDetail.class));
                    userWrapper.setUserDetail(appPreferences.userDetails());
                })
                .compose(applySchedulers())
                .subscribe(message -> {
                    isRequestInProgress = false;
                    listener.onSuccess("Your password has been successfully updated");
                }, consumeOnError(listener));
    }

    /**
     * Get the family link available for this user in the API
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getFamilyLinkList(BaseInteractor.ResponseListener<List<FamilyLinkDetail>> listener) {
        isRequestInProgress = true;

        remoteDataSource.getFamilyLinks(Integer.toString(userWrapper.getUserDetail().getUserId()))
                .compose(applySchedulers())
                .subscribe(familyLinkDetails -> {
                    isRequestInProgress = false;
                    listener.onSuccess(familyLinkDetails);
                }, consumeOnError(listener));
    }

    /**
     * Get the pending family link invites for this user in the API
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getFamilyLinkInvites(BaseInteractor.ResponseListener<List<FamilyLinkDetail>> listener) {
        if (userWrapper.isLoggedIn()) {
            remoteDataSource.getFamilyInvites(Integer.toString(userWrapper.getUserDetail().getUserId()))
                    .compose(applySchedulers())
                    .subscribe(listener::onSuccess, consumeOnError(listener));
        }
    }

    /**
     * Get available relationship types from the API
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getRelationshipTypes(BaseInteractor.ResponseListener<List<Relationship>> listener) {
        localDataSource.getRelationshipTypes()
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * Add a family link using other user's the invite code
     *
     * @param inviteCode     Other user's invite code
     * @param relationshipId This user's {@link UserDetail#userId}
     * @param listener       Listener whether the request is success or failed
     */
    @Override
    public void addFamilyLink(String inviteCode, String relationshipId, BaseInteractor.ResponseListener<String> listener) {
        FamilyLinkInviteRequest request = new FamilyLinkInviteRequest.Builder()
                .inviteCode(inviteCode)
                .relationshipId(relationshipId)
                .inviterId(Integer.toString(userWrapper.getUserDetail().getUserId()))
                .build();

        remoteDataSource.addFamilyLink(request)
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * Accept a pending invite from other users
     *
     * @param inviterId {@link UserDetail#userId} of the user who request to be a family link
     * @param listener  Listener whether the request is success or failed
     */
    @Override
    public void acceptInvite(String inviterId, BaseInteractor.ResponseListener<String> listener) {
        remoteDataSource.acceptInvite(inviterId, Integer.toString(userWrapper.getUserDetail().getUserId()))
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * Reject a pending invite from other users
     *
     * @param inviterId {@link UserDetail#userId} of the user who request to be a family link
     * @param listener  Listener whether the request is success or failed
     */
    @Override
    public void rejectInvite(String inviterId, BaseInteractor.ResponseListener<String> listener) {
        remoteDataSource.rejectInvite(inviterId, Integer.toString(userWrapper.getUserDetail().getUserId()))
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * Get nearby places of the given {@link ph.com.auspex.mers.data.model.request.NearbyPlaceRequest.PlaceType} on the API
     *
     * @param placeType Google Places API Place Type see: <a href="https://developers.google.com/places/supported_types">Google Places API Place Types</a>
     * @param listener  Listener whether the request is success or failed
     */
    @Override
    public void getNearbyPlaces(NearbyPlaceRequest.PlaceType placeType, BaseInteractor.ResponseListener<List<NearbyPlace>> listener) {
        /*NearbyPlaceRequest request = new NearbyPlaceRequest.Builder()
                .latitude(Double.toString(appPreferences.currentLocation().latitude))
                .longitude(Double.toString(appPreferences.currentLocation().longitude))
                .typeId(placeType.getId())
                .build();

        remoteDataSource.getNearbyPlacesByType(request)
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));*/

        Observable
                .create((ObservableOnSubscribe<LatLng>) e -> {
                    if (appPreferences.currentLocation() != null) {
                        e.onNext(appPreferences.currentLocation());
                        e.onComplete();
                    } else {
                        e.onError(new RuntimeException("Last known location not found"));
                    }
                })
                .map(latLng -> new NearbyPlaceRequest.Builder()
                        .latitude(Double.toString(latLng.latitude))
                        .longitude(Double.toString(latLng.longitude))
                        .typeId(placeType.getId())
                        .build())
                .flatMap(remoteDataSource::getNearbyPlacesByType)
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * Get last known location of the user in the {@link AppPreferences}
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getLastKnownLocation(BaseInteractor.ResponseListener<LatLng> listener) {
        if (appPreferences.currentLocation() != null) {
            listener.onSuccess(appPreferences.currentLocation());
        }
    }

    /**
     * Get the details of the Place from Google Places API
     *
     * @param placeId  Place Id of the place
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getNearbyPlaceDetail(String placeId, BaseInteractor.ResponseListener<NearbyPlaceDetail> listener) {
        remoteDataSource.getNearbyPlaceDetail(placeId)
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * @param destination Destination location where the other point of the location will be requested.
     * @param listener    Listener whether the request is success or failed
     * @deprecated This feature is not used within the app anymore
     */
    @Deprecated
    @Override
    public void getDirections(LatLng destination, BaseInteractor.ResponseListener<MapDirection> listener) {
        DirectionsRequest request = new DirectionsRequest.Builder()
                .destLat(destination.latitude)
                .destLong(destination.longitude)
                .originLat(appPreferences.currentLocation().latitude)
                .originLong(appPreferences.currentLocation().longitude)
                .build();

        remoteDataSource.getDirections(request)
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * Verify Registration or Forgot Password Code
     *
     * @param verificationRequest Verification Request which contains the {@link UserDetail#userId} and the verification code
     * @param listener            Listener whether the request is success or failed
     */
    @Override
    public void verifyCode(VerificationRequest verificationRequest, VerificationInteractor.ResponseListener listener) {
        isRequestInProgress = true;

        remoteDataSource.verifyCode(verificationRequest)
                .compose(applySchedulers())
                .subscribe(message -> {
                    isRequestInProgress = false;
                    listener.onSuccess(message);
                }, throwable -> {
                    isRequestInProgress = false;

                    if (throwable instanceof ApiRequestException) {
                        ApiRequestException exception = (ApiRequestException) throwable;

                        if (exception.getRequestCode() != null && exception.getRequestCode().equals(ApiRequestException.REQUEST_CODE.ALREADY_VERIFIED)) {
                            listener.onCodeAlreadyUsed(exception.getMessage());
                        } else if (exception.getRequestCode() != null && exception.getRequestCode().equals(ApiRequestException.REQUEST_CODE.USER_NOT_FOUND)) {
                            listener.onUserNotFound(exception.getMessage());
                        } else {
                            listener.onFailure(throwable.getMessage());
                        }
                    } else {
                        listener.onFailure(throwable.getMessage());
                    }
                });
    }

    /**
     * Get available emergency types on the API
     *
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void getEmergencyTypes(BaseInteractor.ResponseListener<List<EmergencyType>> listener) {
        isRequestInProgress = true;

        localDataSource.getEmergencyTypes()
                .flatMap(emergencyTypes -> {
                    if (emergencyTypes.size() == 0) {
                        return remoteDataSource.getEmergencyTypes();
                    } else {
                        return Observable.just(emergencyTypes);
                    }
                })
                .compose(applySchedulers())
                .subscribe(listener::onSuccess, consumeOnError(listener));
    }

    /**
     * Report an emergency on the API
     *
     * @param request  Emergency Report
     * @param listener Listener whether the request is success or failed
     */
    @Override
    public void reportEmergency(EmergencyRequest request, BaseInteractor.ResponseListener<String> listener) {
        isRequestInProgress = true;

        EmergencyRequest.Builder builder = request.newBuilder();

        if (appPreferences.currentLocation() != null && AppUtil.checkGpsEnabled(context)) {
            builder.latitude(Double.toString(appPreferences.currentLocation().latitude))
                    .longitude(Double.toString(appPreferences.currentLocation().longitude));
        }

        if (appPreferences.userDetails() != null) {
            builder.userId(Integer.toString(appPreferences.userDetails().getUserId()));
        }

        remoteDataSource.reportEmergency(builder.build())
                .compose(applySchedulers())
                .subscribe(s -> {
                    isRequestInProgress = false;
                    listener.onSuccess(s);
                }, consumeOnError(listener));
    }
}