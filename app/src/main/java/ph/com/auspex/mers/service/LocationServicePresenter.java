package ph.com.auspex.mers.service;

import android.location.Location;

public interface LocationServicePresenter {

    void updateCurrentLocation(Location location);
}
