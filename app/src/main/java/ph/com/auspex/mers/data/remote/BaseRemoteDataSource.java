package ph.com.auspex.mers.data.remote;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.SocketTimeoutException;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import ph.com.auspex.mers.data.model.response.ApiResponse;
import ph.com.auspex.mers.data.model.response.ErrorResponse;
import ph.com.auspex.mers.data.remote.exception.ApiRequestException;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * A special class that does various Observable manipulations
 */
public abstract class BaseRemoteDataSource {

    /**
     * Executes an API Request, if the request encounters a {@link SocketTimeoutException} retry the request up to 3 times.
     * <p>
     * If the API Request returns an {@link ApiRequestException} emit an {@link Observable#error(Throwable)}
     * <p>
     * If the API Request is successful emit an {@link Observable} of the given type
     *
     * @param observable Observable from {@link ApiService}
     * @param <T>        Object that is returned from ApiServoce
     * @return Observable based on the condition
     */
    public <T> Observable<T> execute(Observable<ApiResponse<T>> observable) {
        return observable
                .retry(3, whenRequestTimeout())
                .onErrorResumeNext(passApiException())
                .map(getData());
    }

    /**
     * Executes an API Request, if the request encounters a {@link SocketTimeoutException} retry the request up to 3 times.
     * <p>
     * If the API Request returns an {@link ApiRequestException} emit an {@link Observable#error(Throwable)}
     * <p>
     * If the API Request is successful emit an the {@link ApiResponse#getMessage()}
     *
     * @param observable Observable from {@link ApiService}
     * @return Api response error message
     */
    public Observable<String> executeApiGetMessage(Observable<ApiResponse> observable) {
        return observable
                .retry(3, whenRequestTimeout())
                .onErrorResumeNext(passApiException())
                .map(getMessage());
    }

    /**
     * Executes an API Request, if the request encounters a {@link SocketTimeoutException} retry the request up to 3 times.
     * <p>
     * If the API Request returns an {@link ApiRequestException} emit an {@link Observable#error(Throwable)}
     * <p>
     * If the API Request is successful emit an the {@link ApiResponse}
     *
     * @param observable Observable from {@link ApiService}
     * @return {@link ApiResponse}
     */
    public Observable<ApiResponse> executeApiResponse(Observable<ApiResponse> observable) {
        return observable
                .retry(3, whenRequestTimeout())
                .onErrorResumeNext(passApiException());
    }

    /**
     * Executes an API Request, if the request encounters a {@link SocketTimeoutException} retry the request up to 3 times.
     * <p>
     * If the API Request returns an {@link ApiRequestException} emit an {@link Observable#error(Throwable)}
     * <p>
     * If the API Request is successful emit an Observable of the generic type
     *
     * @param observable Observable from {@link ApiService}
     * @param <T>        generic type
     * @return Observable from the generic object
     */
    public <T> Observable<T> executeResponse(Observable<T> observable) {
        return observable
                .retry(3, whenRequestTimeout())
                .onErrorResumeNext(passApiException());
    }

    /**
     * Abstract method that returns {@link ApiResponse#getData()}
     *
     * @param <T> Generic object observable
     * @return Generic Object
     */
    protected <T> Function<ApiResponse<T>, T> getData() {
        return apiResponse -> {
            if (apiResponse.isStatus()) {
                return apiResponse.getData();
            }

            throw new ApiRequestException(apiResponse.getMessage());
        };
    }

    /**
     * Abstract method that returns {@link ApiResponse#getMessage()}
     *
     * @return String message from ApiResponse
     */
    protected Function<ApiResponse, String> getMessage() {
        return apiResponse -> {
            if (apiResponse.isStatus()) {
                return apiResponse.getMessage();
            }

            throw new ApiRequestException(apiResponse.getMessage());
        };
    }

    /**
     * Parse the error json response from Api and force the observable to throw an {@link Exception}
     *
     * @param <T> Observable of generic object
     * @return Exception
     */
    protected <T> Function<Throwable, Observable<T>> passApiException() {
        return throwable -> {
            if (throwable instanceof HttpException) {
                int httpExceptionCode = ((HttpException) throwable).code();
                if (httpExceptionCode == 400 || httpExceptionCode == 404) {
                    try {
                        final ErrorResponse errorResponse = new Gson().fromJson(((HttpException) throwable).response().errorBody().string(), ErrorResponse.class);
                        return Observable.error(new ApiRequestException(errorResponse.getMessage(), errorResponse.getHttpCode()));
                    } catch (IOException e) {
                        Timber.e(e, e.getMessage());
                        return Observable.error(new ApiRequestException(String.format("%s: Bad Request", Integer.toString(httpExceptionCode))));
                    }
                } else if (httpExceptionCode == 500) {
                    Timber.e(throwable, throwable.getMessage());
                    return Observable.error(new ApiRequestException(String.format("%s: We are having problems with the server", Integer.toString(httpExceptionCode))));
                } else {
                    Timber.e(throwable, throwable.getMessage());
                }
            }
            return Observable.error(throwable);
        };
    }

    /**
     * retry an Observable when {@link SocketTimeoutException}
     *
     * @return SocketTimeoutException
     */
    private Predicate<Throwable> whenRequestTimeout() {
        return throwable -> throwable instanceof SocketTimeoutException;
    }
}