package ph.com.auspex.mers.service;

import android.app.Activity;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.service.impl.LocationServicePresenterImpl;

/**
 * Location Service Module
 */
@Module
public class ServiceModule {

    /**
     * Provide ServiceInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return ServiceInteractor
     */
    @ServiceScope
    @Provides
    ServiceInteractor providesServiceInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Location update interval to API
     *
     * @return Location update interval value
     */
    @Named("update_interval")
    @Provides
    @ServiceScope
    long providesLocationUpdateInterval() {
        return 10 * 1000;
    }

    /**
     * Fastest interval for location update
     *
     * @return Location update fastest interval value
     */
    @Named("fastest_interval")
    @Provides
    @ServiceScope
    long providesLocationFastestUpdateInterval() {
        return 0;
    }

    /**
     * Provide a LocationRequest object
     *
     * @param updateInterval  {@link #providesLocationUpdateInterval()}
     * @param fastestInterval {@link #providesLocationFastestUpdateInterval()}
     * @return LocationRequest object
     */
    @Provides
    @ServiceScope
    LocationRequest providesLocationRequest(@Named("update_interval") long updateInterval, @Named("fastest_interval") long fastestInterval) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(updateInterval);
        locationRequest.setFastestInterval(fastestInterval);
        locationRequest.setSmallestDisplacement(0);
        return locationRequest;
    }

    /**
     * Provide LocationSettingsRequest
     *
     * @param locationRequest {@link #providesLocationRequest(long, long)}
     * @return LocationSettingsRequest object
     */
    @Provides
    @ServiceScope
    LocationSettingsRequest providesLocationSettings(LocationRequest locationRequest) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        return builder.build();
    }

    /**
     * Provide Settings Client
     *
     * @param activity                Activity instance
     * @param locationSettingsRequest {@link #providesLocationSettings(LocationRequest)}
     * @return
     */
    @Provides
    @ServiceScope
    SettingsClient providesSettingsClient(Activity activity, LocationSettingsRequest locationSettingsRequest) {
        SettingsClient settingsClient = LocationServices.getSettingsClient(activity);
        settingsClient.checkLocationSettings(locationSettingsRequest);
        return settingsClient;
    }

    /**
     * Provide LocationServicePresenter
     *
     * @param model {@link ServiceInteractor}
     * @return LocationServicePresenter
     */
    @Provides
    @ServiceScope
    LocationServicePresenter providesLocationServicePresenter(ServiceInteractor model) {
        return new LocationServicePresenterImpl(model);
    }

    /**
     * Location Update object
     *
     * @param activity               Current activity instance
     * @param eventBus               {@link ph.com.auspex.mers.data.bus.RxEventBus}
     * @param locationRequest        {@link LocationRequest}
     * @param presenter              {@link #providesLocationServicePresenter(ServiceInteractor)}
     * @param updateLocationInterval Location Update interval
     * @return Location Update object
     */
    @Provides
    @ServiceScope
    LocationUpdate providesLocationUpdate(Activity activity, EventBus eventBus, LocationRequest locationRequest, LocationServicePresenter presenter, @Named("update_interval") long updateLocationInterval) {
        return new LocationUpdate(activity, eventBus, locationRequest, presenter, updateLocationInterval);
    }
}
