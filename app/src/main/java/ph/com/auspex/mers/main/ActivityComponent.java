package ph.com.auspex.mers.main;

import android.app.Activity;

import com.bluelinelabs.conductor.Router;
import com.google.android.gms.ads.AdRequest;

import dagger.Component;
import ph.com.auspex.mers.AppComponent;
import ph.com.auspex.mers.common.controller.BaseController;
import ph.com.auspex.mers.common.controller.ViewStateController;
import ph.com.auspex.mers.common.dialog.ProgressLoadingDialog;
import ph.com.auspex.mers.directories.DirectoryComponent;
import ph.com.auspex.mers.directories.DirectoryModule;
import ph.com.auspex.mers.emergency.EmergencyComponent;
import ph.com.auspex.mers.emergency.EmergencyModule;
import ph.com.auspex.mers.familylink.FamilyLinkComponent;
import ph.com.auspex.mers.familylink.FamilyLinkModule;
import ph.com.auspex.mers.login.LoginComponent;
import ph.com.auspex.mers.login.LoginModule;
import ph.com.auspex.mers.main.router.RouterModule;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.news.NewsComponent;
import ph.com.auspex.mers.news.NewsModule;
import ph.com.auspex.mers.password.PasswordComponent;
import ph.com.auspex.mers.password.PasswordModule;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordComponent;
import ph.com.auspex.mers.password.forgotpassword.ForgotPasswordModule;
import ph.com.auspex.mers.profile.ProfileComponent;
import ph.com.auspex.mers.profile.ProfileModule;
import ph.com.auspex.mers.register.RegisterComponent;
import ph.com.auspex.mers.register.RegisterModule;
import ph.com.auspex.mers.splash.SplashComponent;
import ph.com.auspex.mers.splash.SplashModule;
import ph.com.auspex.mers.verification.VerificationComponent;
import ph.com.auspex.mers.verification.VerificationModule;

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = {
                ActivityModule.class,
                RouterModule.class
        }
)
public interface ActivityComponent {
    Activity activity();

    Router router();

    ProgressLoadingDialog progressLoadingDialog();

    SplashComponent splashComponent(SplashModule splashModule);

    NewsComponent newsComponent(NewsModule newsModule);

    LoginComponent loginComponent(LoginModule loginModule);

    RegisterComponent registerComponent(RegisterModule registerModule);

    VerificationComponent verificationComponent(VerificationModule verificationModule);

    ProfileComponent profileComponent(ProfileModule profileModule);

    ForgotPasswordComponent forgotPasswordComponent(ForgotPasswordModule forgotPasswordModule);

    PasswordComponent passwordComponent(PasswordModule changePasswordModule);

    FamilyLinkComponent familyLinkComponent(FamilyLinkModule familyLinkModule);

    DirectoryComponent directoryComponent(DirectoryModule directoryModule);

    EmergencyComponent emergencyComponent(EmergencyModule emergencyModule);

    void inject(NavigationController navigationController);

    void inject(BaseController baseController);

    void inject(MainActivity mainActivity);

    void inject(ViewStateController viewStateController);
}