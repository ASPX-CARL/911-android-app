package ph.com.auspex.mers.familylink;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.data.model.response.Relationship;

public interface FamilyLinkInteractor extends BaseInteractor {
    void getFamilyLinkList(ResponseListener<List<FamilyLinkDetail>> listener);

    void getFamilyLinkInvites(ResponseListener<List<FamilyLinkDetail>> listener);

    void getRelationshipTypes(ResponseListener<List<Relationship>> listener);

    void addFamilyLink(String inviteCode, String relationshipId, ResponseListener<String> listener);

    void acceptInvite(String inviterId, ResponseListener<String> listener);

    void rejectInvite(String inviterId, ResponseListener<String> listener);

    void getLastKnownLocation(ResponseListener<LatLng> listener);
}
