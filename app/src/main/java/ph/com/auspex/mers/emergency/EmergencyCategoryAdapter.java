package ph.com.auspex.mers.emergency;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.data.model.response.EmergencyType;
import ph.com.auspex.mers.util.GlideImageLoader;

/**
 * Provide an {@link ArrayAdapter} for {@link android.widget.Spinner} in {@link EmergencyController}
 */
public class EmergencyCategoryAdapter extends ArrayAdapter<EmergencyType> {

    private List<EmergencyType> emergencyTypes;

    /**
     * Constructs {@link EmergencyCategoryAdapter} using the following parameters
     *
     * @param context  activity context
     * @param resource layout resource
     * @param objects  {@link EmergencyType} spinner items
     */
    public EmergencyCategoryAdapter(@NonNull Context context, int resource, @NonNull List<EmergencyType> objects) {
        super(context, resource, objects);
        this.emergencyTypes = objects;
    }

    /**
     * View holder pattern implementation
     *
     * @param position    spinner index
     * @param convertView inflate view
     * @param parent      parent view
     * @return view holder
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CategoryVH viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category, parent, false);
            viewHolder = new CategoryVH(convertView);
            convertView.setTag(viewHolder);
            viewHolder.bind(parent.getContext(), emergencyTypes.get(position));
        } else {
            viewHolder = (CategoryVH) convertView.getTag();
            viewHolder.bind(parent.getContext(), emergencyTypes.get(position));
        }

        return convertView;
    }

    /**
     * Display the view for the spinner when not expanded
     *
     * @param position    position in spinner
     * @param convertView inflated view
     * @param parent      parent view
     * @return view holder
     */
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CategoryVH viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category_dropdown_item, parent, false);
            viewHolder = new CategoryVH(convertView);
            convertView.setTag(viewHolder);
            viewHolder.bind(parent.getContext(), emergencyTypes.get(position));
        } else {
            viewHolder = (CategoryVH) convertView.getTag();
            viewHolder.bind(parent.getContext(), emergencyTypes.get(position));
        }

        return convertView;
    }

    /**
     * Returns the {@link EmergencyType} at the given index
     *
     * @param position position in the {@link android.widget.Spinner}
     * @return emergency type
     */
    @Nullable
    @Override
    public EmergencyType getItem(int position) {
        return emergencyTypes.get(position);
    }

    /**
     * Static view holder implementation
     */
    static class CategoryVH {
        @BindView(R.id.title) TextView title;
        @BindView(R.id.icon) ImageView icon;

        public CategoryVH(View rootView) {
            ButterKnife.bind(this, rootView);
        }

        /**
         * Set the view data from POJO
         *
         * @param context       activity context
         * @param emergencyType {@link EmergencyType} POJO
         */
        public void bind(Context context, EmergencyType emergencyType) {
            title.setText(emergencyType.getName());
            GlideImageLoader.loadUrl(context, icon, emergencyType.getImageUrl(), 48, 48);
        }
    }
}
