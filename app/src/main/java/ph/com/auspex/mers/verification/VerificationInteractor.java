package ph.com.auspex.mers.verification;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.VerificationRequest;

/**
 * This is interface is used in order for {@link ph.com.auspex.mers.verification.VerificationContract.VerificationPresenter} and {@link ph.com.auspex.mers.data.AppDataSource} interact
 */
public interface VerificationInteractor extends BaseInteractor {

    void verifyCode(VerificationRequest verificationRequest, ResponseListener listener);

    interface ResponseListener {
        void onSuccess(String message);

        void onFailure(String message);

        void onCodeAlreadyUsed(String message);

        void onUserNotFound(String message);
    }
}
