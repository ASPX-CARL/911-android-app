package ph.com.auspex.mers.common.controller;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.RestoreViewOnCreateController;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.com.auspex.mers.HasComponent;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.dialog.ProgressLoadingDialog;
import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.common.views.BadgeView;
import ph.com.auspex.mers.main.ActivityComponent;

/**
 * Same abstract class as {@link BaseController} but with different {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} method
 *
 * @see BaseController
 */
public abstract class ViewStateController extends RestoreViewOnCreateController implements RemoteView {

    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> component) {
        return component.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    Toolbar toolbar;
    TextView toolbarTitle;
    FrameLayout overlayContainer;
    protected BadgeView badgeView;

    @Inject protected Activity activity;
    @Inject ProgressLoadingDialog progressLoadingDialog;

    private ActivityComponent activityComponent;

    private boolean shouldShowNavigationIcon;
    private Unbinder unbinder;

    public ViewStateController() {
    }

    public ViewStateController(@Nullable Bundle args) {
        super(args);
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedViewState) {
        View view = inflateView(inflater, container, savedViewState);
        unbinder = ButterKnife.bind(this, view);

        injectDependencies();

        toolbar = ButterKnife.findById(activity, R.id.toolbar);
        toolbarTitle = ButterKnife.findById(activity, R.id.toolbar_title);
        overlayContainer = ButterKnife.findById(activity, R.id.overlay_root);
        badgeView = ButterKnife.findById(activity, R.id.badge_view);

        initBadge();

        setHasNavigationIcon(showNavigationIcon());
        initToolbar();

        onViewReady(view, savedViewState);

        return view;
    }

    protected void initBadge() {
        badgeView.setVisibility(View.GONE);
        badgeView.setOnClickListener(v -> onBadgeClicked());
    }

    protected void onBadgeClicked() {
    }

    protected boolean showNavigationIcon() {
        return false;
    }

    protected abstract View inflateView(LayoutInflater inflater, ViewGroup container, Bundle savedViewState);

    protected void injectDependencies() {
        activityComponent = getComponent(ActivityComponent.class);
        activityComponent.inject(this);

        setupComponent(activityComponent);
    }

    private void setHasNavigationIcon(boolean showNavigationIcon) {
        this.shouldShowNavigationIcon = showNavigationIcon;
    }

    protected void initToolbar() {
        if (shouldShowNavigationIcon) {
            // Get Navigation Icon Res
            Drawable drawable = ContextCompat.getDrawable(activity, getNavigationIconResource());
            drawable.setColorFilter(getToolbarIconTint(), PorterDuff.Mode.SRC_IN);

            toolbar.setNavigationIcon(drawable);
            toolbar.setNavigationOnClickListener(v -> onClickNavigationIcon());
        } else {
            toolbar.setNavigationIcon(null);
            toolbar.setNavigationOnClickListener(null);
        }

        setTitle(getTitle());

        if (!TextUtils.isEmpty(getSubtitle())) {
            setSubtitle(getSubtitle());
        }
        onCreateOptionsMenu(getMenuRes());
    }

    protected CharSequence getSubtitle() {
        return "";
    }

    protected CharSequence getTitle() {
        return "";
    }

    @NonNull
    @MenuRes
    protected int getMenuRes() {
        return 0;
    }

    private void onCreateOptionsMenu(int menuRes) {
        toolbar.getMenu().clear();

        if (menuRes != 0) {
            toolbar.inflateMenu(menuRes);
            toolbar.setOnMenuItemClickListener(this::onMenuItemSelected);
        }
    }

    @ColorInt
    private int getToolbarIconTint() {
        return Color.WHITE;
    }

    protected void onClickNavigationIcon() {

    }

    protected boolean onMenuItemSelected(MenuItem item) {
        return false;
    }

    @DrawableRes
    protected int getNavigationIconResource() {
        return R.drawable.ic_arrow_back_black_24dp;
    }

    protected abstract void setupComponent(@NonNull ActivityComponent activityComponent);

    protected void onViewReady(View view, Bundle savedViewState) {

    }

    @Override
    protected void onDestroyView(@NonNull View view) {
        unbinder.unbind();
        super.onDestroyView(view);
    }

    protected final void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void setTitle(@NonNull CharSequence title) {
        if (!TextUtils.isEmpty(title)) {
            toolbarTitle.setText(title);
            toolbarTitle.setVisibility(View.VISIBLE);
        } else {
            toolbarTitle.setText("");
            toolbarTitle.setVisibility(View.GONE);
        }

        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    private void setSubtitle(@NonNull CharSequence subtitle) {
        if (!TextUtils.isEmpty(subtitle)) {
            toolbar.setTitle(!TextUtils.isEmpty(toolbarTitle.getText().toString()) ? toolbarTitle.getText().toString() : "");
            toolbar.setSubtitle(subtitle);
        }

        toolbarTitle.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        if (progressLoadingDialog != null && !progressLoadingDialog.isShowing()) {
            progressLoadingDialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if (progressLoadingDialog != null && progressLoadingDialog.isShowing()) {
            progressLoadingDialog.dismiss();
        }
    }

    @Override
    public void showError(String message) {
        showToast(message);
    }

    /**
     * @deprecated Please use getMenuRes() instead.
     */
    @Deprecated
    @Override
    public final void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * @deprecated Please use onMenuItemSelected(MenuItem item) instead.
     */
    @Deprecated
    @Override
    public final boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setOverlayController(Controller controller) {
        getChildRouter(overlayContainer)
                .setPopsLastView(true)
                .setRoot(RouterTransaction.with(controller)
                        .pushChangeHandler(new FadeChangeHandler()));
    }
}
