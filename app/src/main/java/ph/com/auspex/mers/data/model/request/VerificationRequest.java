package ph.com.auspex.mers.data.model.request;

import java.util.HashMap;

/**
 * Create Verification Request
 */
public class VerificationRequest {

    public enum VerificationType {
        REGISTRATION("registration"),
        PASSWORD("forgot_password"),
        USER_NOT_VERIFIED("registration");

        private String value;

        VerificationType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    private String userId;
    private VerificationType type;
    private String code;

    private VerificationRequest(Builder builder) {
        this.userId = builder.userId;
        this.type = builder.type;
        this.code = builder.code;
    }


    public static class Builder {
        private String userId;
        private VerificationType type;
        private String code;

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder type(VerificationType type) {
            this.type = type;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public VerificationRequest build() {
            return new VerificationRequest(this);
        }
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public HashMap<String, String> create() {
        HashMap<String, String> params = new HashMap<>();

        params.put("u", userId);
        params.put("verification_code", code);
        params.put("verify_via", "mobile");
        params.put("verification_type", type.getValue());

        return params;
    }
}
