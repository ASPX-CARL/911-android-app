package ph.com.auspex.mers.data.model.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;

public class NearbyPlaceDetailDeserializer implements JsonDeserializer<NearbyPlaceDetail> {

    private static final String EMPTY_STRING = "";

    @Override
    public NearbyPlaceDetail deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObjectData = json.getAsJsonObject();

        JsonObject jsonObjectResult = jsonObjectData.get("result").getAsJsonObject();

        String address;
        String phoneNumber;
        String phoneNumberIntl;

        if (jsonObjectResult.has("formatted_address")) {
            address = jsonObjectResult.get("formatted_address").getAsString();
        } else {
            address = EMPTY_STRING;
        }

        if (jsonObjectResult.has("formatted_phone_number")) {
            phoneNumber = jsonObjectResult.get("formatted_phone_number").getAsString();
        } else {
            phoneNumber = EMPTY_STRING;
        }
        if (jsonObjectResult.has("international_phone_number")) {
            phoneNumberIntl = jsonObjectResult.get("international_phone_number").getAsString();
        } else {
            phoneNumberIntl = EMPTY_STRING;
        }

        return new NearbyPlaceDetail(address, phoneNumber, phoneNumberIntl);
    }
}
