package ph.com.auspex.mers.data.model.request;

import android.support.annotation.DrawableRes;

import java.util.HashMap;

import ph.com.auspex.mers.R;

public class NearbyPlaceRequest {

    private NearbyPlaceRequest(Builder builder) {
        this.type = builder.type;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
    }

    public enum PlaceType {
        HOSPITAL("hospital", R.drawable.ic_marker_medical),
        POLICE("police", R.drawable.ic_marker_police),
        FIRE("fire_station", R.drawable.ic_marker_fire);

        String type;
        private int markerRes;

        PlaceType(String type, @DrawableRes int markerRes) {
            this.type = type;
            this.markerRes = markerRes;
        }

        public String getId() {
            return type;
        }

        public int getMarkerRes() {
            return markerRes;
        }
    }

    private String type;
    private String latitude;
    private String longitude;

    public static class Builder {
        private String type;
        private String latitude;
        private String longitude;

        public Builder typeId(String type) {
            this.type = type;
            return this;
        }

        public Builder latitude(String latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(String longitude) {
            this.longitude = longitude;
            return this;
        }

        public NearbyPlaceRequest build() {
            return new NearbyPlaceRequest(this);
        }
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public HashMap<String, String> create() {
        HashMap<String, String> queryMap = new HashMap<>();

        queryMap.put("type", type);
        queryMap.put("latitude", latitude);
        queryMap.put("longitude", longitude);

        return queryMap;
    }
}
