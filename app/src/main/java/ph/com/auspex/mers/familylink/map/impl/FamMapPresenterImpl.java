package ph.com.auspex.mers.familylink.map.impl;

import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.AppEvent;
import ph.com.auspex.mers.data.bus.events.LocationEvent;
import ph.com.auspex.mers.data.bus.events.LowMemoryEvent;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.familylink.FamilyLinkInteractor;
import ph.com.auspex.mers.familylink.map.FamMapContract;

/**
 * Provides an implementation for {@link ph.com.auspex.mers.familylink.map.FamMapContract.FamMapPresenter}
 */
public class FamMapPresenterImpl extends MvpBasePresenter<FamMapContract.FamMapView> implements FamMapContract.FamMapPresenter {

    private final FamilyLinkInteractor model;
    private final RxEventBus rxEventBus;
    private List<FamilyLinkDetail> familyLinkDetails;

    /**
     * Constructs a {@link FamMapPresenterImpl} using {@link FamilyLinkInteractor} and {@link RxEventBus}
     *
     * @param model      interactor
     * @param rxEventBus event bus
     */
    public FamMapPresenterImpl(FamilyLinkInteractor model, RxEventBus rxEventBus) {
        this.model = model;
        this.rxEventBus = rxEventBus;
    }

    @Override
    public void attachView(FamMapContract.FamMapView view) {
        super.attachView(view);

        getPendingInvitesCount();
        observeOnLowMemoryEvent();
    }

    /**
     * Get available family link connections
     */
    @Override
    public void getFamilyLinkPins() {
        model.getFamilyLinkList(new BaseInteractor.ResponseListener<List<FamilyLinkDetail>>() {
            @Override
            public void onSuccess(List<FamilyLinkDetail> familyLinkDetails) {
                if (isViewAttached()) {
                    setFamilyLinkDetails(familyLinkDetails);
                    getView().displayFamilyLinkPins(familyLinkDetails);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().showError(message);
                }
            }
        });
    }

    private void setFamilyLinkDetails(List<FamilyLinkDetail> familyLinkDetails) {
        this.familyLinkDetails = familyLinkDetails;
    }

    /**
     * Get pending family link invitation count
     */
    @Override
    public void getPendingInvitesCount() {
        model.getFamilyLinkInvites(new BaseInteractor.ResponseListener<List<FamilyLinkDetail>>() {
            @Override
            public void onSuccess(List<FamilyLinkDetail> familyLinkDetails) {
                if (isViewAttached()) {
                    getView().displayFamilyLinkInvitesCount(familyLinkDetails.size());
                }
            }

            @Override
            public void onFailure(String message) {
            }
        });
    }

    /**
     * Get user's last known location from {@link android.content.SharedPreferences}
     */
    @Override
    public void getLastKnownLocation() {
        model.getLastKnownLocation(new BaseInteractor.ResponseListener<LatLng>() {
            @Override
            public void onSuccess(LatLng latLng) {
                if (isViewAttached() && latLng != null) {
                    getView().displayLastKnownLocation(latLng);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().showError(message);
                }
            }
        });
    }

    /**
     * Listens for GPS settings update changes
     */
    @Override
    public void observeGpsSettingsChanges() {
        rxEventBus.toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof AppEvent.GpsStatusChangedEvent) {
                        AppEvent.GpsStatusChangedEvent event = (AppEvent.GpsStatusChangedEvent) o;

                        if (isViewAttached()) {
                            getView().toggleGpsBlocker(event.isEnabled());
                        }
                    }
                });
    }

    /**
     * Listens for {@link android.location.Location} updates from {@link ph.com.auspex.mers.service.LocationUpdateService}
     */
    @Override
    public void observeCurrentLocationChanges() {
        rxEventBus.toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof LocationEvent.LocationUpdatedEvent) {
                        LocationEvent.LocationUpdatedEvent event = (LocationEvent.LocationUpdatedEvent) o;

                        if (isViewAttached()) {
                            getView().onCurrentLocationUpdated(event.getLastKnownLocation());

                            if (familyLinkDetails == null) {
                                getFamilyLinkPins();
                            }
                        }
                    }
                });
    }

    private void observeOnLowMemoryEvent() {
        rxEventBus.toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof LowMemoryEvent) {
                        if (isViewAttached()) {
                            getView().onLowMemory();
                        }
                    }
                });
    }
}