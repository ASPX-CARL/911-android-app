package ph.com.auspex.mers.data.repositories;

import io.reactivex.Observable;
import ph.com.auspex.mers.data.model.request.LoginRequest;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.data.model.request.UpdatePasswordRequest;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.data.model.response.AccessToken;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.data.model.response.UserDetail;

public interface UserRepository {

    Observable<AccessToken> login(LoginRequest request);

    Observable<String> register(RegisterRequest request);

    Observable<String> verifyCode(VerificationRequest request);

    // AKA REFRESH FUCKING TOKEN
    Observable<AccessToken> getUserDetails(String userId);

    Observable<String> logout(String userId);

    Observable<AccessToken> updateProfile(UserDetail userDetail);

    Observable<ForgotPasswordResponse> forgotPassword(String emailAddress);

    Observable<AccessToken> updatePassword(UpdatePasswordRequest request);
}
