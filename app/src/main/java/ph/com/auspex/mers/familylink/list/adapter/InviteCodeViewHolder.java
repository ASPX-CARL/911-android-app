package ph.com.auspex.mers.familylink.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.util.AppUtil;

/**
 * Provide a view holder for {@link FamilyLinkRecyclerAdapter}
 */
public class InviteCodeViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_invite_code) TextView tvInviteCode;

    private Context context;
    private String inviteCode;

    private AddButtonClickListener listener;

    /**
     * Constructs an {@link InviteCodeViewHolder} using the following parameters
     *
     * @param context  activity context
     * @param itemView parent view layout
     * @param listener item click listener
     */
    public InviteCodeViewHolder(Context context, View itemView, AddButtonClickListener listener) {
        super(itemView);
        this.context = context;
        this.listener = listener;

        ButterKnife.bind(this, itemView);
    }

    /**
     * Bind the invite code string to the view
     *
     * @param inviteCode invite code string
     */
    public void bind(String inviteCode) {
        this.inviteCode = inviteCode;

        if (!TextUtils.isEmpty(inviteCode)) {
            tvInviteCode.setText(String.format(context.getString(R.string.family_link_invite_code), inviteCode));
        }
    }

    /**
     * Item click listener
     */
    @OnClick(R.id.parent_container)
    void onParentClicked() {
        AppUtil.copyToClipboard(context, inviteCode);
    }

    /**
     * Add button click listener
     */
    @OnClick(R.id.button_add)
    void onAddClicked() {
        listener.onAddButtonClicked();
    }

    public interface AddButtonClickListener {
        void onAddButtonClicked();
    }
}
