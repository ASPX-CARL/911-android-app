package ph.com.auspex.mers.data.model.request;

import ph.com.auspex.mers.R;

/**
 * Gender enum with drawable res value
 */
public enum GenderEnum {
    MALE(R.drawable.placeholder_male),
    FEMALE(R.drawable.placeholder_female);

    private int drawableRes;

    GenderEnum(int drawableRes) {
        this.drawableRes = drawableRes;
    }

    public int getDrawableRes() {
        return drawableRes;
    }
}