package ph.com.auspex.mers.common.dialog;

import android.app.ProgressDialog;
import android.content.Context;

import ph.com.auspex.mers.R;

/**
 * This class provides a reusable Loading Dialog
 *
 * @deprecated as per Android Guidelines because it prevents the user from interacting with the app
 * use {@link android.widget.ProgressBar} instead
 */
@Deprecated
public class ProgressLoadingDialog extends ProgressDialog {

    /**
     * Create a ProgressDialog instance with default message and not cancelable
     *
     * @param context Activity context
     */
    public ProgressLoadingDialog(Context context) {
        super(context);
        setCancelable(false);
        setMessage(context.getResources().getString(R.string.default_loading_message));
    }

    /**
     * Method that sets a different message and shows the Progress Dialog
     *
     * @param message String message
     */
    public void show(String message) {
        setMessage(message);
        super.show();
    }
}