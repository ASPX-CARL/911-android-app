package ph.com.auspex.mers.familylink.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.data.model.response.Relationship;
import ph.com.auspex.mers.familylink.FamilyLinkModule;
import ph.com.auspex.mers.familylink.dialog.adapter.RelationshipAdapter;
import ph.com.auspex.mers.main.ActivityComponent;

public class AddFamilyLinkDialogController extends MosbyController<AddFamilyContract.AddFamilyView, AddFamilyContract.AddFamilyPresenter> implements AddFamilyContract.AddFamilyView {

    @BindView(R.id.spnr_relationship) Spinner spinner;
    @BindView(R.id.family_invite_code) EditText inputInviteCode;

    @Inject AddFamilyContract.AddFamilyPresenter presenter;
    private RelationshipAdapter adapter;

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.familyLinkComponent(new FamilyLinkModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_add_family_link, container, false);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        inputInviteCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @NonNull
    @Override
    public AddFamilyContract.AddFamilyPresenter createPresenter() {
        return presenter;
    }

    @OnClick({R.id.parent_overlay, R.id.button_cancel})
    void closeDialogController() {
        getRouter().popController(this);
    }

    @OnClick(R.id.button_add)
    void onAddButtonClicked() {
        if (!TextUtils.isEmpty(inputInviteCode.getText().toString())) {
            presenter.addFamilyLink(inputInviteCode.getText().toString(), Integer.toString(adapter.getItem(spinner.getSelectedItemPosition()).getId()));
        } else {
            Toast.makeText(getActivity(), "Invite Code cannot be empty", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void prepareRelationshipList(List<Relationship> relationships) {
        adapter = new RelationshipAdapter(getActivity(), R.layout.layout_spinner, relationships);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onFamilyLinkAdded(String message) {
        showToast(message);
        closeDialogController();
    }

    @Override
    protected void initToolbar() {
    }

    @Override
    public boolean handleBack() {
        closeDialogController();
        return true;
    }

    @Override
    protected void initBadge() {
    }
}
