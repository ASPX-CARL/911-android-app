package ph.com.auspex.mers.directories.detail;

import android.Manifest;
import android.animation.Animator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.conductor.ControllerChangeHandler;
import com.bluelinelabs.conductor.ControllerChangeType;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;
import ph.com.auspex.mers.directories.DirectoryContract;
import ph.com.auspex.mers.directories.DirectoryContract.DirectoryDetailView;
import ph.com.auspex.mers.directories.DirectoryModule;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.TopSheetAnimationUtil;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

import static ph.com.auspex.mers.directories.DirectoryContract.DirectoryDetailPresenter;

/**
 * Provides a UI for the directory detail feature. Imitates the animation of a {@link android.support.design.widget.BottomSheetDialog} but appears on the top of the screen
 */
public class DirectoryDetailController extends MosbyController<DirectoryDetailView, DirectoryContract.DirectoryDetailPresenter> implements DirectoryDetailView {

    public static final String TAG = DirectoryDetailController.class.getSimpleName();
    public static final String KEY_NEARBY_PLACE = TAG + ".nearbyPlace";

    @BindView(R.id.tv_place_name) TextView tvPlaceName;
    @BindView(R.id.tv_place_address) TextView tvPlaceAddress;
    @BindView(R.id.tv_place_phone_number) TextView tvPlacePhoneNumber;
    @BindView(R.id.layout_detail_container) LinearLayout detailContainer;
    @BindView(R.id.progress_bar) MaterialProgressBar progressBar;
    @BindView(R.id.dialog_container) RelativeLayout dialogContainer;
    @BindView(R.id.fab_call) FloatingActionButton fabCall;

    @BindString(R.string.html_phone_number_not_available) String textNumberNotAvailable;

    @Inject DirectoryDetailPresenter presenter;

    private NearbyPlace nearbyPlace;
    private NearbyPlaceDetail nearbyPlaceDetail;
    private boolean isExiting = false;

    /**
     * Constructs a {@link DirectoryDetailController} with empty parameters
     */
    public DirectoryDetailController() {
    }

    /**
     * Constructs a {@link DirectoryDetailController} with reference to the parent controller
     *
     * @param args nearby place id
     */
    public DirectoryDetailController(@Nullable Bundle args) {
        super(args);

        nearbyPlace = args.getParcelable(KEY_NEARBY_PLACE);
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        tvPlaceName.setText(nearbyPlace.getName());

        presenter.getNearbyPlaceDetail(nearbyPlace.getPlaceId());
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.directoryComponent(new DirectoryModule()).inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_dialog_directory_detail, container, false);
    }

    @NonNull
    @Override
    public DirectoryDetailPresenter createPresenter() {
        return presenter;
    }

    /**
     * Display {@link NearbyPlaceDetail} response from the API
     *
     * @param nearbyPlaceDetail nearby place detail
     */
    @SuppressWarnings("deprecation")
    @Override
    public void displayDetail(NearbyPlaceDetail nearbyPlaceDetail) {
        this.nearbyPlaceDetail = nearbyPlaceDetail;

        tvPlaceAddress.setText(nearbyPlaceDetail.getAddress());

        if (!TextUtils.isEmpty(nearbyPlaceDetail.getPhoneNumber())) {
            tvPlacePhoneNumber.setText(nearbyPlaceDetail.getPhoneNumberIntl());
        } else {
            tvPlacePhoneNumber.setText(AppUtil.StringToHtml(textNumberNotAvailable));
        }
    }

    /**
     * Call the number on the place detail if available
     */
    @OnClick(R.id.fab_call)
    void onCallButtonClicked() {
        if (!TextUtils.isEmpty(nearbyPlaceDetail.getPhoneNumber())) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestCallPhonePermissions();
                return;
            }
            callNumber();
        } else {
            showToast("No Number Available");
        }
    }

    /**
     * Request for {@link Manifest.permission#CALL_PHONE} permission
     */
    private void requestCallPhonePermissions() {
        Nammu.askForPermission(activity, Manifest.permission.CALL_PHONE, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                callNumber();
            }

            @Override
            public void permissionRefused() {
                showError(activity.getString(R.string.error_permission_not_granted_call));
            }
        });
    }

    /**
     * Call the number provided on the place detail
     */
    private void callNumber() {
        dismiss();

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(String.format("tel:%s", nearbyPlaceDetail.getPhoneNumber())));
        startActivity(intent);
    }

    /**
     * Event after this controller has been displayed
     *
     * @param changeHandler {@link com.bluelinelabs.conductor.Controller} change handler
     * @param changeType    {@link com.bluelinelabs.conductor.Controller} change type
     */
    @Override
    protected void onChangeEnded(@NonNull ControllerChangeHandler changeHandler, @NonNull ControllerChangeType changeType) {
        super.onChangeEnded(changeHandler, changeType);

        if (!isExiting) {
            TopSheetAnimationUtil.topSheetEntranceAnimation(dialogContainer);
        }
    }

    /**
     * Dismiss this controller when the overlay is clicked
     */
    @OnClick(R.id.parent_overlay)
    void dismiss() {
        isExiting = true;
        TopSheetAnimationUtil.topSheetExitAnimation(dialogContainer, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                closeDialogController();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    /**
     * Show {@link android.widget.ProgressBar}
     */
    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        detailContainer.setVisibility(View.GONE);
    }

    /**
     * Hide {@link android.widget.ProgressBar}
     */
    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        detailContainer.setVisibility(View.VISIBLE);
    }

    /**
     * Prevent toolbar from being re-initialized
     */
    @Override
    protected void initToolbar() {
    }

    /**
     * Handle back button pressed event
     *
     * @return always <code>true</code>
     */
    @Override
    public boolean handleBack() {
        closeDialogController();
        return true;
    }

    /**
     * Dismiss this controller
     */
    void closeDialogController() {
        getRouter().popController(this);
    }
}
