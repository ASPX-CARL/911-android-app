package ph.com.auspex.mers.data.model.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;

/**
 * Password Verification Response API Json Deserializer
 */
public class PasswordResponseDeserializer implements JsonDeserializer<ForgotPasswordResponse> {

    @Override
    public ForgotPasswordResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObjectResponse = json.getAsJsonObject();
        JsonObject jsonObjectData = jsonObjectResponse.get("data").getAsJsonObject();

        String message = jsonObjectResponse.get("message").getAsString();
        String userId = jsonObjectData.get("user_id").getAsString();

        return new ForgotPasswordResponse(message, userId);
    }
}
