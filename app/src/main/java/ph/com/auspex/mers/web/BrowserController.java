package ph.com.auspex.mers.web;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindString;
import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.BaseController;
import ph.com.auspex.mers.main.ActivityComponent;

/**
 * This class serves as an internal website viewer for the app
 */
public class BrowserController extends BaseController {

    @BindView(R.id.web_view) WebView webView;
    @BindString(R.string.eula_url) String eurl; // EULA + URL edgy

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        initWebView(eurl);
    }

    /**
     * Initialize WebView
     *
     * @param url url of the website to be displayed
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webView.loadUrl(url);
    }

    /**
     * Inject {@link ActivityComponent} in this controller
     *
     * @param activityComponent {@link ActivityComponent} instance
     */
    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_browser, container, false);
    }

    /**
     * Show up navigation icon
     *
     * @return should show navigation icon
     */
    @Override
    protected boolean showNavigationIcon() {
        return true;
    }

    /**
     * Action when the navigation icon is clicked
     */
    @Override
    protected void onClickNavigationIcon() {
        getRouter().popController(this);
    }

    /**
     * Controller title
     *
     * @return Empty String
     */
    @Override
    protected CharSequence getTitle() {
        return "";
    }

    /**
     * Menu resource
     *
     * @return empty menu resource
     */
    @NonNull
    @Override
    protected int getMenuRes() {
        return 0;
    }
}
