package ph.com.auspex.mers.directories;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.directories.detail.impl.DirectoryDetailPresenterImpl;
import ph.com.auspex.mers.directories.impl.DirectoryPresenterImpl;

@Module
public class DirectoryModule {

    /**
     * Provide DirectoryInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return DirectoryInteractor
     */
    @Provides
    @DirectoryScope
    DirectoryInteractor providesDirectoryInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide DirectoryPresenter
     *
     * @param model {@link DirectoryInteractor}
     * @return DirectoryPresenter
     */
    @Provides
    @DirectoryScope
    DirectoryContract.DirectoryPresenter providesDirectoryPresenter(DirectoryInteractor model, RxEventBus rxEventBus) {
        return new DirectoryPresenterImpl(model, rxEventBus);
    }

    /**
     * Provide DirectoryDetailPresenter
     *
     * @param model {@link DirectoryInteractor}
     * @return DirectoryDetailPresenter
     */
    @Provides
    @DirectoryScope
    DirectoryContract.DirectoryDetailPresenter providesDirectoryDetailPresenter(DirectoryInteractor model) {
        return new DirectoryDetailPresenterImpl(model);
    }
}
