package ph.com.auspex.mers;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

/**
 * ActionBar interface that must be implemented in an {@link android.support.v7.app.AppCompatActivity} in order to use the {@link ActionBar} in a {@link com.bluelinelabs.conductor.Controller}
 */
public interface ActionBarInterface {
    ActionBar getSupportActionBar();

    void setSupportActionBar(Toolbar toolbar);
}