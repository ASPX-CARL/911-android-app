package ph.com.auspex.mers.data;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import ph.com.auspex.mers.BuildConfig;
import ph.com.auspex.mers.data.model.deserializer.BooleanDeserializer;
import ph.com.auspex.mers.data.model.deserializer.DirectionsDeserializer;
import ph.com.auspex.mers.data.model.deserializer.FeedDeserializer;
import ph.com.auspex.mers.data.model.deserializer.GenderDeserializer;
import ph.com.auspex.mers.data.model.deserializer.NearbyPlaceDeserializer;
import ph.com.auspex.mers.data.model.deserializer.NearbyPlaceDetailDeserializer;
import ph.com.auspex.mers.data.model.deserializer.PasswordResponseDeserializer;
import ph.com.auspex.mers.data.model.request.GenderEnum;
import ph.com.auspex.mers.data.model.response.Feed;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;
import ph.com.auspex.mers.data.model.response.MapDirection;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;
import ph.com.auspex.mers.data.remote.ApiService;
import ph.com.auspex.mers.data.remote.AuthInterceptor;
import ph.com.auspex.mers.data.remote.ConnectivityInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    Gson providesGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Boolean.class, new BooleanDeserializer())
                .registerTypeAdapter(ForgotPasswordResponse.class, new PasswordResponseDeserializer())
                .registerTypeAdapter(NearbyPlace.class, new NearbyPlaceDeserializer())
                .registerTypeAdapter(NearbyPlaceDetail.class, new NearbyPlaceDetailDeserializer())
                .registerTypeAdapter(MapDirection.class, new DirectionsDeserializer())
                .registerTypeAdapter(GenderEnum.class, new GenderDeserializer())
                .registerTypeAdapter(Feed.class, new FeedDeserializer())
                .serializeNulls()
                .create();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor providesHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(message -> Timber.d(message));
        interceptor.setLevel(BODY);
        return interceptor;
    }

    @Provides
    @Singleton
    ConnectivityInterceptor providesConnectivityInterceptor(Context context) {
        return new ConnectivityInterceptor(context);
    }

    @Provides
    @Singleton
    AuthInterceptor providesAuthInterceptor() {
        return new AuthInterceptor();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, AuthInterceptor authInterceptor, ConnectivityInterceptor connectivityInterceptor) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(httpLoggingInterceptor);
        }

        builder.addInterceptor(connectivityInterceptor);
        builder.addInterceptor(authInterceptor);

        return builder.build();
    }

    @Provides
    @Singleton
    ApiService providesApiService(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
                .create(ApiService.class);
    }
}
