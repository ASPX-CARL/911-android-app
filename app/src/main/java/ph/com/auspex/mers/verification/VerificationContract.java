package ph.com.auspex.mers.verification;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.request.VerificationRequest;

public interface VerificationContract {

    interface VerificationPresenter extends MvpPresenter<VerificationView> {
        void verifyCode(String code, String userId, VerificationRequest.VerificationType verificationType);
    }

    interface VerificationView extends RemoteView {
        void onVerificationSuccessful(String message);

        void onCodeAlreadyUsed(String message);

        void onUserNotFound(String message);
    }
}
