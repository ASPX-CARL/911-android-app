package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;

/**
 * A special view designed for app's {@link ph.com.auspex.mers.profile.ProfileController} which has {@link AdvancedTextInputEditText} and title {@link TextView}
 */
public class ProfileEditText extends FrameLayout implements AdvancedTextInputEditText.OnInputTextChangedListener {

    @BindView(R.id.field_title) TextView textViewTitle;
    @BindView(R.id.field_edit_text) AdvancedTextInputEditText editTextInput;

    private String hintStr;
    private String title;
    private int drawableRightResId;
    private int drawableTint;
    private int inputType;
    private boolean isEnabled;
    private boolean isFocusable;
    private int inputTextColor;
    private String regexStr;
    private float drawablePadding;
    private String errorStr;
    private int lines;

    private View.OnClickListener onClickListener = v -> {

    };

    private OnValueChangedListener onValueChangedListener;

    public ProfileEditText(@NonNull Context context) {
        super(context);

        throw new UnsupportedOperationException("Please inflate from xml.");
    }

    public ProfileEditText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProfileEditText, 0, 0);

        hintStr = a.getString(R.styleable.ProfileEditText_hint_text);
        title = a.getString(R.styleable.ProfileEditText_title);
        drawableRightResId = a.getResourceId(R.styleable.ProfileEditText_drawable_right, 0);
        drawableTint = a.getResourceId(R.styleable.ProfileEditText_drawable_tint, 0);
        inputType = a.getInt(R.styleable.ProfileEditText_android_inputType, InputType.TYPE_CLASS_TEXT);
        isEnabled = a.getBoolean(R.styleable.ProfileEditText_enabled, true);
        isFocusable = a.getBoolean(R.styleable.ProfileEditText_focusable, true);
        inputTextColor = a.getResourceId(R.styleable.ProfileEditText_text_color, 0);
        regexStr = a.getString(R.styleable.ProfileEditText_profile_regex);
        drawablePadding = a.getDimension(R.styleable.ProfileEditText_drawable_padding, 0);
        errorStr = a.getString(R.styleable.ProfileEditText_error_message);
        lines = a.getInt(R.styleable.ProfileEditText_lines, 1);

        View view = LayoutInflater.from(context).inflate(R.layout.view_custom_edittext, this, true);
        ButterKnife.bind(this, view);

        if (lines > 1) {
            editTextInput.setLines(lines);
            editTextInput.setGravity(Gravity.TOP | Gravity.START);
        }

        if (!TextUtils.isEmpty(hintStr)) {
            editTextInput.setHint(hintStr);
        }

        if (!TextUtils.isEmpty(title)) {
            textViewTitle.setText(title);
        }

        if (drawableRightResId != 0) {
            Drawable drawableRight = ContextCompat.getDrawable(context, drawableRightResId);

            if (drawableTint != 0) {
                DrawableCompat.setTint(drawableRight, drawableTint);
                DrawableCompat.setTintMode(drawableRight, PorterDuff.Mode.SRC_IN);
            }

            editTextInput.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableRight, null);
        }

        editTextInput.setInputType(inputType);
        editTextInput.setEnabled(isEnabled);
        editTextInput.setFocusable(isFocusable);

        if (inputTextColor != 0) {
            editTextInput.setTextColor(ContextCompat.getColor(context, inputTextColor));
        }

        if (!TextUtils.isEmpty(regexStr)) {
            editTextInput.setRegex(regexStr);
        }

        if (drawablePadding != 0) {
            editTextInput.setCompoundDrawablePadding((int) drawablePadding);
        }

        editTextInput.setErrorStr(!TextUtils.isEmpty(errorStr) ? errorStr : "Error");
        editTextInput.setInputTextChangedListener(this);

        a.recycle();
    }

    @OnClick(R.id.field_title)
    void onTitleClicked() {
        if (editTextInput.isFocusable()) {
            editTextInput.requestFocus();

            if (!TextUtils.isEmpty(editTextInput.getText().toString())) {
                editTextInput.setSelection(editTextInput.getText().toString().length());
            }
        } else {
            onClickListener.onClick(editTextInput);
        }
    }

    @OnClick(R.id.field_edit_text)
    void onInputFieldClicked() {
        if (!editTextInput.isFocusable()) {
            onClickListener.onClick(editTextInput);
        } else {
            editTextInput.requestFocus();

            if (!TextUtils.isEmpty(editTextInput.getText().toString())) {
                editTextInput.setSelection(editTextInput.getText().toString().length());
            }
        }
    }

    public void setText(String value) {
        editTextInput.setText(value);
    }

    public String getText() {
        return editTextInput.getText().toString();
    }

    @Override
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onTextChanged(String inputStr) {
        if (onValueChangedListener != null) {
            onValueChangedListener.onValueChanged();
        }
    }

    public void setOnValueChangedListener(OnValueChangedListener onValueChangedListener) {
        this.onValueChangedListener = onValueChangedListener;
    }

    public interface OnValueChangedListener {
        void onValueChanged();
    }

    public boolean hasError() {
        return editTextInput.hasError();
    }

    public void setRequestFocus() {
        editTextInput.requestFocus();
    }

    public void removeFocus() {
        editTextInput.setError(null);
        editTextInput.clearFocus();
    }

    public void setError(String errorStr) {
        editTextInput.showError(errorStr);
    }

    public boolean hasOnValueChangedListener() {
        return onValueChangedListener != null;
    }
}
