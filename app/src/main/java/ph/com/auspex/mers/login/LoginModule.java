package ph.com.auspex.mers.login;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.login.impl.LoginPresenterImpl;

@Module
public class LoginModule {

    /**
     * Provide LoginInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return LoginInteractor
     */
    @Provides
    @LoginScope
    LoginInteractor providesLoginInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide LoginPresenter
     *
     * @param model {@link LoginInteractor}
     * @return LoginPresenter
     */
    @Provides
    @LoginScope
    LoginContract.LoginPresenter providesLoginPresenter(LoginInteractor model) {
        return new LoginPresenterImpl(model);
    }
}
