package ph.com.auspex.mers;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.android.gms.ads.MobileAds;
import com.onesignal.OneSignal;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.CrashManagerListener;

import javax.inject.Inject;

import io.realm.Realm;
import ph.com.auspex.mers.data.DataSourceModule;
import ph.com.auspex.mers.data.NetworkModule;
import ph.com.auspex.mers.data.bus.BusModule;
import ph.com.auspex.mers.pushnotification.OpenHandler;
import ph.com.auspex.mers.util.AppUtil;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MersApplication extends Application {

    @Inject OpenHandler pushNotificationOpenHandler;

    private AppComponent appComponent;

    public static RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        buildAppComponent();

        refWatcher = LeakCanary.install(this);

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(pushNotificationOpenHandler)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font_helvetica_neue_normal))
                .setFontAttrId(R.attr.fontPath)
                .build());

        EasyImage.configuration(this)
                .setImagesFolderName(getString(R.string.app_name))
                .setCopyTakenPhotosToPublicGalleryAppFolder(false)
                .setCopyPickedImagesToPublicGalleryAppFolder(false)
                .setAllowMultiplePickInGallery(false);

        Realm.init(this);

        AppUtil.clearCache(this, 0);

        MobileAds.initialize(this, BuildConfig.GOOGLE_ADMOB_APP_ID);

        OneSignal.sendTag("build", BuildConfig.BUILD_TYPE);

        CrashManager.register(this, BuildConfig.HOCKEY_APP_ID, new CrashManagerListener() {
            @Override
            public boolean shouldAutoUploadCrashes() {
                return true;
            }
        });

        Nammu.init(this);
    }

    /**
     * Method that builds all required application modules
     */
    private void buildAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .dataSourceModule(new DataSourceModule())
                .busModule(new BusModule())
                .build();

        appComponent.inject(this);
    }

    /**
     * Get Application Component instance
     *
     * @return Application Component
     */
    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}