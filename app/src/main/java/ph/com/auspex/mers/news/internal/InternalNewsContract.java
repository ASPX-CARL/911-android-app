package ph.com.auspex.mers.news.internal;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.response.InternalNews;

public interface InternalNewsContract {

    interface InternalNewsPresenter extends MvpPresenter<InternalNewsView> {
        void getInternalNews(boolean refresh);
    }

    interface InternalNewsView extends RemoteView {
        void setData(InternalNews internalNews);

        void onLoadMore();

        void scrollListToTop();
    }
}
