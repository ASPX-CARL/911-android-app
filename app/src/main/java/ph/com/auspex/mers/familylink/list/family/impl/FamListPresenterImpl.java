package ph.com.auspex.mers.familylink.list.family.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.FamilyLinkDetail;
import ph.com.auspex.mers.familylink.FamilyLinkInteractor;
import ph.com.auspex.mers.familylink.list.family.FamListContract.FamListPresenter;
import ph.com.auspex.mers.familylink.list.family.FamListContract.FamListView;

public class FamListPresenterImpl extends MvpBasePresenter<FamListView> implements FamListPresenter {

    private final FamilyLinkInteractor model;

    public FamListPresenterImpl(FamilyLinkInteractor model) {
        this.model = model;
    }

    @Override
    public void attachView(FamListView view) {
        super.attachView(view);

        getFamilyLinkList();
    }

    @Override
    public void getFamilyLinkList() {
        if (isViewAttached()) {
            getView().showLoading();
        }

        model.getFamilyLinkList(new BaseInteractor.ResponseListener<List<FamilyLinkDetail>>() {
            @Override
            public void onSuccess(List<FamilyLinkDetail> familyLinkDetails) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().displayFamilyList(familyLinkDetails);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                }
            }
        });
    }
}
