package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Provide a POJO wrapper for failed api response
 */
public class ErrorResponse {

    @SerializedName("code")
    private String httpCode;
    @SerializedName("message")
    private String message;

    /**
     * Constructs an ErrorResponse instance
     */
    public ErrorResponse() {
    }

    /**
     * Constructs an ErrorsResponse with a provided error message
     *
     * @param message error message
     */
    public ErrorResponse(String message) {
        this.message = message;
    }

    /**
     * Returns the API error response HTTP Code
     *
     * @return http code
     */
    public String getHttpCode() {
        return httpCode;
    }

    /**
     * Returns the error message
     * @return error message
     */
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                ", httpCode='" + httpCode + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
