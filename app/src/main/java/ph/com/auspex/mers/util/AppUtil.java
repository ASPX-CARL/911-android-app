package ph.com.auspex.mers.util;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.io.File;
import java.util.Date;

import id.zelory.compressor.Compressor;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.data.model.request.VerificationRequest;
import ph.com.auspex.mers.verification.VerificationController;
import timber.log.Timber;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Common Application Utilities
 */
public class AppUtil {

    /**
     * Check if internet connection is available utility
     *
     * @param context Application context isntance
     * @return Return true if internet connection is available
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Programmatically close soft keyboard
     *
     * @param activity Current activity instance
     */
    public static void closeSoftkeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    /**
     * Compress a {@link File} using {@link Compressor} library
     *
     * @param context Application context
     * @param file    File to be compressed
     * @return Compressed file
     */
    public static File compressFile(Context context, File file) {
        return new Compressor.Builder(context)
                .setQuality(90)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .build()
                .compressToFile(file);
    }

    /**
     * Check if device GPS is enabled
     *
     * @param context Application context
     * @return return true if device's GPS is enabled
     */
    public static boolean checkGpsEnabled(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled = false;

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Timber.e(e, e.getMessage());
        }

        return gpsEnabled;
    }

    /**
     * Copy text to {@link ClipboardManager}
     *
     * @param context Application context
     * @param text    text to be copied to clipboard
     */
    public static void copyToClipboard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("invite code", text);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(context, "Invite code copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    /**
     * Convert dp to px value
     *
     * @param context Application context
     * @param dp      dp value to be converted to px
     * @return px value of the input dp
     */
    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    /**
     * Create a smart {@link com.google.android.gms.maps.model.Marker} based on the given image
     *
     * @param context    Application context
     * @param source     Marker's display image (user display image)
     * @param isLoggedIn Flag if the user is logged in or not
     * @return Bitmap {@link com.google.android.gms.maps.model.Marker}
     */
    public static Bitmap createBitmapForMarker(Context context, Bitmap source, boolean isLoggedIn) {
        Bitmap output = Bitmap.createBitmap(source.getWidth(),
                source.getWidth(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, source.getWidth(), source.getWidth());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(source.getWidth() / 2, source.getWidth() / 2,
                source.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, rect, rect, paint);

        Bitmap bitmap = Bitmap.createBitmap(output.getWidth() + 10, output.getWidth() + 10, Bitmap.Config.ARGB_8888);
        Canvas canvas1 = new Canvas(bitmap);

        Paint markerPaint = new Paint();
        markerPaint.setColor(isLoggedIn ? ContextCompat.getColor(context, R.color.online_indicator_color) : ContextCompat.getColor(context, android.R.color.darker_gray));
        markerPaint.setAntiAlias(true);

        canvas1.drawCircle(bitmap.getWidth() / 2, bitmap.getWidth() / 2, bitmap.getWidth() / 2, markerPaint);
        canvas1.drawBitmap(output, 5, 5, null);

        return bitmap;
    }

    /**
     * Create bitmap from a vector drawable
     *
     * @param context    Application context
     * @param drawableId Drawable res id
     * @return Converted vector drawable bitmap
     */
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /**
     * Add a radius to google map's current location
     *
     * @param googleMap       {@link GoogleMap} object
     * @param currentLocation {@link LatLng} Latitude and Latitude of the current location
     * @param radius          Radius size
     */
    public static void showCurrentLocationRadius(@NonNull GoogleMap googleMap, @NonNull LatLng currentLocation, @NonNull double radius) {
        googleMap.addCircle(new CircleOptions()
                .center(currentLocation)
                .radius(radius)
                .strokeColor(Color.rgb(0, 136, 255))
                .fillColor(Color.argb(20, 0, 136, 255)));
    }

    /**
     * Convert a mobile number to national format. (Example: convert 0917 123 4567 to +639171234567)
     *
     * @param mobileNumber Mobile number in E.164 Format
     * @return Mobile number in International Format
     */
    public static String convertMobileNumberToNationalFormat(String mobileNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        try {
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(mobileNumber, "PH");
            return phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
        } catch (NumberParseException e) {
            e.printStackTrace();
            throw new UnsupportedOperationException("Cannot parse mobile number. Invalid mobile number format.");
        }
    }

    public static Bundle createVerificationBundle(Intent intent) {
        Bundle args = new Bundle();

        Uri uri = intent.getData();

        args.putString(VerificationController.KEY_USER_ID, uri.getQueryParameter("u"));
        args.putString(VerificationController.KEY_CODE, uri.getQueryParameter("verification_code"));

        String verificationType = uri.getQueryParameter("verification_type");

        if (verificationType.equals(VerificationRequest.VerificationType.REGISTRATION.getValue())) {
            args.putSerializable(VerificationController.KEY_VERIFICATION_TYPE, VerificationRequest.VerificationType.REGISTRATION);
        } else {
            args.putSerializable(VerificationController.KEY_VERIFICATION_TYPE, VerificationRequest.VerificationType.PASSWORD);
        }

        return args;
    }

    @SuppressWarnings("deprecation")
    public static Spanned StringToHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(html);
        }
    }

    /**
     * Get file extension
     *
     * @param url Url
     * @return extension of file found in the Url
     */
    public static String getExtension(String url) {
        return url.substring(url.lastIndexOf("."));
    }

    /**
     * Convert first character of the string to uppercase
     *
     * @param s Input String
     * @return Updated string format
     */
    public static String capitalizeFirstLetter(String s) {
        if (!TextUtils.isEmpty(s)) {
            if (s.length() == 1) {
                return s.toUpperCase();
            } else {
                return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
            }
        } else {
            return null;
        }
    }

    /**
     * Clear cache at every app open
     *
     * @param dir     Cache directory
     * @param numDays number of days in cache
     * @return
     */
    static int clearCacheFolder(final File dir, final int numDays) {

        int deletedFiles = 0;
        if (dir != null && dir.isDirectory()) {
            try {
                for (File child : dir.listFiles()) {

                    //first delete subdirectories recursively
                    if (child.isDirectory()) {
                        deletedFiles += clearCacheFolder(child, numDays);
                    }

                    //then delete the files and subdirectories in this dir
                    //only empty directories can be deleted, so subdirs have been done first
                    if (child.lastModified() < new Date().getTime() - numDays * DateUtils.DAY_IN_MILLIS) {
                        if (child.delete()) {
                            deletedFiles++;
                        }
                    }
                }
            } catch (Exception e) {
                Timber.e(e, "Failed to clean the cache, error");
            }
        }
        return deletedFiles;
    }

    /**
     * Delete the files older than numDays days from the application cache
     * 0 means all files.
     */
    public static void clearCache(final Context context, final int numDays) {
        Timber.i("Starting cache prune, deleting files older than %s days", numDays);
        int numDeletedFiles = clearCacheFolder(context.getCacheDir(), numDays);
        Timber.i("Cache pruning completed, %d files deleted", numDeletedFiles);
    }

    public static boolean hasCamera(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public static boolean hasFlash(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
}
