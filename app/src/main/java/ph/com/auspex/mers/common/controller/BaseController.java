package ph.com.auspex.mers.common.controller;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler;

import javax.inject.Inject;

import ph.com.auspex.mers.HasComponent;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.dialog.ProgressLoadingDialog;
import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.common.views.BadgeView;
import ph.com.auspex.mers.main.ActivityComponent;

/**
 * BaseController class implements common app controller components
 */
public abstract class BaseController extends RefWatchingController implements RemoteView {

    /**
     * This interface must be implemented to the parent activity where the controller will be attached
     *
     * @param component {@link ActivityComponent} class
     * @param <C>       {@link dagger.Component} class name
     * @return {@link dagger.Component} instance
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> component) {
        return component.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    Toolbar toolbar;
    TextView toolbarTitle;
    FrameLayout overlayContainer;
    protected BadgeView badgeView;

    @Inject protected Activity activity;
    @Inject ProgressLoadingDialog progressLoadingDialog;

    private boolean shouldShowNavigationIcon;

    public BaseController() {
    }

    public BaseController(@Nullable Bundle args) {
        super(args);
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedViewState) {
        injectDependencies();
        return super.onCreateView(inflater, container, savedViewState);
    }

    /* @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        View view = inflateView(inflater, container);
        unbinder = ButterKnife.bind(this, view);

        injectDependencies();

        toolbar = ButterKnife.findById(activity, R.id.toolbar);
        toolbarTitle = ButterKnife.findById(activity, R.id.toolbar_title);
        overlayContainer = ButterKnife.findById(activity, R.id.overlay_root);
        badgeView = ButterKnife.findById(activity, R.id.badge_view);
        initBadge();

        setHasNavigationIcon(showNavigationIcon());
        initToolbar();

        onViewReady(view);

        return view;
    }*/

    @Override
    protected void onViewReady(View view, Bundle savedViewState) {
        toolbar = activity.findViewById(R.id.toolbar);
        toolbarTitle = activity.findViewById(R.id.toolbar_title);
        overlayContainer = activity.findViewById(R.id.overlay_root);
        badgeView = activity.findViewById(R.id.badge_view);
        initBadge();

        setHasNavigationIcon(showNavigationIcon());
        initToolbar();

        if (progressLoadingDialog.isShowing()) {
            progressLoadingDialog.dismiss();
        }
    }

    /**
     * Initialize Alert Badge (bell icon on upper left).
     */
    protected void initBadge() {
        badgeView.setVisibility(View.GONE);
        badgeView.setOnClickListener(v -> onBadgeClicked());
    }

    /**
     * Override this method to handle badge click event
     */
    protected void onBadgeClicked() {
    }

    /**
     * Method that shows navigation icon (Upper left icon)
     *
     * @return should show navigation icon
     */
    protected boolean showNavigationIcon() {
        return false;
    }

    /**
     * Set value for {@link #showNavigationIcon()}
     *
     * @param showNavigationIcon
     */
    private void setHasNavigationIcon(boolean showNavigationIcon) {
        this.shouldShowNavigationIcon = showNavigationIcon;
    }

    /**
     * Initialize {@link BaseController} {@link Toolbar}
     */
    protected void initToolbar() {
        if (shouldShowNavigationIcon) {
            // Get Navigation Icon Res
            Drawable drawable = ContextCompat.getDrawable(activity, getNavigationIconResource());
            drawable.setColorFilter(getToolbarIconTint(), PorterDuff.Mode.SRC_IN);

            toolbar.setNavigationIcon(drawable);
            toolbar.setNavigationOnClickListener(v -> onClickNavigationIcon());
        } else {
            toolbar.setNavigationIcon(null);
            toolbar.setNavigationOnClickListener(null);
        }

        setupTitleAndMenu();
    }

    /**
     * Setup Toolbar title and options menu
     */
    private void setupTitleAndMenu() {
        setTitle(getTitle());

        if (!TextUtils.isEmpty(getSubtitle())) {
            setSubtitle(getSubtitle());
        }
        inflateMenu(getMenuRes());
    }

    /**
     * Set Toolbar SubTitle\
     *
     * @return subtitle text
     */
    protected CharSequence getSubtitle() {
        return "";
    }

    /**
     * Set Toolbar Title
     *
     * @return toolbar title text
     */
    protected CharSequence getTitle() {
        return "";
    }

    @NonNull
    @MenuRes
    protected int getMenuRes() {
        return 0;
    }

    /**
     * Inflate controller menu
     *
     * @param menuRes {@link Menu} resource
     */
    private void inflateMenu(int menuRes) {
        toolbar.getMenu().clear();

        if (menuRes != 0) {
            toolbar.inflateMenu(menuRes);
            toolbar.setOnMenuItemClickListener(this::onMenuItemSelected);
        }
    }

    /**
     * Setup toolbar icons color
     *
     * @return {@link Color}
     */
    @ColorInt
    private int getToolbarIconTint() {
        return Color.WHITE;
    }

    /**
     * Set navigationIcon click event
     */
    protected void onClickNavigationIcon() {

    }

    protected boolean onMenuItemSelected(MenuItem item) {
        return false;
    }

    /**
     * Inject BaseController dependencies (fields annotated with {@link Inject})
     */
    protected void injectDependencies() {
        ActivityComponent activityComponent = getComponent(ActivityComponent.class);
        activityComponent.inject(this);

        setupComponent(activityComponent);
    }

    /**
     * Set navigation icon res (default: <i>R.drawable.ic_arrow_back_black_24dp</i>)
     *
     * @return navigation icon res or default icon
     */
    @DrawableRes
    protected int getNavigationIconResource() {
        return R.drawable.ic_arrow_back_black_24dp;
    }

    /**
     * Setup component on child controller
     *
     * @param activityComponent {@link ActivityComponent} instance
     */
    protected abstract void setupComponent(@NonNull ActivityComponent activityComponent);

    /**
     * Reusable {@link Toast} message method
     *
     * @param message Toast message to be displayed
     */
    protected final void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Set toolbar Title
     *
     * @param title toolbar title
     */
    private void setTitle(@NonNull CharSequence title) {
        if (!TextUtils.isEmpty(title)) {
            toolbarTitle.setText(title);
            toolbarTitle.setVisibility(View.VISIBLE);
        } else {
            toolbarTitle.setText("");
            toolbarTitle.setVisibility(View.GONE);
        }

        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    /**
     * Set toolbar subtitle
     *
     * @param subtitle toolbar subtitle string
     */
    private void setSubtitle(@NonNull CharSequence subtitle) {
        if (!TextUtils.isEmpty(subtitle)) {
            toolbar.setTitle(!TextUtils.isEmpty(toolbarTitle.getText().toString()) ? toolbarTitle.getText().toString() : "");
            toolbar.setSubtitle(subtitle);
        }

        toolbarTitle.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        if (progressLoadingDialog != null && !progressLoadingDialog.isShowing()) {
            progressLoadingDialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if (progressLoadingDialog != null && progressLoadingDialog.isShowing()) {
            progressLoadingDialog.dismiss();
        }
    }

    /**
     * Show Error Message
     *
     * @param message error message
     */
    @Override
    public void showError(String message) {
        showToast(message);
    }

    /**
     * @deprecated Please use getMenuRes() instead.
     */
    @Deprecated
    @Override
    public final void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * @deprecated Please use onMenuItemSelected(MenuItem item) instead.
     */
    @Deprecated
    @Override
    public final boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Set overlay Controller (example: dialog and bottomsheet controllers)
     *
     * @param controller Controller that will be put on top of the current controller
     */
    public void setOverlayController(Controller controller) {
        getChildRouter(overlayContainer)
                .setPopsLastView(true)
                .setRoot(RouterTransaction.with(controller)
                        .pushChangeHandler(new FadeChangeHandler()));
    }
}
