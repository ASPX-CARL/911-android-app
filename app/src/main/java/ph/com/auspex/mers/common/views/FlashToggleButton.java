package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.view.View;

import java.util.Collection;

import io.fotoapparat.parameter.Flash;
import io.fotoapparat.parameter.selector.FlashSelectors;
import io.fotoapparat.parameter.selector.SelectorFunction;
import ph.com.auspex.mers.R;

/**
 * Custom class that toggles flash mode of {@link ph.com.auspex.mers.camera.CameraActivity}
 */
public class FlashToggleButton extends AppCompatImageButton implements View.OnClickListener {

    /**
     * Different Camera Flash States
     */
    public enum FlashState {
        AUTO(FlashSelectors.autoFlash(), R.drawable.ic_flash_auto),
        ON(FlashSelectors.on(), R.drawable.ic_flash_on),
        OFF(FlashSelectors.off(), R.drawable.ic_flash_off);

        SelectorFunction<Collection<Flash>, Flash> flash;
        int drawable;

        /*FlashState(SelectorFunction<Flash> flash, int drawable) {
            this.flash = flash;
            this.drawable = drawable;
        }*/

        FlashState(SelectorFunction<Collection<Flash>, Flash> flash, int drawable) {
            this.flash = flash;
            this.drawable = drawable;
        }

        public SelectorFunction<Collection<Flash>, Flash> getFlash() {
            return flash;
        }
    }

    private StateChangeListener listener = state -> {
        // Prevent NPE
    };
    private FlashState defaultState = FlashState.AUTO;

    public FlashToggleButton(Context context) {
        super(context);
        throw new UnsupportedOperationException("Please inflate from xml.");
    }

    /**
     * Create a view instance from xml
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    public FlashToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Initialize view
     */
    private void init() {
        setImageResource(defaultState.drawable);

        setOnClickListener(this);
    }

    public void setListener(StateChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        // rotate state
        switch (defaultState) {
            case AUTO:
                defaultState = FlashState.ON;
                break;
            case ON:
                defaultState = FlashState.OFF;
                break;
            case OFF:
                defaultState = FlashState.AUTO;
                break;
        }

        // send callback back to UI
        setImageResource(defaultState.drawable);
        listener.onStateChanged(defaultState);
    }

    public interface StateChangeListener {
        void onStateChanged(FlashState state);
    }
}
