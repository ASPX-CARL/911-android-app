package ph.com.auspex.mers.camera;

import android.Manifest;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.hardware.provider.CameraProviders;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.parameter.selector.LensPositionSelectors;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.view.CameraView;
import ph.com.auspex.mers.R;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import timber.log.Timber;

import static io.fotoapparat.parameter.selector.AspectRatioSelectors.standardRatio;
import static io.fotoapparat.parameter.selector.FlashSelectors.autoFlash;
import static io.fotoapparat.parameter.selector.FlashSelectors.off;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.autoFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.continuousFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.fixed;
import static io.fotoapparat.parameter.selector.PreviewFpsRangeSelectors.rangeWithHighestFps;
import static io.fotoapparat.parameter.selector.Selectors.firstAvailable;
import static io.fotoapparat.parameter.selector.SensorSensitivitySelectors.highestSensorSensitivity;
import static io.fotoapparat.parameter.selector.SizeSelectors.biggestSize;

public class CameraActivity2 extends AppCompatActivity {

    @BindView(R.id.camera_view) CameraView cameraView;
    @BindView(R.id.zoomSeekBar) SeekBar seekBar;
    @BindView(R.id.result) ImageView imageView;

    private Fotoapparat backFotoapparat;
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera2);
        unbinder = ButterKnife.bind(this);

        requestCameraPermission();
        zoomSeekBar();
    }

    private void zoomSeekBar() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                backFotoapparat.setZoom(progress / (float) seekBar.getMax());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Do nothing
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Do nothing
            }
        });
    }

    private void setupFotoapparat() {
        backFotoapparat = createFotoapparat();
    }

    private Fotoapparat createFotoapparat() {
        return Fotoapparat
                .with(this)
                .cameraProvider(CameraProviders.v2(this))
                .into(cameraView)
                .previewScaleType(ScaleType.CENTER_CROP)
                .photoSize(standardRatio(biggestSize()))
                .lensPosition(LensPositionSelectors.back())
                .focusMode(firstAvailable(
                        continuousFocus(),
                        autoFocus(),
                        fixed()
                ))
                .flash(firstAvailable(
                        autoFlash(),
                        off()
                ))
                .previewFpsRange(rangeWithHighestFps())
                .sensorSensitivity(highestSensorSensitivity())
                .cameraErrorCallback(e -> Toast.makeText(CameraActivity2.this, e.toString(), Toast.LENGTH_LONG).show())
                .build();
    }

    @OnClick(R.id.camera_view)
    void takePictureOnClick() {
        PhotoResult photoResult = backFotoapparat.takePicture();

        try {
            photoResult.saveToFile(createImageFile());
        } catch (IOException e) {
            Timber.e("File not created", e);
        }

        photoResult
                .toBitmap()
                .whenAvailable(result -> {
                    imageView.setImageBitmap(result.bitmap);
                    imageView.setRotation(-result.rotationDegrees);
                    imageView.setVisibility(View.VISIBLE);
                });
    }

    @OnLongClick(R.id.camera_view)
    boolean focusOnLongClick() {
        backFotoapparat.autoFocus();
        return true;
    }

    @OnClick(R.id.result)
    void retakePicture(View view) {
        view.setVisibility(View.GONE);
    }

    private void requestCameraPermission() {
        Nammu.askForPermission(this, Manifest.permission.CAMERA, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                setupFotoapparat();
                cameraView.setVisibility(View.VISIBLE);
                backFotoapparat.start();
            }

            @Override
            public void permissionRefused() {
                Toast.makeText(CameraActivity2.this, "Camera Permission not granted", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestCameraPermission();
    }

    @Override
    protected void onStop() {
        super.onStop();
        backFotoapparat.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}