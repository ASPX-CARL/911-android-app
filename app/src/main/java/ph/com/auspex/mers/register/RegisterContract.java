package ph.com.auspex.mers.register;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.request.RegisterRequest;

public interface RegisterContract {

    interface RegisterPresenter extends MvpPresenter<RegisterView> {
        void registerAccount(RegisterRequest registerRequest);
    }

    interface RegisterView extends RemoteView {
        void onRegisterSuccessful(String userId);

        void onRegisterFailed(String message);
    }
}
