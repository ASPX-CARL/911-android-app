package ph.com.auspex.mers.data.bus.events;

/**
 * Device Gps Status Change Event Listener
 */
public class AppEvent {
    public static class GpsStatusChangedEvent {
        private boolean isGpsEnabled;

        public GpsStatusChangedEvent(boolean isGpsEnabled) {
            this.isGpsEnabled = isGpsEnabled;
        }

        public boolean isEnabled() {
            return isGpsEnabled;
        }
    }
}
