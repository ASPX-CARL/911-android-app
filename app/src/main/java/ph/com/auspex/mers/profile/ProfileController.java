package ph.com.auspex.mers.profile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.UserWrapper;
import ph.com.auspex.mers.common.controller.MosbyController;
import ph.com.auspex.mers.common.dialog.DatePickerDialogFragment;
import ph.com.auspex.mers.common.views.ProfileEditText;
import ph.com.auspex.mers.data.model.response.UserDetail;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.navigation.NavigationController;
import ph.com.auspex.mers.password.updatepassword.UpdatePasswordController;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.DateUtil;
import ph.com.auspex.mers.util.FormValidationUtil;
import ph.com.auspex.mers.util.GlideImageLoader;
import ph.com.auspex.mers.util.MaterialDialogManager;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

/**
 * Profile Controller UI
 */
public class ProfileController extends MosbyController<ProfileContract.ProfileView, ProfileContract.ProfilePresenter> implements ProfileContract.ProfileView,
        DatePickerDialog.OnDateSetListener, ProfileEditText.OnValueChangedListener {

    public static final String TAG = ProfileController.class.getSimpleName();

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.iv_profile_image) ImageView ivProfileImage;
    @BindView(R.id.tv_email_address) TextView tvEmailAddress;
    @BindView(R.id.tv_invite_code) TextView tvInviteCode;
    @BindView(R.id.input_first_name) ProfileEditText inputFirstName;
    @BindView(R.id.input_middle_name) ProfileEditText inputMiddleName;
    @BindView(R.id.input_last_name) ProfileEditText inputLastName;
    @BindView(R.id.input_password) ProfileEditText inputPassword;
    @BindView(R.id.input_mobile_number) ProfileEditText inputMobileNumber;
    @BindView(R.id.input_birthday) ProfileEditText inputBirthday;
    @BindView(R.id.input_gender) ProfileEditText inputGender;
    @BindView(R.id.input_home_address) ProfileEditText inputHomeAddress;
    @BindView(R.id.input_contact_person) ProfileEditText inputContactPerson;
    @BindView(R.id.input_contact_person_mobile_number) ProfileEditText inputContactPersonMobileNumber;
    @BindView(R.id.input_medical_information) ProfileEditText inputMedicalInformation;
    @BindView(R.id.fab) FloatingActionButton fabUpdateProfile;

    @BindViews({R.id.input_first_name, R.id.input_middle_name, R.id.input_last_name,
            R.id.input_mobile_number, R.id.input_birthday, R.id.input_gender, R.id.input_home_address,
            R.id.input_contact_person, R.id.input_contact_person_mobile_number, R.id.input_medical_information})
    List<ProfileEditText> inputFields;

    @BindString(R.string.label_profile) String title;
    @BindString(R.string.password_placeholder) String passwordMask;

    @Inject ProfileContract.ProfilePresenter presenter;
    @Inject FormValidationUtil validationUtil;
    @Inject UserWrapper userWrapper;
    @Inject Router parentRouter;

    private File imageFile;

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.profileComponent(new ProfileModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        initSwipeRefreshLayout();

        inputBirthday.setOnClickListener(v -> showDatePickerDialog());
        inputGender.setOnClickListener(v -> showGenderPicker());
        inputPassword.setOnClickListener(v -> {
            if (fabUpdateProfile.isShown()) {
                showEditProfileCancelDialog(true);
            } else {
                openChangePasswordScreen();
            }
        });

        fabUpdateProfile.hide();
    }

    /**
     * Initialize Swipe Refresh Layout
     */
    private void initSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.getUserDetails());
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_profile, container, false);
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_profile;
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                showLogoutDialog();
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    /**
     * Show Logout Dialog
     */
    private void showLogoutDialog() {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_logout)
                .content(R.string.dialog_content_logout)
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_no)
                .onPositive((dialog, which) -> presenter.logout())
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
    }

    /**
     * Show birthday picker dialog
     */
    private void showDatePickerDialog() {
        DatePickerDialogFragment.newInstance(!TextUtils.isEmpty(inputBirthday.getText()) ? DateUtil.convertStringToDate(inputBirthday.getText(), "MMM dd, yyyy") : null)
                .setOnDateSelectedListener(this)
                .show(activity.getFragmentManager(), DatePickerDialogFragment.TAG);
    }

    @NonNull
    @Override
    public ProfileContract.ProfilePresenter createPresenter() {
        return presenter;
    }

    /**
     * Display {@link UserDetail} object in UI
     *
     * @param userDetail userDetail object
     */
    @Override
    public void displayUserDetails(UserDetail userDetail) {
        if (imageFile == null) {
            int placeholder = userDetail.getGender().equals("Male") ? R.drawable.placeholder_male : R.drawable.placeholder_female;
            ivProfileImage.setImageResource(placeholder);

            GlideImageLoader.loadUrl(activity, ivProfileImage, userDetail.getImageUrl(), placeholder, placeholder, true);
        }

        tvEmailAddress.setText(userDetail.getEmailAddress());
        tvInviteCode.setText(String.format("@%s", userDetail.getInviteCode()));
        inputFirstName.setText(userDetail.getFirstName());
        inputMiddleName.setText(userDetail.getMiddleName());
        inputLastName.setText(userDetail.getLastName());
        inputPassword.setText(passwordMask);
        inputMobileNumber.setText(userDetail.getMobileNumber());
        inputBirthday.setText(userDetail.getBirthday());
        inputGender.setText(userDetail.getGender());
        inputHomeAddress.setText(userDetail.getAddress());
        inputContactPerson.setText(userDetail.getContactPerson());
        inputContactPersonMobileNumber.setText(userDetail.getContactPersonNumber());

        if (!inputFields.get(0).hasOnValueChangedListener()) {
            ButterKnife.apply(inputFields, (ButterKnife.Action<ProfileEditText>) (view, index) -> view.setOnValueChangedListener(ProfileController.this));
        }

        inputMedicalInformation.setText(userDetail.getMedicalInfo());

        removeErrors();
    }

    /**
     * On Logout successful replace this controller with {@link ph.com.auspex.mers.login.LoginController}
     */
    @Override
    public void onLogoutSuccess() {
        if (getParentController() != null) {
            ((NavigationController) getParentController()).replaceCurrentControllerWith(NavigationController.NavigationItem.LOGIN_CONTROLLER);
        }
    }

    /**
     * Open Image Picker dialog
     */
    @Override
    public void openImagePicker() {
        EasyImage.openChooserWithGallery(getActivity(), "Select Profile Picture", 0);
    }

    /**
     * On profile changed listener
     */
    @Override
    public void profileChanged() {
        fabUpdateProfile.show();
    }

    /**
     * On profile revert listener
     */
    @Override
    public void profileReverted() {
        fabUpdateProfile.hide();
    }

    /**
     * Handle onActivityResult after retrieving a picture from the camera or gallery
     *
     * @param requestCode Request Code
     * @param resultCode  Result Code
     * @param data        Intent data received from the Camera or Gallery Activity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {

            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                imageFile = AppUtil.compressFile(activity, imageFiles.get(0));
                GlideImageLoader.loadFile(activity, ivProfileImage, imageFile, true);

                onValueChanged();
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(activity);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    /**
     * On date set listener
     *
     * @param view       {@link DatePickerDialog}
     * @param year       Year from date picker
     * @param month      Month from date picker
     * @param dayOfMonth Day of month from date picker
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        inputBirthday.setText(DateUtil.convertGregorianCalendarToString(year, month, dayOfMonth, "MMM dd, yyyy"));
    }

    /**
     * Show gender picker dialog
     */
    void showGenderPicker() {
        MaterialDialogManager.showGenderPicker(activity, (dialog, itemView, position, text) -> inputGender.setText(text.toString()));
    }

    /**
     * Show loading dialog
     */
    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    /**
     * Dismiss loading dialog
     */
    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * On profile image click listener
     */
    @OnClick(R.id.iv_profile_image)
    void onImageViewClicked() {
        Nammu.askForPermission(activity, Manifest.permission.CAMERA, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                openImagePicker();
            }

            @Override
            public void permissionRefused() {
                showError(activity.getString(R.string.error_permission_not_granted_image));
            }
        });
    }

    /**
     * On {@link FloatingActionButton} click listener
     */
    @OnClick(R.id.fab)
    void onFabClicked() {
        if (!validationUtil.hasEmptyField(inputFirstName, inputMiddleName, inputLastName, inputMobileNumber)
                && validationUtil.validateContactPersonDetails(inputContactPerson, inputContactPersonMobileNumber)
                && !validationUtil.hasError(inputFirstName, inputMiddleName, inputLastName, inputMobileNumber, inputContactPerson, inputContactPersonMobileNumber)) {
            presenter.updateProfile(createUpdateProfileRequest());
        } else {
            Toast.makeText(getActivity(), "Error in fields found. Cannot save your profile.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Show edit profile cancel dialog
     *
     * @param isCancelFromPassword If change password button is clicked
     */
    private void showEditProfileCancelDialog(boolean isCancelFromPassword) {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_update_profile)
                .content(R.string.dialog_title_cancel_update_profile)
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_no)
                .onPositive((dialog, which) -> {
                    if (isCancelFromPassword) {
                        openChangePasswordScreen();
                    } else {
                        resetData();
                    }
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
    }

    /**
     * Replace this controller with {@link UpdatePasswordController}
     */
    private void openChangePasswordScreen() {
        setRetainViewMode(RetainViewMode.RETAIN_DETACH);
        parentRouter.pushController(RouterTransaction.with(new UpdatePasswordController()));
    }

    /**
     * Reset view to the initial data set
     */
    @Override
    public void resetData() {
        imageFile = null;
        fabUpdateProfile.hide();
        removeErrors();

        presenter.getUserDetails();
    }

    /**
     * If the Profile Update is successful
     */
    @Override
    public void onUpdateSuccessful() {
        showToast("Profile Successfully Updated");
    }

    /**
     * Remove {@link ProfileEditText} errors
     */
    @SuppressWarnings("Convert2streamapi")
    private void removeErrors() {
        for (ProfileEditText inputField : inputFields) {
            inputField.removeFocus();
        }
    }

    /**
     * Call this listener if a single field value was changed
     */
    @Override
    public void onValueChanged() {
        presenter.checkForProfileUpdates(userWrapper.getUserDetail(), createUpdateProfileRequest());
    }

    /**
     * Create a {@link UserDetail} that will be used to update the profile
     *
     * @return UserDetail object
     */
    private UserDetail createUpdateProfileRequest() {
        return new UserDetail.Builder()
                .userId(userWrapper.getUserDetail().getUserId())
                .firstName(inputFirstName.getText())
                .middleName(inputMiddleName.getText())
                .lastName(inputLastName.getText())
                .emailAddress(userWrapper.getUserDetail().getEmailAddress())
                .mobileNumber(validationUtil.convertFormat(inputMobileNumber.getText()))
                .birthday(inputBirthday.getText())
                .gender(inputGender.getText())
                .address(inputHomeAddress.getText())
                .contactPerson(inputContactPerson.getText())
                .contactPersonNumber(validationUtil.convertFormat(inputContactPersonMobileNumber.getText()))
                .medicalInformation(inputMedicalInformation.getText())
                .imageFile(imageFile)
                .build();
    }

    @Override
    public boolean handleBack() {
        if (fabUpdateProfile.isShown()) {
            showEditProfileCancelDialog(false);
            return true;
        } else {
            MaterialDialogManager.showCloseApplicationDialog(activity);
            return true;
        }
    }

    @Override
    protected void onAttach(@NonNull View view) {
        if (presenter != null) {
            presenter.attachView(this);
        }

        displayUserDetails(userWrapper.getUserDetail());
    }
}