package ph.com.auspex.mers.directories;

import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

import ph.com.auspex.mers.common.mvp.view.RemoteView;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.data.model.response.NearbyPlaceDetail;

public interface DirectoryContract {

    interface DirectoryPresenter extends MvpPresenter<DirectoryView> {
        void getLastKnownLocation();

        void observeGpsSettingsChanges();

        void observeCurrentLocationChanges();

        void requestNearbyPlaces(NearbyPlaceRequest.PlaceType placeType);

        void requestForDirections(LatLng destination);
    }

    interface DirectoryView extends RemoteView {
        void onCurrentLocationUpdated(LatLng currentLocation);

        // void displayLastKnownLocation(LatLng lastKnownLocation);

        void displayNearbyPlaces(List<NearbyPlace> nearbyPlaces);

        void showGpsSettingsBlocker(boolean shouldShow);

        void showPermissionsBlocker(boolean shouldShow);

        void displayDirection(List<LatLng> points);

        void getNearbyError(String message);

        void onLowMemory();
    }

    interface DirectoryDetailPresenter extends MvpPresenter<DirectoryDetailView> {
        void getNearbyPlaceDetail(String placeId);
    }

    interface DirectoryDetailView extends RemoteView {
        void displayDetail(NearbyPlaceDetail nearbyPlaceDetail);
    }
}
