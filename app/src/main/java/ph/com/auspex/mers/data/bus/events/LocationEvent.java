package ph.com.auspex.mers.data.bus.events;

import com.google.android.gms.maps.model.LatLng;

/**
 * Location Updated event from {@link ph.com.auspex.mers.service.LocationUpdateService}
 */
public class LocationEvent {

    public static class LocationUpdatedEvent {
        private LatLng latLng;

        public LocationUpdatedEvent(LatLng latLng) {
            this.latLng = latLng;
        }

        public LatLng getLastKnownLocation() {
            return latLng;
        }
    }
}
