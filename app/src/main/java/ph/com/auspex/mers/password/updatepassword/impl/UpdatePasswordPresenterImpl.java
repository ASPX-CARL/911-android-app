package ph.com.auspex.mers.password.updatepassword.impl;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.request.UpdatePasswordRequest;
import ph.com.auspex.mers.password.PasswordInteractor;
import ph.com.auspex.mers.password.updatepassword.UpdatePasswordContract.UpdatePasswordPresenter;
import ph.com.auspex.mers.password.updatepassword.UpdatePasswordContract.UpdatePasswordView;

/**
 * Update password presenter implementation
 */
public class UpdatePasswordPresenterImpl extends MvpBasePresenter<UpdatePasswordView> implements UpdatePasswordPresenter {

    private final PasswordInteractor model;

    public UpdatePasswordPresenterImpl(PasswordInteractor model) {
        this.model = model;
    }

    /**
     * Update the user's password in the API
     *
     * @param currentPassword user's current password
     * @param updatedPassword user's new password
     */
    @Override
    public void updatePassword(String currentPassword, String updatedPassword) {
        getView().showLoading();

        UpdatePasswordRequest request = new UpdatePasswordRequest.Builder()
                .currentPassword(currentPassword)
                .updatedPassword(updatedPassword)
                .build();

        model.changePassword(request, new BaseInteractor.ResponseListener<String>() {
            @Override
            public void onSuccess(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().onPasswordUpdated(message);
                }
            }

            @Override
            public void onFailure(String message) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(message);
                }
            }
        });
    }
}
