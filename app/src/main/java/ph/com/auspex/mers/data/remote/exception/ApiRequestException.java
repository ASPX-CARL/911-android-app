package ph.com.auspex.mers.data.remote.exception;

import timber.log.Timber;

/**
 * Api Request Exception which parses RequestCode and message from API's Json Response
 */
public class ApiRequestException extends RuntimeException {

    public enum REQUEST_CODE {
        USER_VERIFICATION_CODE_NOT_FOUND,
        ACCOUNT_ALREADY_VERIFIED,
        FORGOT_PASSWORD_FAILED,
        REGISTRATION_DUPLICATED_EMAIL,
        USER_NOT_VERIFIED,
        INVALID_VERIFICATION_CODE,
        INVALID_TOKEN,
        ALREADY_VERIFIED,
        USER_NOT_FOUND
    }

    private REQUEST_CODE requestCode;
    private String message;

    public ApiRequestException(String message) {
        super(message);
        this.message = message;
    }

    public ApiRequestException(String message, String requestCode) {
        super(message);
        this.message = message;
        try {
            this.requestCode = REQUEST_CODE.valueOf(requestCode);
        } catch (IllegalArgumentException e) {
            Timber.e(e);
        }
    }

    public REQUEST_CODE getRequestCode() {
        return requestCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
