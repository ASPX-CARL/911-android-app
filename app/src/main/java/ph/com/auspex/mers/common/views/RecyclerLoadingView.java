package ph.com.auspex.mers.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.auspex.mers.R;

/**
 * A Custom {@link RecyclerView} that has its own refresh implementation and Loading/Content/Error views
 */
public class RecyclerLoadingView extends FrameLayout implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.empty_view) LinearLayout layoutEmptyView;
    @BindView(R.id.button_action) Button btnAction;
    @BindView(R.id.tv_main_message) TextView tvMessage;
    @BindView(R.id.tv_sub_message) TextView tvSubMessage;
    @BindView(R.id.iv_icon) ImageView ivIcon;

    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;
    private ListRefreshListener listRefreshListener = () -> {
        // Prevent NPE
    };
    private View.OnClickListener buttonActionListener = v -> {
        // Prevent NPE
    };

    public RecyclerLoadingView(@NonNull Context context) {
        super(context);
    }

    /**
     * Create a view instance from xml
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    public RecyclerLoadingView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    /**
     * Initialize view
     *
     * @param context Activity context
     * @param attrs   xml attribute
     */
    private void initView(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RecyclerLoadingView, 0, 0);

        boolean displayDivider = a.getBoolean(R.styleable.RecyclerLoadingView_display_divider, false);
        int orientation = a.getInt(R.styleable.RecyclerLoadingView_android_orientation, LinearLayoutManager.VERTICAL);
        String message = a.getString(R.styleable.RecyclerLoadingView_rlv_text_message);
        String subMessage = a.getString(R.styleable.RecyclerLoadingView_rlv_text_sub_message);
        String actionButtonText = a.getString(R.styleable.RecyclerLoadingView_rlv_text_action_button);
        int drawableRes = a.getResourceId(R.styleable.RecyclerLoadingView_rlv_drawable, 0);

        View view = LayoutInflater.from(context).inflate(R.layout.layout_recycler_loading_view, this, true);
        ButterKnife.bind(this, view);

        setMessage(message);
        setSubMessage(subMessage);
        setActionButtonText(actionButtonText);
        setIcon(drawableRes);

        // init swipeRefreshLayout
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        // init recyclerView
        layoutManager = new LinearLayoutManager(context, orientation, false);
        recyclerView.setLayoutManager(layoutManager);

        if (displayDivider) {
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));
        }

        a.recycle();
    }

    /**
     * Sets the RecyclerView adapter and stop the refresh animation
     *
     * @param adapter {@link RecyclerView.Adapter}
     */
    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(this.adapter);
        setRefreshing(false);
    }

    /**
     * Hide the Empty View and show the RecyclerView
     */
    public void showList() {
        recyclerView.setVisibility(VISIBLE);
        layoutEmptyView.setVisibility(GONE);
    }

    /**
     * Hide the RecyclerView and how the Empty View
     */
    public void showEmptyView() {
        recyclerView.setVisibility(GONE);
        layoutEmptyView.setVisibility(VISIBLE);
    }

    /**
     * Show the {@link SwipeRefreshLayout} refresh animation
     *
     * @param refreshing Refreshing boolean
     */
    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void onRefresh() {
        listRefreshListener.onRefresh();
    }

    public interface ListRefreshListener {
        void onRefresh();
    }

    /**
     * Get the current {@link RecyclerView.Adapter} of the {@link RecyclerView}
     *
     * @return {@link RecyclerView.Adapter}
     */
    public RecyclerView.Adapter getAdapter() {
        return recyclerView.getAdapter();
    }

    /**
     * RecyclerView can perform several optimizations if it can know in advance that RecyclerView's
     * size is not affected by the adapter contents. RecyclerView can still change its size based
     * on other factors (e.g. its parent's size) but this size calculation cannot depend on the
     * size of its children or contents of its adapter (except the number of items in the adapter).
     * <p>
     * If your use of RecyclerView falls into this category, set this to {@code true}. It will allow
     * RecyclerView to avoid invalidating the whole layout when its adapter contents change.
     *
     * @param hasFixedSize true if adapter changes cannot affect the size of the RecyclerView.
     */
    public void setHasFixedSize(boolean hasFixedSize) {
        recyclerView.setHasFixedSize(hasFixedSize);
    }

    /**
     * Set the listener to be notified when a refresh is triggered via the swipe
     * gesture.
     */
    public void setRefreshListener(ListRefreshListener listRefreshListener) {
        this.listRefreshListener = listRefreshListener;
    }

    /**
     * Set the listener to be notified when an action button from an Empty View is clicked
     */
    public void setActionButtonListener(View.OnClickListener buttonActionListener) {
        this.buttonActionListener = buttonActionListener;
    }

    @OnClick(R.id.button_action)
    void onActionButtonClicked(View view) {
        buttonActionListener.onClick(view);
    }

    /**
     * Empty View display icon
     */
    public void setIcon(@DrawableRes int drawableResId) {
        if (drawableResId != 0) {
            ivIcon.setImageResource(drawableResId);
            ivIcon.setVisibility(VISIBLE);
        } else {
            ivIcon.setVisibility(GONE);
        }
    }

    /**
     * Sets Empty View display message
     */
    public void setMessage(@Nullable String message) {
        if (!TextUtils.isEmpty(message)) {
            tvMessage.setText(message);
            tvMessage.setVisibility(VISIBLE);
        } else {
            tvMessage.setVisibility(GONE);
        }
    }

    /**
     * Sets Empty View display sub message
     */
    public void setSubMessage(@Nullable String subMessage) {
        if (!TextUtils.isEmpty(subMessage)) {
            tvSubMessage.setText(subMessage);
            tvSubMessage.setVisibility(VISIBLE);
        } else {
            tvSubMessage.setVisibility(GONE);
        }
    }

    /**
     * Sets Action Button text message
     */
    public void setActionButtonText(@Nullable String text) {
        if (!TextUtils.isEmpty(text)) {
            btnAction.setText(text);
            btnAction.setVisibility(VISIBLE);
        } else {
            btnAction.setVisibility(GONE);
        }
    }

    /**
     * Scrolls the list to the first item
     */
    public void scrollToTop() {
        recyclerView.stopScroll();
        layoutManager.scrollToPositionWithOffset(0, 0);
    }
}