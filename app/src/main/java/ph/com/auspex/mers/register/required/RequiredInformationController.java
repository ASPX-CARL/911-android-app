package ph.com.auspex.mers.register.required;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.BaseController;
import ph.com.auspex.mers.common.dialog.DatePickerDialogFragment;
import ph.com.auspex.mers.common.views.AdvancedTextInputEditText;
import ph.com.auspex.mers.data.model.request.RegisterRequest;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.register.RegisterModule;
import ph.com.auspex.mers.register.optional.OptionalInformationController;
import ph.com.auspex.mers.util.AppUtil;
import ph.com.auspex.mers.util.DateUtil;
import ph.com.auspex.mers.util.FormValidationUtil;
import ph.com.auspex.mers.util.GlideImageLoader;
import ph.com.auspex.mers.util.MaterialDialogManager;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

import static android.text.TextUtils.isEmpty;
import static ph.com.auspex.mers.util.DateUtil.convertStringToDate;

/**
 * RequiredInformationController is the first part of the application registration process
 */
public class RequiredInformationController extends BaseController implements DatePickerDialog.OnDateSetListener {

    public static final String TAG = RequiredInformationController.class.getSimpleName();

    @BindView(R.id.iv_profile_image) ImageView ivProfileImage;
    @BindView(R.id.input_first_name) AdvancedTextInputEditText inputFirstName;
    @BindView(R.id.input_middle_name) AdvancedTextInputEditText inputMiddleName;
    @BindView(R.id.input_last_name) AdvancedTextInputEditText inputLastName;
    @BindView(R.id.input_email_address) AdvancedTextInputEditText inputEmailAddress;
    @BindView(R.id.input_password) AdvancedTextInputEditText inputPassword;
    @BindView(R.id.input_confirm_password) AdvancedTextInputEditText inputConfirmPassword;
    @BindView(R.id.input_birthday) AdvancedTextInputEditText inputBirthday;
    @BindView(R.id.input_mobile_number) AdvancedTextInputEditText inputMobileNumber;

    @BindViews({R.id.input_first_name, R.id.input_middle_name, R.id.input_last_name, R.id.input_email_address,
            R.id.input_password, R.id.input_confirm_password, R.id.input_birthday, R.id.input_mobile_number}) AdvancedTextInputEditText[] inputFields;

    @BindString(R.string.label_account_information) String title;
    @BindString(R.string.format_birthday) String formatBirthday;

    @Inject Router parentRouter;
    @Inject FormValidationUtil validationUtil;

    private File imageFile;

    public RequiredInformationController() {
        setHasOptionsMenu(true);
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.registerComponent(new RegisterModule()).inject(this);
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);
        ivProfileImage.setImageResource(R.drawable.bg_profile_image);
        inputBirthday.setOnClickListener(v -> showDatePickerDialog());
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_register_required, container, false);
    }

    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_register;
    }

    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                handleBack();
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    /**
     * Show birthday picker dialog
     */
    private void showDatePickerDialog() {
        DatePickerDialogFragment
                .newInstance(!isEmpty(inputBirthday.getText().toString()) ? convertStringToDate(inputBirthday.getText().toString(), formatBirthday) : null)
                .setOnDateSelectedListener(this)
                .show(activity.getFragmentManager(), DatePickerDialogFragment.TAG);
    }

    /**
     * Request for permission then open image picker bottom sheet dialog
     */
    @OnClick(R.id.iv_profile_image)
    void onImageViewClicked() {
        Nammu.askForPermission(activity, Manifest.permission.CAMERA, new PermissionCallback() {
            @Override
            public void permissionGranted() {
                openImagePicker();
            }

            @Override
            public void permissionRefused() {
                showError(activity.getString(R.string.error_permission_not_granted_image));
            }
        });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    /**
     * {@link EasyImage} open image chooser with gallery bottom sheet
     */
    private void openImagePicker() {
        EasyImage.openChooserWithGallery(activity, "Select Profile Picture", 0);
    }

    /**
     * Handle onActivityResult after retrieving a picture from the camera or gallery
     *
     * @param requestCode Request Code
     * @param resultCode  Result Code
     * @param data        Intent data received from the Camera or Gallery Activity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                imageFile = AppUtil.compressFile(activity, imageFiles.get(0));
                GlideImageLoader.loadFile(activity, ivProfileImage, imageFile, true);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(activity);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    /**
     * Submit the information to the next form {@link OptionalInformationController(Bundle)}
     */
    @SuppressWarnings("ConfusingArgumentToVarargsMethod")
    @OnClick(R.id.button_next)
    void onNextButtonClicked() {
        if (!validationUtil.hasEmptyField(inputFields) && !validationUtil.hasError(inputFields) && validationUtil.isPasswordSame(inputPassword, inputConfirmPassword)) {
            RegisterRequest request = new RegisterRequest.Builder()
                    .firstName(inputFirstName.getText().toString())
                    .middleName(inputMiddleName.getText().toString())
                    .lastName(inputLastName.getText().toString())
                    .emailAddress(inputEmailAddress.getText().toString())
                    .password(inputPassword.getText().toString())
                    .confirmPassword(inputConfirmPassword.getText().toString())
                    .mobileNumber(validationUtil.convertFormat(inputMobileNumber.getText().toString()))
                    .birthday(inputBirthday.getText().toString())
                    .imageFile(imageFile)
                    .build();

            Bundle args = new Bundle();
            args.putParcelable(OptionalInformationController.KEY_REQUEST, request);

            setRetainViewMode(RetainViewMode.RETAIN_DETACH);
            parentRouter.pushController(RouterTransaction.with(new OptionalInformationController(args)));
        }
    }

    /**
     * On date set listener
     *
     * @param view       {@link DatePickerDialog}
     * @param year       Year from date picker
     * @param month      Month from date picker
     * @param dayOfMonth Day of month from date picker
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        inputBirthday.setText(DateUtil.convertGregorianCalendarToString(year, month, dayOfMonth, formatBirthday));
    }

    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCancelAccountCreationDialog(activity, parentRouter);
        return true;
    }
}
