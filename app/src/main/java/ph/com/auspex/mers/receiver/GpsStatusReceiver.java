package ph.com.auspex.mers.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.AppEvent;
import ph.com.auspex.mers.util.AppUtil;

/**
 * GPS Status Listener for Device's GPS settings changes
 */
public class GpsStatusReceiver extends BroadcastReceiver {
    public static final String GPS_STATUS = "android.location.PROVIDERS_CHANGED";

    private final RxEventBus rxEventBus;

    public GpsStatusReceiver(RxEventBus rxEventBus) {
        this.rxEventBus = rxEventBus;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches(GPS_STATUS)) {
            rxEventBus.send(new AppEvent.GpsStatusChangedEvent(AppUtil.checkGpsEnabled(context)));
        }
    }
}
