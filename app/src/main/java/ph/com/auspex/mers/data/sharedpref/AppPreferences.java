package ph.com.auspex.mers.data.sharedpref;

import com.google.android.gms.maps.model.LatLng;

import de.devland.esperandro.SharedPreferenceActions;
import de.devland.esperandro.SharedPreferenceMode;
import de.devland.esperandro.annotations.SharedPreferences;
import ph.com.auspex.mers.BuildConfig;
import ph.com.auspex.mers.data.model.response.UserDetail;

/**
 * Shared preference convinience
 */
@SharedPreferences(name = BuildConfig.APPLICATION_ID, mode = SharedPreferenceMode.PRIVATE)
public interface AppPreferences extends SharedPreferenceActions {

    /**
     * Returns {@link UserDetail} from {@link android.content.SharedPreferences}
     *
     * @return {@link UserDetail}
     */
    UserDetail userDetails();

    /**
     * Save a new value for {@link UserDetail} in {@link android.content.SharedPreferences}
     *
     * @param userDetails new user detail object
     */
    void userDetails(UserDetail userDetails);

    /**
     * Returns a JWT Token String from {@link android.content.SharedPreferences}
     *
     * @return JWT Token String
     */
    String jwtToken();

    void jwtToken(String jwtToken);

    /**
     * Returns the expiration of the JWT Token from {@link android.content.SharedPreferences}
     *
     * @return JWT Token expiration
     */
    long expiration();

    void expiration(long expiration);

    /**
     * Returns the last known location of the user from {@link android.content.SharedPreferences}
     *
     * @return user's last known location
     */
    LatLng currentLocation();

    void currentLocation(LatLng currentLocation);

    String displayShowCaseView();

    void displayShowCaseView(String randomText);
}
