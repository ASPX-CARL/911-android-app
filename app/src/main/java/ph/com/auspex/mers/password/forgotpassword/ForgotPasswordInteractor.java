package ph.com.auspex.mers.password.forgotpassword;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;
import ph.com.auspex.mers.data.model.response.ForgotPasswordResponse;

public interface ForgotPasswordInteractor extends BaseInteractor {

    void forgotPassword(String emailAddress, ResponseListener<ForgotPasswordResponse> listener);
}
