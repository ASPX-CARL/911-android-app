package ph.com.auspex.mers.newsdetail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ph.com.auspex.mers.common.mvp.view.RemoteView;

public interface NewsDetailContract {

    interface NewsDetailPresenter extends MvpPresenter<NewsDetailView> {
        // internal news motherfucker!
        void getNews(int id);
    }

    interface NewsDetailView extends RemoteView {
        void displayNews(String url);
    }
}
