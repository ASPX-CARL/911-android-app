package ph.com.auspex.mers.service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import ph.com.auspex.mers.AppComponent;
import ph.com.auspex.mers.MersApplication;
import ph.com.auspex.mers.data.bus.RxEventBus;
import ph.com.auspex.mers.data.bus.events.LocationEvent;
import ph.com.auspex.mers.receiver.GpsStatusReceiver;
import ph.com.auspex.mers.util.AppUtil;
import timber.log.Timber;

/**
 * Location Update Service class that retrieves current location and send it to the API
 */
public class LocationUpdateService extends Service implements GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = LocationUpdateService.class.getSimpleName();

    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private Location currentLocation;

    @Inject GpsStatusReceiver gpsStatusReceiver;
    @Inject @Named("update_interval") long updateLocationInterval;
    @Inject RxEventBus rxEventBus;
    @Inject LocationServicePresenter presenter;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    /**
     * Build necessary client upon service creation
     */
    @Override
    public void onCreate() {
        super.onCreate();

        Timber.d("Service Started");

        injectDependencies();
        registerReceiver(gpsStatusReceiver, new IntentFilter(GpsStatusReceiver.GPS_STATUS));
        buildGoogleApiClient();
        buildUpdateIntervalObservable();
    }

    /**
     * Build and inject object dependencies
     */
    private void injectDependencies() {
        DaggerServiceComponent.builder()
                .appComponent(getAppComponent())
                .serviceModule(new ServiceModule())
                .build()
                .inject(this);
    }

    /**
     * Get Application Component
     *
     * @return {@link AppComponent} instance
     */
    private AppComponent getAppComponent() {
        return ((MersApplication) getApplication()).getAppComponent();
    }

    /**
     * Build Google Api Client
     */
    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Initialize {@link LocationRequest} and get {@link LocationServices#FusedLocationApi} last known location
     *
     * @param bundle
     */
    @SuppressWarnings("MissingPermission")
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Location lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (lastKnownLocation != null) {
            setCurrentLocation(lastKnownLocation);
        }

        locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setSmallestDisplacement(10);

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    /**
     * Send current location to the API
     *
     * @param location {@link Location} is the Current Location
     */
    private void setCurrentLocation(Location location) {
        currentLocation = location;
        rxEventBus.send(new LocationEvent.LocationUpdatedEvent(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())));
        presenter.updateCurrentLocation(location);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * onLocationChanged Listener method
     *
     * @param location Current Location
     */
    @Override
    public void onLocationChanged(Location location) {
        Timber.d("onLocationChanged: %s", location.getLatitude() + " " + location.getLongitude());
        setCurrentLocation(location);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    /**
     * Update Interval Observable
     */
    private void buildUpdateIntervalObservable() {
        // Timed service for updating location
        compositeDisposable.add(Observable.interval(0, updateLocationInterval, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Long>() {
                    @Override
                    public void onNext(Long aLong) {
                        if (currentLocation != null && AppUtil.checkGpsEnabled(getApplicationContext())) {
                            presenter.updateCurrentLocation(currentLocation);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
        unregisterReceiver(gpsStatusReceiver);
        stopSelf();
        super.onDestroy();
    }
}
