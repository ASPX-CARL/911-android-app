package ph.com.auspex.mers.util;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.Router;

import ph.com.auspex.mers.R;

/**
 * MaterialDialog Util class
 */
public class MaterialDialogManager {

    /**
     * Display cancel account creation dialog
     *
     * @param activity Activity where the controller is attached
     * @param router   {@link Controller#getRouter()}
     */
    public static void showCancelAccountCreationDialog(Activity activity, Router router) {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_cancel_registration)
                .content(R.string.dialog_negative_cancel_registration)
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_no)
                .onPositive((dialog, which) -> router.popToRoot())
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
    }

    /**
     * Cancel account verification dialog
     *
     * @param activity Activity where the controller is attached
     * @param router   {@link Controller#getRouter()}
     */
    public static void showCancelVerificationDialog(Activity activity, Router router) {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_cancel_verification)
                .content(R.string.dialog_negative_cancel_verification)
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_no)
                .onPositive((dialog, which) -> router.popToRoot())
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
    }

    /**
     * Gender dropdown picker
     *
     * @param activity     Activity where the controller is attached
     * @param listCallback callback after a gender is selected
     */
    public static void showGenderPicker(Activity activity, MaterialDialog.ListCallback listCallback) {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_gender)
                .items(R.array.gender)
                .itemsCallback(listCallback)
                .show();
    }

    /**
     * Show close application dialog
     *
     * @param activity Activity where the controller is attached
     */
    public static void showCloseApplicationDialog(Activity activity) {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_close_application)
                .content(R.string.dialog_content_close_application)
                .positiveText(R.string.dialog_button_yes)
                .negativeText(R.string.dialog_button_no)
                .onPositive((dialog, which) -> activity.finish())
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
    }

    /**
     * Show open GPS Settings dialog
     *
     * @param activity Activity where the controller is attached
     */
    public static void showOpenSettingsDialog(Activity activity) {
        new MaterialDialog.Builder(activity)
                .title(R.string.message_location_services_turned_off)
                .content(R.string.message_location_services_turned_off_sub)
                .positiveText(R.string.button_open_settings)
                .negativeText(R.string.cancel)
                .onPositive((dialog, which) -> {
                    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivity(settingsIntent);
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .positiveColorRes(R.color.app_blue)
                .negativeColorRes(R.color.app_gray)
                .show();
    }
}
