package ph.com.auspex.mers.directories;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.MosbyViewStateController;
import ph.com.auspex.mers.util.MaterialDialogManager;
import ph.com.auspex.mers.common.views.CustomRadioButton;
import ph.com.auspex.mers.data.model.request.NearbyPlaceRequest;
import ph.com.auspex.mers.data.model.response.NearbyPlace;
import ph.com.auspex.mers.directories.DirectoryContract.DirectoryPresenter;
import ph.com.auspex.mers.directories.DirectoryContract.DirectoryView;
import ph.com.auspex.mers.directories.detail.DirectoryDetailController;
import ph.com.auspex.mers.main.ActivityComponent;
import ph.com.auspex.mers.main.MainActivity;
import ph.com.auspex.mers.util.AppUtil;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Provides a UI for the directory feature.
 * This class displays a {@link GoogleMap} with markers for different {@link ph.com.auspex.mers.data.model.response.EmergencyType}
 */
public class DirectoryController extends MosbyViewStateController<DirectoryView, DirectoryPresenter> implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, PermissionCallback, DirectoryView {

    public static final String TAG = DirectoryController.class.getSimpleName();

    @BindView(R.id.mapview) MapView mapView;
    @BindView(R.id.layout_blocker_gps) RelativeLayout layoutGpsSettings;
    @BindView(R.id.layout_blocker_permission) RelativeLayout layoutPermissionSettings;
    @BindView(R.id.button_open_list) Button hideThisButton;

    @BindView(R.id.tv_sub_message) TextView tvMessageSub;

    @BindView(R.id.radio_button_fire) CustomRadioButton rbtnFire;
    @BindView(R.id.radio_button_medical) CustomRadioButton rbtnMedical;
    @BindView(R.id.radio_button_police) CustomRadioButton rbtnPolice;

    @BindColor(R.color.polyline_color) int lineColor;
    @BindString(R.string.nav_directories) String title;

    @Inject DirectoryPresenter presenter;

    private GoogleMap googleMap;
    private LatLng currentLocation;
    private boolean isFirstRun = true;
    private NearbyPlaceRequest.PlaceType activePlaceType = NearbyPlaceRequest.PlaceType.HOSPITAL;

    private int activeCategory = 0;
    private int previousCategory = -1;

    /**
     * Initialize directory controller UI components
     *
     * @param view           parent view
     * @param savedViewState saved view state
     */
    @Override
    protected void onViewReady(View view, Bundle savedViewState) {
        mapView.onCreate(savedViewState);
        mapView.getMapAsync(this);

        tvMessageSub.setText(R.string.message_location_services_turned_off_sub_directory);

        hideThisButton.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);

        if (mapView != null) {
            mapView.onResume();
        }
    }

    @NonNull
    @Override
    public DirectoryPresenter createPresenter() {
        return presenter;
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container, Bundle savedViewState) {
        return inflater.inflate(R.layout.controller_directory, container, false);
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {
        activityComponent.directoryComponent(new DirectoryModule()).inject(this);
    }

    /**
     * Provide a map initialization after it is loaded from the UI
     *
     * @param googleMap map view
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setOnMarkerClickListener(this);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);

        this.googleMap = googleMap;

        Nammu.askForPermission(activity, ACCESS_FINE_LOCATION, this);
    }

    /**
     * Provide an event listener on {@link Marker} click
     *
     * @param marker google marker on map
     * @return <code>true</code> if the marker is clicked
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        NearbyPlace nearbyPlace = (NearbyPlace) marker.getTag();

        Bundle args = new Bundle();
        args.putParcelable(DirectoryDetailController.KEY_NEARBY_PLACE, nearbyPlace);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(nearbyPlace.getCoordinates())
                .zoom(17)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.animateCamera(cameraUpdate);

        setOverlayController(new DirectoryDetailController(args));
        marker.hideInfoWindow();
        return true;
    }

    /**
     * if permission granted setup additional view components
     */
    @SuppressLint("MissingPermission")
    @Override
    public void permissionGranted() {
        layoutPermissionSettings.setVisibility(View.GONE);

        // Check for gps settings
        if (AppUtil.checkGpsEnabled(activity)) {
            ((MainActivity) activity).startService();

            if (googleMap != null) {
                googleMap.setMyLocationEnabled(true);
            }

            presenter.getLastKnownLocation();
            presenter.requestNearbyPlaces(activePlaceType);
        } else {
            showGpsSettingsBlocker(true);
        }
    }

    /**
     * If permission is denied show a UI blocker to prevent using the feature
     */
    @Override
    public void permissionRefused() {
        showPermissionsBlocker(true);
    }

    /**
     * Move camera to location on location updates
     *
     * @param currentLocation user's current location object
     */
    @Override
    public void onCurrentLocationUpdated(LatLng currentLocation) {
        this.currentLocation = currentLocation;
        moveCameraToLocation(currentLocation, currentLocation != null);
    }

    @Override
    protected CharSequence getTitle() {
        return title;
    }

    /**
     * Display nearby places markers on {@link GoogleMap}
     *
     * @param nearbyPlaces nearby places object and markers
     */
    @Override
    public void displayNearbyPlaces(List<NearbyPlace> nearbyPlaces) {
        googleMap.clear();

        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(AppUtil.getBitmapFromVectorDrawable(getActivity(), activePlaceType.getMarkerRes()));

        for (NearbyPlace nearbyPlace : nearbyPlaces) {
            googleMap.addMarker(new MarkerOptions()
                    .position(nearbyPlace.getCoordinates())
                    .icon(icon)
                    .anchor(0.5f, 0.5f))
                    .setTag(nearbyPlace);
        }

        AppUtil.showCurrentLocationRadius(googleMap, currentLocation, 5000);
        moveCameraToLocation(currentLocation, true);
    }

    /**
     * Display gps settings blocker layout
     *
     * @param shouldShow <code>true</code> if device's gps settings is turned off; <code>false</code> otherwise
     */
    @Override
    public void showGpsSettingsBlocker(boolean shouldShow) {
        layoutGpsSettings.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    /**
     * Display permission settings blocker layout
     *
     * @param shouldShow <code>true</code> if permission is not granted; <code>false</code> otherwise
     */
    @Override
    public void showPermissionsBlocker(boolean shouldShow) {
        layoutPermissionSettings.setVisibility(View.VISIBLE);
    }

    /**
     * @param points so what's the point in all of this
     * @deprecated unused feature
     */
    @Deprecated
    @Override
    public void displayDirection(List<LatLng> points) {
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.addAll(points);
        polylineOptions.color(lineColor);
        polylineOptions.width(10);

        if (googleMap != null) {
            googleMap.addPolyline(polylineOptions);
        }

        moveCameraToLocation(currentLocation, true);
        previousCategory = activeCategory;
    }

    /**
     * Re-select the previously selected category if the API response is failed
     *
     * @param message error message that is not used
     */
    @Override
    public void getNearbyError(String message) {
        if (previousCategory != -1) {
            switch (previousCategory) {
                case 0:
                    rbtnMedical.setSelected(true);
                    activeCategory = 0;
                    break;
                case 1:
                    rbtnPolice.setSelected(true);
                    activeCategory = 1;
                    break;
                case 2:
                    rbtnFire.setSelected(true);
                    activeCategory = 2;
                    break;
                default:
                    rbtnMedical.setSelected(true);
                    activeCategory = 0;
                    break;
            }
        }
    }

    @Override
    public void onLowMemory() {
        mapView.onLowMemory();
    }

    /**
     * Request for nearby places when button is clicked
     */
    @OnClick(R.id.fab_my_location)
    void onShowCurrentLocationButtonClicked() {
        if (currentLocation != null) {
            presenter.requestNearbyPlaces(activePlaceType);
        }
    }

    /**
     * Request for permission when button is clicked
     */
    @OnClick(R.id.button_request_permissions)
    void onPermissionsButtonClicked() {
        Nammu.askForPermission(activity, ACCESS_FINE_LOCATION, this);
    }

    /**
     * Open GPS Settings when button is clicked
     */
    @OnClick(R.id.button_settings)
    void onOpenSettingsButtonClicked() {
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(settingsIntent);
    }

    /**
     * Center current location on camera
     *
     * @param location      current user location
     * @param shouldAnimate <code>true</code> if should animate while camera is moving; <code>false</code> to skip on the current location
     */
    private void moveCameraToLocation(LatLng location, boolean shouldAnimate) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)
                .zoom(15)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);

        if (shouldAnimate) {
            googleMap.animateCamera(cameraUpdate);
        } else {
            googleMap.moveCamera(cameraUpdate);
        }
    }

    /**
     * Place category selected listener
     *
     * @param compoundButton Category types radio button disguised as regular buttons
     * @param isChecked      <code>true</code> if the radio button is selected; otherwise <code>false</code>
     */
    @OnCheckedChanged({R.id.radio_button_medical, R.id.radio_button_police, R.id.radio_button_fire})
    void onRadioGroupCheckedChangedListener(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked) {
            switch (compoundButton.getId()) {
                case R.id.radio_button_medical:
                    activePlaceType = NearbyPlaceRequest.PlaceType.HOSPITAL;
                    activeCategory = 0;
                    break;
                case R.id.radio_button_police:
                    activePlaceType = NearbyPlaceRequest.PlaceType.POLICE;
                    activeCategory = 1;
                    break;
                case R.id.radio_button_fire:
                    activePlaceType = NearbyPlaceRequest.PlaceType.FIRE;
                    activeCategory = 2;
                    break;
            }

            presenter.requestNearbyPlaces(activePlaceType);
        }
    }

    /**
     * Returns a boolean when the back button is pressed
     *
     * @return Always <code>true</code>
     */
    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCloseApplicationDialog(activity);
        return true;
    }

    @Override
    protected void onDetach(@NonNull View view) {
        super.onDetach(view);
        hideLoading();
        mapView.onPause();
    }

    @Override
    protected void onDestroyView(@NonNull View view) {
        mapView.onDestroy();
        super.onDestroyView(view);
    }

    @Override
    protected void onSaveViewState(@NonNull View view, @NonNull Bundle outState) {
        super.onSaveViewState(view, outState);
        mapView.onSaveInstanceState(outState);
    }


}
