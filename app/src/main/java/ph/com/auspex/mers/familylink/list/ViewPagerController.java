package ph.com.auspex.mers.familylink.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.support.RouterPagerAdapter;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import ph.com.auspex.mers.R;
import ph.com.auspex.mers.common.controller.BaseController;
import ph.com.auspex.mers.util.MaterialDialogManager;
import ph.com.auspex.mers.familylink.list.family.FamListController;
import ph.com.auspex.mers.familylink.list.invites.FamInvitesController;
import ph.com.auspex.mers.familylink.map.FamMapController;
import ph.com.auspex.mers.main.ActivityComponent;

/**
 * Provides a view pager controller for {@link FamListController} and {@link FamInvitesController}
 */
public class ViewPagerController extends BaseController {

    public static final String TAG = ViewPagerController.class.getSimpleName();
    public static final String KEY_HAS_OPTIONS_MENU = TAG + ".hasOptionsMenu";
    public static final String KEY_DEFAULT_PAGE = TAG + ".defaultPage";

    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @BindView(R.id.view_pager) ViewPager viewPager;

    @BindString(R.string.label_family_link) String title;
    @BindArray(R.array.view_pager_title) String[] pagerTitles;

    private RouterPagerAdapter pagerAdapter;
    private int defaultPage;

    /**
     * Initialize the view pager adapter
     *
     * @param args
     */
    public ViewPagerController(Bundle args) {
        super(args);

        if (args != null) {
            setHasOptionsMenu(true);

            pagerAdapter = new RouterPagerAdapter(this) {
                @Override
                public void configureRouter(@NonNull Router router, int position) {
                    Controller controller = null;
                    switch (position) {
                        case 0:
                            controller = new FamListController();
                            break;
                        case 1:
                            controller = new FamInvitesController();
                            break;
                    }

                    router.setRoot(RouterTransaction.with(controller));
                }

                @Override
                public int getCount() {
                    return pagerTitles.length;
                }

                @Override
                public CharSequence getPageTitle(int position) {
                    return pagerTitles[position];
                }
            };

            defaultPage = args.getInt(KEY_DEFAULT_PAGE, 0);
        }
    }

    @Override
    protected void setupComponent(@NonNull ActivityComponent activityComponent) {

    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.controller_view_pager, container, false);
    }

    /**
     * Initialize the {@link ViewPager}
     *
     * @param view {@link Controller#getView()}
     */
    @Override
    protected void onViewReady(View view, Bundle savedInstanceState) {
        super.onViewReady(view, savedInstanceState);

        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(defaultPage);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Controller controller = pagerAdapter.getRouter(position).getBackstack().get(0).controller();

                if (controller instanceof FamListController) {
                    ((FamListController) controller).onRefresh();
                } else if (controller instanceof FamInvitesController) {
                    ((FamInvitesController) controller).onRefresh();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * Returns the controller title
     *
     * @return controller title
     */
    @Override
    protected CharSequence getTitle() {
        return title;
    }

    /**
     * Returns the controller menu resource
     *
     * @return menu resource
     */
    @NonNull
    @Override
    protected int getMenuRes() {
        return R.menu.menu_family_link;
    }

    /**
     * Provides listener for menu item select
     *
     * @param item menu item
     * @return <code>true</code> if the menu item is selected; <code>false</code> otherwise
     */
    @Override
    protected boolean onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toggle_map:
                getRouter().setRoot(RouterTransaction.with(new FamMapController()));
                return true;
        }
        return super.onMenuItemSelected(item);
    }

    /**
     * Handles back button press event
     *
     * @return always <code>true</code>
     */
    @Override
    public boolean handleBack() {
        MaterialDialogManager.showCloseApplicationDialog(activity);
        return true;
    }
}
