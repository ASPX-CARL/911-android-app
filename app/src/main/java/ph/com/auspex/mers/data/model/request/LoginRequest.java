package ph.com.auspex.mers.data.model.request;

import java.util.HashMap;

/**
 * Create Login Request
 */
public class LoginRequest {

    private String email;
    private String password;

    private LoginRequest(Builder builder) {
        this.email = builder.email;
        this.password = builder.password;
    }


    public static class Builder {
        private String email;
        private String password;

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public LoginRequest build() {
            return new LoginRequest(this);
        }
    }

    /**
     * Create @{@link retrofit2.http.POST} RequestBody
     *
     * @return {@link HashMap} request body map
     */
    public HashMap<String, String> create() {
        HashMap<String, String> request = new HashMap<>();

        request.put("email", email);
        request.put("password", password);

        return request;
    }
}
