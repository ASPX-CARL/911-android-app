package ph.com.auspex.mers.register;

import dagger.Module;
import dagger.Provides;
import ph.com.auspex.mers.data.AppDataSource;
import ph.com.auspex.mers.register.optional.impl.RegisterOptPresenterImpl;

@Module
public class RegisterModule {

    /**
     * Provide RegisterInteractor
     *
     * @param appDataSource {@link AppDataSource} instance from dependency injection graph
     * @return RegisterInteractor
     */
    @RegisterScope
    @Provides
    RegisterInteractor providesRegisterInteractor(AppDataSource appDataSource) {
        return appDataSource;
    }

    /**
     * Provide RegisterPresenter
     *
     * @param model {@link RegisterInteractor}
     * @return RegisterPresenter
     */
    @RegisterScope
    @Provides
    RegisterContract.RegisterPresenter providesOptionalRegisterPresenter(RegisterInteractor model) {
        return new RegisterOptPresenterImpl(model);
    }
}
