package ph.com.auspex.mers.data.model.response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ph.com.auspex.mers.data.remote.ApiService;

/**
 * Provide a POJO and {@link RealmObject} for {@link ApiService#getRelationshipTypes()}
 */
public class Relationship extends RealmObject {

    @PrimaryKey
    @SerializedName("relationship_id")
    private int id;
    private String name;

    /**
     * Constructs a new Relationship object with empty parameters
     */
    public Relationship() {
    }

    /**
     * Returns the relationship id
     *
     * @return relationship id
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the relationship name
     *
     * @return relationship name
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
