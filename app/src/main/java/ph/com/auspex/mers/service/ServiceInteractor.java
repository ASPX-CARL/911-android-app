package ph.com.auspex.mers.service;

import com.google.android.gms.maps.model.LatLng;

import ph.com.auspex.mers.common.mvp.interactor.BaseInteractor;

public interface ServiceInteractor extends BaseInteractor {

    void updateLocation(LatLng currentLocation, boolean shouldUpdateApi, ResponseListener<Boolean> listener);
}
