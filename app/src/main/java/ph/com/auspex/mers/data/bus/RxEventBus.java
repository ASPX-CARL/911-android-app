package ph.com.auspex.mers.data.bus;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * EventBus using RxJava
 */
public class RxEventBus {

    private PublishSubject<Object> bus = PublishSubject.create();

    public void send(Object o) {
        bus.onNext(o);
    }

    public Observable<Object> toObservable() {
        return bus;
    }
}
